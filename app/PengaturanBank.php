<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengaturanBank extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'bank_id', 'rekening', 'atasnama', 'cabang', 'fotobuku'
    ];

    protected $hidden = [];

    public function banks()
    {
        return $this->belongsTo(Banks::class, 'bank_id', 'id');
    }
}
