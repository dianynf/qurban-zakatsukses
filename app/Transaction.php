<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id', 'users_id', 'transaction_total', 'date', 'agent_id', 'total_seluruh', 'quant', 'transaction_status', 'name', 'email', 'nohp', 'jml_hewan', 'invoice', 'bank_id', 'address', 'city', 'state', 'post_code'
    ];

    protected $hidden = [];

    public function details()
    {
        return $this->hasMany(TransactionDetail::class, 'transactions_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function bank()
    {
        return $this->belongsTo(Banks::class, 'bank_id', 'id');
    }

    public function getPaymentExpiredAttribute()
    {
        return '<strong>' . date('d-M-Y', strtotime($this->created_at . '1 days')) . '</strong> <strong>Pukul ' . date('H:i', strtotime($this->created_at)) . ' WIB</strong>';
    }
}
