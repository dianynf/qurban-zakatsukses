<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banks extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'bank_id', 'id');
    }

    public function getPathImgAttribute()
    {
        return asset('banks/' . $this->img);
    }
}
