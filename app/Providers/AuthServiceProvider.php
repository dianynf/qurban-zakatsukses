<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('manage-users', function ($user) {
            return $user->hasAnyRoles(['admin']); // jika membolehkan ,'author' tinggal masukkan datanya
        });

        Gate::define('admin-agent', function ($user) {
            return $user->hasAnyRoles(['admin', 'agent']);
        });

        Gate::define('edit-users', function ($user) {
            return $user->hasAnyRoles(['admin']);
        });

        Gate::define('delete-users', function ($user) {
            return $user->hasRole('admin');
        });

        Gate::define('manage-userbiasa', function ($user) {
            return $user->hasAnyRoles(['user']);
        });
    }
}
