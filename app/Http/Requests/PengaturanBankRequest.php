<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PengaturanBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id' => 'required|integer|exists:bank_id,id',
            'rekening' => 'required|max:20',
            'atasnama' => 'required|max:45',
            'cabang' => 'required|max:45',
            'fotobuku' => 'required|image',
        ];
    }
}
