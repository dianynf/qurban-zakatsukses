<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BanksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rek' => 'required|max:16|unique:banks,rek',
            'name' => 'required',
            'img' => 'required|image|mimes:png,jpg,jpeg|max:2024'
        ];
    }

    public function messages()
    {
        return [
            'rek.required' => 'No rekening harus diisi',
            'rek.max' => 'No rekening maximal 16 karakter',
            'rek.unique' => 'No rekening sudah ada',
            'name.required' => 'Nama bank harus diisi',
            'img.required' => 'Gambar bank harus diisi',
            'img.image' => 'File harus berupa gambar',
            'img.max' => 'Ukuran gambar maximal 2 MB',
            'img.mimes' => 'File harus berekstensi png,jpg,jpeg',
        ];
    }
}
