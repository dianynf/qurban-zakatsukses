<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TransactionRequest;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::user()->role_id;

        if ($user_id == 1) {

            $items = Transaction::with([
                'details', 'product', 'user', 'bank'
            ])->get();

            return view('pages.transaction.index', [
                'items' => $items
            ]);
        } elseif ($user_id == 2 || $user_id == 3) {
            $items = Transaction::with([
                'details', 'product', 'user', 'bank'
            ])->where('users_id', '=', Auth::user()->id)
                ->get();
            return view('pages.transaction.index', [
                'items' => $items
            ]);
        } else {
            abort(403, 'Unauthorized access.');
        }
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
        $data = $request->all();

        Transaction::create($data);
        return redirect()->route('transaction.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Transaction::with([
            'details', 'product', 'user'
        ])->findOrFail($id);

        return view('pages.transaction.detail', [
            'item' => $item
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $item = Transaction::findOrFail($id);
            return view('pages.transaction.edit', [
                'item' => $item
            ]);
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionRequest $request, $id)
    {
        $data = $request->all();

        $item = Transaction::findOrFail($id);

        $item->update($data);

        return redirect()->route('transaction.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_id = Auth::user()->role_id;
        if ($user_id == 1) {
            $item = Transaction::findorFail($id);
            $item->delete();
            return redirect()->route('transaction.index');
        } else {
            return abort(404);
        }
    }
}
