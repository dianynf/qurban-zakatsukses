<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banks;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PengaturanBankController extends Controller
{
    public function edit(Request $request)
    {
        $user_id = Auth::user()->role_id;

        if ($user_id == 1) {
            $banks = Banks::all();
            return view('pages.pengaturan-bank.edit', [
                'banks' => $banks,
                'users' => $request->user()
            ]);
        } elseif ($user_id == 2) {
            return abort(404);
        } else {
            $banks = Banks::all();
            return view('pages.pengaturan-bank.edit', [
                'banks' => $banks,
                'users' => $request->user()
            ]);
        }
    }

    public function update(Request $request)
    {
        $image = $request->file('fotobuku');
        if ($image) {
            $request->user()->update([
                'bank_id' => $request->bank_id,
                'rekening' => $request->rekening,
                'atasnama' => $request->atasnama,
                'cabang' => $request->cabang,
                'fotobuku' => $image->getClientOriginalName()
            ]);
            $image->storeAs('public/bank', $image->getClientOriginalName());
        } else {
            $request->user()->update([
                'bank_id' => $request->bank_id,
                'rekening' => $request->rekening,
                'atasnama' => $request->atasnama,
                'cabang' => $request->cabang,
            ]);
        }




        session()->flash('sukses', 'Pengaturan Bank berhasil di Simpan');
        return redirect()->route('pengaturan-bank.edit');
    }
}
