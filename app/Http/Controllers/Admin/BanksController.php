<?php

namespace App\Http\Controllers\Admin;

use App\Banks;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\BanksRequest;
use App\Http\Requests\Admin\UpdateBankRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;

class BanksController extends Controller
{

    public function index()
    {
        $items = Banks::all();

        return view('pages.admin.banks.index', [
            'items' => $items
        ]);
    }

    public function create()
    {
        return view('pages.admin.banks.create');
    }

    public function store(BanksRequest $request)
    {
        $image = $request->file('img');
        Banks::create([
            'rek' => $request->rek,
            'name' => $request->name,
            'img' => $image->getClientOriginalName()
        ]);

        $image->move('metode_pembayaran/', $image->getClientOriginalName());
        session()->flash('sukses', 'Data Bank Berhasil Ditambahkan');
        return redirect()->route('banks.index');
    }

    public function edit($id)
    {
        $item = Banks::findOrFail($id);

        return view('pages.admin.banks.edit', [
            'item' => $item
        ]);
    }

    public function update(UpdateBankRequest $request, $id)
    {
        $image = $request->file('img');

        $item = Banks::findOrFail($id);

        $image = $request->file('img');

        if (empty($image)) {
            $item->update([
                'rek' => $request->rek,
                'name' => $request->name,
            ]);
        } else {
            File::delete('metode_pembayaran/' . $item->img);

            $image->move('metode_pembayaran/', $image->getClientOriginalName());

            $item->update([
                'rek' => $request->rek,
                'name' => $request->name,
                'img' => $image->getClientOriginalName()
            ]);
        }

        session()->flash('sukses', 'Data Bank Berhasil Ubah');
        return redirect()->route('banks.index');
    }

    public function destroy($id)
    {
        $item = Banks::findorFail($id);
        $item->delete();
        session()->flash('sukses', 'Data Bank Berhasil Hapus');
        return redirect()->route('banks.index');
    }
}
