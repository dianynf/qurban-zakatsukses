<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AddtransaksiRequest;
use App\Addtransaksi;
use App\Product;

use App\Http\Requests\Admin\TransactionRequest;
use App\TransactionDetail;
use App\Transaction;
use App\User;
use App\Banks;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class AddtransaksiController extends Controller
{
    public function create()
    {
        $product = Product::all();
        $user = User::all();
        $banks = Banks::all();
        return view('pages.admin.addtransaksi.create', [
            'product' => $product,
            'user' => $user,
            'banks' => $banks,
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $data['total_seluruh'] = explode('Rp. ', $request->total_seluruh);
        $data['total_seluruh'] = str_replace(array('.', ','), '', $data['total_seluruh']);
        $data['total_seluruh'] = $data['total_seluruh'][1];
        $datee = $request['invoice'];
        $data['invoice'] = $datee . rand(0, 99999999999);
        $lastid = Transaction::create($data)->id;

        if (count($request->username) > 0) {
            foreach ($request->username as $item => $v) {
                $data2 = array(
                    'transactions_id' => $lastid,
                    'username' => $request->username[$item],
                );
                TransactionDetail::insert($data2);
            }
        }
        return redirect()->route('transaction.index');
    }
}
