<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $user_id = Auth::user()->role_id;

        if ($user_id == 1) {
            // data admin
            $total_daftar =
                DB::table("transactions")->where('transaction_status', 'SUCCESS')->where('deleted_at', '=', NULL)->count('transaction_status');
            $total_sudah_bayar =
                DB::table("transactions")->where('transaction_status', 'SUCCESS')->where('deleted_at', '=', NULL)->sum('total_seluruh');

            $transaction = DB::table("transactions")->where('deleted_at', '=', NULL)->count('transaction_total');

            $total_blmp_bayar = DB::table("transactions")->where('transaction_status', 'PENDING')->where('deleted_at', '=', NULL)->count('transaction_status');
            $total_blmc_bayar = DB::table("transactions")->where('transaction_status', 'CANCEL')->where('deleted_at', '=', NULL)->count('transaction_status');
            $total_blmf_bayar = DB::table("transactions")->where('transaction_status', 'FAILED')->where('deleted_at', '=', NULL)->count('transaction_status');
            $total_blmt_bayar = DB::table("transactions")->where('transaction_status', 'INCART')->where('deleted_at', '=', NULL)->count('transaction_status');
            $total = ($total_blmp_bayar + $total_blmc_bayar) + ($total_blmf_bayar + $total_blmt_bayar);

            $transaksi_blmp_bayar = DB::table("transactions")->where('transaction_status', 'PENDING')->where('deleted_at', '=', NULL)->sum('total_seluruh');
            $transaksi_blmc_bayar = DB::table("transactions")->where('transaction_status', 'CANCEL')->where('deleted_at', '=', NULL)->sum('total_seluruh');
            $transaksi_blmf_bayar = DB::table("transactions")->where('transaction_status', 'FAILED')->where('deleted_at', '=', NULL)->sum('total_seluruh');
            $transaksi_blmt_bayar = DB::table("transactions")->where('transaction_status', 'INCART')->where('deleted_at', '=', NULL)->sum('total_seluruh');
            $total_blm_bayar = ($transaksi_blmp_bayar + $transaksi_blmc_bayar) + ($transaksi_blmf_bayar + $transaksi_blmt_bayar);

            return view('pages.dashboard.index', [
                'total_daftar' => $total_daftar,
                'total_sudah_bayar' => $total_sudah_bayar,

                'transaction' => $transaction,

                'total_blm_bayar' => $total,
                'transaksi_blm_bayar' => $total_blm_bayar,

                'transaction_pending' => Transaction::where('transaction_status', 'PENDING')->count(),
                'transaction_daftar' => Transaction::where('transaction_total')->count(),
            ]);
        } elseif ($user_id == 2) {

            $total_daftar = DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            $transaction =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('deleted_at', '=', NULL)
                ->get()->count('total_seluruh');

            // belum bayar
            $total_blmp_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'PENDING')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');

            $total_blmc_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'CANCEL')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');

            $total_blmf_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'FAILED')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');
            $total_blmt_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'INCART')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');
            $total = ($total_blmp_bayar + $total_blmc_bayar) + ($total_blmf_bayar + $total_blmt_bayar);

            // $transaksi_blmp_bayar = DB::table("transactions")->where('transaction_status', 'PENDING')->sum('transaction_total');
            $transaksi_blmp_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'PENDING')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            // $transaksi_blmc_bayar = DB::table("transactions")->where('transaction_status', 'CANCEL')->sum('total_seluruh');
            $transaksi_blmc_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'CANCEL')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            // $transaksi_blmf_bayar = DB::table("transactions")->where('transaction_status', 'FAILED')->sum('total_seluruh');
            $transaksi_blmf_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'FAILED')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            $transaksi_blmt_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'INCART')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            $total_blm_bayar = ($transaksi_blmp_bayar + $transaksi_blmc_bayar) + ($transaksi_blmf_bayar + $transaksi_blmt_bayar);

            // sudah bayar
            $total_sudah_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'SUCCESS')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');

            // $transaksi_sudah_bayar = DB::table("transactions")->where('transaction_status', 'SUCCESS')->sum('transaction_total');
            $transaksi_sudah_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'SUCCESS')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            // data user
            return view('pages.dashboard.index', [
                'transaction' => $transaction,
                'total_daftar' => $total_daftar,

                'total_blm_bayar' => $total,
                'transaksi_blm_bayar' => $total_blm_bayar,

                'total_sudah_bayar' => $total_sudah_bayar,
                'transaksi_sudah_bayar' => $transaksi_sudah_bayar,

                'transaction_pending' => Transaction::where('transaction_status', 'PENDING')->count(),
                'transaction_daftar' => Transaction::where('transaction_total')->count(),
            ]);
        } else {
            // data agent itu menampilkan data dari pembeli link agent tersebut "DASBOARD DAN DOMPET"
            $total_daftar = DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            $transaction =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('deleted_at', '=', NULL)
                ->get()->count('total_seluruh');

            // belum bayar
            $total_blmp_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'PENDING')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');

            $total_blmc_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'CANCEL')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');

            $total_blmf_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'FAILED')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');
            $total_blmt_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'INCART')
                ->where('deleted_at', '=', NULL)
                ->get()->count('transaction_status');
            $total = ($total_blmp_bayar + $total_blmc_bayar) + ($total_blmf_bayar + $total_blmt_bayar);

            // $transaksi_blmp_bayar = DB::table("transactions")->where('transaction_status', 'PENDING')->sum('transaction_total');
            $transaksi_blmp_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'PENDING')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            // $transaksi_blmc_bayar = DB::table("transactions")->where('transaction_status', 'CANCEL')->sum('total_seluruh');
            $transaksi_blmc_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'CANCEL')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            // $transaksi_blmf_bayar = DB::table("transactions")->where('transaction_status', 'FAILED')->sum('total_seluruh');
            $transaksi_blmf_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'FAILED')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            $transaksi_blmt_bayar =
                DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.users_id')
                ->where('transactions.users_id', '=', Auth::user()->id)
                ->where('transaction_status', '=', 'INCART')
                ->where('deleted_at', '=', NULL)
                ->get()->sum('total_seluruh');

            $total_blm_bayar = ($transaksi_blmp_bayar + $transaksi_blmc_bayar) + ($transaksi_blmf_bayar + $transaksi_blmt_bayar);

            return view('pages.dashboard.index', [
                'transaction' => $transaction,
                'total_daftar' => $total_daftar,

                'total_blm_bayar' => $total,
                'transaksi_blm_bayar' => $total_blm_bayar,

                'transaction_pending' => Transaction::where('transaction_status', 'PENDING')->count(),
                'transaction_daftar' => Transaction::where('transaction_total')->count(),
            ]);
        }
    }
}
