<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;

class TransactionsController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.transactions.index', [
            'transaction' => Transaction::count(),
            'transaction_pending' => Transaction::where('transaction_status', 'PENDING')->count(),
            'transaction_daftar' => Transaction::where('transaction_total')->count(),
        ]);
    }
}
