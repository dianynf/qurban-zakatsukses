<?php

namespace App\Http\Controllers;

class MenuController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function program()
    {
        return view('pages.menu.program');
    }

    public function faq()
    {
        return view('pages.menu.faq');
    }

    public function galeri()
    {
        return view('pages.menu.galeri');
    }

    public function download()
    {
        return view('pages.menu.download');
    }
}
