<?php

namespace App\Http\Controllers;

use Mail;
use App\Mail\TransactionSuccess;

use App\Transaction;
use App\TransactionDetail;
use App\Product;
use App\Gallery;
use App\Banks;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Agratek\Payment\Config;
use AgratekTransaction;

class CheckoutController extends Controller
{
    public function index(Request $request, $id)
    {
        //Digunakan untuk menampilkan form chekcout
        //Url: http://server/checkout/$id
        //$id adalah id number tabel transactions
        $item = Transaction::with(['details', 'product', 'user'])
            ->findOrFail($id);
        $hewan = explode("-", $item->product->slug);
        $va = Banks::where('rek', 'like', 'VA%')
            ->orWhere('rek', '=', 'PAY-WITH-FLIP')->get();
        $manual = Banks::where('rek', 'not like', 'VA%')
            ->where('rek', 'not like', 'PAY-WITH-FLIP')->get();

        return view('pages.checkout', [
            'item' => $item,
            'hewan' => $hewan[0],
            'va' => $va,
            'manual' => $manual,
        ]);
    }

    public function process(Request $request, $id)
    {
        //Url: http://server/checkout/1
        //Digunakan untuk processing cart setelah memilih jenis
        //produk qurban
        //1. Simpan pada tabel transaksi
        //2. Status = "INCART"
        //2. Tampilkan laman checkout

        $product = Product::findOrFail($id);

        $data = $request->all();
        $format = explode('Rp. ', $request->total_seluruh);
        $angka1 = str_replace(array('.', ','), '', $format);

        $datee = $request['invoice'];
        $transaction = Transaction::create([
            'product_id' => $id,
            'users_id' => Auth::user()->id ?? null,
            'name' => Auth::user()->username ?? '',
            'email' => Auth::user()->email ?? '',
            'nohp' => Auth::user()->nohp ?? '',
            'bank_id' => '7',
            'invoice' => $datee . rand(0, 99999999999),
            'transaction_total' => $product->harga,
            'quant' => $data['quant'],
            'agent_id' => $data['agent_id'] ?? null,
            'total_seluruh' => $angka1[1],
            'transaction_status' => 'INCART',
            'date' => $data['date']
        ]);
        return redirect()->route('checkout', $transaction->id);
    }

    public function remove(Request $request, $detail_id)
    {
        dd("Remove");
        $item = TransactionDetail::findorFail($detail_id);

        $transaction = Transaction::with(['details', 'product'])
            ->findOrFail($item->transactions_id);

        $transaction->save();
        $item->delete();

        return redirect()->route('checkout', $item->transactions_id);
    }

    public function create(Request $request, $id)
    {
        //Url: http://server/checkout/create/{detail_id}
        //Digunakan untuk checkout
        //1. Simpan pada tabel transaksi
        //2. Status = "INCART"
        //2. Tampilkan laman checkout
        $items = Transaction::with(['details', 'product', 'user'])->findOrFail($id);
        $lastid = $items->id;
        if (count($request->username) > 0) {
            foreach ($request->username as $item => $v) {
                $data2 = array(
                    'transactions_id' => $lastid,
                    'username' => $request->username[$item],
                );
                TransactionDetail::insert($data2);
            }
        }

        // randome angka
        $total = $items->total_seluruh;
        $total_remove_2 = substr($total, 0, -2);
        $total_seluruh_randome = $total_remove_2 . rand(0, 99);

        // if ($request->method()!="POST"){
        //     $total = $items->total_seluruh;
        //     $total_remove_2 = substr($total, 0, -2);
        //     $total_seluruh_randome = $total_remove_2 . rand(0, 99);
        // }else{
        //     $total_seluruh_randome=$items->total_seluruh;
        // }

        $data = $request->all();
        $request->validate([
            'bank_id' => 'required',
            'name' => 'required',
            'nohp' => 'required|max:15',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'post_code' => 'required',

        ]);

        $datee = $request['invoice'];
        $nulltransaksi = Auth::user()->id ?? 2;
        $data['bank_id'] = $request['bank_id'];
        $data['users_id'] = $nulltransaksi;
        $data['invoice'] = $datee . rand(0, 99999999999);
        $data['total_seluruh'] = $total_seluruh_randome;
        $data['name'] = $request['name'];
        $data['nohp'] = $request['nohp'];
        $data['email'] = $request['email'];
        $data['address'] = $request['address'];
        $data['city'] = $request['city'];
        $data['state'] = $request['state'];
        $data['post_code'] = $request['post_code'];
        $item = Transaction::findOrFail($id);
        $item->update($data);

        // dd($items->bank->rek);
        $bank = Banks::findOrFail($data["bank_id"]);
        if (substr($bank->rek, 0, 2) == 'VA' || $bank->rek == 'PAY-WITH-FLIP') {
            return $this->success($request, $id);
        } else {
            return redirect()->route('invoice', $id);
        }
    }

    public function success(Request $request, $id)
    {
        //Digunakan untuk request ke agregator untuk memperoleh no transaksi dan
        //Field-field lain yang dibutuhkan dalam transaksi online

        $transaction = Transaction::with(['details', 'product.galleries', 'user'])
            ->findOrFail($id);
        $transaction->transaction_status = 'PENDING';
        $transaction->save();

        //Start Here
        Config::$clientKey = config('agratek.clientKey');
        Config::$clientId = config('agratek.clientId');
        Config::$clientDom = config('agratek.clientDom');
        Config::$isSandbox = config('agratek.isSandbox');
        $params = array(
            'account' => array(
                "denom" => $transaction->bank->rek,
                "amount" => $transaction->total_seluruh,
                //todo amount ini seharusnya sudah termasuk biaya administrasi
                "description" => $transaction->product->title,
                "invoice_no" => strval($transaction->id),
            ),
            "customer" => array(
                "name" => $transaction->name,
                "phone" => $transaction->nohp,
                "email" => $transaction->email,
                //todo field2 ini wajib diisi saat checkout
                "address" => $request['address'],
                "city" => $request['city'],
                "state" => $request['state'],
                "post_code" => $request['post_code'],
                "country" => "Indonesia",
                "session_id" => session_id(),
                "ip" => $_SERVER['REMOTE_ADDR']
            ),
            'cart' => array(
                "count" => "1",
                "item" => array(
                    array(
                        "img_url" => "",
                        "goods_name" => $transaction->product->title . " " . $transaction->product->berat,
                        "goods_detail" => $transaction->product->about,
                        "goods_amt" => $transaction->product->harga
                    )
                )
            ),
        );
        $params["deliver_to"] = $params["customer"];
        //params biller ini seharusnya di set sama agratek bukan sama zakatsukses
        //$params["biller"] = $params["customer"];
        $result = AgratekTransaction::register($params);
        try {
            if (array_key_exists("va", $result)) {
                $va = $result["va"];
                $data = array(
                    //todo: penambahan field berikut ini dalam tabel transaksi
                    "va_number" => $va["vacct_no"],
                    "valid_date" => $va["valid_date"],
                    "valid_time" => $va["valid_time"],
                    "tx_id" => $result["tx_id"],

                );
                $transaction->update($data);
                $request->session()->put('va', $data);
                return redirect()->route('invoice', $id);

                //$transaction->save();
            } elseif (array_key_exists("pwf", $result)) {
                $pwf = $result["pwf"];
                $data = array(
                    //todo: penambahan field berikut ini dalam tabel transaksi
                    "pwf_url" => $pwf["pwf_url"],
                    "tx_id" => $result["tx_id"],
                );
                $transaction->update($data);
                // header('Location: ' . );
                return redirect()->to("https://" . $pwf["pwf_url"]);
            } else {
                return redirect()->route('checkout', $id);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function sendWhatsapp()
    {
        $nama = request('nama_lengkap');
        $email = request('email');
        $akad = request('akad');
        $nominal = request('nominal');
        $bank = request('bank');

        return redirect("https://wa.me/6282211627700?text=Assalamu%27alaikum%20Warahmatullahi%20Wabarakatuh.%0A%0APerkenalkan%20saya%20"
            . $nama . "%2C%20ingin%20konfirmasi%20untuk%20pemesanan%20hewan%20qurban.%20Mentransfer%20" . $akad . "%20senilai%20Rp.%20" . $nominal . "%20melalui%20rekening%20" . $bank . ".%0A%0ATerima%20Kasih.");
    }
}
