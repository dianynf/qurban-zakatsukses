<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{
    public function edit(Request $request)
    {
        $user_id = Auth::user()->role_id;

        if ($user_id == 1) {
            return view('pages.document.edit', [
                'users' => $request->user()
            ]);
        } elseif ($user_id == 2) {
            return abort(404);
        } else {
            return view('pages.document.edit', [
                'users' => $request->user()
            ]);
        }
    }

    public function update(Request $request)
    {
        $image = $request->file('fotonik');
        $foto = $request->file('fotoselfi');

        if ($foto) {
            $request->user()->update([
                'nik' => $request->nik,
                'fotoselfi' => $foto->getClientOriginalName()
            ]);
            $foto->storeAs('public/fotoselfi', $foto->getClientOriginalName());
        } else if ($image) {
            $request->user()->update([
                'nik' => $request->nik,
                'fotonik' => $image->getClientOriginalName()
            ]);
            $image->storeAs('public/fotonik', $image->getClientOriginalName());
        } else {
            $request->user()->update([
                'nik' => $request->nik
            ]);
        }

        session()->flash('sukses', 'Pengaturan Document berhasil di Simpan');
        return redirect()->route('document.edit');
    }
}
