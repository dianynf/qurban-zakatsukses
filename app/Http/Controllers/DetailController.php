<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;

class DetailController extends Controller
{
    public function index(Request $request, $slug)
    {
        //dd(storage_path());
        $item = Product::with(['galleries'])->where('slug', $slug)->firstOrFail();
        return view('pages.detail', [
            'item' => $item
        ]);
    }
}
