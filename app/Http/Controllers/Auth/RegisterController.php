<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:agen');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255'],
            // 'nik' => ['required', 'string', 'max:20'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'nohp' => ['required', 'min:10', 'max:15'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $date = date("Y-m-d H:i:s");
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'nohp' => $data['nohp'],
            'role_id' => 2,
            'name' => 'user' . rand(0, 9999999),
            'email_verified_at' => $date,
            'fotoselfi' => 'defaultfikzakatsuksesok.jpg',
            'fotonik' => 'defaultnikzakatsuksesok.jpg',
            'fotobuku' => 'defaultfkuzakatsuksesok.png',
        ]);

        return redirect()->route('login');
    }

    public function showAgenRegisterForm()
    {
        return view('auth.agent', ['url' => 'prosess']);
    }

    public function prosess(Request $request)
    {
        $this->validator($request->all())->validate();
        $data = $request->all();

        User::create([
            'username' => $data['username'],
            'nik' => $data['nik'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'nohp' => $data['nohp'],
            'role_id' => 3,
            'roles' => 'AGENT',
            'name' => 'agent' . rand(0, 99999999),
            'alamat' => $data['alamat'],
            'email_verified_at' => date("Y-m-d H:i:s"),
            'fotoselfi' => 'defaultfikzakatsuksesok.jpg',
            'fotonik' => 'defaultnikzakatsuksesok.jpg',
            'fotobuku' => 'defaultfkuzakatsuksesok.png',
        ]);
        return redirect()->route('login');
    }
}
