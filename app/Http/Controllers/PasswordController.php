<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests\UpdatePasswordRequest;

class PasswordController extends Controller
{
    public function edit()
    {
        return view('pages.password.edit');
    }

    /**
     * @param UpdatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePasswordRequest $request)
    {
        $request->user()->update([
            'password' => Hash::make($request->get('password'))
        ]);

        session()->flash('sukses', 'Password Berhasil di Ubah');
        return redirect()->route('user.password.edit');
    }
}
