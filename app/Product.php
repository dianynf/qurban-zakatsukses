<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'slug', 'about', 'berat', 'harga'
    ];

    protected $hidden = [];

    public function galleries()
    {
        return $this->hasMany(Gallery::class, 'product_id', 'id');
    }

    public function addtransaksis()
    {
        return $this->hasMany(Addtransaksi::class, 'product_id', 'id');
    }
}
