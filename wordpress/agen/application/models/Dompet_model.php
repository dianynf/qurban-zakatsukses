<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dompet_model extends CI_Model
{
	function tampil_data()
	{
		return $this->db->get('dompet');
	}

	function detail_data($where, $table)
	{
		return $this->db->get_where($table, $where);
	}
}
