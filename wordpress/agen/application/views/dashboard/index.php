<div class="container-fluid">
	<!-- <div class="row mt-4"> -->
	<div class="row col-mb-12">
		<!-- Earnings (Monthly) Card Example -->
		<div class="col-xl-4 col-md-6">
			<div class="card border-left-warning shadow">
				<div class="card-body">
					<h6 class="h6 font-weight-bold text-gray-800 text-center">Link Referral</h6>
					<div class="small mb-3 text-center"><span id="copyTarget">http://qurban.zakatsukses.org/agen/</span></div>
					<div class="pl-3 text-center">
						<a href="https://www.facebook.com/sharer.php?u=http://qurban.zakatsukses.org&picture=http://qurban.zakatsukses.org/wp-content/uploads/2020/07/Qurban-Pelosok-Nusantara.jpeg" target="_blank" class="btn btn-primary btn-circle">
							<i class="fa fa-facebook"></i>
						</a>
						<a href="https://twitter.com/intent/tweet?url=http://qurban.zakatsukses.org&text=Penuhi panggilan qurban tahun ini melalui : " target="_blank" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i>
						</a>
						<a href="https://web.whatsapp.com/send?text=%5f%E2%80%9CMaka+dirikanlah+salat+karena+Tuhanmu+dan+berqurbanlah.%E2%80%9D+-+Al-Kautsar+%3A2%5f%0D%0A%0D%0APenuhi+panggilan+berquban+tahun+ini+melalui+%3A+%2Ahttps%3A%2F%2Fwww.globalqurban.com%2Fagen%2Fdianbugasnwx%2A%0D%0A%0D%0AInsya+Allah%2C+qurban+yang+Anda+sampaikan+akan+menjangkau+saudara+di+seluruh+pelosok+Indonesia+dan+mancanegara.%0D%0A%0D%0ALabbaik%21+Berqurban+terbaik.+Ajak+semua+keluarga%2C+sahabat+tercinta+untuk+segera+tunaikan+qurbanmu+melalui+Zakat+Sukses.%0D%0A%0D%0A*%23ZakatSukses*" target="_blank" class="btn btn-success btn-circle">
							<i class="fas fa fa-whatsapp"></i>
						</a>
						<button class="btn-light btn-circle" id="copyButton">
							<i class="fa fa-link cp-i"></i>
						</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card border-left-info shadow">
				<div class="card-body">
					<h6 class="h6 font-weight-bold text-gray-800 text-center">Total Daftar</h6>
					<div class="row h-100 mt-2">
						<div class="col-lg-4 text-center">
							<span class="small">Transaksi</span>
							<h4 class="text-center mt-2">0</h4>
						</div>
						<div class="col-lg-8 text-right">
							<span class="small">Donasi</span>
							<h3 class="mt-2"><small><strong>Rp.</strong>0</small></h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-4 col-md-6">
			<div class="card border-left-success shadow">
				<div class="card-body">
					<h6 class="h6 font-weight-bold text-gray-800 text-center">Total Belum Transaksi</h6>
					<div class="row h-100 mt-2">
						<div class="col-lg-4 text-center">
							<span class="small">Transaksi</span>
							<h4 class="text-center mt-2">0</h4>
						</div>
						<div class="col-lg-8 text-right">
							<span class="small">Donasi</span>
							<h3 class="mt-2"><small><strong>Rp.</strong>0</small></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- End of Main Content -->
