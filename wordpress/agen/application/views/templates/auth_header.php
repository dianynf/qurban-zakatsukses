<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title><?= $title; ?></title>
	<!-- Custom fonts for this template-->
	<link href="<?= base_url('assets/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/'); ?>css/style.css">
	<!-- Custom styles for this template-->
	<link href="<?= base_url('assets/'); ?>css/sb-admin-2.min.css" rel="stylesheet">

	<style>
		.button-login {
			background-color: #ff9933;
			border: none;
			color: white;
			padding: 15px 32px;
			text-align: center;
			cursor: pointer;
			text-align: center;
			font-size: 16px;
		}

		.button-login:hover {
			background-color: #ff8000;
			color: white;
		}

		.link-lupp {
			text-decoration: none;
			color: #ff9933;
			font-size: 14px;
		}

		.link-lupp:hover {
			color: #ff8000;
		}

		.modal-heder {
			background-color: #ff9933;
		}

		.hr.new5 {
			border: 3px solid #ff9933;
			border-radius: 5px;
		}

		/* Chrome, Safari, Edge, Opera */
		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
			-webkit-appearance: none;
			margin: 0;
		}

		/* Firefox */
		input[type=number] {
			-moz-appearance: textfield;
		}
	</style>

</head>

<body>
