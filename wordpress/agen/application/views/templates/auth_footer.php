<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>

<script>
	$(function() {
		$('#only-number').on('keydown', '#nohp', function(e) {
			-1 !== $
				.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/
				.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) ||
				35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) &&
				(96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
		});
	});

	$(document).ready(function() {
		$("#show_hide_password a").on('click', function(event) {
			event.preventDefault();
			if ($('#show_hide_password input').attr("type") == "text") {
				$('#show_hide_password input').attr('type', 'password');
				$('#show_hide_password i').addClass("fa-eye-slash");
				$('#show_hide_password i').removeClass("fa-eye");
			} else if ($('#show_hide_password input').attr("type") == "password") {
				$('#show_hide_password input').attr('type', 'text');
				$('#show_hide_password i').removeClass("fa-eye-slash");
				$('#show_hide_password i').addClass("fa-eye");
			}
		});
	});

	$(document).ready(function() {
		$("#show_hide_passwordd a").on('click', function(event) {
			event.preventDefault();
			if ($('#show_hide_passwordd input').attr("type") == "text") {
				$('#show_hide_passwordd input').attr('type', 'password');
				$('#show_hide_passwordd i').addClass("fa-eye-slash");
				$('#show_hide_passwordd i').removeClass("fa-eye");
			} else if ($('#show_hide_passwordd input').attr("type") == "password") {
				$('#show_hide_passwordd input').attr('type', 'text');
				$('#show_hide_passwordd i').removeClass("fa-eye-slash");
				$('#show_hide_passwordd i').addClass("fa-eye");
			}
		});
	});

	$(document).ready(function() {
		$("#show_hide_password_login a").on('click', function(event) {
			event.preventDefault();
			if ($('#show_hide_password_login input').attr("type") == "text") {
				$('#show_hide_password_login input').attr('type', 'password');
				$('#show_hide_password_login i').addClass("fa-eye-slash");
				$('#show_hide_password_login i').removeClass("fa-eye");
			} else if ($('#show_hide_password_login input').attr("type") == "password") {
				$('#show_hide_password_login input').attr('type', 'text');
				$('#show_hide_password_login i').removeClass("fa-eye-slash");
				$('#show_hide_password_login i').addClass("fa-eye");
			}
		});
	});
</script>

<!-- Scrip Wilayah -->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script>
	$(document).ready(function() {
		$("#provinsi").change(function() {
			var url = "<?php echo site_url('auth/add_ajax_kab'); ?>/" + $(this).val();
			$('#kabupaten').load(url);
			return false;
		})

		$("#kabupaten").change(function() {
			var url = "<?php echo site_url('auth/add_ajax_kec'); ?>/" + $(this).val();
			$('#kecamatan').load(url);
			return false;
		})

		$("#kecamatan").change(function() {
			var url = "<?php echo site_url('auth/add_ajax_des'); ?>/" + $(this).val();
			$('#desa').load(url);
			return false;
		})
	});
</script>

</body>

</html>
