<div class="container">
	<!-- Outer Row -->
	<div class="row justify-content-center">
		<div class="col-lg-7">
			<div class="card o-hidden border-0 shadow-lg my-5">
				<div class="card-body p-0">
					<!-- Nested Row within Card Body -->
					<div class="row">
						<div class="col-lg">
							<div class="p-5">
								<div class="text-center">
									<h1 class="h4 text-gray-900 mb-4"><b>Login User</b></h1>
								</div>
								<?= $this->session->flashdata('message'); ?>
								<form class="user" method="post" action="<?= base_url('auth'); ?>">
									<div class="form-group">
										<input type="text" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Email" value="<?= set_value('email '); ?>">
										<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
									</div>
									<div class="form-group">
										<div class="input-group" id="show_hide_password_login">
											<input type="password" class="form-control" id="password" name="password" placeholder="Password">
											<div class="input-group-append">
												<div class="btn btn-default viewpass" data-target="#password">
													<a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6">
												<div class="custom-control custom-checkbox small">
													<input type="checkbox" class="custom-control-input" id="customCheck">
													<label class="custom-control-label" for="customCheck">Ingatkan Login</label>
												</div>
											</div>
											<div class="col-sm-6" style="text-align: right;">
												<a class="small link-lupp" style="text-decoration:none;" href="<?= base_url('auth/forgotpassword'); ?>"><b>Lupa Password</b></a>
											</div>
										</div>
									</div>
									<button type="submit" class="btn button-login btn-user btn-block">
										<b>Login</b>
									</button>
								</form>
								<!-- <hr> -->
								<!-- <div class="text-center">
									<a class="small" href="<?= base_url('auth/forgotpassword'); ?>">Lupa Password</a>
								</div>
								<div class="text-center">
									<a class="small" href="<?= base_url('auth/registration'); ?>">Daftar Akun!</a>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
