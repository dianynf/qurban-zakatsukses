<div class="container-fluid">
	<div class="row mt-12">
		<div class="col md-5">
			<div class="card-deck">
				<div class="col-xl-1 col-md-8 mb-7">
				</div>
				<div class="col-xl-10 col-md-8 mb-5">
					<div class="col-md-12">
						<?= $this->session->flashdata('message'); ?>
					</div>
					<div class="card border-left-warning shadow h-100 py-2">
						<!-- Page Heading -->
						<div class="row">
							<div class="col-lg-12">
								<div class="modal-body">
									<h3 class="h3 mb-4 text-gray-800">Pengaturan Document</h3>
									<hr>
									<?= form_open_multipart('document'); ?>
									<input type="hidden" id="email" name="email" value="<?= $user['email']; ?>">
									<div class="form-group row">
										<label for="nik" class="col-sm-3 col-form-label">No. NIK (KTP)</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="nik" name="nik" value="<?= $user['nik']; ?>" maxlength="20">
											<?= form_error('nik', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-3">Photo NIK (KTP)</div>
										<div class="col-sm-9">
											<div class="row">
												<div class="col-sm-5">
													<img src="<?= base_url('assets/img/fotonik/') . $user['fotonik']; ?>" class="img-thumbnail">
												</div>
												<div class="col-sm-7">
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="fotonik" name="fotonik" value="<?= $user['fotonik']; ?>">
														<label class="custom-file-label" for="fotonik"><?= $user['fotonik']; ?></label>
													</div>
													<span>
														Gambar Photo NIK (KTP) Anda sebaiknya memiliki berukuran tidak lebih dari 2MB.
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-3">Photo NIK (KTP) + Selfi</div>
										<div class="col-sm-9">
											<div class="row">
												<div class="col-sm-5">
													<img src="<?= base_url('assets/img/fotoselfi/') . $user['fotoselfi']; ?>" class="img-thumbnail">
												</div>
												<div class="col-sm-7">
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="fotoselfi" name="fotoselfi" value="<?= $user['fotoselfi']; ?>">
														<label class="custom-file-label" for="fotoselfi"><?= $user['fotoselfi']; ?></label>
													</div>
													<span>
														Gambar Photo NIK (KTP) + Selfi Anda sebaiknya memiliki berukuran tidak lebih dari 2MB.
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-file row justify-content-end">
										<div class="col-sm-9">
											<button type="submit" class="btn btn-success btn-sm">Simpan</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
