<div class="container-fluid">
	<div class="row mt-12">
		<div class="col md-5">
			<div class="card-deck">
				<div class="col-xl-1 col-md-8 mb-7">
				</div>
				<div class="col-xl-10 col-md-8 mb-5">
					<div class="col-md-12">
						<?= $this->session->flashdata('message'); ?>
					</div>
					<div class="card border-left-warning shadow h-100 py-2">
						<div class="row">
							<div class="col-lg-12">
								<div class="modal-body">
									<h3 class="h3 mb-4 text-gray-800">Pengaturan Bank</h3>
									<hr>
									<?= form_open_multipart('bank'); ?>
									<input type="hidden" id="email" name="email" value="<?= $user['email']; ?>">
									<div class="form-group row">
										<label for="bank_id" class="col-sm-3 col-form-label">Nama Bank*</label>
										<div class="col-sm-9">
											<select name="bank_id" id="bank_id" class="form-control">
												<?php
												foreach ($bank as $row2) { ?>
													<option value="<?= $row2->id ?>" <?php if ($row2->id == $user['bank_id']) {
																							echo "selected";
																						}; ?>><?= $row2->nama ?></option>
												<?php }  ?>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="rekening" class="col-sm-3 col-form-label">Account Number (No. Rekening)*</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="rekening" name="rekening" maxlength="20" value="<?= $user['rekening']; ?>">
											<?= form_error('rekening', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="atasnama" class="col-sm-3 col-form-label">Atas Nama Rekening</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="atasnama" name="atasnama" value="<?= $user['atasnama']; ?>">
											<?= form_error('atasnama', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="cabang" class="col-sm-3 col-form-label">Cabang</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="cabang" name="cabang" value="<?= $user['cabang']; ?>" maxlength="45">
											<?= form_error('cabang', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-3">Photo Buku Tabungan</div>
										<div class="col-sm-9">
											<div class="row">
												<div class="col-sm-5">
													<img src="<?= base_url('assets/img/profile/') . $user['fotobuku']; ?>" class="img-thumbnail">
												</div>
												<div class="col-sm-7">
													<div class="custom-file">
														<input type="file" class="custom-file-input" id="fotobuku" name="fotobuku" value="<?= $user['fotobuku']; ?>">
														<label class="custom-file-label" for="fotobuku"><?= $user['fotobuku']; ?></label>
													</div>
													<span>
														Gambar Photo Buku Tabungan Anda sebaiknya memiliki berukuran tidak lebih dari 2MB.
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-file row justify-content-end">
										<div class="col-sm-9">
											<button type="submit" class="btn btn-success btn-sm">Simpan</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
