<div class="container-fluid">
	<div class="row mt-12">
		<div class="col md-5">
			<div class="card-deck">
				<div class="col-xl-1 col-md-8 mb-7">
				</div>
				<div class="col-xl-10 col-md-8 mb-5">
					<div class="col-md-12">
						<?= $this->session->flashdata('message'); ?>
					</div>
					<div class="card border-left-warning shadow h-100 py-2">
						<!-- Page Heading -->
						<div class="row">
							<div class="col-lg-12">
								<div class="modal-body">
									<h3 class="h3 mb-4 text-gray-800">Pengaturan Informasi</h3>
									<hr>
									<?= form_open_multipart('user/edit'); ?>
									<div class="form-group row">
										<label for="name" class="col-sm-2 col-form-label">Full Nama</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="name" name="name" value="<?= $user['name']; ?>">
											<?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="email" class="col-sm-2 col-form-label">Email</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="email" name="email" value="<?= $user['email']; ?>" readonly>
										</div>
									</div>
									<div class="form-group row">
										<label for="nohp" class="col-sm-2 col-form-label">No. Telepon</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="nohp" name="nohp" value="<?= $user['nohp']; ?>">
											<?= form_error('nohp', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" id="alamat" name="alamat" value="<?= $user['alamat']; ?>">
											<?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="provinsi" class="col-sm-2 col-form-label">Provinsi</label>
										<div class="col-sm-10">
											<select class="form-control" disabled>
												<?php
												foreach ($provinsi as $prov) { ?>
													<option value="#" <?php if ($prov->id == $user['provinsi']) {
																			echo "selected";
																		}; ?>><?= $prov->nama ?></option>
												<?php }  ?>
											</select>
											<?= form_error('provinsi', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="kabupaten" class="col-sm-2 col-form-label">Kabupaten</label>
										<div class="col-sm-10">
											<select class="form-control" disabled>
												<?php
												foreach ($kabupaten as $kab) { ?>
													<option value="#" <?php if ($kab->id == $user['kabupaten']) {
																			echo "selected";
																		}; ?>><?= $kab->nama ?></option>
												<?php }  ?>
											</select>
											<?= form_error('kabupaten', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="kecamatan" class="col-sm-2 col-form-label">Kecamatan</label>
										<div class="col-sm-10">
											<select class="form-control" disabled>
												<?php
												foreach ($kecamatan as $kec) { ?>
													<option value="#" <?php if ($kec->id == $user['kecamatan']) {
																			echo "selected";
																		}; ?>><?= $kec->nama ?></option>
												<?php }  ?>
											</select>
											<?= form_error('kecamatan', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="desa" class="col-sm-2 col-form-label">kelurahan</label>
										<div class="col-sm-10">
											<select class="form-control" disabled>
												<?php
												foreach ($desa as $des) { ?>
													<option value="#" <?php if ($des->id == $user['desa']) {
																			echo "selected";
																		}; ?>><?= $des->nama ?></option>
												<?php }  ?>
											</select>
											<?= form_error('desa', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-group row">
										<label for="kodepos" class="col-sm-2 col-form-label">kodepos</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" value="<?= $user['kodepos']; ?>" disabled>
											<?= form_error('kodepos', '<small class="text-danger pl-3">', '</small>'); ?>
										</div>
									</div>
									<div class="form-file row justify-content-end">
										<div class="col-sm-10">
											<button type="submit" class="btn btn-success btn-sm">Simpan</button>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
