<div class="container-fluid">
	<div class="card shadow mb-4">
		<div class="card border-left-warning shadow h-100 py-2">
			<div class="card-body">
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h3 class="h3 mb-0 text-gray-800"><?= $title; ?></h3>
					<a href="<?= base_url('admin/tambahuser'); ?>" class="d-none d-sm-inline-block btn btn-primary btn-sm  shadow-sm">Tambah</a>
				</div>
				<div class="row mt-3">
					<div class="col-md-12">
						<?= $this->session->flashdata('message'); ?>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">Nama</th>
								<th scope="col">Email</th>
								<th scope="col">role</th>
								<th scope="col">Active</th>
								<th scope="col">Image</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($users as $us) : ?>
								<tr>
									<th scope="row"><?= $i++; ?></th>
									<td><?= $us['name']; ?></td>
									<td><?= $us['email']; ?></td>
									<td>
										<?php foreach ($role as $k) { ?>
										<?php if ($k->id == $us['role_id']) {
												echo $k->role;
											}
										} ?>
									</td>
									<td style="text-align: center;">
										<?php
										if ($us['is_active'] == 1) {
											echo "<span class='btn btn-success btn-sm'>Aktif</span>";
										} else {
											echo "<span class='btn btn-danger btn-sm'>Tidak Aktif</span>";
										} ?>
									</td>
									<td><img src="<?= base_url('assets/img/fotoselfi/') . $us['fotoselfi']; ?>" class="img-circle" alt="..." width="40" height="40"></td>
									<td>
										<a href="<?= base_url(); ?>admin/editusers/<?= $us['id']; ?>" class="btn btn-success btn-circle btn-sm">
											<i class="fas fa-edit"></i>
										</a>
										<a href="<?= base_url(); ?>admin/hapus/<?= $us['id']; ?>" class="btn btn-danger btn-circle btn-sm">
											<i class="fas fa-trash"></i>
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->
</div>
