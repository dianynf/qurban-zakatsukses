<div class="container-fluid">
	<div class="card shadow mb-4">
		<div class="card border-left-warning shadow h-100 py-2">
			<div class="card-body">
				<div class="d-sm-flex align-items-center justify-content-between mb-4">
					<h3 class="h3 mb-0 text-gray-800">Data Transaksi Qurban</h3>
				</div>
				<div class="row mt-3">

				</div>
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">Invoice</th>
								<th scope="col">Nama</th>
								<th scope="col">Email</th>
								<th scope="col">Telp</th>
								<th scope="col">Tanggal</th>
								<th scope="col">Total</th>
								<th scope="col">Pembayaran</th>
								<th scope="col">Status Bayar</th>
								<th scope="col">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach ($transaksi as $trn) : ?>
								<tr>
									<th scope="row"><?= $i++; ?></th>
									<td>-</td>
									<td>
										<?php
										foreach ($customer as $k) { ?>
										<?php if ($k->customer_id == $trn->customer_id) {
												echo $k->first_name;
											}
										} ?>
										<?php
										foreach ($customer as $k) { ?>
										<?php if ($k->customer_id == $trn->customer_id) {
												echo $k->last_name;
											}
										} ?>
									</td>
									<td><?php
										foreach ($customer as $k) { ?>
										<?php if ($k->customer_id == $trn->customer_id) {
												echo $k->email;
											}
										} ?>
									</td>
									<td>-</td>
									<td>-</td>
									<td><?= $trn->product_net_revenue ?></td>
									<td>-</td>
									<td>-</td>
									<td>
										<a href="<?= base_url(); ?>transaksi/detail/<?= $trn->order_item_id ?>" class="btn btn-info btn-sm">
											<span class="text">Detail</span>
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
