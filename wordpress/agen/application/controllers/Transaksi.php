<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Transaksi_model');
		$this->load->model('User_model');
		is_logged_in();
	}

	public function index()
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		// $query = $this->db->query('SELECT * FROM `wp9d_wc_order_product_lookup` INNER JOIN wp9d_wc_customer_lookup ON wp9d_wc_order_product_lookup.customer_id=wp9d_wc_customer_lookup.customer_id JOIN user ON wp9d_wc_customer_lookup.email=user.email');

		$data['transaksi'] = $this->Transaksi_model->tampil_data()->result();
		$data['customer'] = $this->db->query("SELECT * FROM wp9d_wc_customer_lookup")->result();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('transaksi/index', $data);
		$this->load->view('templates/footer');
	}

	public function detail($order_item_id)
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$where = array('order_item_id' => $order_item_id);

		// $userLogin = Auth::user()->id;
		$data['customer'] = $this->Transaksi_model->detail_data($where, 'wp9d_wc_order_product_lookup')->result();
		$data['transaksi'] = $this->db->query("SELECT * FROM wp9d_wc_customer_lookup")->result();
		
		if ($_SESSION['email'] != $order_item_id) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('transaksi/detail', $data);
			$this->load->view('templates/footer');
		} else {
			$this->output->set_status_header('404');
		}
	}
}
