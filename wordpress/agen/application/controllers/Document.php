<?php

// use FontLib\Table\Type\name;

defined('BASEPATH') or exit('No direct script access allowed');

class Document extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		// di tendang supaya user sembarangan tdk masuk sembarangan lewat url
		is_logged_in();
	}

	public function index()
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri  Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('nik', 'No. NIK (KTP)', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('user/document', $data);
			$this->load->view('templates/footer');
		} else {

			$nik = $this->input->post('nik');
			$email = $this->input->post('email');
			$upload_foto = $_FILES['fotonik']['name'];
			if ($upload_foto) {
				$config['upload_path'] = './assets/img/fotonik/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = '2048'; //ukuran gambar
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('fotonik')) {
					$old_foto = $data['user']['fotonik'];
					if ($old_foto != 'default.jpg') {
						unlink(FCPATH . 'assets/img/fotonik/' . $old_foto);
					}
					$new_foto = $this->upload->data('file_name');
					$this->db->set('fotonik', $new_foto);
				} else {
					echo $this->upload->display_errors();
				};
			}

			$upload_image = $_FILES['fotoselfi']['name'];

			if ($upload_image) {
				$config['upload_path'] = './assets/img/fotoselfi/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = '2048'; //ukuran gambar
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('fotoselfi')) {
					$old_image = $data['user']['fotoselfi'];
					if ($old_image != 'default.jpg') {
						unlink(FCPATH . 'assets/img/fotoselfi/' . $old_image);
					}
					$new_image = $this->upload->data('file_name');
					$this->db->set('fotoselfi', $new_image);
				} else {
					echo $this->upload->display_errors();
				};
			}
			$this->db->set('nik', $nik);
			$this->db->where('email', $email);
			$this->db->update('user');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
																Data berhasil <strong>diperbaharui!</strong>
																<button type="button" class="close" data-dismiss="alert" aria-label="Close">
																<span aria-hidden="true">&times;</span>
																</button>
															</div>');
			redirect('document');
		}
	}
}
