<?php

// use FontLib\Table\Type\name;

defined('BASEPATH') or exit('No direct script access allowed');

class Bank extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		// di tendang supaya user sembarangan tdk masuk sembarangan lewat url
		is_logged_in();
	}

	public function index()
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('rekening', 'Account Number', 'required|trim');
		$data['bank'] = $this->db->query("SELECT * FROM bank")->result();

		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('user/bank', $data);
			$this->load->view('templates/footer');
		} else {
			$bank_id = $this->input->post('bank_id');
			$rekening = $this->input->post('rekening');
			$atasnama = $this->input->post('atasnama');
			$cabang = $this->input->post('cabang');
			$email = $this->input->post('email');


			//cek jika ada gambar yang akan  diupload
			$upload_image = $_FILES['fotobuku']['name'];

			if ($upload_image) {
				$config['upload_path'] = './assets/img/profile/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size'] = '2048'; //ukuran gambar

				$this->load->library('upload', $config);

				if ($this->upload->do_upload('fotobuku')) {
					//cek gambar lama ngambil dari variabel data
					$old_image = $data['user']['fotobuku'];
					//cek gambar defaul bukan
					if ($old_image != 'default.jpg') {
						//ganti gambar baru
						unlink(FCPATH . 'assets/img/profile/' . $old_image);
					}

					// berisi nama file baru yg mau di masukan di baris ke 70. klu ada gambarnya maka bertambah
					$new_image = $this->upload->data('file_name');
					//klu ga ada gambar maka perintah ini tdk akan di set
					$this->db->set('fotobuku', $new_image);
				} else {
					echo $this->upload->display_errors();
				};
			}

			$this->db->set('bank_id', $bank_id);
			$this->db->set('rekening', $rekening);
			$this->db->set('atasnama', $atasnama);
			$this->db->set('cabang', $cabang);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
															Data berhasil <strong>diperbaharui!</strong>
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															<span aria-hidden="true">&times;</span>
															</button>
														</div>');
			redirect('bank');
		}
	}
}
