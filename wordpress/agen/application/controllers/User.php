<?php

// use FontLib\Table\Type\name;

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->library('form_validation');
		$this->load->helper('url');
		// di tendang supaya user sembarangan tdk masuk sembarangan lewat url
		is_logged_in();
	}

	public function index()
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$data['users'] = $this->User_model->tampil_data()->result();
		$data['provinsi'] = $this->db->query("SELECT * FROM wilayah_provinsi")->result();
		$data['kabupaten'] = $this->db->query("SELECT * FROM wilayah_kabupaten")->result();
		$data['kecamatan'] = $this->db->query("SELECT * FROM wilayah_kecamatan")->result();
		$data['desa'] = $this->db->query("SELECT * FROM wilayah_desa")->result();
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('user/index', $data);
		$this->load->view('templates/footer');
	}

	public function edit()
	{
		$data['title'] = 'Edit Profile';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('name', 'Full name', 'required|trim');
		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('user/index', $data);
			$this->load->view('templates/footer');
		} else {
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$nohp = $this->input->post('nohp');
			$alamat = $this->input->post('alamat');

			$this->db->set('alamat', $alamat);
			$this->db->set('name', $name);
			$this->db->set('nohp', $nohp);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
															Data berhasil <strong>diperbaharui!</strong>
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															<span aria-hidden="true">&times;</span>
															</button>
														</div>');
			redirect('user');
		}
	}

	public function changepassword()
	{
		$data['title'] = 'Cinta Qurban - Menjangkau Pelosok Negeri Dunia';
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('current_password', 'Current Password', 'required|trim');
		$this->form_validation->set_rules('new_password1', 'New Password', 'required|trim|min_length[3]|matches[new_password2]');
		$this->form_validation->set_rules('new_password2', 'Confirm New Password', 'required|trim|min_length[3]|matches[new_password1]');
		if ($this->form_validation->run() == false) {
			$this->load->view('templates/header', $data);
			$this->load->view('templates/sidebar', $data);
			$this->load->view('templates/topbar', $data);
			$this->load->view('user/changepassword', $data);
			$this->load->view('templates/footer');
		} else {
			//ambil data inputan password lama
			$current_password = $this->input->post('current_password');
			//ambil data yg di baru di input
			$new_password = $this->input->post('new_password1');
			if (!password_verify($current_password, $data['user']['password'])) {
				//cek password apakah sama yg dinput dgn data di dlam data base.  klu tdk tampilkan berikut
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password anda salah saat ini!</div>');
				redirect('user/changepassword');
			} else {
				//cek apakah password lama sama dgn inputan password baru
				if ($current_password == $new_password) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kata sandi baru tidak boleh sama dengan kata sandi saat ini!</div>');
					redirect('user/changepassword');
				} else {
					//jika beda maka bener /sudah oke
					// password sudah ok dan dan lebih dari 3 digit
					$password_hash = password_hash($new_password, PASSWORD_DEFAULT);

					$this->db->set('password', $password_hash);
					$this->db->where('email', $this->session->userdata('email'));
					$this->db->update('user');

					$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
															Data berhasil <strong>diperbaharui!</strong>
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															<span aria-hidden="true">&times;</span>
															</button>
														</div>');
					redirect('user/changepassword');
				}
			}
		}
	}

	// public function bank()
	// {
	// 	$data['title'] = 'Pengaturan Bank';
	// 	$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

	// 	$this->form_validation->set_rules('rekening', 'Account Number', 'required|trim');

	// 	if ($this->form_validation->run() == false) {
	// 		$this->load->view('templates/header', $data);
	// 		$this->load->view('templates/sidebar', $data);
	// 		$this->load->view('templates/topbar', $data);
	// 		$this->load->view('user/bank', $data);
	// 		$this->load->view('templates/footer');
	// 	} else {
	// 		$bank_id = $this->input->post('bank_id');
	// 		$rekening = $this->input->post('rekening');
	// 		$atasnama = $this->input->post('atasnama');
	// 		$cabang = $this->input->post('cabang');
	// 		$email = $this->input->post('email');


	// 		//cek jika ada gambar yang akan  diupload
	// 		$upload_image = $_FILES['fotobuku']['name'];

	// 		if ($upload_image) {
	// 			$config['upload_path'] = './assets/img/profile/';
	// 			$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// 			$config['max_size'] = '2048'; //ukuran gambar

	// 			$this->load->library('upload', $config);

	// 			if ($this->upload->do_upload('fotobuku')) {
	// 				//cek gambar lama ngambil dari variabel data
	// 				$old_image = $data['user']['fotobuku'];
	// 				//cek gambar defaul bukan
	// 				if ($old_image != 'default.jpg') {
	// 					//ganti gambar baru
	// 					unlink(FCPATH . 'assets/img/profile/' . $old_image);
	// 				}

	// 				// berisi nama file baru yg mau di masukan di baris ke 70. klu ada gambarnya maka bertambah
	// 				$new_image = $this->upload->data('file_name');
	// 				//klu ga ada gambar maka perintah ini tdk akan di set
	// 				$this->db->set('fotobuku', $new_image);
	// 			} else {
	// 				echo $this->upload->display_errors();
	// 			};
	// 		}

	// 		$this->db->set('bank_id', $bank_id);
	// 		$this->db->set('rekening', $rekening);
	// 		$this->db->set('atasnama', $atasnama);
	// 		$this->db->set('cabang', $cabang);
	// 		$this->db->where('email', $email);
	// 		$this->db->update('user');

	// 		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
	// 														Data berhasil <strong>diperbaharui!</strong>
	// 														<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	// 														<span aria-hidden="true">&times;</span>
	// 														</button>
	// 													</div>');
	// 		redirect('user/bank');
	// 	}
	// }

	// public function document()
	// {
	// 	$data['title'] = 'Pengaturan Document';
	// 	$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

	// 	$this->form_validation->set_rules('nik', 'No. NIK (KTP)', 'required|trim');

	// 	if ($this->form_validation->run() == false) {
	// 		$this->load->view('templates/header', $data);
	// 		$this->load->view('templates/sidebar', $data);
	// 		$this->load->view('templates/topbar', $data);
	// 		$this->load->view('user/document', $data);
	// 		$this->load->view('templates/footer');
	// 	} else {

	// 		$nik = $this->input->post('nik');
	// 		$email = $this->input->post('email');
	// 		$upload_foto = $_FILES['fotonik']['name'];
	// 		if ($upload_foto) {
	// 			$config['upload_path'] = './assets/img/fotonik/';
	// 			$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// 			$config['max_size'] = '2048'; //ukuran gambar
	// 			$this->load->library('upload', $config);
	// 			if ($this->upload->do_upload('fotonik')) {
	// 				$old_foto = $data['user']['fotonik'];
	// 				if ($old_foto != 'default.jpg') {
	// 					unlink(FCPATH . 'assets/img/fotonik/' . $old_foto);
	// 				}
	// 				$new_foto = $this->upload->data('file_name');
	// 				$this->db->set('fotonik', $new_foto);
	// 			} else {
	// 				echo $this->upload->display_errors();
	// 			};
	// 		}

	// 		$upload_image = $_FILES['fotoselfi']['name'];

	// 		if ($upload_image) {
	// 			$config['upload_path'] = './assets/img/fotoselfi/';
	// 			$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// 			$config['max_size'] = '2048'; //ukuran gambar
	// 			$this->load->library('upload', $config);
	// 			if ($this->upload->do_upload('fotoselfi')) {
	// 				$old_image = $data['user']['fotoselfi'];
	// 				if ($old_image != 'default.jpg') {
	// 					unlink(FCPATH . 'assets/img/fotoselfi/' . $old_image);
	// 				}
	// 				$new_image = $this->upload->data('file_name');
	// 				$this->db->set('fotoselfi', $new_image);
	// 			} else {
	// 				echo $this->upload->display_errors();
	// 			};
	// 		}
	// 		$this->db->set('nik', $nik);
	// 		$this->db->where('email', $email);
	// 		$this->db->update('user');
	// 		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
	// 															Data berhasil <strong>diperbaharui!</strong>
	// 															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	// 															<span aria-hidden="true">&times;</span>
	// 															</button>
	// 														</div>');
	// 		redirect('user/document');
	// 	}
	// }
}
