<?php

namespace SejoliSA\Admin;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Shipment {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Current product commission
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	null|array
	 */
	protected $current_commission = NULL;

	/**
	 * Shipping data
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	null|array
	 */
	protected $shipping_data = NULL;

	/**
	 * Shipping libraries data
	 * @since	1.2.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $libraries = array();

	/**
	 * Does order need shipment?
	 * @since 	1.0.0
	 * @var 	boolean
	 */
	protected $order_needs_shipment = false;

	/**
	 * List of used delivery couriers and services.
	 *
	 * @since 1.0.0
	 * @var array
	 */
	private $couriers = array(
		'domestic'      => array(
			'pos'     => array(
				'label'    => 'POS Indonesia',
				'website'  => 'http://www.posindonesia.co.id',
				'active'   => true,
				'services' => array(
					'Surat Kilat Khusus'	=> array(
						'title'	 => 'Surat Kilat Khusus',
						'active' => false
					),
					'Paketpos Biasa'		=> array(
						'title'	 => 'Paket Pos Biasa',
						'active' => true
					),
					'Paket Kilat Khusus'	=> array(
						'title'	 => 'Paket Kilat Khusus',
						'active' => true
					),
					'Express Sameday Dokumen'  => array(
						'title'	 => 'Express Sameday Dokumen',
						'active' => false
					),
					'Express Sameday Barang'   => array(
						'title'	 => 'Express Sameday Barang',
						'active' => true
					),
					'Express Next Day Dokumen' => array(
						'title'	 => 'Express Next Day Dokumen',
						'active' => false
					),
					'Express Next Day Barang'  => array(
						'title'	 => 'Express Next Day Barang',
						'active' => true
					),
					'Paketpos Dangerous Goods' => array(
						'title'  => 'Paketpos Dangerous Goods',
						'active' => false
					),
					'Paketpos Valuable Goods'  => array(
						'title'  => 'Paketpos Valuable Goods',
						'active' => false
					),
					'Kargopos Ritel Train'  => array(
						'title'  => 'Kargopos Ritel Train',
						'active' => false
					),
					'Kargopos Ritel Udara Dn'  => array(
						'title'  => 'Kargopos Ritel Udara Dn',
						'active' => false
					),
					'Paket Jumbo Ekonomi'  => array(
						'title'  => 'Paket Jumbo Ekonomi',
						'active' => false
					),
				)
			),
			'tiki'    => array(
				'label'    => 'TIKI',
				'website'  => 'http://tiki.id',
				'active'   => false,
				'services' => array(
					'TRC' => array(
						'title'  => 'Trucking Service',
						'active' => false
					),
					'REG' => array(
						'title'  => 'Regular Service',
						'active' => false
					),
					'ECO' => array(
						'title'	 => 'Economy Service',
						'active' => false
					),
					'ONS' => array(
						'title'  => 'Over Night Service',
						'active' => false
					),
					'SDS' => array(
						'title'  => 'Same Day Service',
						'active' => false
					),
					'HDS' => array(
						'title'	 => 'Holiday Service',
						'active' => false
					),
				),
			),
			'jne'     => array(
				'label'    => 'JNE',
				'website'  => 'http://www.jne.co.id',
				'active'   => true,
				'services' => array(
					'CTC'    => array(
						'title'	 => 'City Courier',
						'active' => true
					),
					'CTCYES' => array(
						'title'	 => 'City Courier YES',
						'active' => true
					),
					'OKE'    => array(
						'title'	 => 'Ongkos Kirim Ekonomis',
						'active' => true
					),
					'REG'    => array(
						'title'	 => 'Layanan Reguler',
						'active' => true
					),
					'YES'    => array(
						'title'  => 'Yakin Esok Sampai',
						'active' => true
					),
				)
			),
			'rpx'     => array(
				'label'    => 'RPX',
				'website'  => 'http://www.rpx.co.id',
				'active'   => false,
				'services' => array(
					'SDP' => array(
						'title'	 => 'Same Day Package',
						'active' => false
					),
					'MDP' => array(
						'title'	 => 'Mid Day Package',
						'active' => false
					),
					'NDP' => array(
						'title'	 => 'Next Day Package',
						'active' => false
					),
					'RGP' => array(
						'title'  => 'Regular Package',
						'active' => false
					),
					'PAS' => array(
						'title'	 => 'Paket Ambil Suka-Suka',
						'active' => false
					),
					'PSR' => array(
						'title'	 => 'PSR Reguler',
						'active' => false
					),
				)
			),
			'ninja'     => array(
				'label'    => 'Ninja Xpress',
				'website'  => 'https://www.ninjaxpress.co',
				'active'   => true,
				'services' => array(
					'STANDARD' => array(
						'title'	 => 'Standard Service',
						'active' => false
					),
				)
			),
			'pcp'     => array(
				'label'   => 'PCP Express',
				'website' => 'http://www.pcpexpress.com',
				'active'  => false,
				'services' => array(
					'TREX'  => array(
						'title'  => 'Titipan Regular Express',
						'active' => false
					),
					'JET'  => array(
						'title'  => 'Jaminan Esok Tiba',
						'active' => false
					),
					'HIT'  => array(
						'title'  => 'Hari Ini Tiba',
						'active' => false
					),
					'EXIS'  => array(
						'title'  => 'Express Ekonomi',
						'active' => false
					),
					'GODA'  => array(
						'title'  => 'Kargo Darat',
						'active' => false
					),
				)
			),
			'star'    => array(
				'label'    => 'Star Cargo',
				'website'  => 'http://www.starcargo.co.id',
				'active'   => false,
				'services' => array(
					'Express'    => array(
						'title'	 => 'Express',
						'active' => false
					),
					'Reguler'    => array(
						'title'	 => 'Regular',
						'active' => false
					),
					'Dokumen' => array(
						'title'	 => 'Dokumen',
						'active' => false
					),
					'MOTOR' => array(
						'title'	 => 'MOTOR',
						'active' => false
					),
					'MOTOR 150 - 250 CC' => array(
						'title'	 => 'MOTOR 150 - 250 CC',
						'active' => false
					),
				)
			),
			'sicepat' => array(
				'label'   => 'SiCepat Express',
				'website' => 'http://www.sicepat.com',
				'active'  => false,
				'services' => array(
					'REG'      => array(
						'title'	 => 'Layanan Regular',
						'active' => false
					),
					'BEST'     => array(
						'title'	 => 'Besok Sampai Tujuan',
						'active' => false
					),
					'Cargo' => array(
						'title'	 => 'Cargo',
						'active' => false
					),
				)
			),
			'jet'     => array(
				'label'    => 'JET Express',
				'website'  => 'http://www.jetexpress.co.id',
				'active'   => false,
				'services' => array(
					'CRG' => array(
						'title'	 => 'Cargo',
						'active' => false
					),
					'PRI' => array(
						'title'	 => 'Priority',
						'active' => false
					),
					'REG' => array(
						'title'	 => 'Regular',
						'active' => false
					),
					'XPS' => array(
						'title'	 => 'Express',
						'active' => false
					)
				),
			),
			'sap'     => array(
				'label'    => 'SAP Express',
				'website'  => 'http://sap-express.id',
				'active'   => false,
				'services' => array(
					'REG' => array(
						'title'	 => 'Regular Service',
						'active' => false
					),
					'SDS' => array(
						'title'	 => 'Same Day Service',
						'active' => false
					),
					'ODS' => array(
						'title'	 => 'One Day Service',
						'active' => false
					),
				)
			),
			'pahala'  => array(
				'label'    => 'Pahala Express',
				'website'  => 'http://www.pahalaexpress.co.id',
				'active'   => false,
				'services' => array(
					'EXPRESS' => array(
						'title'  => 'Express Service',
						'active' => false
					),
					'ONS'     => array(
						'title'  => 'One Night Service',
						'active' => false
					),
					'SDS'     => array(
						'title'  => 'Same Day Service',
						'active' => false
					),
					'SEPEDA'     => array(
						'title'  => 'Paket Sepeda',
						'active' => false
					),
					'MOTOR SPORT'     => array(
						'title'  => 'Paket Motor Sport',
						'active' => false
					),
					'MOTOR BESAR'     => array(
						'title'  => 'Paket Motor Besar',
						'active' => false
					),
					'MOTOR BEBEK'     => array(
						'title'  => 'Paket Motor Bebek',
						'active' => false
					),
				)
			),
			'slis'    => array(
				'label'    => 'Solusi Ekspres',
				'website'  => 'http://www.solusiekspres.com',
				'active'   => false,
				'services' => array(
					'REGULAR' => array(
						'title'	 => 'Regular Service',
						'active' => false
					),
					'EXPRESS' => array(
						'title'	 => 'Express Service',
						'active' => false
					),
				)
			),
			'jnt'     => array(
				'label'    => 'J&T Express',
				'website'  => 'http://www.jet.co.id',
				'active'   => false,
				'services' => array(
					'EZ' => array(
						'title'	 => 'Regular Service',
						'active' => false
					),
					'JSD' => array(
						'title'	 => 'Same Day Service',
						'active' => false
					),
				)
			),
			'ncs'     => array(
				'label'    => 'Nusantara Card Semesta',
				'website'  => 'http://www.ptncs.com',
				'active'   => false,
				'services' => array(
					'NRS' => array(
						'title'  => 'Regular Service',
						'active' => false
					),
					'ONS' => array(
						'title'  => 'Over Night Service',
						'active' => false
					),
					'SDS' => array(
						'title'  => 'Same Day Service',
						'active' => false
					)
				)
			),
			'lion'     => array(
				'label'    => 'Lion Parcel',
				'website'  => 'http://lionparcel.com',
				'active'   => false,
				'services' => array(
					'ONEPACK' => array(
						'title'  => 'One Day Service',
						'active' => false
					),
					'LANDPACK' => array(
						'title'  => 'Logistic Service',
						'active' => false
					),
					'REGPACK' => array(
						'title'  => 'Regular Service',
						'active' => false
					)
				)
			),
			'pandu'   => array(
				'label'    => 'Pandu Logistics',
				'website'  => 'http://www.pandulogistics.com',
				'active'   => false,
				'services' => array(
					'REG' => array(
						'title'	 => 'Regular Package',
						'active' => false
					),
				)
			),
			'wahana'  => array(
				'label'    => 'Wahana Express',
				'website'  => 'http://www.wahana.com',
				'active'   => false,
				'services' => array(
					'DES' => array(
						'title'	 => 'Normal Service',
						'active' => false
					),
				)
			),
			'indah'  => array(
				'label'    => 'Indah Logistic',
				'website'  => 'http://www.indahonline.com',
				'active'   => false,
				'services' => array(
					'DARAT' => array(
						'title'	 => 'Cargo Darat',
						'active' => false
					),
					'UDARA' => array(
						'title'	 => 'Cargo Udara',
						'active' => false
					),
				)
			),
			'rex'	=> array(
				'label'    => 'Royal Express Indonesia',
				'website'  => 'http://www.rex.co.id',
				'active'   => false,
				'services' => array(
					'EXP' => array(
						'title'	 => 'EXPRESS',
						'active' => false
					),
					'REX-1' => array(
						'title'	 => 'REX-1',
						'active' => false
					),
					'REX-5' => array(
						'title'	 => 'REX-5',
						'active' => false
					),
					'REX-10' => array(
						'title'	 => 'REX-10',
						'active' => false
					),
				)
			),
			'idl'	=> array(
				'label'    => 'Indotama Domestik Lestari',
				'website'  => 'http://www.idlcargo.co.id',
				'active'   => false,
				'services' => array(
					'iSDS' => array(
						'title'	 => 'SAME DAY SERVICES',
						'active' => false
					),
					'iONS' => array(
						'title'	 => 'OVERNIGHT SERVICES',
						'active' => false
					),
					'iSCF' => array(
						'title'	 => 'SPECIAL FLEET',
						'active' => false
					),
					'iREG' => array(
						'title'	 => 'REGULAR',
						'active' => false
					),
					'iCon' => array(
						'title'	 => 'EKONOMIS',
						'active' => false
					),
				),
			),
			'expedito'	=> array(
				'label'    => 'Expedito',
				'website'  => 'http://www.expedito.co.id',
				'active'   => false,
				'services' => array(
					'CityLink' => array(
						'title'	 => 'CityLink',
						'active' => false
					),
					'DPEX' => array(
						'title'	 => 'DPEX',
						'active' => false
					),
					'ARAMEX Indonesia' => array(
						'title'	 => 'ARAMEX Indonesia',
						'active' => false
					),
					'DHL JKT' => array(
						'title'	 => 'DHL JKT',
						'active' => false
					),
					'DHL Singapore' => array(
						'title'	 => 'DHL Singapore',
						'active' => false
					),
					'SF EXPRESS' => array(
						'title'	 => 'SF EXPRESS',
						'active' => false
					),
					'SkyNet WorldWide' => array(
						'title'	 => 'SkyNet Worldwide',
						'active' => false
					),
					'TNT | Fedex' => array(
						'title'	 => 'TNT | Fedex',
						'active' => false
					),
				)
			)
		),
		'international' => array(
			'jne'      => array(
				'label'    => 'JNE',
				'website'  => 'http://www.jne.co.id',
				'services' => array(
					'INTL',
				),
				'account'  => array(
					'pro',
				),
			)
		)
	);

	/**
	 * Get active courier and services;
	 * @since 	1.0.0
	 * @access 	private
	 * @var 	false|array
	 */
	private $available_couriers = false;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register shipment libraries
	 * Hooked via action plugins_loaded, priority 100
	 * @since 	1.2.0
	 * @return 	void
	 */
	public function register_libraries() {

		require_once( SEJOLISA_DIR . 'shipments/main.php');
		require_once( SEJOLISA_DIR . 'shipments/cod.php');

		$this->libraries['cod']	= new \SejoliSa\Shipment\Cod;
	}

	/**
	 * Add JS Vars for localization
	 * Hooked via sejoli/admin/js-localize-data, priority 10
	 * @since 	1.0.0
	 * @param 	array 	$js_vars 	Array of js vars
	 * @return 	array
	 */
	public function set_localize_js_var(array $js_vars) {

		$js_vars['order']['check_physical']	= add_query_arg([
													'ajaxurl' => add_query_arg([
														'action' => 'sejoli-order-check-if-physical'
													], admin_url('admin-ajax.php')),
													'nonce' => wp_create_nonce('sejoli-order-check-if-physical')
												]);

		$js_vars['get_subdistricts'] = [
			'ajaxurl' => add_query_arg([
					'action' => 'get-subdistricts'
				], admin_url('admin-ajax.php')
			),
			'placeholder' => __('Ketik minimal 3 karakter', 'sejoli')
		];

		return $js_vars;
	}

	/**
	 * Get subdistriction options for json
	 * Hooked via action wp_ajax_get-subdistricts
	 * @since  1.0.0
	 * @return json
	 */
	public function get_json_subdistrict_options() {

		$response = sejolisa_get_district_options( $_REQUEST['term'] );

		wp_send_json( $response );
	}

	/**
	 * Get subdistriction options
	 * Hooked via filter sejoli/shipment/subdistricts, priority 1
	 * @since  1.0.0
	 * @return array
	 */
	public function get_subdistrict_options($options = array()) {
		$options = [];

		ob_start();
		require SEJOLISA_DIR . 'json/subdistrict.json';
		$json_data = ob_get_contents();
		ob_end_clean();

		$subdistricts = json_decode($json_data, true);

		foreach($subdistricts as $data):
			$options[$data['subdistrict_id']] = $data['province'] . ' - ' . $data['type'].' '.$data['city'] . ' - ' . $data['subdistrict_name'];
		endforeach;

		asort($options);

		return $options;
	}

	/**
	 * Get subdistrict detail
	 * @since 	1.2.0
	 * @param  	integer $subdistrict_id 	District ID
	 * @return 	array 	District detail
	 */
	public function get_subdistrict_detail($subdistrict_id) {

		ob_start();
		require SEJOLISA_DIR . 'json/subdistrict.json';
		$json_data = ob_get_contents();
		ob_end_clean();

		$subdistricts        = json_decode($json_data, true);
        $key                 = array_search($subdistrict_id, array_column($subdistricts, 'subdistrict_id'));
        $current_subdistrict = $subdistricts[$key];

		return $current_subdistrict;
	}

	/**
	 * Delete shipping transient data everytime carbon fields - theme options saved
	 * Hooked via action carbon_fields_theme_options_container_saved, priority 10
	 * @since 	1.4.0
	 * @return 	void
	 */
	public function delete_cache_data() {
		delete_transient('sejolisa-shipment');
	}

	/**
	 * Setup shipment fields for general options
	 * Hooked via fi;ter sejoli/general/fields, priority 40
	 * @since 	1.0.0
	 * @param  	array  $fields 	Plugin option shipment fields in array
	 * @return 	array
	 */
	public function setup_shipping_fields(array $fields) {

		$shipping_fields = [];

		foreach($this->couriers['domestic'] as $key => $_courier) :

			$main_key = 'shipment_'. $key . '_active';

			$shipping_fields[] = Field::make('checkbox', $main_key, $_courier['label'])
									->set_default_value($_courier['active'])
									->set_help_text($_courier['website'])
									->set_classes('main-title');

			foreach($_courier['services'] as $service => $setting):

				$service_key = sanitize_title($service);
				$shipping_fields[] = Field::make('checkbox', 'shipment_' . $key . '_' . $service_key . '_active', $setting['title'])
										->set_default_value($setting['active'])
										->set_conditional_logic([
											[
												'field'	=> $main_key,
												'value'	=> true
											]
										]);
			endforeach;

		endforeach;

		$shipping_fields = apply_filters('sejoli/shipment/fields', $shipping_fields);

		$fields[] = [
			'title'  => __('Pengiriman', 'sejoli'),
			'fields' => $shipping_fields
		];

		return $fields;
	}

    /**
	 * Setup shipment fields for product
	 * Hooked via filter sejoli/product/fields, priority 30
	 * @since  1.0.0	Initialization
	 * @since  1.2.0 	Add ability to modify product shipment fields
	 * @param  array  	$fields
	 * @return array
	 */
	public function setup_setting_fields(array $fields) {

		$currency = 'Rp. '; // later will be using hook filter;

        $conditionals = [
            'physical'  => [
                [
                    'field' => 'product_type',
                    'value' => 'physical'
                ],[
                    'field' => 'shipment_active',
                    'value' => true
                ]
            ]
        ];

		$fields['shipping'] = [
			'title'	=> __('Pengiriman', 'sejoli'),
			'fields' =>  [
				Field::make( 'separator', 'sep_shipment' , __('Pengaturan Pengiriman', 'sejoli'))
					->set_classes('sejoli-with-help')
					->set_help_text('<a href="' . sejolisa_get_admin_help('shipping') . '" class="thickbox sejoli-help">Tutorial <span class="dashicons dashicons-video-alt2"></span></a>'),

                Field::make('html',     'html_info_shipment')
                    ->set_html('<div class="sejoli-html-message info"><p>'. __('Pengaturan ini hanya <strong>BERLAKU</strong> jika tipe produk adalah produk fisik', 'sejoli') . '</p></div>'),

                Field::make('checkbox', 'shipment_active', __('Aktifkan pengaturan pengiriman'))
                    ->set_option_value('yes')
                    ->set_default_value(false)
                    ->set_conditional_logic([
                        [
                            'field' => 'product_type',
                            'value' => 'physical'
                        ]
                    ]),

                Field::make('text', 'shipment_weight', __('Berat barang (dalam gram)', 'sejoli'))
                    ->set_attribute('type', 'number')
                    ->set_attribute('min', 100)
                    ->set_required(true)
                    ->set_conditional_logic($conditionals['physical']),

				// Field::make('html', 'html_info_shipment_select')
				// 	->set_html("<select name=carbon_fields_compact_input[_shipment_origin]></select>")
				// 	->set_conditional_logic($conditionals['physical'])
				//
				Field::make('select', 'shipment_origin', __('Awal pengiriman', 'sejoli'))
	                ->set_options(array($this, 'get_subdistrict_options'))
	                ->set_required(true)
	                ->set_help_text(__('Ketik nama kecamatan untuk pengiriman', 'sejoli')),
            ]
        ];

        return $fields;
    }

	/**
	 * Add shipping data to product meta
	 * Hooked via filter sejoli/product/meta-data, priority 100
	 * @since 	1.0.0
	 * @param  	WP_Post 	$product
	 * @param  	int     	$product_id
	 * @return 	WP_Post
	 */
	public function setup_product_meta(\WP_Post $product, int $product_id) {

		$product->shipping = [
			'active' => boolval(carbon_get_post_meta($product_id, 'shipment_active')),
			'weight' => intval(carbon_get_post_meta($product_id, 'shipment_weight')),
			'origin' => intval(carbon_get_post_meta($product_id, 'shipment_origin'))
		];

		return $product;
	}

	/**
	 * Set current order needs shipment
	 * Hooked via action sejoli/order/need-shipment, priority 1
	 * @since 1.1.1
	 * @param boolean $need_shipment [description]
	 */
	public function set_order_needs_shipment($need_shipment = false) {
		$this->order_needs_shipment = $need_shipment;
	}

	/**
	 * Validate shipping
	 * Hooked via filter sejoli/checkout/is-shipping-valid, priority 1
	 * @since  1.0.0
	 * @param  bool    	$valid
	 * @param  WP_Post 	$product
	 * @param  array   	$post_data
	 * @param  bool  	$is_calculate	Check if current request is to calculate only or to checkout
	 * @return bool
	 */
	public function validate_shipping_when_checkout(bool $valid, \WP_Post $product, array $post_data, $is_calculate = false) {

		if('digital' === $product->type && !$this->order_needs_shipment) :
			return $valid;
		endif;

		/**
		 * Check courier data
		 */
		if(isset($post_data['shipment']) && !empty($post_data['shipment']) && 'undefined' !== $post_data['shipment']) :

			list($courier,$service,$cost)	= explode(':::', $post_data['shipment']);

			$this->shipping_data = [
				'courier'     => $courier,
				'service'     => $service,
				'cost'        => floatval($cost),
				'district_id' => intval($post_data['district_id'])
			];
		else :
			$valid = false;
			sejolisa_set_message( __('Kurir belum dipilih', 'sejoli') );
		endif;

		if(false === $is_calculate) :

			if(!empty($post_data['user_name'])) :
				if(is_array($this->shipping_data)) :
					$this->shipping_data['receiver'] = sanitize_text_field($post_data['user_name']);
				endif;
			else :
				$valid = false;
				sejolisa_set_message( __('Nama penerima belum diisi', 'sejoli'));
			endif;

			if(!empty($post_data['user_phone'])) :
				if(is_array($this->shipping_data)) :
					$this->shipping_data['phone'] = sanitize_text_field($post_data['user_phone']);
				endif;
			else :
				$valid = false;
				sejolisa_set_message( __('Nomor telpon penerima belum diisi', 'sejoli'));
			endif;

			/**
			 * Check address
			 */
			if(isset($post_data['address']) && !empty($post_data['address'])) :
				if(is_array($this->shipping_data)) :
					$this->shipping_data['address'] = sanitize_textarea_field($post_data['address']);
				endif;
			endif;

		endif;

		return $valid;
	}

	/**
	 * Get available couriers
	 * Hooked via filter sejoli/shipment/available-couriers, priority 100
	 * @since 	1.0.0
	 * @param  	array 	$available_couriers
	 * @return 	array
	 */
	public function get_available_couriers($available_couriers = []) {

		if(false === $this->available_couriers) :

			foreach($this->couriers['domestic'] as $key => $_courier) :

				$main_key = 'shipment_'. $key . '_active';
				$active = boolval(carbon_get_theme_option($main_key));

				if(false !== $active) :

					foreach($_courier['services'] as $service => $active) :

						$service_key = sanitize_title($service);
						$service_key = 'shipment_' . $key . '_' . $service_key . '_active';
						$active      = boolval(carbon_get_theme_option($service_key));

						if(false !== $active) :

							if(!isset($available_couriers[$key])) :
								$available_couriers[$key] = array();
							endif;

							$available_couriers[$key][] = $service;

						endif;

					endforeach;
				endif;

			endforeach;

			$this->available_couriers = $available_couriers;

		endif;

		return $this->available_couriers;
	}

	/**
	 * Get available courier services
	 * Hooked via filter sejoli/shipment/available-courier-services, priority 100
	 * @since 	1.0.0
	 * @param  	array  $services
	 * @return 	array
	 */
	public function get_available_courier_services($services = array()) {
		$available_couriers = $this->get_available_couriers();

		if(
			false !== $available_couriers &&
			is_array($available_couriers) &&
			0 < count($available_couriers)
		) :

			foreach($available_couriers as $courier => $_services) :
				foreach($_services as $_service) :
					$services[] = $_service;
				endforeach;
			endforeach;

		endif;

		return $services;
	}

	/**
	 * Calculate shipment cost
	 * Hooked via action sejoli/shipment/calculation, priority 100
	 * @param  	array  $post_data
	 * @return 	void
	 */
	public function calculate_shipment_cost(array $post_data) {

		$available_couriers = apply_filters('sejoli/shipment/available-couriers', []);

		if(
			false !== $available_couriers &&
			is_array($available_couriers) &&
			0 < count($available_couriers)
		) :
			$couriers       = implode(':', array_keys($available_couriers));
			$product        = sejolisa_get_product($post_data['product_id']);
			$product_weight = intval($product->shipping['weight']);


			$response       = sejolisa_get_shipment_cost([
		        'destination_id' => $post_data['district_id'],
		        'origin_id'      => $product->shipping['origin'],
		        'weight'         => apply_filters('sejoli/product/weight', $product_weight, $post_data ),
		        'courier'        => $couriers,
		        'quantity'       => $post_data['quantity']
		    ]);

			$response['shipment'] = apply_filters('sejoli/shipment/options', $response['shipment'], $post_data);

			sejolisa_set_respond($response, 'shipment');
		else :
			sejolisa_set_respond($response, 'shipment');
		endif;


	}

	/**
	 * Set shipment data to order meta,
	 * Hooked via filter sejoli/order/meta-data, priority 100
	 * @since 	1.0.0
	 * @param 	array 	$meta_data
	 * @param 	array  	$order_data
	 * @return  array
	 */
	public function set_order_meta($meta_data = [], array $order_data) {

		if(false !== $this->shipping_data && is_array($this->shipping_data)) :
			$meta_data['need_shipment'] = true;
			$meta_data['shipping_data'] = $this->shipping_data;
			$meta_data['free_shipping']	= apply_filters('sejoli/order/is-free-shipping', false);
		endif;

		return $meta_data;
	}

	/**
	 * Add shipping cost to grand total
	 * Hooked via filter sejoli/order/grand-total, priority 300
	 * @since  	1.0.0
	 * @param 	float 	$grand_total
	 * @param 	array 	$post_data
	 * @return 	float
	 */
	public function add_shipping_cost(float $grand_total, array $post_data) {

		if(false !== $this->shipping_data && is_array($this->shipping_data) && isset($this->shipping_data['cost'])) :
			$grand_total += $this->shipping_data['cost'];
		endif;

		return $grand_total;
	}

	/**
     * Set shipment value to cart
     * Hooked via filter sejoli/order/cart-detail, 10
     * @since 1.0.0
     * @param array $cart_detail
     * @param array $order_data
     * @return array $cart_detail
     */
    public function set_cart_detail(array $cart_detail, array $order_data) {

		if(false !== $this->shipping_data && is_array($this->shipping_data)) :
			$cart_detail['shipment_fee'] = $this->shipping_data['cost'];

		endif;

        return $cart_detail;
    }

	/**
	 * Reduce grand total with shipment if there is any shipping data in order meta
	 * Hooked via filter sejoli/commission/order-grand-total, priority 1
	 * @param  float  $grand_total
	 * @param  array  $order_data
	 * @return float
	 */
	public function reduce_with_shipping_cost(float $grand_total, array $order_data) {

		if(isset($order_data['meta_data']['shipping_data'])) :
			$grand_total -= floatval($order_data['meta_data']['shipping_data']['cost']);
		endif;

		return $grand_total;
	}

	/**
	 * Translate order meta shipping data for order detail
	 * Hooked via sejoli/order/detail priority 100
	 * @since 	1.0.0
	 * @param 	array $order_data
	 * @return 	array
	 */
	public function add_shipping_info_in_order_data(array $order_data) {

		if(isset($order_data['meta_data']['need_shipment']) && true === boolval($order_data['meta_data']['need_shipment'])) :

			$buyer = sejolisa_get_user($order_data['user_id']);

			$order_data['meta_data']['shipping_data'] = wp_parse_args($order_data['meta_data']['shipping_data'],[
				'courier'	=> NULL,
				'address' 	=> NULL,
				'receiver'	=> $buyer->display_name,
				'phone'		=> $buyer->meta->phone
			]);

			$shipping = $order_data['meta_data']['shipping_data'];

			ob_start();
			printf( __('%s %s, ongkos %s', 'sejoli'), $shipping['courier'], $shipping['service'], sejolisa_price_format($shipping['cost']) );
			$content = ob_get_contents();
			ob_end_clean();

			$order_data['courier'] = $content;
			$order_data['address'] = $shipping['address'];
			$district              = (isset($shipping['district_id'])) ? $this->get_subdistrict_detail($shipping['district_id']) : NULL;

			$content = '';
			ob_start();

			if(!empty($district)) :
	            echo "<br />"; printf( __('Kecamatan %s. ', 'sejoli'), $district['subdistrict_name'] );
				printf( '%s %s. ', $district['type'], $district['city'] );
	            printf( __('Provinsi %s', 'sejoli'), $district['province'] );
			endif;

			$content = ob_get_contents();
			ob_end_clean();

			$order_data['address'] .= $content;




		endif;

		return $order_data;
	}

	/**
	 * Display shipping info
	 * Hooked via sejoli/notification/content/order-meta
	 * @param  string $content      	[description]
	 * @param  string $media        	[description]
	 * @param  string $recipient_type   [description]
	 * @param  array  $invoice_data 	[description]
	 * @return string
	 */
	public function add_shipping_info_in_notification(string $content, string $media, $recipient_type, array $invoice_data) {

		if(
			in_array($recipient_type, ['buyer', 'admin']) &&
			isset($invoice_data['order_data']['meta_data']['need_shipment']) &&
			true === boolval($invoice_data['order_data']['meta_data']['need_shipment'])
		) :
			$shipping  = $invoice_data['order_data']['meta_data']['shipping_data'];
			$meta_data = $invoice_data['order_data']['meta_data'];
			$district  = (isset($shipping['district_id'])) ? $this->get_subdistrict_detail($shipping['district_id']) : NULL;

			$content .= sejoli_get_notification_content(
							'shipment',
							$media,
							array(
								'shipping'  => $shipping,
								'district'	=> $district,
								'meta_data' => $meta_data
							)
						);
		endif;

		return $content;
	}
}
