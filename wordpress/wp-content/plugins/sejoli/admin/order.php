<?php

namespace SejoliSA\Admin;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Order {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * All available order statuses
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array 	   $status 		Order status
	 */
	protected $status = [];

	/**
	* Current order user_id
	* @since 	1.0.0
	* @access 	protected
	* @var		null|integer
	*/
   protected $user_id = NULL;

	 /**
 	 * Current order coupon_id
 	 * @since 	1.0.0
 	 * @access 	protected
 	 * @var		null|integer
 	 */
	protected $coupon_id = NULL;

	/**
	 * Current order affiliate_id
	 * @since 	1.0.0
	 * @access 	protected
	 * @var		null|integer
	 */
	protected $affiliate_id = NULL;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version     = $version;
		$this->status      = [
            'on-hold'     		=> __('Menunggu pembayaran', 'sejoli'),
			'payment-confirm' 	=> __('Pembayaran dikonfirmasi', 'sejoli'),
            'in-progress' 		=> __('Pesanan diproses', 'sejoli'),
            'shipping'    		=> __('Proses pengiriman', 'sejoli'),
            'completed'   		=> __('Selesai', 'sejoli'),
			'refunded'    		=> __('Refund', 'sejoli'),
			'cancelled'   		=> __('Batal', 'sejoli')
        ];
	}

	/**
	 * Register cron jobs
	 * Hooked via action admin_init, priority 100
	 * @since 	1.4.1
	 * @return 	void
	 */
	public function register_cron_jobs() {

		// delete coupon post
		if(false === wp_next_scheduled('sejoli/order/cancel-incomplete-order')) :

			wp_schedule_event(time(), 'quarterdaily', 'sejoli/order/cancel-incomplete-order');

		else :

			$recurring 	= wp_get_schedule('sejoli/order/cancel-incomplete-order');

			if('quarterdaily' !== $recurring) :
				wp_reschedule_event(time(), 'quarterdaily', 'sejoli/order/cancel-incomplete-order');
			endif;

		endif;

	}

	/**
	 * Add JS Vars for localization
	 * Hooked via sejoli/admin/js-localize-data, priority 1
	 * @since 	1.0.0
	 * @param 	array 	$js_vars 	Array of js vars
	 * @return 	array
	 */
	public function set_localize_js_var(array $js_vars) {
		$js_vars['order'] = [
			'table' => [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-table'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-render-order-table')
			],
			'chart' => [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-chart'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-render-order-chart')
			],
			'update' => [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-update'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-order-update')
			],
			'shipping' => [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-shipping'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-order-shipping')
			],
			'input_resi' => [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-input-resi'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-order-input-resi')
			],
			'detail' => [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-detail'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-order-detail')
			],
			'export_prepare'   =>  [
				'ajaxurl' => add_query_arg([
					'action' => 'sejoli-order-export-prepare'
				], admin_url('admin-ajax.php')),
				'nonce' => wp_create_nonce('sejoli-order-export-prepare')
			],
			'status'   => apply_filters('sejoli/order/status', [])
		];
		return $js_vars;
	}

	/**
	 * Get available order status
	 * Hooked via filter sejoli/order/status, priority 1
	 * @since  1.0.0
	 * @param  array  $status
	 * @return array
	 */
	public function get_status($status = []) {
		return $this->status;
	}

	/**
	 * Calculate order total
	 * @param  float  $total
	 * @param  array  $order_data
	 * @return float
	 */
	public function calculate_total($total = 0.0, array $order_data) {
		$product_id    = $order_data['product_id'];
		$product_price = apply_filters('sejoli/product/price', 0, get_post($product_id));
		$quantity      = absint($order_data['quantity']);
		$quantity      = (0 < $quantity) ? $quantity : 1;

		return floatval($product_price * $quantity);
	}

	/**
	 * Set order user_id
	 * Hooked via action sejoli/order/set-user, priority 999
	 * @since 	1.0.0
	 * @param 	$user_id
	 */
	public function set_user($user_id) {
		$this->user_id = intval($user_id);
	}

	/**
	 * Set order coupon_id
	 * Hooked via action sejoli/order/set-coupon, priority 999
	 * @since 	1.0.0
	 * @param 	$user_id
	 */
	public function set_coupon($coupon_id) {
		$this->coupon_id = intval($coupon_id);
	}

	/**
	 * Set order affiliate id
	 * Hooked via action sejoli/order/set-affiliate, priority 999
	 * @since 	1.0.0
	 * @return 	void
	 */
	public function set_affiliate() {
		$affiliate_data         = sejolisa_get_affiliate_checkout();
		$is_affiliate_permanent = boolval(carbon_get_theme_option('sejoli_permanent_affiliate'));

		if(!empty($affiliate_data['user_meta']) && false !== $is_affiliate_permanent) :
			$this->affiliate_id = $affiliate_data['user_meta'];
		elseif(!empty($affiliate_data['coupon'])) :
			$this->affiliate_id = $affiliate_data['coupon'];
		elseif(!empty($affiliate_data['link'])) :
			$this->affiliate_id = $affiliate_data['link'];
		endif;

		// Same user and affiliate ID value
		if(
			is_user_logged_in() &&
			get_current_user_id() === intval($this->affiliate_id)) :

			$this->affiliate_id = 0;
		endif;
	}

    /**
     * Order created
     * Hooked via action sejoli/order/create, priority 999
     * @since  1.0.0
     * @param  array $order_data
     * @return void
     */
    public function create(array $order_data) {

		$order_data = wp_parse_args($order_data, [
            'product_id'      => NULL,
            'user_id'         => $this->user_id,
            'affiliate_id'    => $this->affiliate_id,
            'coupon_id'       => $this->coupon_id,
			'coupon'		  => NULL,
            'quantity'        => 1,
            'status'          => 'on-hold',
            'payment_gateway' => 'manual',
			'type'			  => 'regular',
			'meta_data'		  => []
        ]);

		// Order type checking must be first before set grand total
		$order_data['type']	= apply_filters('sejoli/order/type', 'regular', $order_data);

		// calculate grand total
		if(!isset($order_data['grand_total'])) :
			$order_data['grand_total'] = apply_filters('sejoli/order/grand-total', 0, $order_data);
		endif;

		// if grand total is 0, then status will be in-progress or completed

		if(0.0 === floatval($order_data['grand_total'])) :
			$product = sejolisa_get_product($order_data['product_id']);

			// if product is need of shipment
			if(false !== $product->shipping['active']) :
				$order_data['status'] = 'in-progress';
			else :
				$order_data['status'] = 'completed';
			endif;
		endif;

		//set meta data value
		$order_data['meta_data'] = apply_filters('sejoli/order/meta-data', $order_data['meta_data'], $order_data);

		$respond = sejolisa_create_order($order_data);

		$respond['messages']['info']    = sejolisa_get_messages('info');
		$respond['messages']['warning'] = sejolisa_get_messages('warning');

		sejolisa_set_respond($respond, 'order');

		if(false !== $respond['valid']) :

			do_action('sejoli/log/write', 'order created', $order_data);

			if(!empty($order_data['coupon'])) :
				do_action('sejoli/coupon/update-usage', $order_data['coupon']);
			endif;

			$order_data = $respond['order'];

			do_action('sejoli/order/new', $order_data);
			do_action('sejoli/order/set-status/'.$order_data['status'], $order_data);

		endif;
    }

	/**
	 * Create renew order
	 * Hooked via action sejoli/order/renew, priority 999
	 * @since 	1.0.0
	 * @param  	array  $order_data
	 * @return 	void
	 */
	public function renew(array $order_data) {
		$order_data = wp_parse_args($order_data, [
			'order_parent_id' => NULL,
            'product_id'      => NULL,
            'user_id'         => get_current_user_id(),
            'affiliate_id'    => $this->affiliate_id,
            'coupon_id'       => $this->coupon_id,
            'quantity'        => 1,
            'status'          => 'on-hold',
            'payment_gateway' => 'manual',
			'type'			  => 'subscription-regular',
            'total'           => NULL,
			'meta_data'		  => []
        ]);

		// Order type checking must be first before set grand total
		$order_data['type']	= apply_filters('sejoli/order/type', 'regular', $order_data);

		// calculate grand total
		if(!isset($order_data['grand_total'])) :
			$order_data['grand_total'] = apply_filters('sejoli/order/grand-total', 0, $order_data);
		endif;

		// if grand total is 0, then status will be in-progress or completed
		if(0 === $order_data['grand_total']) :
			$order_data['status'] = 'completed';
		endif;

		//set meta data value
		$order_data['meta_data'] = apply_filters('sejoli/order/meta-data', $order_data['meta_data'], $order_data);

		$respond = sejolisa_create_order($order_data);
		$respond['messages']['info']    = sejolisa_get_messages('info');
		$respond['messages']['warning'] = sejolisa_get_messages('warning');

		sejolisa_set_respond($respond, 'order');

		if(false !== $respond['valid']) :

			$order_data = $respond['order'];
			do_action('sejoli/order/new', 		 $order_data);
			do_action('sejoli/order/set-status/'.$order_data['status'], $order_data);

		endif;
	}

    /**
     * Update status order
     * Hooked via action sejoli/order/update-status, priorirty
     * @since  1.0.0
     * @param  array  $args
     * @return void
     */
    public function update_status(array $args) {

		$args = wp_parse_args($args, [
	        'ID'       => NULL,
	        'status'   => NULL
	    ]);

	    $respond = sejolisa_get_order([
	        'ID' => $args['ID']
	    ]);

		if(false !== $respond['valid'] && isset($respond['orders']) && isset($respond['orders']['ID'])) :

			$order       = $respond['orders'];
			$prev_status = $order['status'];
			$new_status  = $args['status'];

			if($prev_status === $new_status) :
				sejolisa_set_respond([
						'valid' => false,
						'order' => $order,
						'messages' => [
							'error' => [
								sprintf(__('Can\'t update since current order status and given status are same. The status is %s', 'sejoli'), $new_status)
							]
						]
					],
					'order'
				);
				return;
			endif;

			// We need this hook later to validate if we can allow moving status to another
			// For example, we prevent moving order with status completed to on-hold
			$allow_update_status = apply_filters('sejoli/order/allow-update-status',
				true,
				[
					'prev_status' => $prev_status,
					'new_status'  => $new_status
				],
				$order);

			// is allowed
			if(true === $allow_update_status) :

				do_action('sejoli/order/update-status-from/'. sanitize_title($order['status']), $new_status, $order);

				$respond = sejolisa_update_order_status($args);

				if(false !== $respond['valid']) :
					$order['status'] = $new_status;
					do_action('sejoli/order/status-updated', 	$order);
					do_action('sejoli/order/set-status/'.sanitize_title($new_status), $order);
				endif;

				$respond['messages']['success'][0] = sprintf(__('Order ID #%s updated from %s to %s', 'sejoli'), $order['ID'], $prev_status, $new_status);

				sejolisa_set_respond($respond, 'order');

			else :
				sejolisa_set_respond([
						'valid' => false,
						'order' => $order,
						'messages' => [
							'error' => [
								sprintf(__('Updating order status from %s to %s is not allowed', 'sejoli'), $prev_status, $new_status)
							]
						]
					],
					'order'
				);
			endif;
		else :
			sejolisa_set_respond($respond, 'order');
		endif;
    }

    /**
     * Delete order
     * Hooked via action sejoli/order/delete
     * @since  1.0.0
     * @param  int|Sejoli_Order $order
     * @return
     */
    public function delete($order) {

    }

	/**
	 * Update order status by ajax
	 * Hooked via action wp_ajax_sejoli-order-update, priority 1
	 * @return json
	 */
	public function update_status_by_ajax() {

		$response = [];

		if(wp_verify_nonce($_POST['nonce'], 'sejoli-order-update')) :

			$post = wp_parse_args($_POST,[
				'orders' => NULL,
				'status' => 'on-hold'
			]);

			if(is_array($post['orders']) && 0 < count($post['orders'])) :

				if(!in_array($post['status'], ['delete', 'resend'])) :

					foreach($post['orders'] as $order_id) :
						do_action('sejoli/order/update-status', [
							'ID'     => $order_id,
							'status' => $post['status']
						]);

						$response[] = sprintf( __('Order %s updated to %s', 'sejoli'), $order_id, $post['status']);
					endforeach;

				elseif('resend' === $post['status'] ) :

					foreach($post['orders'] as $order_id) :

						$get_response = sejolisa_get_order([ 'ID' => $order_id]);

						if(false !== $get_response['valid']) :

							$order = $get_response['orders'];

							do_action('sejoli/notification/order/' . $order['status'], $order);

							$response[] = sprintf( __('Order %s resent notification %s', 'sejoli'), $order_id, $order['status']);

						endif;

					endforeach;

				else :
					// delete
				endif;
			endif;
		endif;

		wp_send_json($response);
		exit;
	}

	/**
     * Register admin menu under sejoli main menu
     * Hooked via action admin_menu, priority 999
     * @since 1.0.0
     * @return void
     */
    public function register_admin_menu() {

        add_submenu_page( 'crb_carbon_fields_container_sejoli.php', __('Penjualan', 'sejoli'), __('Penjualan', 'sejoli'), 'manage_sejoli_orders', 'sejoli-orders', [$this, 'display_order_page']);

    }

    /**
     * Display order page
     * @since 1.0.0
     */
    public function display_order_page() {
        require plugin_dir_path( __FILE__ ) . 'partials/order/page.php';
    }

	/**
	 * Export order data to CSV
	 * Hooked via action sejoli_ajax_sejoli-order-export, priority 1
	 * @since 	1.1.0
	 * @return 	void
	 */
	public function export_csv() {

		$post_data = wp_parse_args($_GET,[
			'sejoli-nonce' => NULL,
			'backend'      => false
		]);

		if(wp_verify_nonce($post_data['sejoli-nonce'], 'sejoli-order-export')) :

			$filename = 'export-orders';

			if(!current_user_can('manage_sejoli_orders') || false === $post_data['backend']) :
				$post_data['affiliate_id']	= get_current_user_id();
			endif;

			if(isset($post_data['affiliate_id'])) :
				$filename .= '-'. $post_data['affiliate_id'];
			endif;

			unset($post_data['backend'], $post_data['sejoli-nonce']);

			$response   = sejolisa_get_orders($post_data);

			$csv_data = [];
			$csv_data[0]	= array(
				'INV', 'product', 'created_at', 'name', 'email', 'phone', 'price', 'status', 'affiliate', 'affiliate_id'
			);

			$i = 1;
			foreach($response['orders'] as $order) :

				$csv_data[$i] = array(
					$order->ID,
					$order->product->post_title,
					$order->created_at,
					$order->user_name,
					$order->user_email,
					get_user_meta($order->user_id, '_phone', true),
					$order->grand_total,
					$order->status,
					$order->affiliate_id,
					$order->affiliate_name,
				);
				$i++;
			endforeach;

			header('Content-Type: text/csv');
			header('Content-Disposition: attachment; filename="' . $filename . '.csv"');

			$fp = fopen('php://output', 'wb');
			foreach ($csv_data as $line) :
			    fputcsv($fp, $line, ',');
			endforeach;
			fclose($fp);

		endif;
		exit;
	}

	/**
	 * Cancel incomplete order
	 * Hooked via action sejoli/order/cancel-incomplete-order, priority 100
	 * @since 	1.2.3
	 * @return 	void
	 */
	public function cancel_incomplete_order() {

		$day = intval(carbon_get_theme_option('sejoli_autodelete_incomplete_order'));

		if(0 < $day) :

			$response = \SejoliSA\Model\Order::reset()
		                ->set_filter('status', 'on-hold')
						->set_filter('created_at', date('Y-m-d 00:00:00', strtotime('-' . $day.' day')), '<=')
						->set_data_length(20)
						->set_data_order('created_at', 'ASC')
		                ->get()
						->respond();

			if(false !== $response['valid'] && 0 < count($response['orders'])) :

				set_time_limit(0);

				foreach($response['orders'] as $order) :

					do_action('sejoli/order/update-status', [
						'ID'          => $order->ID,
						'status'      => 'cancelled'
					]);

				endforeach;

				do_action(
					'sejoli/log/write',
					'autocancel-order',
					sprintf(
						__('Cancel %s orders', 'sejoli'),
						count($response['orders'])
					)
				);

			endif;

		endif;

		exit;
	}
}
