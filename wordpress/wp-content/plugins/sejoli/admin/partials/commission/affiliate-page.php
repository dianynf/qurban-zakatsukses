<?php
    $date = date('Y-m-d',strtotime('-30day')) . ' - ' . date('Y-m-d');
    $export_link = add_query_arg(array(
                        'sejoli-nonce'  => wp_create_nonce('sejoli-affiliate-commission-export'),
                        'action'        => 'sejoli-affiliate-commission-detail'
                    ),admin_url('admin-ajax.php'));
?>
<div class="wrap">
    <h1 class="wp-heading-inline">
        <?php _e('Data Komisi per Affiliasi', 'sejoli'); ?>
	</h1>

    <div class="ui three stackable cards sejoli-full-widget information blocked">

        <div class="ui card orange orange">
            <div class="content">
                <div class="header"><?php _e('Komisi potensial', 'sejoli'); ?></div>
            </div>
            <div class="content value">Rp. 0</div>
        </div>

        <div class="ui card green green">
            <div class="content">
                <div class="header"><?php _e('Komisi belum dibayar', 'sejoli'); ?></div>
            </div>
            <div class="content value">Rp. 0</div>
        </div>

        <div class="ui card blue blue">
            <div class="content">
                <div class="header"><?php _e('Komisi sudah dibayar', 'sejoli'); ?></div>
            </div>
            <div class="content value"> Rp. 0</div>
        </div>

    </div>
    <br />

    <div class="sejoli-form-filter box" style='float:right;'>
        <a href='<?php echo $export_link; ?>' name="button" class='export-csv button'><?php _e('Export CSV', 'sejoli'); ?></a>
    </div>
    <div class="sejoli-table-wrapper">
        <div class="sejoli-table-holder">
            <table id="sejoli-commission" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th><?php _e('Affiliasi',               'sejoli'); ?></th>
                        <th><?php _e('Komisi potensial',        'sejoli'); ?></th>
                        <th><?php _e('Komisi belum dibayar',    'sejoli'); ?></th>
                        <th><?php _e('Komisi sudah dibayar',    'sejoli'); ?></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                    <tr>
                        <th><?php _e('Affiliasi',               'sejoli'); ?></th>
                        <th><?php _e('Komisi potensial',        'sejoli'); ?></th>
                        <th><?php _e('Komisi belum dibayar',    'sejoli'); ?></th>
                        <th><?php _e('Komisi sudah dibayar',    'sejoli'); ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">

let sejoli_table;

(function( $ ) {
	'use strict';
    $(document).ready(function() {

        sejoli_table = $('#sejoli-commission').DataTable({
            language: dataTableTranslation,
            searching: false,
            processing: false,
            serverSide: true,
            ajax: {
                type: 'POST',
                url: sejoli_admin.affiliate_commission.table.ajaxurl,
                data: function(data) {
                    data.action   = 'sejoli-affiliate-commission-table';
                    data.security = sejoli_admin.affiliate_commission.table.nonce
                    data.backend  = true;
                }
            },
            pageLength : -1,
            lengthMenu : [
                [-1],
                [dataTableTranslation.all],
            ],
            order: [
                [ 0, "desc" ]
            ],
            columnDefs: [
                {
                    targets: [1, 2, 3],
                    orderable: false
                },{
                    targets: 0,
                    data : 'ID',
                    render : function(data, type, full) {
                        let tmpl = $.templates('#order-detail'),
                            subsctype = null;

                        return tmpl.render({
                            id : full.ID,
                            name : full.display_name
                        })
                    }
                },{
                    targets: 1,
                    width: '20%',
                    data: 'pending_commission',
                    className : 'price',
                    render : function(data ,type, full) {
                        return sejoli_admin.text.currency + sejoli.helper.formatPrice(data);
                    }
                },{
                    targets: 2,
                    width: '20%',
                    data : 'unpaid_commission',
                    className : 'price',
                    render : function(data ,type, full) {
                        return sejoli_admin.text.currency + sejoli.helper.formatPrice(data);
                    }
                },{
                    targets: 3,
                    width: '20%',
                    data : 'paid_commission',
                    className : 'price',
                    render : function(data ,type, full) {
                        return sejoli_admin.text.currency + sejoli.helper.formatPrice(data);
                    }
                }
            ],
            initComplete: function(settings, json) {
                $('.sejoli-full-widget .orange .content.value').html(sejoli_admin.text.currency + sejoli.helper.formatPrice(json.info.pending_commission));
                $('.sejoli-full-widget .green .content.value').html(sejoli_admin.text.currency + sejoli.helper.formatPrice(json.info.unpaid_commission));
                $('.sejoli-full-widget .blue .content.value').html(sejoli_admin.text.currency + sejoli.helper.formatPrice(json.info.paid_commission));
            }
        });

        $('body').on('click', '.affiliate-detail-trigger', function(){

            let affiliate_id = $(this).data('affiliate');

            $.ajax({
               url : sejoli_admin.affiliate_commission.confirm.ajaxurl,
               type : 'POST',
               data : {
                   affiliate : affiliate_id,
                   nonce : sejoli_admin.affiliate_commission.confirm.nonce
               },
               beforeSend : function() {
                   sejoli.helper.blockUI('.sejoli-table-holder');
               },success : function(response) {
                   sejoli.helper.unblockUI('.sejoli-table-holder');
                   sejoli_render_confirmation(response);
               }
           });
        });
    });
})(jQuery);
</script>

<script id='order-detail' type="text/x-jsrender">
<button type='button' class='affiliate-detail-trigger ui mini button' data-id='{{:order_id}}' data-affiliate='{{:id}}'>DETAIL</button>
<strong>
    {{:name}}
</strong>
</script>

<?php require 'confirm-affiliate-commission-modal-content.php'; ?>
