<?php if(!current_user_can('manage_sejoli_orders')) { return; } ?>
<div class="notice notice-info is-dismissible sejoli-help-message">
    <h2>Penggunaan SEJOLI</h2>
    <p>Yang perlu anda atur untuk penggunaan SEJOLI adalah sebagai berikut :</p>
    <p>
        <a href='<?php echo admin_url('admin.php?page=crb_carbon_fields_container_sejoli.php'); ?>' class='button button-primary'>Pengaturan Umum</a>
        <a href='<?php echo admin_url('admin.php?page=crb_carbon_fields_container_notifikasi.php'); ?>' class='button button-primary'>Pengaturan Notifikasi</a>
        <a href='<?php echo admin_url('admin.php?page=sejoli-coupons'); ?>' class='button button-primary'>Pengaturan Kupon</a>
        <a href='<?php echo admin_url('post-new.php?post_type=sejoli-product'); ?>' class='button button-primary'>Buat Produk</a>
        <a href='<?php echo admin_url('post-new.php?post_type=sejoli-access'); ?>' class='button button-primary'>Buat Akses Produk</a>
        <a href='https://www.facebook.com/groups/2451322814904654/' target="_blank" class='button' style='background-color: #007100;color: white;border-color: #007100;'>FB Grup Support</a>
    </p>
</div>
