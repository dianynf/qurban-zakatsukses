<?php

namespace SejoliSA\Front;

class Register
{

    /**
     * register user
     * Hooked via action sejoli/register
     * @return void
     */
    public function submit_register()
    {
        $enable_registration = boolval(carbon_get_theme_option('sejoli_enable_registration'));

        if(false === $enable_registration || is_user_logged_in()) :
            wp_redirect( home_url('member-area/login') );
            exit;
        endif;

        if(isset($_POST['sejoli-nonce']) && wp_verify_nonce($_POST['sejoli-nonce'],'user-register')) :

            $request = wp_parse_args($_POST, [
                'full_name'        => '',
                'email'            => '',
                'password'         => '',
                'confirm_password' => '',
                'wa_phone'         => '',
            ]);

            $errors = [];

            if ( empty( $request['full_name'] ) ) :
                $errors[] = __('Nama Lengkap wajib diisi','sejoli');
            endif;

            if ( empty( $request['email'] ) ) :
                $errors[] = __('Email wajib diisi','sejoli');
            endif;

            if ( empty( $request['password'] ) || empty($request['confirm_password']) ) :
                $errors[] = __('Password dan Konfirmasi Password wajib diisi','sejoli');
            elseif( $request['password'] !== $request['confirm_password'] ):
                $errors[] = __('Password dan Konfirmasi Password harus sama','sejoli');
            endif;

            if ( empty( $request['wa_phone'] ) ) :
                $errors[] = __('Nomor WhatsApp wajib diisi','sejoli');
            endif;

            if ( $errors ) :
                $messages = $errors;
                do_action('sejoli/set-messages',$messages,'error');
            else:

                $userdata = array(
                    'user_email'    => $request['email'],
                    'first_name'    => $request['full_name'],
                    'display_name'  => $request['full_name'],
                    'user_login'    => $request['email'],
                    'user_pass'     => $request['password'],
                    'role'          => 'sejoli-member',
                );

                $user_id = wp_insert_user( $userdata ) ;

                if ( ! is_wp_error( $user_id ) ) :

                    update_user_meta($user_id, '_phone', $request['wa_phone']);
                    wp_new_user_notification( $user_id, null, 'both' );

                    $messages = [
                        sprintf(__('Register berhasil, silahkan login menggunakan akun anda di halaman <a href="%s">login</a>','sejoli'),sejoli_get_endpoint_url('login')),
                    ];
                    do_action('sejoli/set-messages',$messages,'success');

                else:

                    if ( $user_id->get_error_message() ) :
                        $messages = [
                            $user_id->get_error_message()
                        ];
                    else:
                        $messages = [
                            sprintf(__('Register gagal, silahkan coba lagi','sejoli'),sejoli_get_endpoint_url('login'))
                        ];
                    endif;

                    do_action('sejoli/set-messages',$messages,'error');

                endif;

            endif;

        endif;
    }

}
