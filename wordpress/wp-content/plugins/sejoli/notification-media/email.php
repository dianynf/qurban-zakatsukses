<?php

namespace SejoliSA\NotificationMedia;

class Email extends Main {

    /**
     * Attachment for file
     * @var [type]
     */
    public $attachments = false;

    /**
     * Construction
     */
    public function __construct() {
    }

    /**
     * Send the notification
     * @since   1.0.0
     * @return  void
     */
    public function send(array $recipients, $content, $title, $recipient_type = 'buyer', $attachments = array()) {

        $this->set_recipients($recipients);
        $this->set_title($title);
        $this->set_content($content);

        if('buyer' === $recipient_type) :
            $attachments = apply_filters('sejoli/notification/email/attachments', $this->attachments, $this->data);
        endif;

        ob_start();
        include SEJOLISA_DIR . '/template/email/template.php';
        $content = ob_get_clean();

        $logo        = '';
        $upload_logo = carbon_get_theme_option('notification_email_logo');

        if($upload_logo) :
            $image = wp_get_attachment_image_src($upload_logo, 'medium');
            if($image) :
                $logo = sprintf('<img src="%s" alt="%s" />', $image[0], get_bloginfo('name'));
            endif;
        endif;

        $is_sent = wp_mail(
            $this->recipients,
            $this->title,
            str_replace([
                    '{{logo}}',
                    '{{content}}',
                    '{{footer}}',
                    '{{copyright}}'
                ],[
                    $logo,
                    wpautop($this->content),
                    carbon_get_theme_option('notification_email_footer'),
                    carbon_get_theme_option('notification_email_copyright')
                ],
                $content
            ),
            [
                'Content-Type: text/html; charset=UTF-8',
                sprintf('From: %s <%s>', carbon_get_theme_option('notification_email_from_name'), carbon_get_theme_option('notification_email_from_address')),
                sprintf('Reply-top: %s <%s>', carbon_get_theme_option('notification_email_reply_name'), carbon_get_theme_option('notification_email_reply_address'))
            ],
            $attachments
        );

    }
}
