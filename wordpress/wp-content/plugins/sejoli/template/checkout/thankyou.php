<?php
include 'header-thankyou.php';
include 'header-logo.php';

$thumbnail_url = get_the_post_thumbnail_url($order['product']->ID,'full');

?>
<div class="ui text container">
    <div class="thankyou">
        <h2>Halo <?php echo $order['user']->display_name; ?></h2>
        <div class="thankyou-info-1">
            <p>Terimakasih banyak untuk pemesanannya, data pemesanan <?php echo $order['user']->display_name; ?> sudah kami terima</p>
        </div>
        <div class="pesanan-anda">
            <h3>Cek Pesanan Anda</h3>
            <table class="ui table">
                <thead>
                    <tr>
                        <th><?php _e('Produk yang anda beli', 'sejoli'); ?></th>
                        <th><?php _e('Biaya', 'sejoli'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="ui stackable grid">
                                <?php
                                if ( $thumbnail_url ) :
                                    ?>
                                    <div class="four wide column">
                                        <img src="<?php echo $thumbnail_url; ?>">
                                    </div>
                                    <?php
                                endif;
                                ?>
                                <div class="twelve wide column">
                                    Invoice ID: <?php echo $order['ID']; ?><br>
                                    <?php echo $order['product']->post_title; ?>
                                    <?php if(1 < intval($order['quantity'])) : ?>
                                    X <?php echo $order['quantity']; ?>
                                    <?php endif; ?>
                                    <?php
                                    if ( isset( $order['product']->subscription, $order['product']->subscription['regular'] ) &&
                                        $order['product']->subscription['active'] == 1
                                        ) :
                                        $period = $order['product']->subscription['regular']['period'];
                                        switch ( $period ) {
                                            case 'daily':
                                                $period = 'Hari';
                                                break;

                                            case 'monthly':
                                                $period = 'Bulan';
                                                break;

                                            case 'yearly':
                                                $period = 'Tahun';
                                                break;

                                            default:
                                                $period = 'Hari';
                                                break;
                                        }
                                        echo '<br>Durasi: per '.$order['product']->subscription['regular']['duration'].' '.$period;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </td>
                        <td><?php
                        $product_price = $order['product']->price;
                        $quantity      = absint($order['quantity']);
                        if(
                            property_exists($order['product'], 'subscription') &&
                            in_array( $order['type'], array('subscription-regular', 'subscription-signup') )
                        ) :
                            $product_price = $order['product']->subscription['regular']['price'];

                        endif;

                            echo sejolisa_price_format( $quantity * $product_price );
                        ?></td>
                    </tr>
                    <?php
                    if ( isset( $order['product']->subscription, $order['product']->subscription['signup'] ) &&
                        $order['product']->subscription['active'] == 1 &&
                        $order['product']->subscription['signup']['active'] == 1 &&
                        'subscription-signup' === $order['type']
                        ) :
                        ?>
                        <tr>
                            <td><?php _e('Biaya Awal','sejoli'); ?></td>
                            <td><?php echo sejolisa_price_format($order['product']->subscription['signup']['fee']); ?></td>
                        </tr>
                        <?php
                    endif;

                    if ( isset( $order['meta_data']['variants'] ) ) :
                        foreach ( $order['meta_data']['variants'] as $key => $value ) :
                            ?>
                            <tr>
                                <td><?php echo ucwords($value['type']); ?>: <?php echo $value['label']; ?></td>
                                <td><?php echo sejolisa_price_format($quantity * $value['raw_price']); ?></td>
                            </tr>
                            <?php
                        endforeach;
                    endif;

                    if ( isset( $order['meta_data']['shipping_data'] ) ) :
                        ?>
                        <tr>
                            <td>Biaya Pengiriman: <br><?php echo $order['meta_data']['shipping_data']['courier'].' - '.$order['meta_data']['shipping_data']['service']; ?></td>
                            <td><?php echo sejolisa_price_format($order['meta_data']['shipping_data']['cost']); ?></td>
                        </tr>
                        <?php
                    endif;

                    if ( isset( $order['coupon'] ) ) :

                        $coupon_value = $order['coupon']['discount']['value'];
                        if ( $order['coupon']['discount']['type'] === 'percentage' ) :
                            $coupon_value = ( $coupon_value / 100 ) * $product_price;
                        endif;

                        ?>
                        <tr>
                            <td>Kode diskon: <?php echo $order['coupon']['code']; ?></td>
                            <td>- <?php echo sejolisa_price_format($coupon_value); ?></td>
                        </tr>
                        <?php

                    endif;

                    if(
                        isset( $order['meta_data']['free_shipping']) &&
                        false !== $order['meta_data']['free_shipping'] &&
                        isset( $order['meta_data']['shipping_data'])
                    ) :
                    ?>
                    <tr>
                        <td>Gratis Ongkos Kirim</td>
                        <td>- <?php echo sejolisa_price_format($order['meta_data']['shipping_data']['cost']); ?></td>
                    </tr>
                    <?php
                    endif;

                    $fee = apply_filters('sejoli/payment/fee', 0, $order);

                    if ( 0 !== $fee ) :
                        ?>
                        <tr>
                            <td>Biaya Transaksi</td>
                            <td><?php echo sejolisa_price_format($fee); ?></td>
                        </tr>
                        <?php
                    endif;
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th><?php echo sejolisa_coloring_unique_number( sejolisa_price_format($order['grand_total']) ); ?></th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="countdown-payment">
            <h3>Segera lakukan pembayaran dalam waktu</h3>
            <?php
            $date = new DateTime( $order['created_at'] );
            // $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
            $timer = absint(carbon_get_theme_option('sejoli_countdown_timer'));
            $date->add(new DateInterval('PT' . $timer . 'H'));
            ?>
            <div class="countdown-payment-run" data-datetime="<?php echo $date->format('Y-m-d H:i:s'); ?>"></div>
            <p>(Sebelum <?php echo sejolisa_datetime_indonesia( $date->format('Y-m-d H:i:s') ); ?>)</p>
        </div>
        <div class="transfer">
            <h3>Tolong transfer ke</h3>
            <table class="ui table">
                <thead>
                    <tr>
                        <th style="width:35%">Nomor Rekening</th>
                        <th style="width:35%"><span class="no-rekening"><?php echo $order['payment_info']['account_number']; ?></span></th>
                        <th style="width:30%"><a class="copy-btn" data-target=".no-rekening">Copy</a></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Ke Bank</td>
                        <?php if('other' === $order['payment_info']['bank'])  : ?>
                        <td><?php
                            $bank_image = $order['payment_info']['logo'];
                            if(empty($bank_image)) :
                                $bank_image = 'https://via.placeholder.com/150';
                            endif;

                            echo $order['payment_info']['bank_name'];

                            if(isset($order['payment_info']['owner'])) :
                                echo ' a.n '.$order['payment_info']['owner'];
                            endif;
                         ?></td>
                         <td><img src="<?php echo $bank_image; ?>"></td>
                        <?php else : ?>
                        <td><?php
                            echo $order['payment_info']['bank'];

                            if(isset($order['payment_info']['owner'])) :
                                echo ' a.n '.$order['payment_info']['owner'];
                            endif;
                        ?></td>
                        <td><img src="<?php echo SEJOLISA_URL; ?>public/img/<?php echo $order['payment_info']['bank']; ?>.png"></td>
                        <?php endif; ?>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total Biaya</th>
                        <th><?php echo sejolisa_coloring_unique_number( sejolisa_price_format($order['grand_total']) ); ?><span class="total-biaya"><?php echo $order['grand_total']; ?></span></th>
                        <th><a class="copy-btn" data-target=".total-biaya">Copy</a></th>
                    </tr>
                </tfoot>
            </table>
            <div class="transfer-info">
                <div class="segitiga"></div>
                <p><b>Penting!</b> Mohon transfer sampai 3 digit terakhir yaitu <b><?php echo sejolisa_coloring_unique_number( sejolisa_price_format($order['grand_total']) ); ?></b> Karena sistem kami tidak bisa mengenali pembayaran Anda bila jumlahnya tidak sesuai</p>
            </div>
        </div>
        <div class="catatan">
            <h3>Catatan</h3>
            <ul>
                <li>Setelah melakukan konfirmasi pembayaran, verifikasi pesanan Anda akan kami proses dalam 60 menit dan selambat-lambatnya 1x24 jam. </li>
                <li>Pembayaran diatas jam 21.00 WIB akan di proses hari berikutnya. </li>
                <li>Data pembelian dan petunjuk pembayaran juga sudah kami kirim ke email Anda, silakan cek email dari kami di inbox, promotion, dan atau di folder Spam.</li>
            </ul>
        </div>
        <div class="thankyou-info-2">
            <p><b>Wajib:</b> Setelah melakukan transfer pembayaran, harap mengkonfirmasi pembayaran Anda melalui halaman ini:</p>
        </div>
        <a target="_blank" href="<?php echo site_url('confirm'); ?>" class="submit-button massive ui green button">KONFIRMASI PEMBAYARAN</a>
    </div>
</div>

<?php
include 'footer-secure.php';
include 'footer.php';
