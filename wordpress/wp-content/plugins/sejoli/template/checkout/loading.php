<?php
include 'header-loading.php';
?>
<div class="loading holder">
    <h2>Mohon Tunggu</h2>
    <p>Pembelian sedang kami proses...</p>
    <p style='text-align:center'>
        <img src="<?php echo SEJOLISA_URL; ?>public/img/creditcard.png" alt="loading">
    </p>
    <input type="hidden" id="order_id" value="<?php echo $order_id; ?>">
</div>
<script>
    jQuery(document).ready(function($){
        setTimeout(function(){
            sejoliSaCheckout.loading();
        }, 3000);
    });
</script>
<?php
include 'footer.php';
