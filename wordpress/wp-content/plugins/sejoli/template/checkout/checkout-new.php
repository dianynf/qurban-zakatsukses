<?php

do_action('sejoli/checkout-template/header');

do_action('sejoli/checkout-template/list-item');

do_action('sejoli/checkout-template/user-data');

do_action('sejoli/checkout-template/shipment');

do_action('sejoli/checkout-template/payment-gateway');

do_action('sejoli/checkout-template/result');

do_action('sejoli/checkout-template/footer');
