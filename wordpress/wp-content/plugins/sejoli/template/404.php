<?php sejoli_header(); ?>
    <div class="ui negative icon message">
        <i class="exclamation triangle icon"></i>
        <div class="content">
            <div class="header">404 Page Not Found</div>
            <p>Maaf halaman yang anda cari tidak ada.</p>
        </div>
    </div>
<?php sejoli_footer(); ?>
