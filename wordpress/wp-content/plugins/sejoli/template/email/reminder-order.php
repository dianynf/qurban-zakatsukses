Halo {{buyer-name}}

Kami ingin memberitahukan bahwa pesanan {{buyer-name}} yang berikut ini

{{order-detail}}
{{order-meta}}

Sudah {{order-day}} hari {{buyer-name}} belum menyelesaikan pembayaran, silahkan lakukan transfer ya.

Setelah transfer, teman-teman lakukan konfirmasi pembayaran di siniya, {{confirm-url}}

Jika teman-teman menerima email ini padahal sudah melakukan transfer, silahkan kontak kami disertai dengan nomor invoice dan bukti pembayaran

Terima kasih

Admin Sejoli.co.id
