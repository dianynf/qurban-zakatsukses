Halo {{buyer-name}}

Kami ingin memberitahukan bahwa pesanan {{buyer-name}} yang berikut ini

{{recurring-data}}

Akan habis masa berlakunya {{subscription-day}} hari lagi, lakukan order disini {{renew-url}}

Setelah transfer, teman-teman lakukan konfirmasi pembayaran disini ya, {{confirm-url}}

Jika teman-teman menerima email ini padahal sudah melakukan transfer, silahkan kontak kami disertai dengan nomor invoice dan bukti pembayaran

Terima kasih

Admin Sejoli.co.id
