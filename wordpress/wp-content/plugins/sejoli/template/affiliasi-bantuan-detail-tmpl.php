<i class="close icon"></i>
<div class="header">
    <?php _e( '{{:help_title}}', 'sejoli' ); ?>
</div>
<div class="content scrolling">    
    <a href="{{:help_image}}" download="{{:help_title}}" class="ui blue button" style="margin-bottom: 15px;"><i class="download icon"></i> <?php _e( 'Download','sejoli' ); ?></a><br>
    <img src="{{:help_image}}" alt="{{:help_title}}">
</div>
<div class="actions">
</div>