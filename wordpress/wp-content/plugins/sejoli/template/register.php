<?php sejoli_header('login'); ?>

<div class="ui stackable centered grid">
    <div class="five wide column center aligned">
        <div class="ui image">
            <img src="<?php echo sejolisa_logo_url(); ?>" class="image icon">
        </div>
        <?php sejoli_get_template_part('messages.php'); ?>
        <form class="ui large form" method="POST" action="<?php echo sejoli_get_endpoint_url('register'); ?>">

            <div class="ui stacked segment">
                <div class="required field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="full_name" placeholder="Nama Lengkap" value="<?php echo isset($_POST['full_name']) ? $_POST['full_name'] : ''; ?>">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui left icon input">
                        <i class="envelope icon"></i>
                        <input type="email" name="email" placeholder="Alamat Email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="confirm_password" placeholder="Konfirmasi Password">
                    </div>
                </div>
                <div class="required field">
                    <div class="ui left icon input">
                        <i class="whatsapp icon"></i>
                        <input type="number" name="wa_phone" placeholder="Nomor WhatsApp" value="<?php echo isset($_POST['wa_phone']) ? $_POST['wa_phone'] : ''; ?>">
                    </div>
                </div>
                <button type='submit' class="ui fluid large teal submit button">
                    Register
                </button>
            </div>
            <?php wp_nonce_field('user-register','sejoli-nonce'); ?>
        </form>

        <div class="ui message">
            Sudah punya akun? <a href="<?php echo sejoli_get_endpoint_url('login'); ?>">Login</a>
        </div>
    </div>
</div>
<?php sejoli_footer('login'); ?>
