<div class="row">
    <div class="eight wide column">
        <table class="ui striped celled table">
            <thead>
                <tr>
                    <th class="three wide"><?php _e( 'Peringkat', 'sejoli' ); ?></th>
                    <th class="ten wide"><?php _e( 'Nama', 'sejoli' ); ?></th>
                    <th class="three wide"><?php _e( 'Sales', 'sejoli' ); ?></th>
                </tr>
            </thead>
            <tbody id="leaderboard-table-row-holder">
                {{:content}}
            </tbody>
        </table>
    </div>
</div>