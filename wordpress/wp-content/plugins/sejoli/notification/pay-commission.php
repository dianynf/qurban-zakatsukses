<?php

namespace SejoliSA\Notification;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PayCommission extends Main {

    /**
     * Affiliate data
     * @since   1.0.0
     * @var     array
     */
    protected $affiliate;

    /**
     * Recipient data
     * @since   1.0.0
     * @var     array
     */
    protected $recipiens;

    /**
     * Commission data
     * @since   1.1.0
     * @var     array
     */
    protected $commission_data = array();

    /**
     * Attachment for file
     * @since   1.0.0
     * @var     bool|array
     */
    public $attachments = false;

    /**
     * Construction
     */
    public function __construct() {
        add_filter('sejoli/notification/fields',    [$this, 'add_setting_fields'], 100);
    }

    /**
     * Add notification setting fields
     * Hooked via filter, sejoli/notification/fields priority 25
     * @since   1.0.0
     * @param   array $fields All fields for notification setting form
     */
    public function add_setting_fields(array $fields) {

        $fields['pay-commission'] = [
			'title'  => __('Pembayaran Komisi', 'sejoli'),
			'fields' => [

                // Untuk buyer
				Field::make('separator'	,'sep_pay_commission_email', 	__('Email' ,'sejoli'))
					->set_help_text(__('Pengaturan konten untuk media email', 'sejoli')),

				Field::make('text', 	'pay_commission_email_title',	 __('Judul' ,'sejoli'))
					->set_required(true)
					->set_default_value(__('{{affiliate-name}}, Komisi untuk anda sudah dibayarkan ', 'sejoli')),

				Field::make('rich_text', 'pay_commission_email_content', __('Konten', 'sejoli'))
					->set_required(true)
					->set_default_value(sejoli_get_notification_content('pay-affiliate-commission')),

				Field::make('separator'	,'sep_pay_commission_sms', 	__('SMS' ,'sejoli'))
					->set_help_text(__('Pengaturan konten untuk media sms', 'sejoli')),

				Field::make('textarea', 'pay_commission_sms_content', __('Konten', 'sejoli')),

				Field::make('separator'	,'sep_pay_commission_whatsapp', 	__('WhatsApp' ,'sejoli'))
					->set_help_text(__('Pengaturan konten untuk media whatsapp', 'sejoli')),

				Field::make('textarea', 'pay_commission_whatsapp_content', __('Konten', 'sejoli')),

			]
		];



        return $fields;
    }

    /**
     * Prepare content for notification
     * @since   1.0.0
     * @return  void
     */
    protected function set_content() {

        // ***********************
		// Setup content for buyer
		// ***********************

		$this->content['affiliate']['email']['title']      = carbon_get_theme_option('pay_commission_email_title');
		$this->content['affiliate']['email']['content']    = $this->set_notification_content(
    															carbon_get_theme_option('pay_commission_email_content'),
    															'email',
                                                                'affiliate'
														    );

		if(!empty(carbon_get_theme_option('pay_commission_whatsapp_content'))) :

            $this->set_enable_send('whatsapp', 'affiliate', true);
			$this->content['affiliate']['whatsapp']['content'] = $this->set_notification_content(
				                                                carbon_get_theme_option('pay_commission_whatsapp_content'),
				                                                'whatsapp',
                                                                'affiliate'
			                                                 );
        endif;

		if(!empty(carbon_get_theme_option('pay_commission_sms_content'))) :
            $this->set_enable_send('sms', 'affiliate', true);
			$this->content['affiliate']['sms']['content']     = $this->set_notification_content(
				carbon_get_theme_option('pay_commission_sms_content'),
				'sms',
                'affiliate'
			);
        endif;

    }

    /**
     * Render shortcode, overwrite parent class
     * @since   1.1.0
     * @param   string  $content
     * @return  string
     */
    public function render_shortcode($content) {

        foreach( $this->commission_data as $key => $value) :
            if('attachments' !== $key) :
                $content = str_replace("{{".$key."}}", $value, $content);
            endif;
        endforeach;

        return $content;
    }

    /**
     * Trigger to send notification
     * @since   1.0.0
     * @param   array   $commission
     * @return  void
     */
    public function trigger(array $commission) {

        $media_libraries       = $this->get_media_libraries();
        $this->commission_data = $commission;

        $this->set_content();

		$media_libraries['email']->send(
			array($commission['affiliate-email']),
			$this->render_shortcode($this->content['affiliate']['email']['content']),
			$this->render_shortcode($this->content['affiliate']['email']['title']),
            'affiliate',
            $commission['attachments']
		);

        if(!empty(carbon_get_theme_option('pay_commission_whatsapp_content'))) :

            $media_libraries['whatsapp']->send(
                array($commission['affiliate-phone']),
                $this->render_shortcode($this->content['affiliate']['whatsapp']['content'])
            );

        endif;

        if(!empty(carbon_get_theme_option('pay_commission_sms_content'))) :

            $media_libraries['sms']->send(
                array($commission['affiliate-phone']),
                $this->render_shortcode($this->content['buyer']['sms']['content'])
            );

        endif;
    }
}
