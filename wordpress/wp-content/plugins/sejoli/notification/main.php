<?php

namespace SejoliSA\Notification;

use Carbon\Carbon;

class Main {

    /**
	 * Shortcode data
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $shortcode_data;

	/**
	 * Order data
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $order_data;

	/**
	 * Product data
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $product_data;

	/**
	 * Buyer data
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $buyer_data;

	/**
	 * Affiliate data
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $affiliate_data = false;

	/**
	 * Coupon data
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $coupon_data = false;

    /**
	 * Notification content
	 *
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	string
	 */
	protected $content = array(
		'buyer' => array(
			'active' => true,
			'email'	=> array(
				'active' => true,
				'title'   => '',
				'content' => ''
			),
			'sms'	=> array(
				'active'  => false,
				'content' => ''
			),
			'whatsapp' => array(
				'active'  => false,
				'content' => ''
			)
		),
		'admin' => array(
			'active' => false,
			'email'	=> array(
				'active' => true,
				'title'   => '',
				'content' => ''
			),
			'sms'	=> array(
				'active'  => false,
				'content' => ''
			),
			'whatsapp' => array(
				'active'  => false,
				'content' => ''
			)
		),
		'affiliate' => array(
			'active' => false,
			'email'	=> array(
				'active' => true,
				'title'   => '',
				'content' => ''
			),
			'sms'	=> array(
				'active'  => false,
				'content' => ''
			),
			'whatsapp' => array(
				'active'  => false,
				'content' => ''
			)
		),
	);

	/**
	 * Store value if is able to send to specific role and specific media
	 * @since 	1.0.0
	 * @access 	protected
	 * @var 	array
	 */
	protected $able_send = array(
		'email'	=> [
			'buyer'     => true,
			'admin'     => false,
			'affiliate' => false,
		],

		'whatsapp'	=> [
			'buyer'     => false,
			'admin'     => false,
			'affiliate' => false,
		],

		'sms'	=> [
			'buyer'     => false,
			'admin'     => false,
			'affiliate' => false,
		],
	);



    /**
     * Construction
     */
    public function __construct() {

    }

    /**
	 * Prepare shortcode data
	 * @since 	1.0.0
	 * @return 	void
	 */
	protected function prepare_shortcode_data() {

		$timer = absint(carbon_get_theme_option('sejoli_countdown_timer'));

		$this->shortcode_data = [
			'{{memberurl}}'         => home_url('/member-area/'),
			'{{member-url}}'        => home_url('/member-area/'),
			'{{sitename}}'          => get_bloginfo('name'),
			'{{siteurl}}'           => home_url('/'),
			'{{site-url}}'          => home_url('/'),
			'{{order-id}}'          => $this->order_data['ID'],
			'{{invoice-id}}'        => $this->order_data['ID'],
			'{{order-grand-total}}' => trim(sejolisa_price_format($this->order_data['grand_total'])),
			'{{buyer-name}}'        => $this->buyer_data->display_name,
			'{{buyer-email}}'       => $this->buyer_data->user_email,
			'{{buyer-phone}}'       => $this->buyer_data->meta->phone,
			'{{product-name}}'      => $this->product_data->post_title,
			'{{quantity}}'          => $this->order_data['quantity'],
			'{{confim-url}}'		=> home_url('/confirm'),
			'{{confirm-url}}'		=> home_url('/confirm'),
			'{{order-day}}'			=> Carbon::createFromDate($this->order_data['created_at'])->diffInDays(Carbon::now()) + 1,
			'{{close-time}}'        => __('pukul', 'sejoli') . ' ' . date('H:i, d F Y', (strtotime($this->order_data['created_at']) + ($timer * HOUR_IN_SECONDS)) )
		];

		if(is_object($this->affiliate_data)) :
			$this->shortcode_data['{{affiliate-name}}']  = $this->affiliate_data->display_name;
			$this->shortcode_data['{{affiliate-phone}}'] = $this->affiliate_data->phone;
			$this->shortcode_data['{{affiliate-email}}'] = $this->affiliate_data->user_email;
			$this->shortcode_data['{{affiliate-tier}}']  = $this->affiliate_data->tier;
			$this->shortcode_data['{{commission}}']      = $this->affiliate_data->commission;
		endif;

		$this->shortcode_data = apply_filters('sejoli/notification/shortcode', $this->shortcode_data, [
			'order_data'     => $this->order_data,
			'buyer_data'     => $this->buyer_data,
			'product_data'   => $this->product_data,
			'affiliate_data' => $this->affiliate_data
		]);
	}

	/**
	 * Set notification content
	 * Hooked via filter sejoli/notification/content, priority 999
	 * @since 	1.0.0
	 * @param 	string 	$content
	 * @param 	array  	$details
	 * @param 	string 	$media
	 * @return 	string
	 */
	public function set_notification_content($content, $media = 'email', $recipient_type = 'buyer') {

		$user_access = $order_detail = $order_meta = '';

	    $directory         = apply_filters(
								'sejoli/'. $media .'/template-directory',
								SEJOLISA_DIR . 'template/' .$media. '/',
								$media,
								NULL,
								array()
							);
	    $order_detail_file = $directory . 'order-detail.php';
		$order_meta_file   = $directory . 'order-meta.php';
		$user_access_file  = $directory . 'user-access.php';

	    if(file_exists($order_detail_file)) :
	        ob_start();
	        require $order_detail_file;
	        $order_detail = ob_get_contents();
	        ob_end_clean();
	    endif;

		if(file_exists($order_meta_file)) :
	        ob_start();
	        require $order_meta_file;
	        $order_meta = ob_get_contents();
	        ob_end_clean();
	    endif;

        if(file_exists($user_access_file)) :
	        ob_start();
	        require $user_access_file;
	        $user_access = ob_get_contents();
	        ob_end_clean();
	    endif;

        $content = str_replace('{{user-access}}', $user_access, $content);

		$order_detail = apply_filters(
						'sejoli/notification/content/order-detail',
						$order_detail,
						$media,
						$recipient_type,
						[
							'order_data'     => $this->order_data,
							'buyer_data'     => $this->buyer_data,
							'product_data'   => $this->product_data,
							'affiliate_data' => $this->affiliate_data,
						]
					);

		$order_meta = apply_filters(
						'sejoli/notification/content/order-meta',
						$order_meta,
						$media,
						$recipient_type,
						[
							'order_data'     => $this->order_data,
							'buyer_data'     => $this->buyer_data,
							'product_data'   => $this->product_data,
							'affiliate_data' => $this->affiliate_data,
						]
					);

		$payment_gateway = apply_filters(
							'sejoli/notification/content/payment-gateway',
							'',
							$media,
							$recipient_type,
							[
								'order_data'     => $this->order_data,
							]
						);

		$content = str_replace('{{order-detail}}', $order_detail, $content);
		$content = str_replace('{{order-meta}}',   $order_meta, $content);
		$content = str_replace('{{payment-gateway}}',   $payment_gateway, $content);

		return $content;
	}

    /**
	 * Render shortcode data
	 * @since 	1.0.0
	 * @param  	string $content
	 * @return 	string
	 */
	public function render_shortcode($content) {

		$content = str_replace(
						array_keys($this->shortcode_data),
						array_values($this->shortcode_data),
						$content
					);

		return $content;
	}


    /**
     * Setup all related data via order_data and prepare for shortcode_data
     * @since 	1.0.0
     * @return
     */
    public function prepare(array $order_data) {

		$this->order_data   = $order_data;
		$this->buyer_data   = (
								isset($order_data['user']) &&
								is_a($order_data['user'], 'WP_User')
							  ) ? $order_data['user'] : sejolisa_get_user(intval($order_data['user_id']));

		$this->product_data = (
								isset($order_data['product']) &&
								is_a($order_data['product'], 'WP_Post')
							  ) ? $order_data['product'] : sejolisa_get_product(intval($order_data['product_id']));

		// if(0 !== $order_data['affiliate_id'] && !isset($order_data['affiliate_data'])) :
		// 	$this->affiliate_data = sejolisa_get_user(intval($order_data['affiliate_id']));

		if(isset($order_data['affiliate_data'])) :
			$this->affiliate_data = $order_data['affiliate_data'];
		elseif(isset($order_data['affiliate'])) :
			$this->affiliate_data = $order_data['affiliate'];
		endif;

		$this->prepare_shortcode_data();
    }

	/**
	 * Get all media libraries
	 * @since 	1.0.0
	 * @return 	array
	 */
	protected function get_media_libraries() {
		return (array) apply_filters('sejoli/notification/available-media-libraries', []);
	}

	/**
	 * Set if current notification event is able to send to specific role and specific media
	 * @since 	1.0.0
	 * @param 	string 		$role 		The user role
	 * @param 	string 		$media 		The notification media
	 * @param 	boolean 	$enable		Set if is enabled to send
	 * @param 	boolean 	Enable value to send
	 */
	protected function set_enable_send($media, $role, $enable = false) {
		$this->able_send[$media][$role] = $enable;
		return $enable;
	}

	/**
	 * Get able value if current notification event is able to send to admin
	 * @since 	1.0.0
	 * @param 	string 		$role 		The user role
	 * @param 	string 		$media 		The notification media
	 * @return 	boolean
	 */
	protected function is_able_to_send($media, $role) {
		return (boolean) isset($this->able_send[$media][$role]) ? $this->able_send[$media][$role] : false;
	}

	/**
	 * Set recipient title
	 * @since 	1.0.0
	 * @param 	string $recipient
	 * @param 	string $media
	 * @param 	string $content
	 */
	protected function set_recipient_title($recipient, $media, $content) {
		$this->content[$recipient][$media]['title'] = $content;
	}

	/**
	 * Set recipient content
	 * @since 	1.0.0
	 * @param 	string $recipient
	 * @param 	string $media
	 * @param 	string $content
	 */
	protected function set_recipient_content($recipient, $media, $content) {
		$this->content[$recipient][$media]['content'] = $content;
	}

	/**
	 * Get recipient title
	 * @since 	1.0.0
	 * @param  	string $recipient
	 * @param  	string $media
	 * @return 	string
	 */
	protected function get_recipient_title($recipient, $media) {
		return $this->content[$recipient][$media]['title'];
	}

	/**
	 * Get recipient content
	 * @since 	1.0.0
	 * @param  	string $recipient
	 * @param  	string $media
	 * @return 	string
	 */
	protected function get_recipient_content($recipient, $media) {
		return $this->content[$recipient][$media]['content'];
	}
}
