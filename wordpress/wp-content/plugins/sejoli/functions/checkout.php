<?php
/**
 * Check if current page is sejoli checkout built-in page, included thank-you and loading
 * @since   1.0.0
 * @param   string  $page
 * @return  boolean
 */
function sejolisa_verify_checkout_page( $page = '' ) {

    $valid = false;

    global $wp, $sejolisa, $wp_query;

    if ( !empty( $page ) &&
        isset( $wp->query_vars['sejolisa_checkout_page'], $_GET['order_id'] ) &&
        $wp->query_vars['sejolisa_checkout_page'] === $page ) :

        $order_id = intval($_GET['order_id']);

        $response = sejolisa_get_order([
            'ID' => $order_id
        ]);

        if ( false !== $response['valid'] ) :
            $sejolisa['order']  = $response['orders'];
        endif;

        $valid = true;

    endif;

    return $valid;

}
