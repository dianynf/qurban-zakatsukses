=== Plugin Name ===
Contributors: ridwanarifandi
Donate link: https://ridwan-arifandi.com
Tags: membership, affiliate, content, checkout
Requires at least: 5.4.0
Tested up to: 5.4.1
Stable tag: 1.4.1.2

Premium membership, affiliate and reseller plugin

== Description ==

Coming soon

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `sejoli.zip` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Input your license key with sejoli member access data

== Frequently Asked Questions ==

Coming soon

== Screenshots ==

Coming soon

== Changelog ==

1.4.1.2
* Fix follow up problem
* Fix content problem in woowa and WooWandroid
* Fix coupon use for own-self coupon
* Fix code access to closed product
* Fix wrong payment instruction for manual payment
* Fix condition logic in user group update field
* Restructurize duitku.com fields

1.4.1.1
* Fix user group condition in checkout page

1.4.1
* Add reset license in web installation
* Add new order notification for affiliate
* Add max value unique code for each manual transaction. Max value is 999
* Enhance request register license with extra detail in response
* Enhance request validate license with extra detail in response

1.4.0.4
* Fix non-affiliate coupon bug in member area
* Fix donation problem

1.4.0.3
* Fix non-affiliate coupon bug in member area

1.4.0.2
* Panic update!

1.4.0.1
* Fix problem in affiliate link
* Fix problem warning info in functions/user.php
* Fix problem in registering quarterdaily cron job

1.4.0
* Add user group function
* Add multiple couriers
* Add new respond in sejoli-license and sejoli-validate-license request
* Fix compatibility with WPUF

1.3.3
* Add new cron schedules to fix auto cancel order, quarterdaily cron time
* Add new function in payment confirmation to check by order amount
* Fix problem in license checking
* Fix problem in checkout form, checkout button wouldn't be unblocked after displaying error message

1.3.2.2
* Fix problem in thank you page that display free ongkir even doesn't use any coupon

1.3.2.1
* Fix problem in confirmation page

1.3.2
* Display total commission based on status ( paid commission, unpaid commission and potential commission)
* Add member message
* Fix facebook pixel problem with affiliate
* Fix free shipping coupon

1.3.1
* Fix problem with duitku.com, that the total must be integer instead float
* Add callback url field in duitku.com setup
* Change yearly statistic to last 12 months

1.3.1
* Fix problem with duitku.com, that the total must be integer instead float
* Add callbacl url field in duitku.com setup
* Change yearly statistic to last 12 months

1.3.0
* 2nd public release
* Integration with duitku.com
* Integration with learnpress, need add-on
* Add donation/crowdfunding system, need add-on
* Create custom user capability to access admin dashboard
* Modify checkout JS for donation implementation
* Fix many issues

1.2.3
* Add LOG menu under SEJOLI
* Add auto cancel for on-hold order
* Add auto delete log after 30 days
* Add auto delete coupon post data
* Add member area menu logo
* Add option to enable/disable register form
* Add note placeholder option for physical product
* Update error message display when license checking but cant access the license server
* Update third parties composer
* Fix spintax for confirm url
* Fix way to check license from GET to POST
* Fix quantity input for physical product
* Fix detail shipping location in whatsapp
* Remove archive page for content and coupon

1.2.2
* Fix invalid coupon, happens in several hosting
* Change member area header menu

1.2.1
* Fix problem with wrong subscription detail
* Fix problem with subscription data in thank you page
* Adjust color for expired subscription

1.2.0.1
* Fix with shortcode render

1.2.0
* Add COD shipment
* Add followup and reminder autonotification
* Add ability to delete coupon
* Update member area
* Update physical product checkout detail
* Display detail district in shipment
* Fix calculate grand total with variant quantity

1.1.9
* Add limitation to create affiliate coupon
* Add checkout product description
* Fix bug using manual transaction without unique code

1.1.8.1
* Fix bug broken member menu
* Fix bug with order status payment-confirm can't be updated to completed with payment process

1.1.8
* Fix bug when apply a coupon
* Fix bug when login on a checkout form
* Fix block bug when processing in checkout and confirm
* Fix bug when checking license

1.1.7
* Add note for order
* Add product options to manage checkout
* Add customizable member area menu
* Add redirect after login
* Update JS script to block when checkout process
* Update carbonfield library
* Hide payment option if transaction is zero
* Fix problem with woowa integration

1.1.6
* Add code access to closed product checkout
* Add woowa and woowandroid
* Add more function to confirmation process
* Fix problem with JS compatibility

= 1.1.5 =
* Add reset password
* Fix problem with JS compatibility

= 1.1.4 =
* Add cron job to count coupon usage
* Fix problem to increase coupon usage in checkout
* Fix problem with custom affiliate link
* Fix problem JS compatibility WordPress 5.3 in member menu

= 1.1.3 =
* Add affiliasi in admin menu
* Add export affiliasi commission to CSV
* Fix problem with variant product that doesn't use shipping method
* Fix problem with required bank name field in manual transaction

= 1.1.2 =
* Add manual bank account for BCA
* Hide courier and shipment cost if shipping option not active
* Fix problem when sending notification when pay commission
* Fix problem when sending notification when order cancelled
* Fix problem when sending notification when order refunded

= 1.1.1 =

* Add other bank in manual transaction
* Add option to setup countdown in invoice
* Add option to set redirect if in homepage to member-area
* Add cronjob to fix commission status problem
* Add warning info if coupon is not valid to use
* Fix bug when sending bulk notification
* Fix notice problem when calculating
* Fix bug for commission status
* Remove charset and collation from database setup

= 1.1.0 =
* Add mass notification
* Add filter order by grand total
* Add affiliate name in user table
* Fix bug when update commission to paid
* Fix bug for email content
* Fix bug for automatic payment

= 1.0.3 =

* Add CSV export order data
* Add notification setup for cancel, refund and paid commission
* Fix problem with cpanel plesk database connection
* Fix problem with counting license number
* Remove password for whatsapp and email
* Remove attachments for email ( later we will add whitelist file extension for this)

= 1.0.2 =
* Add video tutorial for access and coupon
* Change text from Bantuan to Tutorial
* Change whatsapp contact to email in thank you page
* Hide sales info in leaderboard for non admin user
* Hide delete action for button
* Fix bug when trigger to display order detail
* Fix bug when display account number
* Fix bug when display affiliate bonus
* Fix problem when editing access page with elementor

= 1.0.1 =
* Add display affiliate name in checkout
* Add display affiliate name in checkout when apply a coupon
* Fix bug when displaying account number for manual transaction
* Fix bug when updating the option, caused by moota_account_owner field
* Remove attachments if recipient is both admin and affiliate

= 1.0.0 =
* First release
