<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit77fd603524708525441e1e09f2874ed1
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Agratek\\Payment\\' => 16,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Agratek\\Payment\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit77fd603524708525441e1e09f2874ed1::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit77fd603524708525441e1e09f2874ed1::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit77fd603524708525441e1e09f2874ed1::$classMap;

        }, null, ClassLoader::class);
    }
}
