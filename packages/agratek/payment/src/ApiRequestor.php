<?php

namespace Agratek\Payment;

use Illuminate\Support\Facades\Log;

/**
 * Send request to Agratek API
 * Better don't use this class directly, use CoreApi, Transaction
 */

class ApiRequestor
{

    /**
     * Actually send request to API server
     *
     * @param string  $userid
     * @param string  $password / key
     */


    private static function jsonRpcHeader($userid, $password)
    {
        date_default_timezone_set('UTC');
        $inttime     = strval(time() - strtotime('1970-01-01 00:00:00'));
        $value       = "$userid&" . $inttime;
        $key         = $password;
        $signature   = hash_hmac('sha256', $value, $key, true);
        $signature64 = base64_encode($signature);
        $headers      = array(
            "userid: $userid",
            "signature:$signature64",
            "key:$inttime"
        );

        return $headers;
    }

    public static function jsonRpcClient($url, $user_id, $user_key, $method, $data_hash)
    {
        $headers = ApiRequestor::jsonRpcHeader($user_id, $user_key);
        $ch = curl_init();
        $rand_id = random_int(1000000, 9999999);
        $params = array(
            CURLOPT_RETURNTRANSFER => TRUE,
            //CURLOPT_COOKIEJAR => $this->cookie_file->path,
            //CURLOPT_COOKIEFILE => $this->cookie_file->path,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.1.13) Gecko/20100914 Firefox/3.5.13( .NET CLR 3.5.30729)',
            CURLOPT_POST => TRUE,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => json_encode(array(
                'jsonrpc' => '2.0',
                'method' => $method,
                'params' => $data_hash,
                'id' => $rand_id
            ))
        );

        Log::critical("Request: " . json_encode($params));
        curl_setopt_array($ch, $params);
        $result = curl_exec($ch);

        // dd($params);

        if ($result === false) {
            throw new \Exception('CURL Error: ' . curl_error($ch), curl_errno($ch));
        } else {
            Log::critical("Response: $result");
            try {
                $result_array = json_decode($result, TRUE);
            } catch (\Exception $e) {
                throw new \Exception("API Request Error unable to decode API response: " . $result . ' | Request url: ' . $url);
            }
            // dd($result_array);
            if (array_key_exists("error", $result_array)) {
                $message = 'Agratek Payment Error (' . $result_array["error"]["code"] . '): '
                    . $result_array["error"]["message"];
                throw new \Exception($message, $result_array["error"]["code"]);
            } elseif (array_key_exists("result", $result_array)) {
                return $result_array["result"];
            } else {
                throw new \Exception("Respon not JSON RPC 2.0");
            }
        }

        curl_close($ch);
        return $result; // ['result'];
    }
}
