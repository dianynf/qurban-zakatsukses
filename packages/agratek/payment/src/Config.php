<?php

namespace Agratek\Payment;

/**
 * Agratek Payment Configuration
 */
class Config
{

    /**
     * Your merchant's client ID
     * 
     * @static
     */
    public static $clientId;
    /**
     * Your merchant's client key
     * 
     * @static
     */
    public static $clientKey;
    public static $clientDom;
    /**
     * True for production
     * false for sandbox mode
     * 
     * @static
     */
    public static $isSandbox = true;
    /**
     * Set it true to enable 3D Secure by default
     * 
     * @static
     */
    public static $is3ds = false;
    /**
     *  Set Append URL notification
     * 
     * @static
     */
    public static $appendNotifUrl;
    /**
     *  Set Override URL notification
     * 
     * @static
     */
    public static $overrideNotifUrl;
    /**
     * Enable request params sanitizer (validate and modify charge request params).
     * See Agratek Payment_Sanitizer for more details
     * 
     * @static
     */
    public static $isSanitized = false;
    /**
     * Default options for every request
     * 
     * @static
     */
    public static $curlOptions = array();

    const SANDBOX_BASE_URL = "https://devel.agratek.id/api/merchant";
    const PRODUCTION_BASE_URL = "https://live.agratek.id/api/merchant";

    /**
     * Get baseUrl
     * 
     * @return string Agratek Payment API URL, depends on $isProduction
     */
    public static function getBaseUrl()
    {
        return Config::$isSandbox ?
        Config::SANDBOX_BASE_URL:Config::PRODUCTION_BASE_URL ;
    }

    /**
     * Get snapBaseUrl
     * 
     * @return string Snap API URL, depends on $isProduction
     */
    // public static function getSnapBaseUrl()
    // {
        // return Config::$isProduction ?
        // Config::SNAP_PRODUCTION_BASE_URL : Config::SNAP_SANDBOX_BASE_URL;
    // }
}
