<?php

namespace Agratek\Payment;
use DateTime;
use DateInterval;
/**
 * API methods to get transaction status, approve and cancel transactions
 */
class Transaction
{

    /**
     * Retrieve transaction status
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return mixed[]
     */
    public static function status($params)
    {
        $tommorow=new DateTime('now');
        $data= array(
            "time_stamp"=> $tommorow->format('YmdHis'),
            "tx_id"=>$params["tx_id"],
            "invoice_no"=> $params["invoice_no"],
            "amount"=> $params["amount"]
        );
        return ApiRequestor::jsonRpcClient(
            Config::getBaseUrl() ,
            Config::$clientId,
            Config::$clientKey,
            "status",
            $data
        );
    }

    /**
     * Approve challenge transaction
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return string
     */
    public static function approve($id)
    {
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $id . '/approve',
            Config::$serverKey,
            false
        )->status_code;
    }

    /**
     * Cancel transaction before it's settled
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return string
     */
    public static function cancel($id)
    {
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $id . '/cancel',
            Config::$serverKey,
            false
        )->status_code;
    }
  
    /**
     * Expire transaction before it's setteled
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return mixed[]
     */
    public static function expire($id)
    {
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $id . '/expire',
            Config::$serverKey,
            false
        );
    }

    /**
     * Transaction status can be updated into refund
     * if the customer decides to cancel completed/settlement payment.
     * The same refund id cannot be reused again.
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return mixed[]
     */
    public static function refund($id, $params)
    {
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $id . '/refund',
            Config::$serverKey,
            $params
        );
    }

    /**
     * Transaction status can be updated into refund
     * if the customer decides to cancel completed/settlement payment.
     * The same refund id cannot be reused again.
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return mixed[]
     */
    public static function refundDirect($id, $params)
    {
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $id . '/refund/online/direct',
            Config::$serverKey,
            $params
        );
    }

    /**
     * Deny method can be triggered to immediately deny card payment transaction
     * in which fraud_status is challenge.
     * 
     * @param string $id Order ID or transaction ID
     * 
     * @return mixed[]
     */
    public static function deny($id)
    {
        return ApiRequestor::post(
            Config::getBaseUrl() . '/' . $id . '/deny',
            Config::$serverKey,
            false
        );
    }
    
    public static function register($params)
    {
        $payloads = array(
          'credit_card' => array(
              'secure' => Config::$is3ds
          )
        );

        $today = new DateTime('now');
        $tommorow = new DateTime('now');
        $tommorow->add(new DateInterval('P1D'));
        $additional="";
        $account = $params["account"];
        if (substr($account["denom"],0,2)=='VA')
          $type="va";
        elseif($account["denom"]=='PAY-WITH-FLIP')
          $type="pwf";

        switch ($type) {
            case "va":
                $additional = array("va"=> array(
                    "valid_date"=> $tommorow->format('Ymd'),
                    "valid_time"=> $tommorow->format('His'),
                    "fix_acct_id"=> ""
                    )
                );
                break;
            case "pwf":
                $additional = array("pwf"=> array(
                  "title"=> $account["description"],
                  "expired_date"=> $tommorow->format('Y-m-d H:m'), 
                  "redirect_url"=>  Config::$clientDom,
                  "type"=> "SINGLE"
                ));
                break;
            default:
                $additional = "";
        }
        
        // "customer"=> array(
          // "name"=> $customer["name"],
          // "phone"=> $customer["phone"],
          // "email"=> $customer["email"],
          // "address"=> $customer["address"],
          // "city"=> $customer["city"],
          // "state"=> $customer["state"],
          // "post_code"=> $customer["post_code"],
          // "country"=> $customer["country"],
          // "session_id"=> $customer["session_id"],
          // "ip"=> $customer["ip"],
          // "agent"=> "Mozilla/5.0 (Windows NT 10.0; Win64; x64)",
          // "language"=> "en-US"
        // ),
        
        $data = array(
          "denom"=> $params["account"]["denom"],
          "amount"=> $params["account"]["amount"],
          "invoice_no"=> $params["account"]["invoice_no"],
          "description"=> $params["account"]["description"],
          "customer"=> $params["customer"],
          "time_stamp"=> $today->format("YmdHis"),
          "currency"=> "IDR",
          "fee"=> 0,
          "vat"=> 0,
          "notax_amt"=> 0,
          "req_dt"=> $today->format("Ymd"),
          "req_tm"=> $today->format("His"),
          "biller"=> "", // diisi sesuai data partner
          "deliver_to"=>$params["deliver_to"],
          "cart"=> $params["cart"],
          "server"=> Config::$clientDom,
        );
        if ($additional!="")
        {
          $data = array_merge($data, $additional);
        }
        $params=array(
          "data"=> $data
        );
                
        $result = ApiRequestor::jsonRpcClient(
            Config::getBaseUrl(),
            Config::$clientId,
            Config::$clientKey,
            'register',
            $params
        );

        return $result;
    }  
}
