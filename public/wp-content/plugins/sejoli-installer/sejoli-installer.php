<?php

/**
 *
 * @wordpress-plugin
 * Plugin Name:       Sejoli Installer
 * Plugin URI:        https://sejoli.co.id
 * Description:       Sejoli installer. Setelah selesa install, anda bisa menghapus plugin ini
 * Version:           1.0.0
 * Author:            Sejoli
 * Author URI:        https://sejoli.co.id
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       qmquote
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

global $sejolisa_ins_installer;
$sejolisa_ins_installer = [
	'slug'      => 'sejoli/sejoli.php',
	'installed' => true
];

add_action('init', 'sejolisa_ins_check_plugin', 1);

/**
 * Check if qmquote plugin exist and active
 * Hooked via action init, 1
 * @return void
 */
function sejolisa_ins_check_plugin() {
	global $sejolisa_ins_installer;
	$plugin_slug  = $sejolisa_ins_installer['slug'];

	if ( ! function_exists( 'get_plugins' ) ) :
		require_once ABSPATH . 'wp-admin/includes/plugin.php';
	endif;

    $all_plugins = get_plugins();

	if(!isset($all_plugins[$plugin_slug])) :
		$sejolisa_ins_installer['installed'] = false;
	endif;
}

add_action('admin_init', 'sejolisa_ins_download_request', 1);

/**
 * Download request
 * Hooked via action admin_init, priority 1
 * @return void
 */
function sejolisa_ins_download_request() {

	global $sejolisa_ins_installer;

	if(isset($_GET['sejolisanonce']) && wp_verify_nonce($_GET['sejolisanonce'], 'download-plugin')) :

		$plugin_slug  = $sejolisa_ins_installer['slug'];
		$download_url = 'https://member.sejoli.id/plugins/sejoli.zip';

		include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
	    wp_cache_flush();

	    $upgrader  = new Plugin_Upgrader();
	    $installed = $upgrader->install( $download_url );

		if ( !is_wp_error( $installed ) && $installed ) :
			echo 'Activating new plugin.';
			$activate = activate_plugin( $plugin_slug );
			echo 'activate : ['. $activate .']';
			var_dump($activate);

			wp_redirect(admin_url('plugins.php'));
		else :
		    echo 'Could not install the new plugin.';
		endif;

		exit;
	endif;
}

add_action('admin_notices', 'sejolisa_ins_display_notice', 1);

/**
 * Display notice if qmquote plugin is not installed
 * Hooked via action admin_notices. priority 1
 * @return void
 */
function sejolisa_ins_display_notice() {

	global $sejolisa_ins_installer;

	if(true === $sejolisa_ins_installer['installed']) :
		return;
	endif;

	?>
	<div class="notice notice-warning">
		<h3>Download Sejoli</h3>
		<p>
			Klik tombol di bawah untuk mendownload plugin Sejoli.
		</p>
		<p>
			<a href="#" class='button button-primary sejoli-installer'><?php _e('Download Sejoli', 'sejoli'); ?></a>
		</p>
	</div>
	<?php
}

add_action('admin_footer', 'sejolisa_ins_footer_script', 999);

function sejolisa_ins_footer_script() {

	$url = wp_nonce_url(
		admin_url(),
		'download-plugin',
		'sejolisanonce'
	);

	?>
	<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.sejoli-installer').click(function(){
			let element = jQuery(this);
			jQuery.ajax({
				url :'<?php echo $url; ?>',
				beforeSend : function() {
					element.html('Installing, please wait...').attr('disabled',true);
				},
				success : function(respond) {
					element.html('Plugin installed. We will redirect you to plugins page');
					setTimeout(function(){
						window.location.replace("<?php echo admin_url('admin.php?page=sejoli-license-form'); ?>");
					},500);
				}
			})
		});
	})
	</script>
	<?php
}

/**
 * Debugging
 */
if(!function_exists('__debug')) :
function __debug()
{
	$bt     = debug_backtrace();
	$caller = array_shift($bt);
	$args   = [
		"file"  => $caller["file"],
		"line"  => $caller["line"],
		"args"  => func_get_args()
	];

	if ( class_exists( 'WP_CLI' ) ) :
		?><pre><?php print_r($args); ?></pre><?php
	else :
		do_action('qm/info',$args);
	endif;
}
endif;
