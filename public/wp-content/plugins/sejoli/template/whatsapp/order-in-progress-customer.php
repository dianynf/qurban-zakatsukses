<?php printf(__('Halo %s', 'sejoli'),'{{buyer-name}}' ); ?>
<?php _e('Terima kasih Pembayaran {{buyer-name}} telah berhasil kami terima dan verifikasi. Semoga rezekinya {{buyer-name}} oleh Alloh diluaskan seluas lautan, usahanya diberikan keuntungan berlimpah, keluarganya diberikan kesehatan, dilancarkan segala urusannya, sisa umurnya diberikan keberkahan dan transaksi kita di ridhoi Alloh SWT. Aamiin...', 'sejoli'); ?>
<?php _e('Berikut detail pembelian {{buyer-name}}', 'sejoli'); ?>
{{order-detail}}
{{order-meta}}
<?php _e('Jika kesulitan silakan Hubungi 085974902378 (WA/SMS/Telepon)', 'sejoli'); ?>
