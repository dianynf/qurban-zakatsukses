<?php sejoli_header(); ?>
    <?php
    $current_user = wp_get_current_user();
    $subdistrict = $current_user->_destination;
    if ( !empty( $subdistrict ) ) :
        $district = sejolisa_get_district_options_by_ids( [$subdistrict] );
        if ( !empty( $district['results'] ) ) :
            $subdistrict = $district['results'][0];
        endif;
    endif;
    ?>
    <h2 class="ui header">Profile</h2>
    <form id="profile" class="ui form" method="post">
        <input style="display:none" type="email" name="email">
        <input style="display:none" type="password" name="password">
        <div class="required field">
            <label>Nama</label>
            <input type="text" name="name" placeholder="Nama" value="<?php echo $current_user->first_name; ?>">
        </div>
        <div class="required field">
            <label>Alamat Email</label>
            <input type="email" name="real_email" placeholder="Alamat Email" value="<?php echo $current_user->user_email; ?>">
        </div>
        <div class="required field">
            <label>No Handphone</label>
            <input type="text" name="phone" placeholder="No Handphone" value="<?php echo $current_user->_phone; ?>">
        </div>
        <div class="field">
            <label>Alamat Lengkap</label>
            <textarea name="address" placeholder="Alamat Lengkap"><?php echo $current_user->_address; ?></textarea>
        </div>
        <div class="field">
            <label>Kecamatan</label>
            <select name="kecamatan" id="kecamatan">
                <option value="">Silakan Ketik Nama Kecamatannya</option>
                <?php
                if ( $subdistrict ) :
                    ?>
                    <option selected value="<?php echo $subdistrict['id']; ?>"><?php echo $subdistrict['text']; ?></option>
                    <?php
                endif;
                ?>
            </select>
        </div>
        <div class="field">
            <label>Informasi Rekening</label>
            <textarea name="_bank_info" placeholder="Informasi Rekening"><?php echo $current_user->_bank_info; ?></textarea>
        </div>
        <div class="field">
            <label>Password Baru</label>
            <input type="password" name="password_baru" placeholder="Password Baru" value="">
        </div>
        <div class="field">
            <label>Konfirmasi Password Baru</label>
            <input type="password" name="konfirmasi_password_baru" placeholder="Konfirmasi Password Baru" value="">
        </div>
        <?php wp_nonce_field('ajax-nonce', 'security'); ?>
        <button class="ui primary button" type="submit">Submit</button>
    </form>
    <div class="alert-holder profile-alert-holder">
    </div>
    <script id="alert-template" type="text/x-js-render">
        <div class="ui {{:type}} message">
            <i class="close icon"></i>
            <div class="header">
                {{:type}}
            </div>
            {{if messages}}
                <ul class="list">
                {{props messages}}
                    <li>{{>prop}}</li>
                {{/props}}
                </ul>
            {{/if}}
        </div>
    </script>
    <script>
    (function( $ ) {
        'use strict';
        $(document).ready(function(){
            sejoli.profile.init();
        });
    })( jQuery );
    </script>
<?php sejoli_footer(); ?>
