<?php

$fb_pixel = array(
    'active'           => boolval(carbon_get_post_meta(get_the_ID(),'fb_pixel_active')),
    'affiliate_active' => boolval(carbon_get_post_meta(get_the_ID(),'fb_pixel_affiliate_active')),
    'id'               => carbon_get_post_meta(get_the_ID(),'fb_pixel_id'),
    'links'            => sejolisa_get_product_fb_pixel_links(get_the_ID())
);

$product = sejolisa_get_product(get_the_ID());

if (
    isset( $fb_pixel['id'], $fb_pixel['active'] ) &&
    true === boolval($fb_pixel['active']) &&
    !empty( $fb_pixel['id'] )
) :
    $event = $fb_pixel['links']['checkout']['type'];
    ?>
    <!-- Facebook Pixel Code | Vendor -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script','https://connect.facebook.net/en_US/fbevents.js');

    jQuery(document).ready(function(){

        fbq('init', sejoli_fb_pixel.id);
        fbq('track', 'PageView');
        fbq('track', sejoli_fb_pixel.event.checkout, {
            content_ids: sejoli_fb_pixel.product_id,
            content_type: sejoli_fb_pixel.content_type,
            currency: sejoli_fb_pixel.currency,
            value: sejoli_fb_pixel.value
        });

        if(sejoli_fb_pixel.affiliate_id) {
            fbq('init', sejoli_fb_pixel.affiliate_id);
            fbq('track', 'PageView');
            fbq('track', sejoli_fb_pixel.event.checkout, {
                content_ids: sejoli_fb_pixel.product_id,
                content_type: sejoli_fb_pixel.content_type,
                currency: sejoli_fb_pixel.currency,
                value: sejoli_fb_pixel.value
            });
        }
    });
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $fb_pixel['id']; ?>&ev=PageView&noscript=1"/></noscript>
    <?php if ( !empty( $fb_pixel['links']['checkout']['type'] ) ) : ?>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $fb_pixel['id']; ?>&ev=<?php echo $event;?>&noscript=1"/></noscript>
    <?php endif; ?>
    <!-- End Facebook Pixel Code -->
    <?php
endif;
