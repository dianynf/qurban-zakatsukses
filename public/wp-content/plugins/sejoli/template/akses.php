<?php sejoli_header(); ?>
    <h2 class="ui header">Akses</h2>
    <div class="ui three column doubling stackable cards item-holder masonry grid">
    </div>
    <div class="bonus-modal-holder ui modal"></div>
    <script id="item-template" type="text/x-js-render">
        <?php include 'akses-item-tmpl.php'; ?>
    </script>
    <script id='bonus-template' type="text/x-js-render">
        <?php include 'jsrender/tmpl-bonus-access.php'; ?>
    </script>
    <script>
        jQuery(document).ready(function($){
            sejoli.akses.init();

            jQuery('body').on('click', '.open-bonus', function(){
                let affiliate_id = jQuery(this).data('affiliate'),
                    product_id =  jQuery(this).data('product');

                jQuery.ajax({
                    type     : 'get',
                    url      : sejoli_member_area.bonus.ajaxurl,
                    dataType : 'json',
                    data     : {
                        nonce : sejoli_member_area.bonus.nonce,
                        affiliate : affiliate_id,
                        product : product_id
                    },
                    beforeSend : function() {
                        sejoli.block('.sejolisa-memberarea-content');
                    },
                    success : function(response) {
                        sejoli.unblock('.sejolisa-memberarea-content');
                        if('' !== response.content) {
                            let tmpl = $.templates('#bonus-template'),
                                html = tmpl.render(response);

                            $('.bonus-modal-holder').html(html).modal('show');
                        }
                    }
                })
                return false;
            });
        });
    </script>
<?php sejoli_footer(); ?>
