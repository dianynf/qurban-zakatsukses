<div class="ui icon message">
    <i class="exclamation triangle icon"></i>
    <div class="content">
        <div class="header">
        <?php _e( 'Maaf', 'sejoli' ); ?>
        </div>
        <p><?php _e( 'Tidak ada data ditemukan.', 'sejoli' ); ?></p>
    </div>
</div>