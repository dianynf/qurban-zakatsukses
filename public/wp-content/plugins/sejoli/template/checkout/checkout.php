<?php
include 'header.php';
include 'header-logo.php';

$product = sejolisa_get_product($post->ID);
$use_checkout_description = boolval(carbon_get_post_meta($post->ID, 'display_product_description'));
?>

<div class="ui text container">
    <?php if(false !== $use_checkout_description) : ?>
    <div class='deskripsi-produk'>
        <?php echo apply_filters('the_content', carbon_get_post_meta($post->ID, 'checkout_product_description')); ?>
    </div>
    <?php endif; ?>
    <div class="produk-dibeli">

        <?php do_action('sejoli/checkout-template/before-product', $product); ?>

        <table class="ui unstackable table">
            <thead>
                <tr>
                    <th style="width:55%">Pesanan anda</th>
                    <th style="width:45%">Biaya</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="ui placeholder">
                            <div class="image header">
                                <div class="line"></div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="ui placeholder">
                            <div class="paragraph">
                                <div class="line"></div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Total</th>
                    <th>
                        <div class="total-holder">
                            <div class="ui placeholder">
                                <div class="paragraph">
                                    <div class="line"></div>
                                </div>
                            </div>
                        </div>
                    </th>
                </tr>
                <?php do_action('sejoli/checkout-template/after-product', $product); ?>
                <tr>
                    <th colspan="2">
                        <span class="secure-tagline-icon"><i class="check circle icon"></i> Secure 100%</span>
                        <?php if(false !== $product->form['warranty_label']) : ?>
                        <span class="secure-tagline-icon"><i class="check circle icon"></i> Garansi Uang Kembali</span>
                        <?php endif; ?>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="kode-diskon">
        <div class="data-holder">
            <div class="ui fluid placeholder">
                <div class="paragraph">
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="login">
        <div class="data-holder">
            <div class="ui fluid placeholder">
                <div class="paragraph">
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="informasi-pribadi">
        <div class="data-holder">
        </div>
    </div>
    <div class="metode-pembayaran">
        <h3>Pilih Metode Pembayaran</h3>
        <div class="ui doubling grid data-holder">
            <div class="eight wide column">
                <div class="ui placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <div class="ui placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="beli-sekarang element-blockable">
        <div class="data-holder">
            <div class="ui fluid placeholder">
                <div class="paragraph">
                    <div class="line"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="affiliate-name" style='padding-top:4rem'>

    </div>
    <div class="alert-holder checkout-alert-holder"></div>
</div>
<script id="produk-dibeli-template" type="text/x-jsrender">
    {{if product}}
        <tr>
            <td>
                <div class="ui stackable grid">
                    {{if product.image}}
                        <div class="four wide column">
                            <img src="{{:product.image}}">
                        </div>
                    {{/if}}
                    <div class="twelve wide column">
                        {{:product.title}}
                        {{if subscription}}
                            {{if subscription.duration}}
                                <br>Durasi: {{:subscription.duration.string}}
                            {{/if}}
                        {{/if}}
                        <input type="hidden" id="product_id" name="product_id" value="{{:product.id}}">
                    </div>
                </div>
            </td>
            <td>{{:product.price}}</td>
        </tr>
    {{/if}}
    {{if subscription}}
        {{if subscription.signup}}
            <tr>
                <td>Biaya Awal</td>
                <td>{{:subscription.signup.price}}</td>
            </tr>
        {{/if}}
    {{/if}}
    {{if shipment}}
        <tr>
            <td>Biaya Pengiriman</td>
            <td>{{:shipment.value}}</td>
        </tr>
    {{/if}}
    {{if coupon}}
        <tr>
            <td>
                Kode diskon: {{:coupon.code}}, <a class="hapus-kupon">Hapus kupon</a>
                <input type="hidden" id="coupon" name="coupon" value="{{:coupon.code}}">
            </td>
            <td>{{:coupon.value}}</td>
        </tr>
    {{/if}}
    {{if wallet}}
        <tr>
            <td>
                <?php _e('Dana di dompet yang anda gunakan', 'sejoli'); ?>
            </td>
            <td>{{:wallet}}</td>
        </tr>
    {{/if}}
    {{if transaction}}
        <tr>
            <td>Biaya Transaksi</td>
            <td>{{:transaction.value}}</td>
        </tr>
    {{/if}}
</script>
<script id="metode-pembayaran-template" type="text/x-jsrender">
    {{if payment_gateway}}
        {{props payment_gateway}}
            <div class="eight wide column">
                <div class="ui radio checkbox {{if key == 0}}checked{{/if}}">
                    <input type="radio" name="payment_gateway" tabindex="0" class="hidden" value="{{>prop.id}}" {{if key == 0}}checked="checked"{{/if}}>
                    <label><img src="{{>prop.image}}" alt="{{>prop.title}}"></label>
                </div>
            </div>
        {{/props}}
    {{/if}}
</script>
<script id="alert-template" type="text/x-jsrender">
    <div class="ui {{:type}} message">
        <i class="close icon"></i>
        <div class="header">
            {{:type}}
        </div>
        {{if messages}}
            <ul class="list">
                {{props messages}}
                    <li>{{>prop}}</li>
                {{/props}}
            </ul>
        {{/if}}
    </div>
</script>
<script id="login-template" type="text/x-jsrender">
    {{if current_user.id}}
        <div class="login-welcome">
            <p>Hai, kamu akan order menggunakan akun <span class="name">{{:current_user.name}}</span>, <a href="<?php echo wp_logout_url( get_permalink() ); ?>">Logout</a></p>
        </div>
    {{else}}
        <?php if(false !== $product->form['login_field']) : ?>
        <div class="login-form-toggle">
            <p>Sudah mempunyai akun ? <a>Login</a></p>
        </div>
        <form class="ui form login-form">
            <h3>Login</h3>
            <div class="required field">
                <label>Alamat Email</label>
                <input type="email" name="login_email" id="login_email" placeholder="Masukan alamat email">
            </div>
            <div class="required field">
                <label>Password</label>
                <input type="password" name="login_password" id="login_password" placeholder="Masukan password anda">
            </div>
            <button type="submit" class="submit-login massive right ui green button">LOGIN</button>
            <div class="alert-holder login-alert-holder"></div>
        </form>
        <?php endif; ?>
    {{/if}}
</script>
<script id="apply-coupon-template" type="text/x-jsrender">
    <?php if(false !== $product->form['coupon_field']) : ?>
    <div class="kode-diskon-form-toggle">
        <p><img src="<?php echo SEJOLISA_URL; ?>public/img/voucher.png"> Punya Kode Diskon ? <a>Klik Untuk Masukan Kode</a></p>
    </div>
    <?php endif; ?>
    <form class="kode-diskon-form">
        <h4>Kode Diskon</h4>
        <p>Masukan kode jika anda punya</p>
        <div class="ui fluid action input">
            <input type="text" name="apply_coupon" id="apply_coupon" placeholder="Masukan kode diskon">
            <button type="submit" class="submit-coupon massive ui green button">APPLY</button>
        </div>
        <div class="alert-holder coupon-alert-holder"></div>
    </form>
</script>
<script id="informasi-pribadi-template" type="text/x-jsrender">
    <div class="informasi-pribadi-info">
        <p>Isi data-data di bawah untuk informasi akses di website ini.</p>
    </div>
    <h3>Informasi Pribadi</h3>
    <div class="ui form">
        <div class="required field">
            <label>Nama</label>
            <input type="text" name="user_name" id="user_name" placeholder="Masukan nama anda">
        </div>
        <div class="required field">
            <label>Alamat Email</label>
            <p>Kami akan mengirimkan konfirmasi pembayaran dan password ke alamat ini</p>
            <input type="email" name="user_email" id="user_email" placeholder="Masukan alamat email">
            <div class="alert-holder user-email-alert-holder"></div>
        </div>
        <div class="required field">
            <label>Password</label>
            <input type="password" name="user_password" id="user_password" placeholder="Masukan password anda">
        </div>
        <div class="required field">
            <label>No Handphone</label>
            <p>Kami akan menggunakan no hp untuk keperluan administrasi</p>
            <input type="text" name="user_phone" id="user_phone" placeholder="Masukan no handphone">
            <div class="alert-holder user-phone-alert-holder"></div>
        </div>
    </div>
</script>
<script id="beli-sekarang-template" type="text/x-jsrender">
    <div class="ui stackable grid">
        <div class="eight wide column">
            <div class="total-bayar">
                <h4>Total Bayar</h4>
                <div class="total-holder">
                    <div class="ui placeholder">
                        <div class="paragraph">
                            <div class="line"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="eight wide column">
            <button data-fb-pixel-event="<?php echo isset( $fb_pixel['links']['submit']['type'] ) ? $fb_pixel['links']['submit']['type'] : ''; ?>" type="submit" class="submit-button massive right floated ui green button">PROSES SEKARANG</button>
        </div>
    </div>
</script>
<script>
jQuery(document).ready(function($){
    sejoliSaCheckout.init();
});
</script>
<?php
include 'footer-secure.php';
include 'footer.php';
