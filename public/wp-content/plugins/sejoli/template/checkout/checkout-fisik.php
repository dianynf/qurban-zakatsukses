<?php
global $post;

include 'header.php';
include 'header-logo.php';

$product = sejolisa_get_product($post->ID);
$use_checkout_description = boolval(carbon_get_post_meta($post->ID, 'display_product_description'));
?>

<div class="ui text container">
    <form id='sejoli-checkout-fisik' class="checkout-fisik" method="POST" action=''>
        <?php if(false !== $use_checkout_description) : ?>
        <div class='deskripsi-produk'>
            <?php echo apply_filters('the_content', carbon_get_post_meta($post->ID, 'checkout_product_description')); ?>
        </div>
        <?php endif; ?>
        <div class="detail-pesanan">
            <h3>Detail Pesanan</h3>
            <div class='data-holder'>
                <div class="ui fluid placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kode-diskon">
            <div class="data-holder">
                <div class="ui fluid placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="login">
            <div class="data-holder">
                <div class="ui fluid placeholder">
                    <div class="paragraph">
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tujuan-pengiriman">
            <h3>Tujuan Pengiriman</h3>
            <div class="ui form">
                <div class="field">
                    <input type="text" name="nama_penerima" id="nama_penerima" placeholder="Nama penerima">
                </div>
                <div class="field">
                    <p>
                        <strong><?php _e('PERHATIAN!', 'sejoli'); ?></strong><br />
                        <?php _e('Harap diisi dengan alamat yang lengkap dan jelas, agar memudahkan pengiriman', 'sejoli'); ?></p>
                    <textarea name="alamat_lengkap" id="alamat_lengkap" placeholder="Jln Kedondong, nomor C44 (Rumah hook)\nKomplek Pertamina Klayan \nCirebon \nJawa Barat \n45121"></textarea>
                </div>

                <?php if(true === boolval($product->shipping['active'])) : ?>
                <div class="field">
                    <p><?php _e('Pilih kecamatan pengiriman', 'sejoli'); ?></p>
                    <select name="kecamatan" id="kecamatan">
                        <option value="">Silakan Ketik Nama Kecamatannya</option>
                    </select>
                </div>
                <?php else : ?>
                <input type="hidden" name="kecamatan" value="-">
                <?php endif; ?>

                <?php if( false !== $product->form['email_field']) : ?>
                    <div class="field">
                        <input type="email" name="alamat_email" id="alamat_email" placeholder="Alamat Email">
                        <div class="alert-holder user-email-alert-holder"></div>
                    </div>
                <?php endif; ?>
                <div class="field">
                    <input type="text" name="nomor_telepon" id="nomor_telepon" placeholder="Nomor telepon">
                    <div class="alert-holder user-phone-alert-holder"></div>
                </div>
            </div>
        </div>
        <?php if(true === boolval($product->shipping['active'])) : ?>
        <div class="metode-pengiriman">
            <h3>Pilih Metode Pengiriman</h3>
            <div class="data-holder">
                <p>Pilih Kecamatan terlebih dulu</p>
            </div>
        </div>
        <?php else : ?>
            <input type="hidden" name="shipping_method" id="shipping_method" value="FREE:::FREE:::0" />
        <?php endif; ?>

        <div class="metode-pembayaran">
            <h3>Pilih Metode Pembayaran</h3>
            <div class="ui doubling grid data-holder">
                <div class="eight wide column">
                    <div class="ui placeholder">
                        <div class="paragraph">
                            <div class="line"></div>
                        </div>
                    </div>
                </div>
                <div class="eight wide column">
                    <div class="ui placeholder">
                        <div class="paragraph">
                            <div class="line"></div>
                        </div>
                    </div>
                </div>
                <div class="eight wide column">
                    <div class="ui placeholder">
                        <div class="paragraph">
                            <div class="line"></div>
                        </div>
                    </div>
                </div>
                <div class="eight wide column">
                    <div class="ui placeholder">
                        <div class="paragraph">
                            <div class="line"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="element-blockable">
            <div class="rincian-pesanan">
                <h3>Rincian Pesanan</h3>
                <table class="ui unstackable table">
                    <tbody>
                        <tr>
                            <td>
                                <div class="ui placeholder">
                                    <div class="paragraph">
                                        <div class="line"></div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="ui placeholder">
                                    <div class="paragraph">
                                        <div class="line"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Total Bayar</th>
                            <th><div class="total-holder">Rp 0</div></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="beli-sekarang">
                <button data-fb-pixel-event="<?php echo isset( $fb_pixel['links']['submit']['type'] ) ? $fb_pixel['links']['submit']['type'] : ''; ?>" type="submit" class="submit-button massive ui green button" disabled>PROSES SEKARANG</button>
            </div>
            <div class="affiliate-name"></div>
            <div class="alert-holder checkout-alert-holder"></div>
        </div>
    </form>
</div>
<script id="produk-dibeli-template" type="text/x-jsrender">
    {{if product}}
        <tr>
            <td>
                {{:product.title}}
                @{{:product.price}}
                {{if 1 < product.quantity}}
                x {{:product.quantity}} pcs
                {{/if}}
                <input type="hidden" id="product_id" name="product_id" value="{{:product.id}}">
            </td>
            <td>{{:product.subtotal}}</td>
        </tr>
    {{/if}}
    {{props variants}}
        <tr>
            <td>{{:prop.type}}: {{:prop.label}}</td>
            <td>{{:prop.price}}</td>
        </tr>
    {{/props}}
    {{if shipment}}
        <tr>
            <td>Biaya Pengiriman</td>
            <td>{{:shipment.value}}</td>
        </tr>
    {{/if}}
    {{if wallet}}
        <tr>
            <td>
                <?php _e('Dana di dompet yang anda gunakan', 'sejoli'); ?>
            </td>
            <td>{{:wallet}}</td>
        </tr>
    {{/if}}
    {{if coupon}}
        <tr>
            <td>
                Kode diskon: {{:coupon.code}}, <a class="hapus-kupon">Hapus kupon</a>
                <input type="hidden" id="coupon" name="coupon" value="{{:coupon.code}}">
            </td>
            <td>{{:coupon.value}}</td>
        </tr>
    {{/if}}
    {{if transaction}}
        <tr>
            <td>Biaya Transaksi</td>
            <td>{{:transaction.value}}</td>
        </tr>
    {{/if}}
</script>
<script id="metode-pembayaran-template" type="text/x-jsrender">
    {{if payment_gateway}}
        {{props payment_gateway}}
            <div class="eight wide column">
                <div class="ui radio checkbox {{if key == 0}}checked{{/if}}">
                    <input type="radio" name="payment_gateway" tabindex="0" class="change-calculate hidden" value="{{>prop.id}}" {{if key == 0}}checked="checked"{{/if}}>
                    <label><img src="{{>prop.image}}" alt="{{>prop.title}}"></label>
                </div>
            </div>
        {{/props}}
    {{/if}}
</script>
<script id="metode-pengiriman-template" type="text/x-jsrender">
    {{if shipping_methods.length > 0 }}
        <div class="ui form">
            <div class="field">
                <select name="shipping_method" id="shipping_method" class="change-calculate select2-filled">
                    <option value="">Pilih Metode Pengiriman</option>
                    {{props shipping_methods}}
                        <option value="{{>prop.id}}">{{>prop.title}}</option>
                    {{/props}}
                </select>
            </div>
        </div>
    {{else}}
        <p>Pilih Kecamatan terlebih dulu</p>
    {{/if}}
</script>
<script id="apply-coupon-template" type="text/x-jsrender">
    <?php if(false !== $product->form['coupon_field']) : ?>
    <div class="kode-diskon-form-toggle">
        <p><img src="<?php echo SEJOLISA_URL; ?>public/img/voucher.png"> Punya Kode Diskon ? <a>Klik Untuk Masukkan Kode</a></p>
    </div>
    <div class="kode-diskon-form">
        <h4>Kode Diskon</h4>
        <p>Masukkan kode jika anda punya</p>
        <div class="ui fluid action input">
            <input type="text" name="apply_coupon" id="apply_coupon" placeholder="Masukan kode diskon">
            <button type="submit" class="submit-coupon massive ui green button">APPLY</button>
        </div>
        <div class="alert-holder coupon-alert-holder"></div>
    </div>
    <?php endif; ?>
</script>
<script id="alert-template" type="text/x-jsrender">
    <div class="ui {{:type}} message">
        <i class="close icon"></i>
        <div class="header">
            {{:type}}
        </div>
        {{if messages}}
            <ul class="list">
                {{props messages}}
                    <li>{{>prop}}</li>
                {{/props}}
            </ul>
        {{/if}}
    </div>
</script>
<script id="detail-pesanan-template" type="text/x-jsrender">
    {{if product}}
        <div class="detail-pesanan-table">
            <table class="ui table">
                <tr>
                    {{if product.image}}
                        <td><img src="{{:product.image}}" alt=""></td>
                        <td>
                    {{else}}
                        <td colspan="2">
                    {{/endif}}
                            <h4>{{:product.title}}</h4>
                            <input type="hidden" id="product_id" name="product_id" value="{{:product.id}}">
                            <?php
                            $enable_quantity = carbon_get_post_meta( get_the_ID(), 'enable_quantity' );
                            if ( $enable_quantity ) :
                                ?>
                                <div class="ui labeled input">
                                    <label class='ui label' for='qty' style='line-height:150%'><?php _e('Jumlah pembelian', 'sejoli'); ?></label>
                                    <input type="number" class="qty change-calculate-affect-shipping" name="qty" id="qty" value="1" min="1" placeholder="Qty">
                                    <!-- <span class="stock">stock tersisa <span class="stock-value">{{:product.stock}}</span> pcs</span> -->
                                </div>
                                <?php
                            else : ?>
                            <input type="hidden" class="qty change-calculate-affect-shipping" name="qty" id="qty" value="1" placeholder="Qty">
                            <?php endif; ?>
                        </td>
                        <td class='product-price'>
                            <h4>Harga per pcs</h4>
                            {{:product.price}}
                        </td>
                </tr>
                <?php do_action('sejoli/checkout-template/after-product', $product); ?>
            </table>
        </div>
        {{if product.variation !== '' }}
            <div class="detail-variation">
                <div class="ui form">
                    <div class="ui stackable grid">
                        {{props product.variation}}
                            <div class="eight wide column">
                                <div class="{{if prop.required}}required{{/if}} field">
                                    <label>{{:prop.label}}</label>
                                    <select name="variants[{{>key}}]" class="select2-filled variations-select2 change-calculate-affect-shipping" placeholder="Pilih {{:prop.label}}">
                                        <option value="">Pilih {{:prop.label}}</option>
                                        {{props prop.options}}
                                            <option value="{{>key}}">
                                                {{:prop.label}}
                                                {{if prop.price }}
                                                (+ {{:prop.price}} )
                                                {{/if}}
                                            </option>
                                        {{/props}}
                                    </select>
                                </div>
                            </div>
                        {{/props}}
                    </div>
                </div>
            </div>
        {{/if}}
        {{if product.fields.note_field === true }}
        <div class='catatan-pemesanan'>
            <div class='ui form'>
                <div class="field">
                    <label>Catatan pemesanan</label>
                    {{if product.fields.note_placeholder !== ''}}
                    <p>{{:product.fields.note_placeholder}}</p>
                    {{/if}}
                    <textarea id='order-note' name='meta_data[note]' placeholder="<?php echo carbon_get_post_meta($post->ID, 'note_field_placeholder_text'); ?>"></textarea>
                </div>
            </div>
        </div>
        {{/if}}
    {{/if}}
</script>

<!-- LOGIN TEMPLATE -->
<script id="login-template" type="text/x-jsrender">
    {{if current_user.id}}
        <div class="login-welcome">
            <p>Hai, kamu akan order menggunakan akun <span class="name">{{:current_user.name}}</span>, <a href="<?php echo wp_logout_url( get_permalink() ); ?>">Logout</a></p>
        </div>
    {{else}}
    <?php if(false !== $product->form['login_field']) : ?>
        <div class="login-form-toggle">
            <p>Sudah mempunyai akun ? <a>Login</a></p>
        </div>
        <div class="ui form login-form">
            <h3>Login</h3>
            <div class="required field">
                <label>Alamat Email</label>
                <input type="email" name="login_email" id="login_email" placeholder="Masukan alamat email">
            </div>
            <div class="required field">
                <label>Password</label>
                <input type="password" name="login_password" id="login_password" placeholder="Masukan password anda">
            </div>
            <button type="submit" class="submit-login massive right ui fluid green button">LOGIN</button>
            <div class="alert-holder login-alert-holder"></div>
        </div>
    <?php endif; ?>
    {{/if}}
</script>

<script type='text/javascript'>
jQuery(document).ready(function($){

    sejoliSaCheckoutFisik.init();

    var textAreas = document.getElementsByTagName('textarea');

    Array.prototype.forEach.call(textAreas, function(elem) {
        elem.placeholder = elem.placeholder.replace(/\\n/g, '\n');
    });
});
</script>
<?php
include 'footer-secure.php';
include 'footer.php';
