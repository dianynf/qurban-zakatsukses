<?php sejoli_header(); ?>
    <h2 class="ui header">Affiliasi Link</h2>
    <form id="affiliate-link-generator" class="ui form">
        <div class="ui fluid action input">
            <select id="product_id" name="product_id" class="ui fluid dropdown">
                <option value=""><?php _e( 'Pilih Produk', 'sejoli' ); ?></option>
            </select>
            <button id="affiliate-link-generator-button" class="ui primary button">
                <?php _e( 'Generate', 'sejoli' ); ?>
            </button>
        </div>
    </form><br>
    <div id="affiliate-link-holder">
        <div class="ui info message"><?php _e( 'Silahkan pilih produk', 'sejoli' ); ?></div>
    </div>
    <form id="aff-link-parameter" class="ui form">
        <h3><?php _e('Tambah Parameter ke Link','sejoli'); ?></h3>
        <div class="required field">
            <label><?php _e('Platform','sejoli'); ?></label>
            <select name="param-platform" name="param-platform" id="param-platform" class="select2-filled" required>
                <option value=""><?php _e('--Pilih Platform--','sejoli'); ?></option>
                <?php
                $platforms = sejolisa_get_acquisition_platforms();
                foreach ( $platforms as $key => $value ) :
                    ?>
                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
        <div class="required field">
            <label><?php _e('ID','sejoli'); ?></label>
            <input type="text" name="param-id" id="param-id" placeholder="<?php _e('ID','sejoli'); ?>" required>
        </div>
        <button class="ui blue button" type="submit"><?php _e('Submit','sejoli'); ?></button>
    </form>
    <script id="affiliate-link-tmpl" type="text/x-jsrender">
    {{props data}}
        <div class='field'>
            <label for="aff-link-{{:key}}"><b>{{:prop.label}}</b></label>
            <p>{{:prop.description}}</p>
            <div class="ui fluid action input">
                <input id="aff-link-{{:key}}" name="aff-link-{{:key}}" type="text" value="{{:prop.affiliate_link}}" readonly>
                <button class="ui teal right labeled icon button copy-btn" data-clipboard-target="#aff-link-{{:key}}"><i class="copy icon"></i> <?php _e( 'Copy', 'sejoli' ); ?></button>
            </div>
        </div>
    {{/props}}
    </script>
    <script>
    (function( $ ) {
        'use strict';
        $(document).ready(function(){
            sejoli.affiliate.link.init();
        });
    })( jQuery );
    </script>
<?php sejoli_footer(); ?>
