<div class="ui icon <?php echo $vars['message']['style']; ?> message">

    <?php if(!empty($vars['message']['icon'])) : ?>
    <i class="<?php echo $vars['message']['icon']; ?> icon"></i>
    <?php endif; ?>

    <div class="content">
        <div class="header">
            <?php echo $vars['message']['title']; ?>
        </div>
        <?php echo wpautop($vars['message']['content']); ?>
    </div>
</div>
