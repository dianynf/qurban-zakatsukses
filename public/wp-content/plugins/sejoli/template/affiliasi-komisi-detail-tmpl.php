<i class="close icon"></i>
<div class="header"><?php _e( 'Komisi Detail', 'sejoli' ); ?></div>
<div class="content">
    <table class="ui striped very basic table">
        <tbody>
            <tr>
                <td><b><?php _e( 'Tanggal', 'sejoli' ); ?></b></td>
                <td>{{:date}}</td>
            </tr>
            <tr>
                <td><b><?php _e( 'Produk', 'sejoli' ); ?></b></td>
                <td>{{:product}}</td>
            </tr>
            <tr>
                <td><b><?php _e( 'Jumlah', 'sejoli' ); ?></b></td>
                <td>{{:amount}}</td>
            </tr>
            <tr>
                <td><b><?php _e( 'Tipe', 'sejoli' ); ?></b></td>
                <td>{{:type}}</td>
            </tr>
            <tr>
                <td><b><?php _e( 'Catatan', 'sejoli' ); ?></b></td>
                <td>{{:note}}</td>
            </tr>
        </tbody>
    </table>
</div>