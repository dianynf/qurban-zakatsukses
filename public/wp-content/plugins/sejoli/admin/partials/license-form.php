<div class="wrap">
    <h1>Form input lisensi - Sejoli</h1>
    <p>
        Gunakan lisensi yang sah agar bisnis anda berkah
    </p>
    <form class="" action="" method="post">
        <table class='form-table' role='presentation'>
            <tbody>
                <tr>
                    <th scope='row'>
                        Email
                    </th>
                    <td>
                        <input type="email" name="data[user_email]" value="" class='regular-text' required />
                        <p class="description" id="tagline-description">Diisi dengan email yang anda gunakan di sejoli.id</p>
                    </td>
                </tr>
                <tr>
                    <th scope='row'>
                        Password
                    </th>
                    <td>
                        <input type="text" name="data[user_pass]" value="" class='regular-text' required />
                        <p class="description" id="tagline-description">Diisi dengan pass yang anda gunakan di sejoli.id</p>
                    </td>
                </tr>
                <tr>
                    <th scope='row'>
                        Kode Lisensi
                    </th>
                    <td>
                        <input type="text" name="data[license]" value="" class='regular-text' required />
                        <p class="description" id="tagline-description">Diisi dengan kode lisensi yang anda dapatkan</p>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class='submit'>
            <button type="submit" name="button" class='button button-primary'><?php _e('Cek kode lisensi', 'sejoli'); ?></button>
        </p>
        <?php wp_nonce_field('sejoli-input-license'); ?>
    </form>
</div>
