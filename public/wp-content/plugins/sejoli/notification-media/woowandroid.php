<?php

namespace SejoliSA\NotificationMedia;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class WooWandroid extends WhatsApp {
    /**
     * Construction
     */
    public function __construct() {
        add_filter('sejoli/whatsapp/setup-fields', [$this, 'setup_fields'], 1);
    }

    /**
     * Get name of service
     * @return string
     */
    public function get_label() {
        return 'woowandroid';
    }

    /**
     * Add setup fields to whatsapp fields
     * Hooked via filter sejoli/whatsapp/setup-fields, priority 1
     * @since   1.0.0
     * @param   array  $fields
     * @return  array
     */
    public function setup_fields(array $fields) {

        $setup_fields = [
            Field::make('text', 'woowandroid_api_key',  __('Woowandroid CS ID', 'sejoli'))
                ->set_required(true)
                ->set_conditional_logic([
                    [
                        'field' => 'notification_whatsapp_service',
                        'value' => 'woowandroid'
                    ]
                ])
        ];

        return array_merge($fields, $setup_fields);
    }

    /**
     * Send content
     * @since   1.0.0
     * @param   array  $recipients
     * @param   string $content
     * @param   string $title
     * @return  void
     */
    public function send(array $recipients, $content, $title = '', $recipient_type = 'buyer') {

        $api_key = carbon_get_theme_option('woowandroid_api_key');

        foreach($recipients as $recipient) :

            $phone_number = str_replace('', '+', apply_filters('sejoli/user/phone', $recipient));

            if(empty($phone_number)) :
                continue;
            endif;

            do_action('sejoli/log/write', 'prepare woowandroid', ['phone_number' => $phone_number, 'content' => $content]);

            $post_data = array(
                'app_id'             => '429d3472-da0f-4b2b-a63e-4644050caf8f',
                'include_player_ids' => array($api_key),
                'data'               => array(
                    'type'    => 'Notification',
                    'message' => $content,
                    'no_wa'   => $phone_number
                ),
                'contents'  => array(
                    'en'    => 'Sejoli Notification'
                ),
                'headings'  => array(
                    'en'    => 'Sejoli Notification'
                )
            );

            $response = wp_safe_remote_post('https://onesignal.com/api/v1/notifications',[
                'headers'   => array(
                    'Authorization' => 'Basic NjY0NzE3MTYtMzc3ZC00YmY5LWJhNzQtOGRiMWM1ZTNhNzBh',
                    'Content-Type'  => 'application/json; charset=utf-8'
                ),
                'body' => json_encode($post_data)
            ]);

            do_action('sejoli/log/write', 'response woowandroid', (array) wp_remote_retrieve_body($response));

            sleep ( 2 );

        endforeach;
    }
}
