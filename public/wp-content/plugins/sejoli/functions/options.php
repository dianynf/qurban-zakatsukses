<?php

use Nahid\JsonQ\Jsonq;

/**
 * Get available payment options
 * @since   1.0.0
 * @return  array
 */
function sejolisa_get_payment_options() {
    return apply_filters('sejoli/payment/payment-options', []);
}

/**
 * Get available district options
 * @since   1.0.0
 * @return  array
 */
function sejolisa_get_district_options($term = '') {

    $subdistrict_file = SEJOLISA_DIR . 'json/subdistrict.json';

    $jsonq = new Jsonq($subdistrict_file);

    $search = sanitize_text_field( ucwords( $term ) );
    $subdistricts = $jsonq->where('subdistrict_name', 'contains' , $search )->get();

    $response = [
        'results' => []
    ];

    foreach ( $subdistricts as $key => $value ) :
        $response['results'][] = [
            'id' => $value['subdistrict_id'],
            'text' => $value['type'].' '.$value['city'].' - '.$value['subdistrict_name'],
        ];
    endforeach;

    return $response;
}

function sejolisa_get_district_options_by_ids( array $ids ) {

    $subdistrict_file = SEJOLISA_DIR . 'json/subdistrict.json';

    $jsonq = new Jsonq($subdistrict_file);

    $subdistricts = $jsonq->where('subdistrict_id', 'in' , $ids )->get();

    $response = [
        'results' => []
    ];

    foreach ( $subdistricts as $key => $value ) :
        $response['results'][] = [
            'id' => $value['subdistrict_id'],
            'text' => $value['type'].' '.$value['city'].' - '.$value['subdistrict_name'],
        ];
    endforeach;

    return $response;
}
