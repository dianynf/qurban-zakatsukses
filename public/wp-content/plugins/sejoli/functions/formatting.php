<?php
/**
 * Extend strpos function to enable needle as array
 * @param  string   $haystack [description]
 * @param  mixed    $needle   [description]
 * @param  integer  $offset   [description]
 * @return boolean
 */
function sejolisa_strpos_array($haystack,$needle,$offset = 0)
{
    if(!is_array($needle)) :
        $needle = array($needle);
    endif;

    foreach($needle as $query) :
        if(false !== strpos($haystack, $query, $offset)) :
             return true;
        endif;
    endforeach;

    return false;
}

/**
 * Set price format
 * @since   1.0.0
 * @param   float|integer    $price
 * @param   string           $currency
 * @param   string           $thousand
 * @return  string
 */
function sejolisa_price_format($price, $currency = 'Rp. ', $thousand = '.') {
    $prefix = (0 > $price) ? '-' : '';
    return $prefix.' '.$currency . number_format(absint($price), 0, ',', '.');
}

/**
 * Set coloring unique number
 * @since   1.0.0
 * @param   string    $price
 */
function sejolisa_coloring_unique_number($price) {

    $price_ = '';
    $price_arr = explode('.',$price);
    $price_arr_count = count($price_arr);
    $price_arr_count_ = $price_arr_count-1;

    if ( $price_arr_count > 2 ) :
        foreach ( $price_arr as $key => $value ) :
            if ( 0 === $key ) :
                $price_ .= $value;
            elseif ( $price_arr_count_ === $key ):
                $price_ .= ".<span class='sejoli-unique-number'>".$value."</span>";
            else:
                $price_ .= '.'.$value;
            endif;
        endforeach;
    else:
        $price_ = $price;
    endif;

    return $price_;

}

if(!function_exists('sejolisa_get_sensored_string')) :

/**
 * Change all chars except first and last
 * @since   1.3.3
 * @param  string   $string           Given string
 * @param  string   $replace_char     Characater that will replace
 * @return string   String that has been replaced
 */
function sejolisa_get_sensored_string(string $string, $replace_char = '*') {

    $words = explode(' ', $string);

    foreach($words as $i => $word) :

        $length    = strlen($word);
        $words[$i] = substr($word, 0, 1).str_repeat('*', $length - 2).substr($word, $length - 1, 1);

    endforeach;

    return implode(' ', $words);
}

endif;
