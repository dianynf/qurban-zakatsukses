<?php

namespace SejoliSA\Front;

class Checkout
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Disable checkout page
	 * @since 	1.1.6
	 * @access 	protected
	 * @var 	boolean
	 */
	protected $disable_checkout = false;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * If current page is loading page
	 * @since	1.3.2
	 * @var 	boolean
	 */
	protected $is_loading_page = false;

	/**
	 * If current page is thank you page
	 * @since	1.3.2
	 * @var 	boolean
	 */
	protected $is_thankyou_page = false;

	/**
	 * If current page is renew order page
	 * @since	1.3.2
	 * @var 	boolean
	 */
	protected $is_renew_order_page = false;

	/**
	 * Current order
	 * @since 	1.3.2
	 * @var 	false|array
	 */
	protected $current_order = false;

    /**
     * enqueue scripts
     * hooked via action wp_enqueue_scripts
     *
     * @return void
     */
    public function enqueue_scripts()
    {
        // register css
        // wp_register_style( '', '',[],'','all');
        wp_register_style( 'select2', 			'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/css/select2.min.css',	[],'','all');
        wp_register_style( 'google-font', 		'https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap',			[],'','all');
        wp_register_style( 'semantic-ui', 		'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css',[],'2.4.1','all');
        wp_register_style( 'flipclock', 		'https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.css',	[],'0.7.8','all');
        wp_register_style( 'sejoli-checkout', 	SEJOLISA_URL.'public/css/sejoli-checkout.css',								[],$this->version,'all');

        // register js
        // wp_register_script( '', '',['jquery'],'',false);
        wp_register_script( 'select2', 				'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.9/js/select2.min.js',['jquery'],'',false);
        wp_register_script( 'blockUI', 				'https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js',['jquery'],'2.70',false);
        wp_register_script( 'jsrender', 			'https://cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.4/jsrender.min.js',['jquery'],'1.0.4',false);
        wp_register_script( 'semantic-ui', 			'https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.js',['jquery'],'2.4.1',false);
        wp_register_script( 'flipclock', 			'https://cdnjs.cloudflare.com/ajax/libs/flipclock/0.7.8/flipclock.min.js',['jquery'],'0.7.8',false);
		wp_register_script( 'sejoli-public', 		SEJOLISA_URL. 'public/js/sejoli-public.js', 		['jquery'], $this->version, false );
        wp_register_script( 'sejoli-checkout', 		SEJOLISA_URL. 'public/js/sejoli-checkout.js',	  	['jquery'], 	$this->version,false);
		wp_register_script( 'sejoli-checkout-renew',SEJOLISA_URL. 'public/js/sejoli-checkout-renew.js',	['jquery', 'sejoli-checkout'],	$this->version,false);

        if ( sejolisa_is_checkout_page() ) :

            // load css
            wp_enqueue_style('select2');
            wp_enqueue_style('google-font');
            wp_enqueue_style('semantic-ui');
            wp_enqueue_style('flipclock');
            wp_enqueue_style('sejoli-checkout');

            // load js
            wp_enqueue_script('jquery');
            wp_enqueue_script('select2');
            wp_enqueue_script('blockUI');
            wp_enqueue_script('jsrender');
            wp_enqueue_script('semantic-ui');
            wp_enqueue_script('flipclock');
            wp_enqueue_script('sejoli-checkout');
            wp_localize_script('sejoli-checkout', 'sejoli_checkout', [
                'product_id' => get_the_ID(),
                'ajax_url'   => site_url('/'),
                'ajax_nonce' => [
                    'get_calculate'        => wp_create_nonce('sejoli-checkout-ajax-get-calculate'),
                    'get_payment_gateway'  => wp_create_nonce('sejoli-checkout-ajax-get-payment-gateway'),
                    'apply_coupon'         => wp_create_nonce('sejoli-checkout-ajax-apply-coupon'),
                    'submit_checkout'      => wp_create_nonce('sejoli-checkout-ajax-submit-checkout'),
                    'submit_login'         => wp_create_nonce('sejoli-checkout-ajax-submit-login'),
                    'get_current_user'     => wp_create_nonce('sejoli-checkout-ajax-get-current-user'),
                    'delete_coupon'        => wp_create_nonce('sejoli-checkout-ajax-delete-coupon'),
                    'loading'              => wp_create_nonce('sejoli-checkout-ajax-loading'),
                    'confirm'              => wp_create_nonce('sejoli-checkout-ajax-confirm'),
                    'get_shipping_methods' => wp_create_nonce('sejoli-checkout-ajax-get-shipping-methods'),
                    'get_subdistrict'      => wp_create_nonce('sejoli-checkout-ajax-get-subdistrict'),
                    'check_user_email'     => wp_create_nonce('sejoli-checkout-ajax-check-user-email'),
                    'check_user_phone'     => wp_create_nonce('sejoli-checkout-ajax-check-user-phone'),
                ]
            ]);

        endif;

		if( sejolisa_verify_checkout_page('renew') ) :

			wp_enqueue_script( 	'sejoli-checkout-renew' );

			wp_localize_script(	'sejoli-checkout-renew', 'sejoli_checkout_renew', array(
				'order_id'	=> intval($_GET['order_id']),
				'ajax_nonce' => [
                    'get_calculate'        => wp_create_nonce('sejoli-checkout-renew-ajax-get-calculate'),
                    'get_payment_gateway'  => wp_create_nonce('sejoli-checkout-renew-ajax-get-payment-gateway'),
                    'apply_coupon'         => wp_create_nonce('sejoli-checkout-renew-ajax-apply-coupon'),
                    'submit_checkout'      => wp_create_nonce('sejoli-checkout-renew-ajax-submit-checkout'),
                    'delete_coupon'        => wp_create_nonce('sejoli-checkout-renew-ajax-delete-coupon')

				]
			));
		endif;
    }

    /**
     * Replace single product template with current sejoli
     * Hooked via filter single_template, priority 100
     * @since 	1.0.0
     * @param 	string 	$template	Single template file
     * @return 	string	Modified single template file
     */
    public function set_single_template( $template )
    {
        global $post;

        if ( $post->post_type === 'sejoli-product' ) :

			if ( $post->_product_type === 'digital' ) :
                $file_name = 'checkout.php';
            else:
                $file_name = 'checkout-fisik.php';
            endif;

            if ( file_exists( SEJOLISA_DIR . 'template/checkout/'.$file_name ) ) :
                $template = SEJOLISA_DIR . 'template/checkout/'.$file_name;
            endif;

        endif;

        return $template;
    }

	/**
	 * Display checkout header
	 * @since 	1.0.0
	 * @return 	void
	 */
	public function display_checkout_header() {
		sejoli_get_template_part( 'checkout/partials/header.php');
	}

	/**
	 * Display checkout footer
	 * @since 	1.0.0
	 * @return 	void
	 */
	public function display_checkout_footer() {
		sejoli_get_template_part( 'checkout/partials/footer.php');
	}

	/**
	 * Set checkout respond
	 * @since 	1.4.0
	 * @param 	array 		$respond
	 * @param 	array 		$calculate
	 * @param 	integer 	$quantity
	 * @param 	string 		$coupon
	 * @param 	boolean 	$is_coupon
	 */
	protected function set_checkout_respond($respond, $calculate = array(), $quantity = 1, $coupon = NULL, $is_coupon = false) {

		$prepare_calculate = array();

		if(
			isset($respond['cart_detail']) &&
			is_array($respond['cart_detail'])
		) :

			foreach ( $respond['cart_detail'] as $key => $value ) :

				if ( strpos($key, 'variant-') !== false ) :

					$prepare_calculate['variants'][] = [
						'type'  => ucwords($value['type']),
						'label' => $value['label'],
						'price' => sejolisa_price_format($value['raw_price'] * $quantity)
					];

				endif;

			endforeach;

		endif;

		if ( isset( $respond['cart_detail']['subscription'] ) ) :
			$prepare_calculate['subscription'] = $respond['cart_detail']['subscription'];
		endif;

		if ( isset( $respond['cart_detail']['transaction_fee'] ) ) :
			$prepare_calculate['transaction']['value'] = sejolisa_price_format( $respond['cart_detail']['transaction_fee'] );
		endif;

		if ( isset( $respond['cart_detail']['shipment_fee'] ) ) :
			$prepare_calculate['shipment']['value'] = sejolisa_price_format( $respond['cart_detail']['shipment_fee'] );
		endif;

		if ( isset( $respond['cart_detail']['coupon_value'] ) && !empty($coupon)) :
			$prepare_calculate['coupon']['code'] = $coupon;
			$prepare_calculate['coupon']['value'] = sejolisa_price_format( $respond['cart_detail']['coupon_value'] );
		endif;

		if ( isset( $respond['cart_detail']['wallet'] ) ) :
			$prepare_calculate['wallet']	=  '-' . sejolisa_price_format( $respond['cart_detail']['wallet']);
		endif;

		if(false !== $is_coupon) :
			$calculate['data'] = array_merge($calculate['data'], $prepare_calculate);
		else :
			$calculate = array_merge($calculate, $prepare_calculate);
		endif;

		return $calculate;
	}

	/**
	 * Set close template
	 * Hooked via single_template, priority 9999
	 * @since 	1.0.0
	 * @since 	1.4.1 	Add condition for only-group buy
	 * @param 	string $template
	 * @return 	string
	 */
	public function set_close_template($template) {

		global $post;

		if ( $post->post_type === 'sejoli-product' )  :

			if( true === $this->disable_checkout ) :
				$template = SEJOLISA_DIR . 'template/checkout/close.php';
			else :

				$response = sejolisa_check_user_permission_by_product_group($post->ID);

				if(false === boolval($response['allow'])) :
					$template = SEJOLISA_DIR . 'template/checkout/restricted.php';
				endif;
			endif;

		endif;

		return $template;
	}

	/**
	 * Disable checkout page
	 * Hooked via action template_redirect, priority 999
	 * @since 	1.0.0
	 * @return 	void
	 */
    public function close_checkout()
    {
        if ( is_singular('sejoli-product') ) :

			global $post;

			$this->disable_checkout = sejolisa_is_product_closed($post->ID);

        endif;

    }

    /**
     * sejoli get calculate by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function get_calculate_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-get-calculate' ) ) :

            $request = wp_parse_args( $_POST,[
                'product_id'      => 0,
                'coupon'          => NULL,
                'quantity'        => 1,
                'type'            => 'regular',
                'payment_gateway' => 'manual',
                'shipment'        => NULL,
                'variants'        => [],
				'wallet'		  => false,
            ]);

            $response = [];

            if ( $request['product_id'] > 0 ) :

                do_action('sejoli/frontend/checkout/calculate', $request);

                $response['calculate'] = sejolisa_get_respond('calculate');

            endif;

            wp_send_json($response);

        endif;
    }

	/**
     * Renew calculate renew by ajax
     * Hooked via action parse_request
     * @since 	1.1.9
     * @return 	json
     */
    public function get_renew_calculate_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-renew-ajax-get-calculate' ) ) :

            $request = wp_parse_args( $_POST,[
				'order_id'		  => NULL,
                'product_id'      => NULL,
                'coupon'          => NULL,
                'quantity'        => 1,
                'type'            => 'regular',
                'payment_gateway' => 'manual',
                'shipment'        => NULL,
                'variants'        => [],
            ]);

            $calculate = $response = [];

            if (
				!empty( $request['product_id'] )  &&
				!empty( $request['order_id'] )
			) :

                do_action('sejoli/checkout/calculate-renew', $request);

                $calculation = sejolisa_get_respond('total');

				if(false !== $calculation['valid']) :

					$product = sejolisa_get_product( $request['product_id'] );

		        	if ( is_a( $product, 'WP_Post' ) ) :

			            $quantity            = intval($calculation['detail']['quantity']);
			            $product_price       = $calculation['cart_detail']['subscription']['regular']['raw'];
			            $product_total_price = $quantity * $product_price;

			            $calculate = [
			                'product' => [
			                    'id'        => $product->ID,
			                    'image'     => get_the_post_thumbnail_url($product->ID, 'full'),
			                    'title'     => sprintf( __('Perpanjangan order INV %s<br /> Produk: %s', 'sejoli'), $request['order_id'], $product->post_title),
			                    'price'     => sejolisa_price_format( $product_price ),
			                    'stock'     => 0,
			                    'variation' => NULL,
			                    'quantity'  => $calculation['detail']['quantity'],
			                    'subtotal'  => sejolisa_price_format( $product_total_price ),
								'fields'	=> $product->form
			                ],

			                'affiliate' => $calculation['affiliate'],
			                'total'     => sejolisa_coloring_unique_number( sejolisa_price_format( $calculation['total'] ) ),
							'raw_total' => floatval($calculation['total'])
			            ];

			            if ( isset( $calculation['cart_detail']['subscription'] ) ) :
			                $calculate['subscription'] = $calculation['cart_detail']['subscription'];
			            endif;

			            if ( isset( $calculation['cart_detail']['transaction_fee'] ) ) :
			                $calculate['transaction']['value'] = sejolisa_price_format( $calculation['cart_detail']['transaction_fee'] );
			            endif;

			            if ( isset( $calculation['cart_detail']['coupon_value'] ) ) :
			                $calculate['coupon']['code'] = $request['coupon'];
			                $calculate['coupon']['value'] = sejolisa_price_format( $calculation['cart_detail']['coupon_value'] );
			            endif;

					endif;

		        endif;

            endif;

            wp_send_json(array('calculate' => $calculate));

        endif;
    }

    /**
     * setup calculate data
     * hooked via action sejoli/frontend/checkout/calculate
     *
     * @return void
     */
    public function calculate( $request ) {
        $calculate = [];

        $product = sejolisa_get_product( $request['product_id'] );

        if ( is_a( $product, 'WP_Post' ) ) :

            do_action('sejoli/checkout/calculate', $request);

            $respond = sejolisa_get_respond('total');

            $variation = '';

            if ( !empty( $product->variants ) ) :
                $variation = $product->variants;
            endif;

            $quantity            = intval($respond['detail']['quantity']);
			$coupon 			 = (array_key_exists('coupon', $request)) ? $request['coupon'] : NULL;
            $product_price       = $product->price;
            $product_total_price = $quantity * $product->price;

            $calculate = [
                'product' => [
                    'id'        => $product->ID,
                    'image'     => get_the_post_thumbnail_url($product->ID,'full'),
                    'title'     => $product->post_title,
                    'price'     => sejolisa_price_format( $product_price ),
                    'stock'     => 0,
                    'variation' => $variation,
                    'quantity'  => $quantity,
                    'subtotal'  => sejolisa_price_format( $product_total_price ),
					'fields'	=> $product->form
                ],
                'affiliate' => $respond['affiliate'],
                'total'     => sejolisa_coloring_unique_number( sejolisa_price_format( $respond['total'] ) ),
				'raw_total' => floatval($respond['total'])
            ];

			$calculate = $this->set_checkout_respond($respond, $calculate, $quantity, $coupon);

        endif;

        sejolisa_set_respond( $calculate, 'calculate' );

    }

    /**
     * sejoli check user email by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function check_user_email_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-check-user-email' ) ) :

            $request = wp_parse_args( $_POST,[
                'email'      => '',
            ]);

            if ( is_email( $request['email'] ) ) :

                $user = sejolisa_get_user( $request['email'] );

                if ( is_a($user,'WP_User') && $user->ID > 0 ) :

                    wp_send_json_error([__('Alamat Email sudah terdaftar silahkan login menggunakan akun anda','sejoli')]);

                endif;

            endif;

            wp_send_json_success();

        endif;
    }

    /**
     * sejoli check user phone by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function check_user_phone_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-check-user-phone' ) ) :

            $request = wp_parse_args( $_POST,[
                'phone'      => '',
            ]);

            if ( ! empty( $request['phone'] ) ) :

                $user = sejolisa_get_user( $request['phone'] );

                if ( is_a($user,'WP_User') && $user->ID > 0 ) :

                    wp_send_json_error([__('No Handphone sudah terdaftar silahkan login menggunakan akun anda','sejoli')]);

                endif;

            endif;

            wp_send_json_success();

        endif;
    }

    /**
     * sejoli get payment gateway by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function get_payment_gateway_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-get-payment-gateway' ) ) :

            $request = wp_parse_args( $_POST, []);

            $response = [];

            $response['payment_gateway'] = apply_filters('sejoli/frontend/checkout/payment-gateway', [], $request );

            wp_send_json($response);

        endif;
    }

    /**
     * setup payment gateway data
     * hooked via filter sejoli/frontend/checkout/payment-gateway
     *
     * @return void
     */
    public function payment_gateway( $payment_gateway, $request )
    {

        $payment_gateway = [];

        $payment_options = sejolisa_get_payment_options();

        foreach ( $payment_options as $key => $value ) :

            $payment_gateway[] = [
                'id' => $key,
                'title' => $value['label'],
                'image' => $value['image'],
            ];

        endforeach;

        return $payment_gateway;

    }

    /**
     * sejoli apply coupon by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function apply_coupon_by_ajax()
    {
		$request = NULL;

		// Ordinary checkout
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-apply-coupon' ) ) :

            $request = wp_parse_args( $_POST,[
                'product_id'      => 0,
                'coupon'          => NULL,
                'quantity'        => 1,
                'type'            => 'regular',
                'payment_gateway' => 'manual',
                'shipment'        => NULL,
				'calculate'		  => 'default'
            ]);

		elseif ( sejoli_ajax_verify_nonce( 'sejoli-checkout-renew-ajax-apply-coupon' ) ) :

			$request = wp_parse_args( $_POST,[
				'order_id'		  => 0,
                'product_id'      => 0,
                'coupon'          => NULL,
                'quantity'        => 1,
				'calculate'		  => 'renew'
            ]);

        endif;

		if(is_array($request) && !empty($request['coupon']) ) :

			$coupon = sejolisa_get_coupon_by_code($request['coupon']);

			if ( $coupon['valid'] ) :

				do_action('sejoli/frontend/checkout/apply-coupon', $coupon, $request);

				$response = sejolisa_get_respond('apply-coupon');

			else :

				$response = [
					'valid' => false,
					'messages' => [__('Kode kupon tidak valid')]
				];

			endif;

			wp_send_json($response);

		endif;
    }

    /**
     * sejoli apply coupon
     * hooked via action sejoli/frontend/checkout/apply-coupon
     *
     * @return json
     */
    public function apply_coupon( $coupon, $request )
    {
		if(!isset($request['calculate']) || 'default' === $request['calculate']) :
        	do_action('sejoli/checkout/calculate', $request);
		else :
			do_action('sejoli/checkout/calculate-renew', $request);
		endif;

        $respond = sejolisa_get_respond('total');

        if (
			isset( $respond['messages']['warning'] ) &&
            is_array( $respond['messages']['warning'] ) &&
			0 < count( $respond['messages']['warning'] ) &&
			empty( $respond['messages']['info'] )
		) :
            $respond = [
                'valid' => false,
                'messages' => $respond['messages']['warning'],
            ];

			sejolisa_set_respond($respond, 'apply-coupon');

        else:

            $product = sejolisa_get_product( $request['product_id'] );

            $discount_value = $coupon['coupon']['discount']['value'];

            if ( $coupon['coupon']['discount']['type'] === 'percentage' ) :
                $discount_value = ( $discount_value / 100 ) * $product->price;
            endif;

            $thumbnail_url = get_the_post_thumbnail_url($product->ID,'full');
            if ( $thumbnail_url === false ) :
                $thumbnail_url = '';
            endif;

			$quantity            = intval($respond['detail']['quantity']);
			$product_price       = isset($respond['cart_detail']['subscription']['regular']['raw']) ?
									$respond['cart_detail']['subscription']['regular']['raw'] : $product->price;
			$product_total_price = $quantity * $product_price;

            $calculate = [
                'valid'    => true,
                'messages' => $respond['messages']['info'],
                'data'     => [
                    'affiliate' => $respond['affiliate'],
                    'product'   => [
                        'id'    => $product->ID,
                        'image' => $thumbnail_url,
                        'title' =>
							(!isset($request['calculate']) || 'default' === $request['calculate']) ?
							$product->post_title :
							sprintf( __('Perpanjangan order INV %s<br /> Produk: %s', 'sejoli'), $request['order_id'], $product->post_title),
                        'price' => sejolisa_price_format( $product_total_price ),
                    ],
                    'coupon' => [
                        'code'  => $coupon['coupon']['code'],
                        'value' => sejolisa_price_format($discount_value),
                    ],
                    'total' 	=> sejolisa_coloring_unique_number( sejolisa_price_format( $respond['total'] ) ),
					'raw_total' => floatval( $respond['total'])
                ]
            ];

			$calculate = $this->set_checkout_respond($respond, $calculate, $quantity, $request['coupon'], true);

			sejolisa_set_respond($calculate, 'apply-coupon');

        endif;

    }

    /**
     * sejoli submit checkout by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function submit_checkout_by_ajax()
    {
		$request = NULL;

        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-submit-checkout' ) ) :

            $request = wp_parse_args($_POST,[
                'user_id'         => NULL,
                'affiliate_id'    => NULL,
                'coupon'          => NULL,
                'payment_gateway' => 'manual',
                'quantity'        => 1,
                'user_email'      => NULL,
                'user_name'       => NULL,
                'user_password'   => NULL,
                'user_phone'      => NULL,
                'district_id'     => NULL,
                'shipment'        => NULL,
				'wallet'		  => NULL,
                'product_id'      => 0,
                'variants'        => [],
				'other'			  => [],
            ]);

			$checkout_type = 'default';

		elseif ( sejoli_ajax_verify_nonce( 'sejoli-checkout-renew-ajax-submit-checkout' ) ) :

			$request = wp_parse_args($_POST,[
                'user_id'         => NULL,
                'affiliate_id'    => NULL,
                'coupon'          => NULL,
				'wallet'		  => NULL,
                'payment_gateway' => 'manual',
                'quantity'        => 1,
                'product_id'      => 0,
				'order_id'        => 0
            ]);

			$checkout_type = 'renew';

		endif;

		if(is_array($request)) :

            $current_user = wp_get_current_user();

            if ( isset( $current_user->ID ) && $current_user->ID > 0 ) :
                $request['user_id'] = $current_user->ID;
            endif;

			if('default' === $checkout_type) :
            	do_action('sejoli/checkout/do', $request);
			else :
				do_action('sejoli/checkout/renew', $request);
			endif;

            $order    = sejolisa_get_respond('order');
            $checkout = sejolisa_get_respond('checkout');

            // __debug($order,$checkout);

            if(false === $checkout['valid']) :

                $response = [
                    'valid' => false,
                    'messages' => $checkout['messages']['error'],
                ];

            elseif(false == $order['valid']) :

                $response = [
                    'valid' => false,
                    'messages' => $order['messages']['error'],
                ];

            else:

                $d_order = $order['order'];

                $messages = [sprintf( __('Order created successfully. Order ID #%s', 'sejolisa'), $d_order['ID'] )];

                if(0 < count($order['messages']['warning'])) :
                    foreach($order['messages']['warning'] as $message) :
                        $messages[] = $message;
                    endforeach;
                endif;

                if(0 < count($order['messages']['info'])) :
                    foreach($order['messages']['info'] as $message) :
                        $messages[] = $message;
                    endforeach;
                endif;

                $response = [
                    'valid' => true,
                    'messages' => $messages,
                    'redirect_link' => site_url('checkout/loading?order_id='.$d_order['ID']),
                    'data' => [
                        'order' => $d_order
                    ]
                ];

            endif;

            wp_send_json($response);

        endif;
    }

    /**
     * sejoli submit login by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function submit_login_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-submit-login' ) ) :

            $request = wp_parse_args($_POST,[
                'login_email'    => NULL,
                'login_password' => NULL,
            ]);

            $errors = [];

            if ( empty( $request['login_email'] ) ) :
                $errors[] = __('Alamat email wajib diisi');
            endif;

            if ( empty( $request['login_password'] ) ) :
                $errors[] = __('Password wajib diisi');
            endif;

            if ( empty( $errors ) ) :

                $credentials = array(
                    'user_login'    => $request['login_email'],
                    'user_password' => $request['login_password'],
                    'remember'      => 1,
                );

                $secure_cookie = apply_filters( 'secure_signon_cookie', '', $credentials );

                $user = wp_authenticate( $credentials['user_login'], $credentials['user_password'] );

                if ( !is_wp_error( $user ) ) :

                    wp_set_auth_cookie( $user->ID, $credentials['remember'], $secure_cookie );

                    wp_send_json_success(['Login success']);

                else:

                    $errors[] = __('Alamat Email atau Password salah');

                endif;

            endif;

            wp_send_json_error($errors);

        endif;
    }

    /**
     * sejoli get current user by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function get_current_user_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-get-current-user' ) ) :

            $request = wp_parse_args( $_POST, []);

            $response = [];

            $response['current_user'] = apply_filters('sejoli/frontend/checkout/current-user', [], $request );

            wp_send_json($response);

        endif;
    }

    /**
     * setup current user data
     * hooked via filter sejoli/frontend/checkout/current-user
     *
     * @return void
     */
    public function current_user( $current_user, $request )
    {

        $current_user = [];

        $user = wp_get_current_user();

        if ( $user->ID > 0 ) :

            if ( !empty( $user->first_name ) ) :
                $name = $user->first_name;
            else:
                $name = $user->display_name;
            endif;

            $address = $user->_address;

            $subdistrict = $user->_destination;
            if ( !empty( $subdistrict ) ) :
                $district = sejolisa_get_district_options_by_ids( [$subdistrict] );
                if ( !empty( $district['results'] ) ) :
                    $subdistrict = $district['results'][0];
                endif;
            endif;

            $current_user = [
                'id'    => $user->ID,
                'name'  => $name,
                'email' => $user->user_email,
                'phone' => $user->_phone,
                'address' => $address,
                'subdistrict' => $subdistrict,
            ];

        endif;

        return $current_user;

    }

    /**
     * sejoli delete coupon by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function delete_coupon_by_ajax()
    {
		$request = NULL;

		// Ordinary order
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-delete-coupon' ) ) :

            $request = wp_parse_args( $_POST,[
                'product_id'      => 0,
                'coupon'          => NULL,
                'quantity'        => 1,
                'type'            => 'regular',
                'payment_gateway' => 'manual',
				'calculate'		  => 'default'
            ]);

		// Renew order
		elseif ( sejoli_ajax_verify_nonce( 'sejoli-checkout-renew-ajax-delete-coupon' ) ) :

            $request = wp_parse_args( $_POST,[
				'order_id'		  => 0,
                'product_id'      => 0,
                'coupon'          => NULL,
                'quantity'        => 1,
                'type'            => 'regular',
                'payment_gateway' => 'manual',
				'calculate'		  => 'renew'
            ]);

        endif;

		if(is_array($request)) :

			$request['coupon'] = NULL;

			if ( intval($request['product_id']) > 0 ) :

				do_action('sejoli/frontend/checkout/delete-coupon', $request);

				$response = sejolisa_get_respond('delete-coupon');

				wp_send_json($response);

			endif;

			$response = [
				'valid' => false,
				'messages' => [__('Hapu kupon gagal')]
			];

			wp_send_json($response);

		endif;
    }

    /**
     * sejoli delete coupon
     * hooked via action sejoli/frontend/checkout/delete-coupon
     *
     * @return json
     */
    public function delete_coupon( $request )
    {
		if(!isset($request['calculate']) || 'default' === $request['calculate']) :
        	do_action('sejoli/checkout/calculate', $request);
		else :
			do_action('sejoli/checkout/calculate-renew', $request);
		endif;

        $respond    	 = sejolisa_get_respond('total');
        $product      	 = sejolisa_get_product( $request['product_id'] );
		$quantity        = intval($respond['detail']['quantity']);
		$product_price   = isset($respond['cart_detail']['subscription']['regular']['raw']) ?
								$respond['cart_detail']['subscription']['regular']['raw'] : $product->price;
		$product_total_price = $quantity * $product_price;

        $calculate = [
            'valid'    => true,
            'messages' => [__('Hapus kupon berhasil')],
            'data'     => [
                'product' => [
                    'id'    => $product->ID,
                    'image' => get_the_post_thumbnail_url($product->ID,'full'),
                    'title' => (!isset($request['calculate']) || 'default' === $request['calculate']) ?
								$product->post_title :
								sprintf( __('Perpanjangan order INV %s<br /> Produk: %s', 'sejoli'), $request['order_id'], $product->post_title),
                    'price' => sejolisa_price_format( $product_price ),
                ],
                'total' => sejolisa_coloring_unique_number( sejolisa_price_format( $respond['total'] ) )
            ]
        ];

		$calculate = $this->set_checkout_respond($respond, $calculate, $quantity, '', true);

        sejolisa_set_respond($calculate, 'delete-coupon');
    }

	/**
	 * Register custom query variables
	 * Hooked via filter parse_query, priority 999
	 * @since 	1.0.0
	 * @param  	array 	$vars [description]
	 * @return 	array
	 */
    public function custom_query_vars( $vars )
    {
        $vars[] = "sejolisa_checkout_page";
        $vars[] = "sejolisa_checkout_id";
		$vars[] = 'order_id';

        return $vars;
    }

	/**
	 * Check requested page
	 * Hooked via action parse_request, priority 999
	 * @since 	1.3.2
	 * @return 	void
	 */
	public function check_requested_page() {

		global $sejolisa;

		if( sejolisa_verify_checkout_page('loading') ) :
			$this->is_loading_page = true;
			$this->current_order   = $sejolisa['order'];
		elseif( sejolisa_verify_checkout_page( 'thank-you' ) ) :
			$this->is_thankyou_page = true;
			$this->current_order   = $sejolisa['order'];
		elseif( sejolisa_verify_checkout_page('renew') ) :
			$this->is_renew_order_page = true;
			$this->current_order   = $sejolisa['order'];
		endif;

		// __print_debug(array(
		// 	'loading'     => $this->is_loading_page,
		// 	'regular'     => $this->is_regular_page,
		// 	'renew_order' => $this->is_renew_order_page,
		// 	'order'		  => $this->current_order
		// ));
		// exit;
	}

	/**
	 * Check request template page
	 * Hooked via action template_redirect, priority 800
	 * @since 	1.3.2
	 * @return 	void
	 */
	public function check_requested_template() {

		global $sejolisa;

		if(
			$this->is_thankyou_page &&
			$this->current_order
		) :

			do_action('sejoli/thank-you/render', $this->current_order);

		endif;
	}

	/**
	 * Set template file based on request
	 * Hooked via filter template_include, priority 999
	 * @since 	1.3.2
	 * @param  	string 	$template_file
	 * @return 	string
	 */
	public function set_template_file($template_file) {

		if($this->is_loading_page) :

			$template_file = SEJOLISA_DIR . 'template/checkout/loading.php';

		elseif($this->is_thankyou_page) :

			if(0.0 === floatval($this->current_order['grand_total'])) :
				$template_file = SEJOLISA_DIR . 'template/checkout/thankyou-free.php';
			else :
				$template_file = SEJOLISA_DIR . 'template/checkout/thankyou.php';
			endif;

		elseif($this->is_renew_order_page) :
			$template_file = SEJOLISA_DIR . 'template/checkout/checkout-renew.php';
		endif;

		return $template_file;
	}

	/**
	 * Display renew checkout
	 * Hooked via action parse_request, priority 999
	 * @since 	1.1.9
	 * @return 	void
	 */
	public function setup_checkout_renew()
	{

		if(sejolisa_verify_checkout_page('renew') && isset($_GET['order_id'])) :

			global $sejolisa, $post;

			$order_id 		 = intval($_GET['order_id']);
			$current_user_id = get_current_user_id();
			$response        = sejolisa_check_subscription($order_id);
			$subscription    = $response['subscription'];

			if(false === $response['valid'] || $current_user_id !== intval($subscription->user_id) ):

				wp_die(
					sprintf( __('Anda tidak bisa mengakses pembaharuan langganan order %s', 'sejoli'), $order_id),
					__('Tidak bisa pembaharuan langganan', 'sejoli')
				);

			else :

				$sejolisa['subscription'] = (array) $subscription;

			endif;

		endif;
	}

    /**
     * sejoli checkout loading by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function loading_by_ajax()
    {

        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-loading' ) ) :

            $request = wp_parse_args($_POST, [
                'order_id' => 0
            ]);

            if ( intval( $request['order_id'] ) > 0 ) :

                do_action('sejoli/frontend/checkout/loading', $request);

                $response = sejolisa_get_respond('loading');

            else:

                $response = [
                    'valid' => false
                ];

            endif;

            wp_send_json($response);

        endif;

    }

    /**
     * sejoli checkout loading
     * hooked via action sejoli/frontend/checkout/loading
     *
     * @return json
     */
    public function loading( $request )
    {

        $response = [
            'valid' => true,
            'redirect_link' => site_url('checkout/thank-you?order_id='.$request['order_id']),
        ];

        sejolisa_set_respond($response,'loading');

    }

    /**
     * sejoli get shipping methods by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function get_shipping_methods_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-get-shipping-methods' ) ) :

            $request = wp_parse_args( $_POST, [
                'product_id'    => NULL,
                'district_id'	=> NULL,
                'quantity'		=> 1,
                'shipment'      => NULL,
                'variants'      => [],
            ]);

            $response = [];

            $response = apply_filters('sejoli/frontend/checkout/shipping-methods', [], $request );

            wp_send_json($response);

        endif;
    }

    /**
     * setup shipping methods data
     * hooked via filter sejoli/frontend/checkout/shipping-methods
     *
     * @return void
     */
    public function shipping_methods( $shipping_methods, $request )
    {

        $shipping_details = [
			'shipping_methods'	 => [],
			'messages'			 => ''
		];

        do_action('sejoli/checkout/shipment-calculate', $request);

        $shipment_data = sejolisa_get_respond('shipment');

        if ( $shipment_data['valid'] &&
            !empty( $shipment_data['shipment'] ) ) :

            foreach ( $shipment_data['shipment'] as $key => $value ) :

                $shipping_details['shipping_methods'][] = [
                    'id' => $key,
                    'title' => $value,
                    'image' => '',
                ];

            endforeach;

			$shipping_details['messages'] = $shipment_data['messages'];

        endif;

        return $shipping_details;

    }

    /**
     * sejoli get subdistrict by ajax
     * hooked via action parse_request
     *
     * @return json
     */
    public function get_subdistrict_by_ajax()
    {
        if ( sejoli_ajax_verify_nonce( 'sejoli-checkout-ajax-get-subdistrict' ) ) :

            $request = wp_parse_args( $_POST,[
                'term' => '',
            ]);

            $response = sejolisa_get_district_options( $request['term'] );

            wp_send_json($response);

        endif;
    }
}
