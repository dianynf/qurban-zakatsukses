<?php
namespace SejoliSA\JSON;

Class License extends \SejoliSA\JSON
{
    /**
     * Many license IDs
     * @since   1.0.0
     * @var     array
     */
    protected $licenses = array();

    /**
     * Construction
     */
    public function __construct() {

    }

    /**
     * Set user options
     * @since   1.0.0
     * @return  json
     */
    public function set_for_options() {

    }

    /**
     * Set table data
     * Hooked via action wp_ajax_sejoli-license-table, priority 1
     * @since   1.0.0
     * @return  json
     */
    public function set_for_table() {

		$table = $this->set_table_args($_POST);

		$data    = [];

        if(isset($_POST['backend']) && current_user_can('manage_sejoli_licenses')) :

        else :
            $table['filter']['user_id'] = get_current_user_id();
        endif;

		$response = sejolisa_get_licenses($table['filter'], $table);

		if(false !== $response['valid']) :
			$data = $response['licenses'];
		endif;

		echo wp_send_json([
			'table'           => $table,
			'draw'            => $table['draw'],
			'data'            => $data,
			'recordsTotal'    => $response['recordsTotal'],
			'recordsFiltered' => $response['recordsTotal'],
		]);

		exit;
    }

    /**
     * Update multiple licenses
     * Hooked via action wp_ajax_sejoli-license-update, priority 1
     * @return  void
     */
    public function update_licenses() {

        $post_data = wp_parse_args($_POST,[
            'licenses'  => NULL,
            'status'    => NULL,
            'nonce'     => NULL
        ]);

        if(
            wp_verify_nonce($post_data['nonce'], 'sejoli-license-update') &&
            is_array($post_data['licenses'])
        ) :
            if(in_array($post_data['status'], ['active', 'pending'])) :
                sejolisa_update_status_licenses($post_data['status'], $post_data['licenses']);
            else :
                sejolisa_reset_licenses($post_data['licenses']);
            endif;
        endif;
        exit;
    }
}
