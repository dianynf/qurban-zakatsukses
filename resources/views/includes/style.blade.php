<link rel='dns-prefetch' href='//platform-api.sharethis.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Cinta Qurban &raquo; Feed"
    href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Cinta Qurban &raquo; Comments Feed"
    href="/comments/feed/" />
<script type="text/javascript">
    window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/",
        "svgExt": ".svg",
        "source": {
            "concatemoji": "\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.1"
        }
    };
    ! function(e, a, t) {
        var r, n, o, i, p = a.createElement("canvas"),
            s = p.getContext && p.getContext("2d");

        function c(e, t) {
            var a = String.fromCharCode;
            s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0);
            var r = p.toDataURL();
            return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()
        }

        function l(e) {
            if (!s || !s.fillText) return !1;
            switch (s.textBaseline = "top", s.font = "600 32px Arial", e) {
                case "flag":
                    return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356,
                        56826, 55356, 56819
                    ], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418,
                        56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447
                    ], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203,
                        56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447
                    ]));
                case "emoji":
                    return !c([55357, 56424, 8205, 55356, 57212], [55357, 56424, 8203, 55356, 57212])
            }
            return !1
        }

        function d(e) {
            var t = a.createElement("script");
            t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
        }
        for (i = Array("flag", "emoji"), t.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, o = 0; o < i.length; o++) t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports
            .everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports
                .everythingExceptFlag && t.supports[i[o]]);
        t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t
            .readyCallback = function() {
                t.DOMReady = !0
            }, t.supports.everything || (n = function() {
                t.readyCallback()
            }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !
                1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
                "complete" === a.readyState && t.readyCallback()
            })), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji), d(r
                .wpemoji)))
    }(window, document, window._wpemojiSettings);

</script>
<style type="text/css">
    img.wp-smiley,
    img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
    }

</style>
<link rel='stylesheet' id='pt-cv-public-style-css'
    href='/wp-content/plugins/content-views-query-and-display-post-page/public/assets/css/cv.css?ver=2.3.3'
    type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'
    href='/wp-includes/css/dist/block-library/style.min.css?ver=5.5.1' type='text/css'
    media='all' />
<link rel='stylesheet' id='contact-form-7-css'
    href='/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2'
    type='text/css' media='all' />
<link rel='stylesheet' id='wpdm-font-awesome-css'
    href='/wp-content/plugins/download-manager/assets/fontawesome/css/all.min.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='wpdm-front-bootstrap-css'
    href='/wp-content/plugins/download-manager/assets/bootstrap/css/bootstrap.min.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='wpdm-front-css'
    href='/wp-content/plugins/download-manager/assets/css/front.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='zita-font-awesome-css'
    href='/wp-content/themes/zita/third-party/font-awesome/css/font-awesome.css?ver=4.7.0'
    type='text/css' media='all' />
<link rel='stylesheet' id='zita-menu-style-css'
    href='/wp-content/themes/zita/css/zita-menu.css?ver=1.0.0' type='text/css'
    media='all' />
<link rel='stylesheet' id='zita-style-css'
    href='/wp-content/themes/zita/style.css?ver=1.0.0' type='text/css' media='all' />
<style id='zita-style-inline-css' type='text/css'>
    @media (min-width: 769px) {
        body {
            font-size: 15px
        }
    }

    @media (max-width: 768px) {
        body {
            font-size: 15px
        }
    }

    @media (max-width: 550px) {
        body {
            font-size: 15px
        }
    }

    body,
    button,
    input,
    select,
    textarea,
    #respond.comment-respond #submit,
    .read-more .zta-button,
    button,
    [type='submit'],
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    th,
    th a,
    dt,
    b,
    strong {
        font-family: Montserrat;
        text-transform: ;
        font-weight: 400;
    }

    .woocommerce .page-title,
    h2.widget-title,
    .site-title span,
    h2.entry-title,
    h2.entry-title a,
    h1.entry-title,
    h2.comments-title,
    h3.comment-reply-title,
    h4.author-header,
    .zita-related-post h3,
    #content.blog-single .zita-related-post ul li h3 a,
    h3.widget-title,
    .woocommerce ul.products li.product .woocommerce-loop-product__title,
    .woocommerce-page ul.products li.product .woocommerce-loop-product__title,
    .woocommerce h1.product_title,
    .woocommerce-Tabs-panel h2,
    .related.products h2,
    section.up-sells h2,
    .cross-sells h2,
    .cart_totals h2,
    .woocommerce-billing-fields h3,
    .woocommerce-account .addresses .title h3 {
        font-family: Montserrat;
        text-transform: ;
        font-weight: 700;
    }

    @media (min-width: 769px) {
        .entry-content h1 {
            font-size: 48px
        }
    }

    @media (max-width: 768px) {
        .entry-content h1 {
            font-size: 48px
        }
    }

    @media (max-width: 550px) {
        .entry-content h1 {
            font-size: 48px
        }
    }

    .entry-content h1 {
        font-family: Montserrat;
        text-transform: ;
        font-weight: 400;
    }

    @media (min-width: 769px) {
        .zita-logo img {
            max-width: 100px
        }
    }

    @media (max-width: 768px) {
        .zita-logo img {
            max-width: 122px
        }
    }

    @media (max-width: 550px) {
        .zita-logo img {
            max-width: 101px
        }
    }

    a:hover,
    .inifiniteLoader,
    mark,
    .single .nav-previous:hover:before,
    .single .nav-next:hover:after,
    .page-numbers.current,
    .page-numbers:hover,
    .prev.page-numbers:hover,
    .next.page-numbers:hover,
    .zita-load-more #load-more-posts:hover,
    article.zita-article h2.entry-title a:hover,
    .zita-menu li a:hover,
    .main-header .zita-menu>li>a:hover,
    .woocommerce nav.woocommerce-pagination ul li a:focus,
    .woocommerce nav.woocommerce-pagination ul li a:hover,
    .woocommerce nav.woocommerce-pagination ul li span.current,
    .zita-menu li.menu-active>a,
    .main-header .main-header-bar a:hover,
    .zita-menu .content-social .social-icon li a:hover,
    .mhdrleftpan .content-social .social-icon a:hover,
    .mhdrrightpan .content-social .social-icon a:hover {
        color: #d69431
    }

    .main-header-col1 {
        display: flex;
    }

    .agent {
        color: #FFF;
        margin: auto;
        margin-left: 40px;
        padding: 2px;
        font-size: 12px;
        border-radius: 5px;
        background-color: #ff9642;
    }

    .page-numbers.current,
    .page-numbers:hover,
    .prev.page-numbers:hover,
    .next.page-numbers:hover,
    .zita-load-more #load-more-posts:hover {
        border-color: #d69431
    }

    #respond.comment-respond #submit,
    .read-more .zta-button,
    button,
    [type='submit'],
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .zita-cart p.buttons a,
    .wc-proceed-to-checkout .button.alt.wc-forward,
    .main-header .main-header-bar a.main-header-btn {
        border-color: #d69431;
        background-color: #d69431
    }

    #move-to-top,
    .zta-date-meta .posted-on,
    .mhdrleftpan .header-pan-icon span,
    .mhdrrightpan .header-pan-icon span {
        background: #d69431
    }

    .inifiniteLoader,
    .summary .yith-wcwl-wishlistaddedbrowse a,
    .summary .yith-wcwl-wishlistexistsbrowse a {
        color: #d69431
    }

    .zita_overlayloader {
        background: #f5f5f5
    }

    .woocommerce ul.products li.product .onsale,
    .woocommerce span.onsale,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle {
        background: #d69431
    }

    .cart-contents .cart-crl {
        background: #d69431
    }

    .cart-crl:before {
        border-color: #d69431
    }

    .woocommerce #respond input#submit.alt.disabled,
    .woocommerce #respond input#submit.alt.disabled:hover,
    .woocommerce #respond input#submit.alt:disabled,
    .woocommerce #respond input#submit.alt:disabled:hover,
    .woocommerce #respond input#submit.alt:disabled[disabled],
    .woocommerce #respond input#submit.alt:disabled[disabled]:hover,
    .woocommerce a.button.alt.disabled,
    .woocommerce a.button.alt.disabled:hover,
    .woocommerce a.button.alt:disabled,
    .woocommerce a.button.alt:disabled:hover,
    .woocommerce a.button.alt:disabled[disabled],
    .woocommerce a.button.alt:disabled[disabled]:hover,
    .woocommerce button.button.alt.disabled,
    .woocommerce button.button.alt.disabled:hover,
    .woocommerce button.button.alt:disabled,
    .woocommerce button.button.alt:disabled:hover,
    .woocommerce button.button.alt:disabled[disabled],
    .woocommerce button.button.alt:disabled[disabled]:hover,
    .woocommerce input.button.alt.disabled,
    .woocommerce input.button.alt.disabled:hover,
    .woocommerce input.button.alt:disabled,
    .woocommerce input.button.alt:disabled:hover,
    .woocommerce input.button.alt:disabled[disabled],
    .woocommerce input.button.alt:disabled[disabled]:hover {
        border-color: #d69431;
        background-color: #d69431;
    }

    a,
    .single .nav-previous:before,
    .single .nav-next:after,
    .zita-menu li a,
    .main-header .zita-menu>li>a {
        color: #f2f2f2
    }

    a:hover,
    .single .nav-previous:hover:before,
    .single .nav-next:hover:after,
    article.zita-article h2.entry-title a:hover,
    .zita-menu li a:hover,
    .main-header .zita-menu>li>a:hover,
    .zita-menu li.menu-active>a,
    .main-header .main-header-bar a:hover,
    .zita-menu .content-social .social-icon li a:hover,
    .mhdrleftpan .content-social .social-icon a:hover,
    .mhdrrightpan .content-social .social-icon a:hover {
        color:
    }

    body,
    .zita-site #content .entry-meta {
        color:
    }

    article.zita-article h2.entry-title a,
    #sidebar-primary h2.widget-title,
    .woocommerce h1.product_title,
    .woocommerce-Tabs-panel h2,
    .related.products h2,
    section.up-sells h2,
    .cross-sells h2,
    .cart_totals h2,
    .woocommerce-billing-fields h3,
    .woocommerce-account .addresses .title h3,
    h1.page-title,
    h1.entry-title {
        color:
    }

    .menu-toggle .menu-btn,
    .bar-menu-toggle .menu-btn {
        background: rgba(0, 0, 0, 0);
        border-color: #000000
    }

    .menu-toggle .icon-bar,
    .bar-menu-toggle .icon-bar {
        background: #000000
    }

    .menu-toggle .menu-btn,
    .bar-menu-toggle .menu-btn {
        border-radius: 0px;
    }

    .menu-icon-inner {
        color: #000000
    }

    .menu-custom-html>a button,
    .read-more .zta-button,
    #respond.comment-respond #submit,
    button,
    [type='submit'],
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .zita-cart p.buttons a,
    .wc-proceed-to-checkout .button.alt.wc-forward,
    .main-header .main-header-bar a.main-header-btn {
        background: #0274be;
        color: #ffffff;
        border-color: #0274be;
    }

    .menu-custom-html>a button,
    .read-more .zta-button,
    #respond.comment-respond #submit,
    button,
    [type='submit'],
    .woocommerce #respond input#submit,
    .woocommerce a.button,
    .woocommerce button.button,
    .woocommerce input.button,
    .woocommerce #respond input#submit.alt,
    .woocommerce a.button.alt,
    .woocommerce button.button.alt,
    .woocommerce input.button.alt,
    .main-header .main-header-bar a.main-header-btn {
        border-radius: px;
    }

    .menu-custom-html>a button:hover,
    .read-more .zta-button:hover,
    #respond.comment-respond #submit:hover,
    button:hover,
    [type='submit']:hover,
    .woocommerce #respond input#submit:hover,
    .woocommerce a.button:hover,
    .woocommerce button.button:hover,
    .woocommerce input.button:hover,
    .woocommerce #respond input#submit.alt:hover,
    .woocommerce a.button.alt:hover,
    .woocommerce button.button.alt:hover,
    .woocommerce input.button.alt:hover,
    .zita-cart p.buttons a:hover,
    .main-header .main-header-bar .main-header .main-header-bar a.main-header-btn:hover,
    .main-header .main-header-bar a.main-header-btn:hover {
        background: #0274be;
        color: #ffffff;
        border-color: #0274be
    }

    .woocommerce #respond input#submit.alt.disabled,
    .woocommerce #respond input#submit.alt.disabled:hover,
    .woocommerce #respond input#submit.alt:disabled,
    .woocommerce #respond input#submit.alt:disabled:hover,
    .woocommerce #respond input#submit.alt:disabled[disabled],
    .woocommerce #respond input#submit.alt:disabled[disabled]:hover,
    .woocommerce a.button.alt.disabled,
    .woocommerce a.button.alt.disabled:hover,
    .woocommerce a.button.alt:disabled,
    .woocommerce a.button.alt:disabled:hover,
    .woocommerce a.button.alt:disabled[disabled],
    .woocommerce a.button.alt:disabled[disabled]:hover,
    .woocommerce button.button.alt.disabled,
    .woocommerce button.button.alt.disabled:hover,
    .woocommerce button.button.alt:disabled,
    .woocommerce button.button.alt:disabled:hover,
    .woocommerce button.button.alt:disabled[disabled],
    .woocommerce button.button.alt:disabled[disabled]:hover,
    .woocommerce input.button.alt.disabled,
    .woocommerce input.button.alt.disabled:hover,
    .woocommerce input.button.alt:disabled,
    .woocommerce input.button.alt:disabled:hover,
    .woocommerce input.button.alt:disabled[disabled],
    .woocommerce input.button.alt:disabled[disabled]:hover {
        border-color: #0274be;
        background-color: #0274be;
    }

    .mhdrleft.zta-transparent-header .top-header-bar,
    .mhdrleft.zta-transparent-header .top-header-bar:before,
    .mhdrleft.zta-transparent-header .main-header-bar,
    .mhdrleft.zta-transparent-header .main-header-bar:before,
    .mhdrleft.zta-transparent-header .bottom-header-bar,
    .mhdrleft.zta-transparent-header .bottom-header-bar:before,
    .zita-site .mhdrleft.zta-transparent-header .main-header-bar:before {
        background: transparent;
    }

    .mhdrright.zta-transparent-header .top-header-bar,
    .mhdrright.zta-transparent-header .top-header-bar:before,
    .mhdrright.zta-transparent-header .main-header-bar,
    .mhdrright.zta-transparent-header .main-header-bar:before,
    .mhdrright.zta-transparent-header .bottom-header-bar,
    .mhdrright.zta-transparent-header .bottom-header-bar:before,
    .zita-site .mhdrright.zta-transparent-header .main-header-bar:before {
        background: transparent;
    }

    .mhdrcenter.zta-transparent-header .top-header-bar,
    .mhdrcenter.zta-transparent-header .top-header-bar:before,
    .mhdrcenter.zta-transparent-header .main-header-bar,
    .mhdrcenter.zta-transparent-header .main-header-bar:before,
    .mhdrcenter.zta-transparent-header .bottom-header-bar,
    .mhdrcenter.zta-transparent-header .bottom-header-bar:before,
    .zita-site .mhdrcenter.zta-transparent-header .main-header-bar:before {
        background: transparent;
    }

    .mhdfull.zta-transparent-header,
    .mhdfull.zta-transparent-header .top-header-bar,
    .mhdfull.zta-transparent-header .main-header-bar,
    .mhdfull.zta-transparent-header .bottom-header-bar,
    .mhdfull.zta-transparent-header .top-header-bar:before,
    .mhdfull.zta-transparent-header .main-header-bar:before,
    .mhdfull.zta-transparent-header .bottom-header-bar:before {
        background: transparent;
    }

    .shrink .sider-inner ul#zita-menu {
        overflow: hidden;
    }

    .main-header-bar {
        border-bottom-width: 1px;
    }

    .main-header-bar {
        border-bottom-color: #eee
    }

    header .container,
    #container.site- container,
    footer .container,
    #content #container,
    #content.site-content.boxed #container,
    #content.site-content.contentbox #container,
    #content.site-content.fullwidthcontained #container {
        max-width: px;
    }

    .top-header-container {
        line-height: 40px;
    }

    .top-header-bar {
        border-bottom-width: 1px;
    }

    .top-header-bar {
        border-bottom-color: #eee
    }

    .bottom-header-container {
        line-height: 40px;
    }

    .bottom-header-bar {
        border-bottom-width: 1px;
    }

    .bottom-header-bar {
        border-bottom-color: #eee
    }

    .top-footer-container {
        line-height: 40px;
    }

    .top-footer-bar {
        border-bottom-width: 1px;
    }

    .top-footer-bar {
        border-bottom-color: #eee
    }

    .bottom-footer-container {
        line-height: 61px;
    }

    .bottom-footer-bar {
        border-top-width: 1px;
    }

    .bottom-footer-bar {
        border-top-color: rgba(255, 255, 255, 0.39)
    }

    .site-content #sidebar-primary {
        width: 35%
    }

    .site-content #primary {
        width: 65%
    }

    #move-to-top {
        border-radius: 2px;
        -moz-border-radius: 2px;
        -webkit-border-radius: 2px;
        color: #e2ad1b;
        background: #ffffff
    }

    #move-to-top:hover {
        color: #fff;
        background: #015782;
    }

    .searchfrom .search-btn {
        font-size: 15px;
        border-radius: px;
    }

    .top-header-bar .searchfrom .search-btn,
    .main-header-bar .searchfrom .search-btn,
    .bottom-header-bar .searchfrom .search-btn,
    .zita-menu .menu-custom-search .searchfrom a {
        color: ;
        background: ;
        border-color:
    }

    .top-header-bar .searchfrom .search-btn:hover,
    .main-header-bar .searchfrom .search-btn:hover,
    .bottom-header-bar .searchfrom .search-btn:hover {
        color:
    }

    .widget-area #searchform .form-content,
    .searchfrom #searchform .form-content {
        width: 100%;
    }

    .widget-area #searchform .form-content:before,
    .searchfrom #searchform .form-content:before {
        color: #015782;
        font-size: px;
    }

    .widget-area input#s,
    .searchfrom #searchform input#s {
        background-color: ;
        border-color: ;
    }

    .widget-area #searchform input[type=submit],
    .widget-area input#s,
    .widget-area #searchform .form-content:before,
    .searchfrom #searchform .form-content:before,
    .searchfrom input#s,
    .searchfrom #searchform input[type=submit] {
        height: px;
        line-height: px;
        border-radius: 0px;
    }

    .form-content input#s::-webkit-input-placeholder,
    .form-content input#s {
        color: #bbb;
        font-size: px;
    }

    .main-header .main-header-bar,
    .mhdrleftpan header,
    .mhdrrightpan header {
        background-color: #fff;
        background-image: url('');
    }

    .zita-site .main-header-bar:before,
    header.mhdrrightpan:before,
    header.mhdrleftpan:before {
        background: #fff;
        opacity: 0.7
    }

    .main-header-bar p,
    .main-header .zita-menu>li>a,
    .main-header .menu-custom-html,
    .main-header .menu-custom-widget,
    .main-header .widget-title,
    header.mhdrleftpan p,
    header.mhdrrightpan p,
    header.mhdrleftpan .widget-title,
    header.mhdrrightpan .widget-title,
    header.mhdrrightpan .content-html,
    header.mhdrleftpan .content-html,
    .mhdrrightpan .zita-menu a,
    .mhdrleftpan .zita-menu a,
    .mhdrleftpan .content-widget,
    .mhdrrightpan .content-widget,
    header.mhdrleftpan .top-header .top-header-bar .widget-title,
    header.mhdrrightpan .top-header .top-header-bar .widget-title,
    .mhdrrightpan .zita-menu li a,
    .mhdrleftpan .zita-menu li a,
    .mhdrrightpan .bottom-header .zita-menu>li>a,
    .mhdrleftpan .bottom-header .zita-menu>li>a {
        color: #555
    }

    .main-header .main-header-bar a,
    .mhdrleftpan .content-social .social-icon a,
    .mhdrrightpan .content-social .social-icon a,
    .zita-menu .content-social .social-icon li a {
        color: #9c9c9c
    }

    .main-header .main-header-bar a:hover {
        color:
    }

    .zita-cart p.buttons a.checkout {
        background: transparent;
        border-color: #9c9c9c;
        color: #9c9c9c;
    }

    header.mhdminbarleft p,
    header.mhdminbarright p,
    header.mhdminbarleft .widget-title,
    header.mhdminbarright .widget-title,
    header.mhdminbarleft .content-html,
    header.mhdminbarright .content-html,
    .mhdminbarleft .zita-menu a,
    .mhdminbarright .zita-menu a,
    .mhdminbarleft .content-widget,
    .mhdminbarright .content-widget,
    header.mhdminbarleft .top-header .top-header-bar .widget-title,
    header.mhdminbarright .top-header .top-header-bar .widget-title,
    .mhdminbarleft .zita-menu li a,
    .mhdminbarright .zita-menu li a,
    .mhdminbarleft .bottom-header .zita-menu>li>a,
    .mhdminbarright .bottom-header .zita-menu>li>a {
        color: #555
    }

    .widget-footer .widget-footer-bar {
        background: #333
    }

    .widget-footer .widget-footer-bar .widget-title,
    .widget-footer .widget-footer-bar,
    .widget-footer .widget-footer-bar a {
        color: #fff
    }

    .widget-footer .widget-footer-bar a:hover {
        color: #d69431
    }

    .widget-footer .widget-footer-bar a:hover {
        color:
    }

    .bottom-footer .bottom-footer-bar {
        background: #333
    }

    .bottom-footer .content-html,
    .bottom-footer .zita-menu>li>a,
    .bottom-footer .content-widget,
    .bottom-footer .bottom-footer-bar .widget-title,
    .zita-bottom-menu li a,
    .bottom-footer .bottom-footer-bar a {
        color: #fff
    }

    .bottom-footer .bottom-footer-bar a:hover {
        color: #d69431
    }

    .bottom-footer .bottom-footer-bar a:hover {
        color:
    }

    .zita-cart,
    .zita-cart ul.cart_list li span,
    .zita-cart p {
        background: #ffff;
        color: #808285;
    }

    .zita-cart ul.cart_list li a {
        color: #9c9c9c;
    }

    .zita-cart p.buttons a.checkout {
        background: transparent;
        border-color: #9c9c9c;
        color: #9c9c9c;
    }

</style>

<style>
    a {
        text-decoration: none;
        display: inline-block;
        padding: 10px 15px;
    }

    .previous {
        color: black;
    }

</style>
<link rel='stylesheet' id='zita-google-font-montserrat-css'
    href='//fonts.googleapis.com/css?family=Montserrat%3A300%2C400%2C500%2C700&#038;subset=latin&#038;ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'
    href='/wp-includes/css/dashicons.min.css?ver=5.5.1' type='text/css' media='all' />
<link rel='stylesheet' id='joinchat-css'
    href='/wp-content/plugins/creame-whatsapp-me/public/css/joinchat.min.css?ver=4.0.9'
    type='text/css' media='all' />
<style id='joinchat-inline-css' type='text/css'>
    .joinchat {
        --red: 37;
        --green: 211;
        --blue: 102;
    }

</style>
<link rel='stylesheet' id='elementor-icons-css'
    href='/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.9.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css'
    href='/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.0.2'
    type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-legacy-css'
    href='/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.0.2'
    type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'
    href='/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.2'
    type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-325-css'
    href='/wp-content/uploads/elementor/css/post-325.css?ver=1598504098' type='text/css'
    media='all' />
<link rel='stylesheet' id='elementor-pro-css'
    href='/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=2.4.7'
    type='text/css' media='all' />
<link rel='stylesheet' id='lfb-style-css'
    href='/wp-content/plugins/lead-form-builder/elementor//css/lfb-styler.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='elaet-frontend-min-style-css'
    href='/wp-content/plugins/zita-site-library/addons//assets/css/elaet.frontend.min.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='elite-addons-style-css'
    href='/wp-content/plugins/zita-site-library/addons//assets/css/elite-addons-style.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='twentytwenty-css-css'
    href='/wp-content/plugins/zita-site-library/addons//assets/css/twentytwenty.css?ver=5.5.1'
    type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-5-all-css'
    href='/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=3.0.2'
    type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-4-shim-css'
    href='/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=3.0.2'
    type='text/css' media='all' />
<link rel='stylesheet' id='elementor-global-css'
    href='/wp-content/uploads/elementor/css/global.css?ver=1598504099' type='text/css'
    media='all' />
<link rel='stylesheet' id='elementor-post-6-css'
    href='/wp-content/uploads/elementor/css/post-6.css?ver=1598504099' type='text/css'
    media='all' />
<link rel='stylesheet' id='lfb_f_css-css'
    href='/wp-content/plugins/lead-form-builder/css/f-style.css?ver=5.5.1' type='text/css'
    media='all' />
<link rel='stylesheet' id='font-awesome-css'
    href='/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0'
    type='text/css' media='all' />
<link rel='stylesheet' id='google-fonts-1-css'
    href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.5.1'
    type='text/css' media='all' />
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'
    id='jquery-core-js'></script>
<script type='text/javascript'
    src='/wp-content/plugins/download-manager/assets/bootstrap/js/bootstrap.bundle.min.js?ver=5.5.1'
    id='wpdm-front-bootstrap-js'></script>
<script type='text/javascript' id='frontjs-js-extra'>
    /* <![CDATA[ */
    var wpdm_url = {
        "home": "\/",
        "site": "\/",
        "ajax": "\/wp-admin\/admin-ajax.php"
    };
    var wpdm_asset = {
        "spinner": "<i class=\"fas fa-sun fa-spin\"><\/i>"
    };
    /* ]]> */

</script>
<script type='text/javascript'
    src='/wp-content/plugins/download-manager/assets/js/front.js?ver=3.1.05'
    id='frontjs-js'></script>
<script type='text/javascript'
    src='/wp-content/plugins/download-manager/assets/js/chosen.jquery.min.js?ver=5.5.1'
    id='jquery-choosen-js'></script>
<script type='text/javascript'
    src='/wp-content/plugins/wp-table-builder/inc/admin/js/WPTB_ResponsiveFrontend.js?ver=1.2.6'
    id='wp-table-builder_responsive-frontend-js'></script>
<script type='text/javascript'
    src='/wp-content/plugins/wp-table-builder/inc/frontend/js/wp-table-builder-frontend.js?ver=1.2.6'
    id='wp-table-builder-js'></script>
<script type='text/javascript'
    src='//platform-api.sharethis.com/js/sharethis.js#product=ga&#038;property=5f0bbf8f218f8e001ab0c1e1'
    id='googleanalytics-platform-sharethis-js'></script>
<script type='text/javascript'
    src='/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=3.0.2'
    id='font-awesome-4-shim-js'></script>


<link rel="https://api.w.org/" href="/wp-json/" />
<link rel="alternate" type="application/json" href="/wp-json/wp/v2/pages/6" />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml"
    href="/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 5.5.1" />
<link rel="canonical" href="/" />
<link rel='shortlink' href='/' />
<link rel="alternate" type="application/json+oembed"
    href="/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fqurban.zakatsukses.org%2F" />
<link rel="alternate" type="text/xml+oembed"
    href="/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fqurban.zakatsukses.org%2F&#038;format=xml" />

<!-- Affiliates Manager plugin v2.7.6 - https://wpaffiliatemanager.com/ -->

<script>
    var wpdm_site_url = '/';
    var wpdm_home_url = '/';
    var ajax_url = '/wp-admin/admin-ajax.php';
    var wpdm_ajax_url = '/wp-admin/admin-ajax.php';
    var wpdm_ajax_popup = '0';

</script>
<style>
    .wpdm-download-link.btn.btn-primary. {
        border-radius: 4px;
    }

</style>


<style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }

</style>
<script>
    (function() {
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-172498035-1', 'auto');
        ga('send', 'pageview');
    })();

</script>
<link rel="icon" href="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-32x32.png"
    sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-192x192.png"
    sizes="192x192" />
<link rel="apple-touch-icon"
    href="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-180x180.png" />
<meta name="msapplication-TileImage"
    content="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-270x270.png" />
<style type="text/css" id="wp-custom-css">
    /** Start Block Kit CSS: 141-3-1d55f1e76be9fb1a8d9de88accbe962f **/

    .envato-kit-138-bracket .elementor-widget-container>*:before {
        content: "[";
        color: #ffab00;
        display: inline-block;
        margin-right: 4px;
        line-height: 1em;
        position: relative;
        top: -1px;
    }

    .envato-kit-138-bracket .elementor-widget-container>*:after {
        content: "]";
        color: #ffab00;
        display: inline-block;
        margin-left: 4px;
        line-height: 1em;
        position: relative;
        top: -1px;
    }

    /** End Block Kit CSS: 141-3-1d55f1e76be9fb1a8d9de88accbe962f **/

</style>
<meta name="generator" content="WordPress Download Manager 3.1.05" />
<style>
    @import url('https://fonts.googleapis.com/css?family=Rubik:400,500');



    .w3eden .fetfont,
    .w3eden .btn,
    .w3eden .btn.wpdm-front h3.title,
    .w3eden .wpdm-social-lock-box .IN-widget a span:last-child,
    .w3eden #xfilelist .panel-heading,
    .w3eden .wpdm-frontend-tabs a,
    .w3eden .alert:before,
    .w3eden .panel .panel-heading,
    .w3eden .discount-msg,
    .w3eden .panel.dashboard-panel h3,
    .w3eden #wpdm-dashboard-sidebar .list-group-item,
    .w3eden #package-description .wp-switch-editor,
    .w3eden .w3eden.author-dashbboard .nav.nav-tabs li a,
    .w3eden .wpdm_cart thead th,
    .w3eden #csp .list-group-item,
    .w3eden .modal-title {
        font-family: Rubik, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        text-transform: uppercase;
        font-weight: 500;
    }

    .w3eden #csp .list-group-item {
        text-transform: unset;
    }

</style>
<style>
    :root {
        --color-primary: #4a8eff;
        --color-primary-rgb: 74, 142, 255;
        --color-primary-hover: #4a8eff;
        --color-primary-active: #4a8eff;
        --color-secondary: #4a8eff;
        --color-secondary-rgb: 74, 142, 255;
        --color-secondary-hover: #4a8eff;
        --color-secondary-active: #4a8eff;
        --color-success: #18ce0f;
        --color-success-rgb: 24, 206, 15;
        --color-success-hover: #4a8eff;
        --color-success-active: #4a8eff;
        --color-info: #2CA8FF;
        --color-info-rgb: 44, 168, 255;
        --color-info-hover: #2CA8FF;
        --color-info-active: #2CA8FF;
        --color-warning: #f29e0f;
        --color-warning-rgb: 242, 158, 15;
        --color-warning-hover: orange;
        --color-warning-active: orange;
        --color-danger: #ff5062;
        --color-danger-rgb: 255, 80, 98;
        --color-danger-hover: #ff5062;
        --color-danger-active: #ff5062;
        --color-green: #30b570;
        --color-blue: #0073ff;
        --color-purple: #8557D3;
        --color-red: #ff5062;
        --color-muted: rgba(69, 89, 122, 0.6);
        --wpdm-font: Rubik, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    }

    .wpdm-download-link.btn.btn-primary {
        border-radius: 4px;
    }

</style>
