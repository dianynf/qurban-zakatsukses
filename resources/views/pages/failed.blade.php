@extends('layouts.success')
@section('title', 'Checkout Success')

@section('content')
    <main>
        <div class="section-success d-flex align-items-center">
            <div class="col text-center">
                <img src="{{ url('frontend/images/ic_mail.png') }}" width="200px" alt="Transaksi Sukses" />
                <h1 class="mt-4">Oops!</h1>
                <p>
                    Transaksi Anda Gagal
                    <br />
                    Silahkan hubungi perwakilan kami jika terjadi masalah
                </p>
                <a href="{{ url('/') }}" class="btn btn-home-page mt-3 px-5">
                    Kembali Ke Home
                </a>
            </div>
        </div>
    </main>
@endsection
