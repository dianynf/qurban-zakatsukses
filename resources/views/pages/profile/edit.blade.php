@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-xl-12 col-md-8 mb-5">
            @if (session()->has('sukses'))
                <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                    {{ session()->get('sukses') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="col-md-12">
            </div>
            <div class="card border-left-warning shadow h-100 py-2">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="modal-body">
                            <h3 class="h3 mb-4 text-gray-800">Pengaturan Informasi</h3>
                            <hr>
                            <form method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                                @method('patch')
                                @csrf
                                <div class="form-group row">
                                    <label for="username" class="col-sm-2 col-form-label">Full Nama</label>
                                    <div class="col-sm-10">
                                        <input id="username" type="text"
                                            class="form-control @error('username') is-invalid @enderror" name="username"
                                            value="{{ old('username', $users->username) }}" autocomplete="username" autofocus>
                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Username</label>
                                    <div class="col-sm-10">
                                        <input id="name" type="text"
                                            class="form-control @error('name') is-invalid @enderror" name="name"
                                            value="{{ old('name', $users->name) }}" autocomplete="name" readonly autofocus>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                        <input id="email" type="email"
                                            class="form-control @error('email') is-invalid @enderror" name="email"
                                            value="{{ old('email', $users->email) }}" autocomplete="email" readonly>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nohp" class="col-sm-2 col-form-label">No. Telepon</label>
                                    <div class="col-sm-10">
                                        <input id="nohp" type="number"
                                            class="form-control @error('nohp') is-invalid @enderror" name="nohp"
                                            value="{{ old('nohp', $users->nohp) }}" autocomplete="nohp">
                                        @error('nohp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <input id="alamat" type="alamat"
                                            class="form-control @error('alamat') is-invalid @enderror" name="alamat"
                                            value="{{ old('alamat', $users->alamat) }}" autocomplete="alamat">
                                        @error('alamat')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                {{-- <div class="form-group row">
                                    <label for="provinsi" class="col-sm-2 col-form-label">Provinsi</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" disabled>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kabupaten" class="col-sm-2 col-form-label">Kabupaten</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" disabled>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kecamatan" class="col-sm-2 col-form-label">Kecamatan</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" disabled>

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="desa" class="col-sm-2 col-form-label">kelurahan</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" disabled>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kodepos" class="col-sm-2 col-form-label">kodepos</label>
                                    <div class="col-sm-10">
                                        <input id="kodepos" type="kodepos"
                                            class="form-control @error('kodepos') is-invalid @enderror" name="kodepos"
                                            value="{{ old('kodepos', $users->kodepos) }}" autocomplete="kodepos" readonly>
                                        @error('kodepos')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> --}}
                                <div class="form-group row">
                                    <div class="col-sm-3">Foto</div>
                                        <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <img src="{{ Storage::url('public/image/' . $users->image) }}"
                                                                class="img-thumbnail">
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="image"
                                                                    placeholder="Foto">
                                                                <label class="custom-file-label"
                                                                    for="image">{{ $users->image }}</label>
                                                            </div>
                                                            <span>
                                                                Gambar Photo Buku Tabungan Anda sebaiknya memiliki
                                                                berukuran tidak lebih dari 2MB.
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                </div>
                                <div class="form-file row justify-content-end">
                                    <div class="col-sm-10">
                                        <button type="submit"
                                            class="btn admin-login btn-user btn-block admin-btn">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
