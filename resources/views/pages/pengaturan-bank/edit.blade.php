@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
    @if($users->rekening == null || $users->nik == null || $users->atasnama == null || $users->cabang == null)
                    <div class="container">
                        <div class="alert alert-danger" role="alert">
                            Silahkan Lengkapi Data Anda Sebagai Agen,
                                    <a href="{{ route('pengaturan-bank.edit') }}">Kelengkapan Document</a>
                                        dan
                                    <a href="{{ route('document.edit') }}">Pengaturan Bank</a>
                        </div>
                    </div>
                    @endif
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="col-md-12">

                        </div>
                        @if (session()->has('sukses'))
                            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                {{ session()->get('sukses') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">Pengaturan Bank</h3>
                                        <hr>
                                        <form action="{{ route('pengaturan-bank.update') }}" method="POST"
                                            enctype="multipart/form-data">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group row">
                                                <label for="bank_id" class="col-sm-3 col-form-label">Nama Bank*</label>
                                                <div class="col-sm-9">
                                                    <select name="bank_id" required class="form-control"  required>
                                                        {{-- <option value="0">Pilih Bank</option> --}}
                                                        {{-- @foreach ($banks as $ba)
                                                            <option value="{{ $ba->id }}"
                                                                {{ $users->bank_id == $ba->name ? 'selected' : '' }}>
                                                                {{ $ba->name }}
                                                            </option>
                                                        @endforeach --}}

                                                        @foreach($banks as $user)
                                                            <option value="{{ $user->id }}" {{ $user->id == $users->bank_id ? 'selected' : '' }}>{{ $user->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class=" form-group row">
                                                <label for="rekening" class="col-sm-3 col-form-label">Account
                                                    Number (No. Rekening)*</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="rekening" name="rekening"
                                                        maxlength="20" value="{{ old('rekening', $users->rekening) }}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="atasnama" class="col-sm-3 col-form-label">Atas Nama
                                                    Rekening</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="atasnama" name="atasnama"
                                                        value="{{ old('rekening', $users->atasnama) }}"  required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="cabang" class="col-sm-3 col-form-label">Cabang</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="cabang" name="cabang"
                                                        value="{{ old('rekening', $users->cabang) }}" maxlength="45"  required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">Photo Buku Tabungan</div>
                                                <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-sm-5">
                                                            <img src="{{ Storage::url('public/bank/' . $users->fotobuku) }}"
                                                                class="img-thumbnail">
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="fotobuku"
                                                                    placeholder="Image  required">
                                                                <label class="custom-file-label"
                                                                    for="fotobuku">{{ $users->fotobuku }}</label>
                                                            </div>
                                                            <span>
                                                                Gambar Photo Buku Tabungan Anda sebaiknya memiliki
                                                                berukuran tidak lebih dari 2MB.
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-file row justify-content-end">
                                                <div class="col-sm-9">
                                                    <button type="submit"
                                                        class="btn admin-login btn-sm btn-block admin-btn">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
