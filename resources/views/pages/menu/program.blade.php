<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title>Program Qurban &#8211; Cinta Qurban</title>
    <link rel='dns-prefetch' href='//platform-api.sharethis.com' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Cinta Qurban &raquo; Feed"
        href="/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Cinta Qurban &raquo; Comments Feed"
        href="/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.0.0\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.5.3"
            }
        };
        ! function(e, a, t) {
            var r, n, o, i, p = a.createElement("canvas"),
                s = p.getContext && p.getContext("2d");

            function c(e, t) {
                var a = String.fromCharCode;
                s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, e), 0, 0);
                var r = p.toDataURL();
                return s.clearRect(0, 0, p.width, p.height), s.fillText(a.apply(this, t), 0, 0), r === p.toDataURL()
            }

            function l(e) {
                if (!s || !s.fillText) return !1;
                switch (s.textBaseline = "top", s.font = "600 32px Arial", e) {
                    case "flag":
                        return !c([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) && (!c([55356,
                            56826, 55356, 56819
                        ], [55356, 56826, 8203, 55356, 56819]) && !c([55356, 57332, 56128, 56423, 56128, 56418,
                            56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447
                        ], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203,
                            56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447
                        ]));
                    case "emoji":
                        return !c([55357, 56424, 8205, 55356, 57212], [55357, 56424, 8203, 55356, 57212])
                }
                return !1
            }

            function d(e) {
                var t = a.createElement("script");
                t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
            }
            for (i = Array("flag", "emoji"), t.supports = {
                    everything: !0,
                    everythingExceptFlag: !0
                }, o = 0; o < i.length; o++) t.supports[i[o]] = l(i[o]), t.supports.everything = t.supports
                .everything && t.supports[i[o]], "flag" !== i[o] && (t.supports.everythingExceptFlag = t.supports
                    .everythingExceptFlag && t.supports[i[o]]);
            t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t
                .readyCallback = function() {
                    t.DOMReady = !0
                }, t.supports.everything || (n = function() {
                    t.readyCallback()
                }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load",
                    n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
                    "complete" === a.readyState && t.readyCallback()
                })), (r = t.source || {}).concatemoji ? d(r.concatemoji) : r.wpemoji && r.twemoji && (d(r.twemoji),
                    d(r.wpemoji)))
        }(window, document, window._wpemojiSettings);

    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }

    </style>
    <link rel='stylesheet' id='pt-cv-public-style-css'
        href='/wp-content/plugins/content-views-query-and-display-post-page/public/assets/css/cv.css?ver=2.3.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css'
        href='/wp-includes/css/dist/block-library/style.min.css?ver=5.5.3' type='text/css'
        media='all' />
    <link rel='stylesheet' id='contact-form-7-css'
        href='/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='wpdm-font-awesome-css'
        href='/wp-content/plugins/download-manager/assets/fontawesome/css/all.min.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='wpdm-front-bootstrap-css'
        href='/wp-content/plugins/download-manager/assets/bootstrap/css/bootstrap.min.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='wpdm-front-css'
        href='/wp-content/plugins/download-manager/assets/css/front.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='zita-font-awesome-css'
        href='/wp-content/themes/zita/third-party/font-awesome/css/font-awesome.css?ver=4.7.0'
        type='text/css' media='all' />
    <link rel='stylesheet' id='zita-menu-style-css'
        href='/wp-content/themes/zita/css/zita-menu.css?ver=1.0.0' type='text/css'
        media='all' />
    <link rel='stylesheet' id='zita-style-css'
        href='/wp-content/themes/zita/style.css?ver=1.0.0' type='text/css' media='all' />
    <style id='zita-style-inline-css' type='text/css'>
        @media (min-width: 769px) {
            body {
                font-size: 15px
            }
        }

        @media (max-width: 768px) {
            body {
                font-size: 15px
            }
        }

        @media (max-width: 550px) {
            body {
                font-size: 15px
            }
        }

        body,
        button,
        input,
        select,
        textarea,
        #respond.comment-respond #submit,
        .read-more .zta-button,
        button,
        [type='submit'],
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        th,
        th a,
        dt,
        b,
        strong {
            font-family: Montserrat;
            text-transform: ;
            font-weight: 400;
        }

        .woocommerce .page-title,
        h2.widget-title,
        .site-title span,
        h2.entry-title,
        h2.entry-title a,
        h1.entry-title,
        h2.comments-title,
        h3.comment-reply-title,
        h4.author-header,
        .zita-related-post h3,
        #content.blog-single .zita-related-post ul li h3 a,
        h3.widget-title,
        .woocommerce ul.products li.product .woocommerce-loop-product__title,
        .woocommerce-page ul.products li.product .woocommerce-loop-product__title,
        .woocommerce h1.product_title,
        .woocommerce-Tabs-panel h2,
        .related.products h2,
        section.up-sells h2,
        .cross-sells h2,
        .cart_totals h2,
        .woocommerce-billing-fields h3,
        .woocommerce-account .addresses .title h3 {
            font-family: Montserrat;
            text-transform: ;
            font-weight: 700;
        }

        @media (min-width: 769px) {
            .entry-content h1 {
                font-size: 48px
            }
        }

        @media (max-width: 768px) {
            .entry-content h1 {
                font-size: 48px
            }
        }

        @media (max-width: 550px) {
            .entry-content h1 {
                font-size: 48px
            }
        }

        .entry-content h1 {
            font-family: Montserrat;
            text-transform: ;
            font-weight: 400;
        }

        @media (min-width: 769px) {
            .zita-logo img {
                max-width: 100px
            }
        }

        @media (max-width: 768px) {
            .zita-logo img {
                max-width: 122px
            }
        }

        @media (max-width: 550px) {
            .zita-logo img {
                max-width: 101px
            }
        }

        a:hover,
        .inifiniteLoader,
        mark,
        .single .nav-previous:hover:before,
        .single .nav-next:hover:after,
        .page-numbers.current,
        .page-numbers:hover,
        .prev.page-numbers:hover,
        .next.page-numbers:hover,
        .zita-load-more #load-more-posts:hover,
        article.zita-article h2.entry-title a:hover,
        .zita-menu li a:hover,
        .main-header .zita-menu>li>a:hover,
        .woocommerce nav.woocommerce-pagination ul li a:focus,
        .woocommerce nav.woocommerce-pagination ul li a:hover,
        .woocommerce nav.woocommerce-pagination ul li span.current,
        .zita-menu li.menu-active>a,
        .main-header .main-header-bar a:hover,
        .zita-menu .content-social .social-icon li a:hover,
        .mhdrleftpan .content-social .social-icon a:hover,
        .mhdrrightpan .content-social .social-icon a:hover {
            color: #d69431
        }

        .page-numbers.current,
        .page-numbers:hover,
        .prev.page-numbers:hover,
        .next.page-numbers:hover,
        .zita-load-more #load-more-posts:hover {
            border-color: #d69431
        }

        #respond.comment-respond #submit,
        .read-more .zta-button,
        button,
        [type='submit'],
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .zita-cart p.buttons a,
        .wc-proceed-to-checkout .button.alt.wc-forward,
        .main-header .main-header-bar a.main-header-btn {
            border-color: #d69431;
            background-color: #d69431
        }

        #move-to-top,
        .zta-date-meta .posted-on,
        .mhdrleftpan .header-pan-icon span,
        .mhdrrightpan .header-pan-icon span {
            background: #d69431
        }

        .inifiniteLoader,
        .summary .yith-wcwl-wishlistaddedbrowse a,
        .summary .yith-wcwl-wishlistexistsbrowse a {
            color: #d69431
        }

        .zita_overlayloader {
            background: #f5f5f5
        }

        .woocommerce ul.products li.product .onsale,
        .woocommerce span.onsale,
        .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
        .woocommerce .widget_price_filter .ui-slider .ui-slider-handle {
            background: #d69431
        }

        .cart-contents .cart-crl {
            background: #d69431
        }

        .cart-crl:before {
            border-color: #d69431
        }

        .woocommerce #respond input#submit.alt.disabled,
        .woocommerce #respond input#submit.alt.disabled:hover,
        .woocommerce #respond input#submit.alt:disabled,
        .woocommerce #respond input#submit.alt:disabled:hover,
        .woocommerce #respond input#submit.alt:disabled[disabled],
        .woocommerce #respond input#submit.alt:disabled[disabled]:hover,
        .woocommerce a.button.alt.disabled,
        .woocommerce a.button.alt.disabled:hover,
        .woocommerce a.button.alt:disabled,
        .woocommerce a.button.alt:disabled:hover,
        .woocommerce a.button.alt:disabled[disabled],
        .woocommerce a.button.alt:disabled[disabled]:hover,
        .woocommerce button.button.alt.disabled,
        .woocommerce button.button.alt.disabled:hover,
        .woocommerce button.button.alt:disabled,
        .woocommerce button.button.alt:disabled:hover,
        .woocommerce button.button.alt:disabled[disabled],
        .woocommerce button.button.alt:disabled[disabled]:hover,
        .woocommerce input.button.alt.disabled,
        .woocommerce input.button.alt.disabled:hover,
        .woocommerce input.button.alt:disabled,
        .woocommerce input.button.alt:disabled:hover,
        .woocommerce input.button.alt:disabled[disabled],
        .woocommerce input.button.alt:disabled[disabled]:hover {
            border-color: #d69431;
            background-color: #d69431;
        }

        a,
        .single .nav-previous:before,
        .single .nav-next:after,
        .zita-menu li a,
        .main-header .zita-menu>li>a {
            color: #f2f2f2
        }

        a:hover,
        .single .nav-previous:hover:before,
        .single .nav-next:hover:after,
        article.zita-article h2.entry-title a:hover,
        .zita-menu li a:hover,
        .main-header .zita-menu>li>a:hover,
        .zita-menu li.menu-active>a,
        .main-header .main-header-bar a:hover,
        .zita-menu .content-social .social-icon li a:hover,
        .mhdrleftpan .content-social .social-icon a:hover,
        .mhdrrightpan .content-social .social-icon a:hover {
            color:
        }

        body,
        .zita-site #content .entry-meta {
            color:
        }

        article.zita-article h2.entry-title a,
        #sidebar-primary h2.widget-title,
        .woocommerce h1.product_title,
        .woocommerce-Tabs-panel h2,
        .related.products h2,
        section.up-sells h2,
        .cross-sells h2,
        .cart_totals h2,
        .woocommerce-billing-fields h3,
        .woocommerce-account .addresses .title h3,
        h1.page-title,
        h1.entry-title {
            color:
        }

        .menu-toggle .menu-btn,
        .bar-menu-toggle .menu-btn {
            background: rgba(0, 0, 0, 0);
            border-color: #000000
        }

        .menu-toggle .icon-bar,
        .bar-menu-toggle .icon-bar {
            background: #000000
        }

        .menu-toggle .menu-btn,
        .bar-menu-toggle .menu-btn {
            border-radius: 0px;
        }

        .menu-icon-inner {
            color: #000000
        }

        .menu-custom-html>a button,
        .read-more .zta-button,
        #respond.comment-respond #submit,
        button,
        [type='submit'],
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .zita-cart p.buttons a,
        .wc-proceed-to-checkout .button.alt.wc-forward,
        .main-header .main-header-bar a.main-header-btn {
            background: #0274be;
            color: #ffffff;
            border-color: #0274be;
        }

        .menu-custom-html>a button,
        .read-more .zta-button,
        #respond.comment-respond #submit,
        button,
        [type='submit'],
        .woocommerce #respond input#submit,
        .woocommerce a.button,
        .woocommerce button.button,
        .woocommerce input.button,
        .woocommerce #respond input#submit.alt,
        .woocommerce a.button.alt,
        .woocommerce button.button.alt,
        .woocommerce input.button.alt,
        .main-header .main-header-bar a.main-header-btn {
            border-radius: px;
        }

        .menu-custom-html>a button:hover,
        .read-more .zta-button:hover,
        #respond.comment-respond #submit:hover,
        button:hover,
        [type='submit']:hover,
        .woocommerce #respond input#submit:hover,
        .woocommerce a.button:hover,
        .woocommerce button.button:hover,
        .woocommerce input.button:hover,
        .woocommerce #respond input#submit.alt:hover,
        .woocommerce a.button.alt:hover,
        .woocommerce button.button.alt:hover,
        .woocommerce input.button.alt:hover,
        .zita-cart p.buttons a:hover,
        .main-header .main-header-bar .main-header .main-header-bar a.main-header-btn:hover,
        .main-header .main-header-bar a.main-header-btn:hover {
            background: #0274be;
            color: #ffffff;
            border-color: #0274be
        }

        .woocommerce #respond input#submit.alt.disabled,
        .woocommerce #respond input#submit.alt.disabled:hover,
        .woocommerce #respond input#submit.alt:disabled,
        .woocommerce #respond input#submit.alt:disabled:hover,
        .woocommerce #respond input#submit.alt:disabled[disabled],
        .woocommerce #respond input#submit.alt:disabled[disabled]:hover,
        .woocommerce a.button.alt.disabled,
        .woocommerce a.button.alt.disabled:hover,
        .woocommerce a.button.alt:disabled,
        .woocommerce a.button.alt:disabled:hover,
        .woocommerce a.button.alt:disabled[disabled],
        .woocommerce a.button.alt:disabled[disabled]:hover,
        .woocommerce button.button.alt.disabled,
        .woocommerce button.button.alt.disabled:hover,
        .woocommerce button.button.alt:disabled,
        .woocommerce button.button.alt:disabled:hover,
        .woocommerce button.button.alt:disabled[disabled],
        .woocommerce button.button.alt:disabled[disabled]:hover,
        .woocommerce input.button.alt.disabled,
        .woocommerce input.button.alt.disabled:hover,
        .woocommerce input.button.alt:disabled,
        .woocommerce input.button.alt:disabled:hover,
        .woocommerce input.button.alt:disabled[disabled],
        .woocommerce input.button.alt:disabled[disabled]:hover {
            border-color: #0274be;
            background-color: #0274be;
        }

        .mhdrleft.zta-transparent-header .top-header-bar,
        .mhdrleft.zta-transparent-header .top-header-bar:before,
        .mhdrleft.zta-transparent-header .main-header-bar,
        .mhdrleft.zta-transparent-header .main-header-bar:before,
        .mhdrleft.zta-transparent-header .bottom-header-bar,
        .mhdrleft.zta-transparent-header .bottom-header-bar:before,
        .zita-site .mhdrleft.zta-transparent-header .main-header-bar:before {
            background: transparent;
        }

        .mhdrright.zta-transparent-header .top-header-bar,
        .mhdrright.zta-transparent-header .top-header-bar:before,
        .mhdrright.zta-transparent-header .main-header-bar,
        .mhdrright.zta-transparent-header .main-header-bar:before,
        .mhdrright.zta-transparent-header .bottom-header-bar,
        .mhdrright.zta-transparent-header .bottom-header-bar:before,
        .zita-site .mhdrright.zta-transparent-header .main-header-bar:before {
            background: transparent;
        }

        .mhdrcenter.zta-transparent-header .top-header-bar,
        .mhdrcenter.zta-transparent-header .top-header-bar:before,
        .mhdrcenter.zta-transparent-header .main-header-bar,
        .mhdrcenter.zta-transparent-header .main-header-bar:before,
        .mhdrcenter.zta-transparent-header .bottom-header-bar,
        .mhdrcenter.zta-transparent-header .bottom-header-bar:before,
        .zita-site .mhdrcenter.zta-transparent-header .main-header-bar:before {
            background: transparent;
        }

        .mhdfull.zta-transparent-header,
        .mhdfull.zta-transparent-header .top-header-bar,
        .mhdfull.zta-transparent-header .main-header-bar,
        .mhdfull.zta-transparent-header .bottom-header-bar,
        .mhdfull.zta-transparent-header .top-header-bar:before,
        .mhdfull.zta-transparent-header .main-header-bar:before,
        .mhdfull.zta-transparent-header .bottom-header-bar:before {
            background: transparent;
        }

        .shrink .sider-inner ul#zita-menu {
            overflow: hidden;
        }

        .main-header-bar {
            border-bottom-width: 1px;
        }

        .main-header-bar {
            border-bottom-color: #eee
        }

        header .container,
        #container.site- container,
        footer .container,
        #content #container,
        #content.site-content.boxed #container,
        #content.site-content.contentbox #container,
        #content.site-content.fullwidthcontained #container {
            max-width: px;
        }

        .top-header-container {
            line-height: 40px;
        }

        .top-header-bar {
            border-bottom-width: 1px;
        }

        .top-header-bar {
            border-bottom-color: #eee
        }

        .bottom-header-container {
            line-height: 40px;
        }

        .bottom-header-bar {
            border-bottom-width: 1px;
        }

        .bottom-header-bar {
            border-bottom-color: #eee
        }

        .top-footer-container {
            line-height: 40px;
        }

        .top-footer-bar {
            border-bottom-width: 1px;
        }

        .top-footer-bar {
            border-bottom-color: #eee
        }

        .bottom-footer-container {
            line-height: 61px;
        }

        .bottom-footer-bar {
            border-top-width: 1px;
        }

        .bottom-footer-bar {
            border-top-color: rgba(255, 255, 255, 0.39)
        }

        .site-content #sidebar-primary {
            width: 35%
        }

        .site-content #primary {
            width: 65%
        }

        #move-to-top {
            border-radius: 2px;
            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            color: #e2ad1b;
            background: #ffffff
        }

        #move-to-top:hover {
            color: #fff;
            background: #015782;
        }

        .searchfrom .search-btn {
            font-size: 15px;
            border-radius: px;
        }

        .top-header-bar .searchfrom .search-btn,
        .main-header-bar .searchfrom .search-btn,
        .bottom-header-bar .searchfrom .search-btn,
        .zita-menu .menu-custom-search .searchfrom a {
            color: ;
            background: ;
            border-color:
        }

        .top-header-bar .searchfrom .search-btn:hover,
        .main-header-bar .searchfrom .search-btn:hover,
        .bottom-header-bar .searchfrom .search-btn:hover {
            color:
        }

        .widget-area #searchform .form-content,
        .searchfrom #searchform .form-content {
            width: 100%;
        }

        .widget-area #searchform .form-content:before,
        .searchfrom #searchform .form-content:before {
            color: #015782;
            font-size: px;
        }

        .widget-area input#s,
        .searchfrom #searchform input#s {
            background-color: ;
            border-color: ;
        }

        .widget-area #searchform input[type=submit],
        .widget-area input#s,
        .widget-area #searchform .form-content:before,
        .searchfrom #searchform .form-content:before,
        .searchfrom input#s,
        .searchfrom #searchform input[type=submit] {
            height: px;
            line-height: px;
            border-radius: 0px;
        }

        .form-content input#s::-webkit-input-placeholder,
        .form-content input#s {
            color: #bbb;
            font-size: px;
        }

        .main-header .main-header-bar,
        .mhdrleftpan header,
        .mhdrrightpan header {
            background-color: #fff;
            background-image: url('');
        }

        .zita-site .main-header-bar:before,
        header.mhdrrightpan:before,
        header.mhdrleftpan:before {
            background: #fff;
            opacity: 0.7
        }

        .main-header-bar p,
        .main-header .zita-menu>li>a,
        .main-header .menu-custom-html,
        .main-header .menu-custom-widget,
        .main-header .widget-title,
        header.mhdrleftpan p,
        header.mhdrrightpan p,
        header.mhdrleftpan .widget-title,
        header.mhdrrightpan .widget-title,
        header.mhdrrightpan .content-html,
        header.mhdrleftpan .content-html,
        .mhdrrightpan .zita-menu a,
        .mhdrleftpan .zita-menu a,
        .mhdrleftpan .content-widget,
        .mhdrrightpan .content-widget,
        header.mhdrleftpan .top-header .top-header-bar .widget-title,
        header.mhdrrightpan .top-header .top-header-bar .widget-title,
        .mhdrrightpan .zita-menu li a,
        .mhdrleftpan .zita-menu li a,
        .mhdrrightpan .bottom-header .zita-menu>li>a,
        .mhdrleftpan .bottom-header .zita-menu>li>a {
            color: #555
        }

        .main-header .main-header-bar a,
        .mhdrleftpan .content-social .social-icon a,
        .mhdrrightpan .content-social .social-icon a,
        .zita-menu .content-social .social-icon li a {
            color: #9c9c9c
        }

        .main-header .main-header-bar a:hover {
            color:
        }

        .zita-cart p.buttons a.checkout {
            background: transparent;
            border-color: #9c9c9c;
            color: #9c9c9c;
        }

        header.mhdminbarleft p,
        header.mhdminbarright p,
        header.mhdminbarleft .widget-title,
        header.mhdminbarright .widget-title,
        header.mhdminbarleft .content-html,
        header.mhdminbarright .content-html,
        .mhdminbarleft .zita-menu a,
        .mhdminbarright .zita-menu a,
        .mhdminbarleft .content-widget,
        .mhdminbarright .content-widget,
        header.mhdminbarleft .top-header .top-header-bar .widget-title,
        header.mhdminbarright .top-header .top-header-bar .widget-title,
        .mhdminbarleft .zita-menu li a,
        .mhdminbarright .zita-menu li a,
        .mhdminbarleft .bottom-header .zita-menu>li>a,
        .mhdminbarright .bottom-header .zita-menu>li>a {
            color: #555
        }

        .widget-footer .widget-footer-bar {
            background: #333
        }

        .widget-footer .widget-footer-bar .widget-title,
        .widget-footer .widget-footer-bar,
        .widget-footer .widget-footer-bar a {
            color: #fff
        }

        .widget-footer .widget-footer-bar a:hover {
            color: #d69431
        }

        .widget-footer .widget-footer-bar a:hover {
            color:
        }

        .bottom-footer .bottom-footer-bar {
            background: #333
        }

        .bottom-footer .content-html,
        .bottom-footer .zita-menu>li>a,
        .bottom-footer .content-widget,
        .bottom-footer .bottom-footer-bar .widget-title,
        .zita-bottom-menu li a,
        .bottom-footer .bottom-footer-bar a {
            color: #fff
        }

        .bottom-footer .bottom-footer-bar a:hover {
            color: #d69431
        }

        .bottom-footer .bottom-footer-bar a:hover {
            color:
        }

        .zita-cart,
        .zita-cart ul.cart_list li span,
        .zita-cart p {
            background: #ffff;
            color: #808285;
        }

        .zita-cart ul.cart_list li a {
            color: #9c9c9c;
        }

        .zita-cart p.buttons a.checkout {
            background: transparent;
            border-color: #9c9c9c;
            color: #9c9c9c;
        }

    </style>
    <link rel='stylesheet' id='zita-google-font-montserrat-css'
        href='//fonts.googleapis.com/css?family=Montserrat%3A300%2C400%2C500%2C700&#038;subset=latin&#038;ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='dashicons-css'
        href='/wp-includes/css/dashicons.min.css?ver=5.5.3' type='text/css' media='all' />
    <link rel='stylesheet' id='joinchat-css'
        href='/wp-content/plugins/creame-whatsapp-me/public/css/joinchat.min.css?ver=4.0.9'
        type='text/css' media='all' />
    <style id='joinchat-inline-css' type='text/css'>
        .joinchat {
            --red: 37;
            --green: 211;
            --blue: 102;
        }

    </style>
    <link rel='stylesheet' id='elementor-icons-css'
        href='/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.9.1'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css'
        href='/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.0.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-legacy-css'
        href='/wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.0.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css'
        href='/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-325-css'
        href='/wp-content/uploads/elementor/css/post-325.css?ver=1598504098'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css'
        href='/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=2.4.7'
        type='text/css' media='all' />
    <link rel='stylesheet' id='lfb-style-css'
        href='/wp-content/plugins/lead-form-builder/elementor//css/lfb-styler.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elaet-frontend-min-style-css'
        href='/wp-content/plugins/zita-site-library/addons//assets/css/elaet.frontend.min.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elite-addons-style-css'
        href='/wp-content/plugins/zita-site-library/addons//assets/css/elite-addons-style.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='twentytwenty-css-css'
        href='/wp-content/plugins/zita-site-library/addons//assets/css/twentytwenty.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-5-all-css'
        href='/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=3.0.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-4-shim-css'
        href='/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=3.0.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-global-css'
        href='/wp-content/uploads/elementor/css/global.css?ver=1598504099' type='text/css'
        media='all' />
    <link rel='stylesheet' id='elementor-post-568-css'
        href='/wp-content/uploads/elementor/css/post-568.css?ver=1598639178'
        type='text/css' media='all' />
    <link rel='stylesheet' id='lfb_f_css-css'
        href='/wp-content/plugins/lead-form-builder/css/f-style.css?ver=5.5.3'
        type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'
        href='/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0'
        type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'
        href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.5.3'
        type='text/css' media='all' />
    <script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'
        id='jquery-core-js'></script>
    <script type='text/javascript'
        src='/wp-content/plugins/download-manager/assets/bootstrap/js/bootstrap.bundle.min.js?ver=5.5.3'
        id='wpdm-front-bootstrap-js'></script>
    <script type='text/javascript' id='frontjs-js-extra'>
        /* <![CDATA[ */
        var wpdm_url = {
            "home": "\/",
            "site": "\/",
            "ajax": "\/wp-admin\/admin-ajax.php"
        };
        var wpdm_asset = {
            "spinner": "<i class=\"fas fa-sun fa-spin\"><\/i>"
        };
        /* ]]> */

    </script>
    <script type='text/javascript'
        src='/wp-content/plugins/download-manager/assets/js/front.js?ver=3.1.05'
        id='frontjs-js'></script>
    <script type='text/javascript'
        src='/wp-content/plugins/download-manager/assets/js/chosen.jquery.min.js?ver=5.5.3'
        id='jquery-choosen-js'></script>
    <script type='text/javascript'
        src='/wp-content/plugins/wp-table-builder/inc/admin/js/WPTB_ResponsiveFrontend.js?ver=1.2.6'
        id='wp-table-builder_responsive-frontend-js'></script>
    <script type='text/javascript'
        src='/wp-content/plugins/wp-table-builder/inc/frontend/js/wp-table-builder-frontend.js?ver=1.2.6'
        id='wp-table-builder-js'></script>
    <script type='text/javascript'
        src='//platform-api.sharethis.com/js/sharethis.js#product=ga&#038;property=5f0bbf8f218f8e001ab0c1e1'
        id='googleanalytics-platform-sharethis-js'></script>
    <script type='text/javascript'
        src='/wp-content/plugins/elementor/assets/lib/font-awesome/js/v4-shims.min.js?ver=3.0.2'
        id='font-awesome-4-shim-js'></script>
    <link rel="https://api.w.org/" href="/wp-json/" />
    <link rel="alternate" type="application/json" href="/wp-json/wp/v2/pages/568" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
        href="/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.5.3" />
    <link rel="canonical" href="/program-qurban/" />
    <link rel='shortlink' href='/?p=568' />
    <link rel="alternate" type="application/json+oembed"
        href="/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fqurban.zakatsukses.org%2Fprogram-qurban%2F" />
    <link rel="alternate" type="text/xml+oembed"
        href="/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fqurban.zakatsukses.org%2Fprogram-qurban%2F&#038;format=xml" />

    <!-- Affiliates Manager plugin v2.7.6 - https://wpaffiliatemanager.com/ -->

    <script>
        var wpdm_site_url = '/';
        var wpdm_home_url = '/';
        var ajax_url = '/wp-admin/admin-ajax.php';
        var wpdm_ajax_url = '/wp-admin/admin-ajax.php';
        var wpdm_ajax_popup = '0';

    </script>
    <style>
        .wpdm-download-link.btn.btn-primary. {
            border-radius: 4px;
        }

    </style>


    <style type="text/css">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }

    </style>
    <script>
        (function() {
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-172498035-1', 'auto');
            ga('send', 'pageview');
        })();

    </script>
    <link rel="icon"
        href="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-32x32.png"
        sizes="32x32" />
    <link rel="icon"
        href="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-192x192.png"
        sizes="192x192" />
    <link rel="apple-touch-icon"
        href="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-180x180.png" />
    <meta name="msapplication-TileImage"
        content="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res-1-270x270.png" />
    <style type="text/css" id="wp-custom-css">
        /** Start Block Kit CSS: 141-3-1d55f1e76be9fb1a8d9de88accbe962f **/

        .envato-kit-138-bracket .elementor-widget-container>*:before {
            content: "[";
            color: #ffab00;
            display: inline-block;
            margin-right: 4px;
            line-height: 1em;
            position: relative;
            top: -1px;
        }

        .envato-kit-138-bracket .elementor-widget-container>*:after {
            content: "]";
            color: #ffab00;
            display: inline-block;
            margin-left: 4px;
            line-height: 1em;
            position: relative;
            top: -1px;
        }

        /** End Block Kit CSS: 141-3-1d55f1e76be9fb1a8d9de88accbe962f **/

    </style>
    <meta name="generator" content="WordPress Download Manager 3.1.05" />
    <style>
        @import url('https://fonts.googleapis.com/css?family=Rubik:400,500');



        .w3eden .fetfont,
        .w3eden .btn,
        .w3eden .btn.wpdm-front h3.title,
        .w3eden .wpdm-social-lock-box .IN-widget a span:last-child,
        .w3eden #xfilelist .panel-heading,
        .w3eden .wpdm-frontend-tabs a,
        .w3eden .alert:before,
        .w3eden .panel .panel-heading,
        .w3eden .discount-msg,
        .w3eden .panel.dashboard-panel h3,
        .w3eden #wpdm-dashboard-sidebar .list-group-item,
        .w3eden #package-description .wp-switch-editor,
        .w3eden .w3eden.author-dashbboard .nav.nav-tabs li a,
        .w3eden .wpdm_cart thead th,
        .w3eden #csp .list-group-item,
        .w3eden .modal-title {
            font-family: Rubik, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            text-transform: uppercase;
            font-weight: 500;
        }

        .w3eden #csp .list-group-item {
            text-transform: unset;
        }

    </style>
    <style>
        :root {
            --color-primary: #4a8eff;
            --color-primary-rgb: 74, 142, 255;
            --color-primary-hover: #4a8eff;
            --color-primary-active: #4a8eff;
            --color-secondary: #4a8eff;
            --color-secondary-rgb: 74, 142, 255;
            --color-secondary-hover: #4a8eff;
            --color-secondary-active: #4a8eff;
            --color-success: #18ce0f;
            --color-success-rgb: 24, 206, 15;
            --color-success-hover: #4a8eff;
            --color-success-active: #4a8eff;
            --color-info: #2CA8FF;
            --color-info-rgb: 44, 168, 255;
            --color-info-hover: #2CA8FF;
            --color-info-active: #2CA8FF;
            --color-warning: #f29e0f;
            --color-warning-rgb: 242, 158, 15;
            --color-warning-hover: orange;
            --color-warning-active: orange;
            --color-danger: #ff5062;
            --color-danger-rgb: 255, 80, 98;
            --color-danger-hover: #ff5062;
            --color-danger-active: #ff5062;
            --color-green: #30b570;
            --color-blue: #0073ff;
            --color-purple: #8557D3;
            --color-red: #ff5062;
            --color-muted: rgba(69, 89, 122, 0.6);
            --wpdm-font: Rubik, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        .wpdm-download-link.btn.btn-primary {
            border-radius: 4px;
        }

    </style>
</head>
<!-- layout class call -->
<!-- layout class call -->

<body
    class="page-template-default page page-id-568 wp-custom-logo fullwidthcontained mhdrleft abv-none fullwidth no-home elementor-default elementor-kit-325 elementor-page elementor-page-568">


    <div id="page" class="zita-site">
        <header class="mhdrleft      ">
            <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
            <!-- minbar header -->
            <!-- end minbar header -->
            <!-- top-header start -->

            <div class="main-header mhdrleft stack right-menu linkeffect-none">
                <div class="main-header-bar two">
                    <div class="container">
                        <div class="main-header-container">
                            <div class="main-header-col1">
                                <div class="zita-logo">
                                    <a href="/" class="custom-logo-link" rel="home"><img
                                            width="3071" height="2480"
                                            src="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res.png"
                                            class="custom-logo" alt="Cinta Qurban"
                                            srcset="/wp-content/uploads/2020/06/cropped-Logo-ZS-High-Res.png 1x, /wp-content/uploads/2020/06/Logo-ZS-High-Res.png 2x"
                                            sizes="(max-width: 3071px) 100vw, 3071px" /></a>
                                </div>
                            </div>
                            <div class="main-header-col2">
                                <nav>
                                    <!-- Menu Toggle btn-->
                                    <div class="menu-toggle">
                                        <button type="button" class="menu-btn" id="menu-btn">
                                            <div class="btn">
                                                <span class="icon-bar" tabindex="-1"></span>
                                                <span class="icon-bar" tabindex="-1"></span>
                                                <span class="icon-bar" tabindex="-1"></span>
                                            </div>
                                            <div class="text">
                                            </div>

                                        </button>
                                    </div>
                                    <div class="sider main zita-menu-hide right">
                                        <div class="sider-inner">
                                            <ul id="zita-menu" class="zita-menu" data-menu-style=horizontal>
                                                <li id="menu-item-137"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137">
                                                    <a href="{{ route('faq') }}"><span
                                                            class="zita-menu-link">FAQ</span></a>
                                                </li>
                                                <li id="menu-item-137"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-137">
                                                    <a href="{{ route('program') }}"><span
                                                            class="zita-menu-link">Program</span></a>
                                                </li>
                                                <li id="menu-item-567"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-567">
                                                    <a href="{{ route('galeri') }}"><span
                                                            class="zita-menu-link">Galeri</span></a>
                                                </li>
                                                <li id="menu-item-912"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-912">
                                                    <a
                                                        href="https://docs.google.com/forms/d/e/1FAIpQLSdHwMQvsmskC1Y5kWMh8BuRps16dLHUdMcvabrEKtXMWWDgnQ/viewform?usp=sf_link"><span
                                                            class="zita-menu-link">Daftar</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <!-- Responsive Menu Structure-->
                            </div> <!-- col-2-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- bottom-header end-->
        </header>
        <div id="content" class="site-content fullwidthcontained">
            <div id="container" class="site-container no-sidebar">
                <div id="primary" class="main content-area">
                    <main id="main" class="site-main" role="main">
                        <article id="post-568" class="zita-article post-568 page type-page status-publish hentry">
                            <div class="entry-header entry-page">
                                <h1 class='entry-title'>Program Qurban</h1>
                            </div>
                            <div class="entry-content">
                                <div data-elementor-type="wp-page" data-elementor-id="568"
                                    class="elementor elementor-568" data-elementor-settings="[]">
                                    <div class="elementor-inner">
                                        <div class="elementor-section-wrap">
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-9e6bd6e elementor-section-stretched elementor-section-height-min-height elementor-section-content-top elementor-section-boxed elementor-section-height-default elementor-section-items-middle"
                                                data-id="9e6bd6e" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-4f288c5"
                                                            data-id="4f288c5" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-6833ca3 elementor-widget elementor-widget-image"
                                                                        data-id="6833ca3" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img width="1000" height="1100"
                                                                                    src="/wp-content/uploads/2022/06/qurban pelosok.png"
                                                                                    class="attachment-full size-full"
                                                                                    alt="" loading="lazy"
                                                                                    srcset="/wp-content/uploads/2022/06/qurban pelosok.png 1000w, /wp-content/uploads/2022/06/qurban pelosok.png 273w, /wp-content/uploads/2022/06/qurban pelosok.png 931w, /wp-content/uploads/2022/06/qurban pelosok.png 768w, /wp-content/uploads/2022/06/qurban pelosok.png 600w"
                                                                                    sizes="(max-width: 1000px) 100vw, 1000px" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-098c237"
                                                            data-id="098c237" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-aa474df elementor-widget elementor-widget-text-editor"
                                                                        data-id="aa474df" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <h1><strong>QURBAN PELOSOK
                                                                                        NUSANTARA</strong></h1>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-ddb765c elementor-widget elementor-widget-text-editor"
                                                                        data-id="ddb765c" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>LAZ Zakat Sukses meluncurkan program Qurban Pelosok Nusantara yang dilaksanakan di wilayah pelosok NTT meliputi Kabupaten Alor, Desa Baranusa, Desa Pameti Karata, Kec. Lewa, Kab. Sumba Timur. Lokasi dapat berubah sesuai kondisi, namun tetap di wilayah pelosok Nusa Tenggara Timur.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-3ed5d78"
                                                            data-id="3ed5d78" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-5e415db elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="5e415db" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-86ec94b"
                                                            data-id="86ec94b" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-b619e5f elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="b619e5f" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                                <div class="elementor-container elementor-column-gap-narrow">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-116ab9f"
                                                            data-id="116ab9f" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-3f628af elementor-widget elementor-widget-image"
                                                                        data-id="3f628af" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img width="640" height="704"
                                                                                    src="/wp-content/uploads/2022/06/qurban sukses.png"
                                                                                    class="attachment-large size-large"
                                                                                    alt="" loading="lazy"
                                                                                    srcset="/wp-content/uploads/2022/06/qurban sukses.png 931w, /wp-content/uploads/2022/06/qurban sukses.png 273w, /wp-content/uploads/2022/06/qurban sukses.png 768w, /wp-content/uploads/2022/06/qurban sukses.png 600w, /wp-content/uploads/2022/06/qurban sukses.png 1000w"
                                                                                    sizes="(max-width: 640px) 100vw, 640px" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-7db88bb"
                                                            data-id="7db88bb" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-d71e6d2 elementor-widget elementor-widget-text-editor"
                                                                        data-id="d71e6d2" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <h1><strong>QURBAN SUKSES</strong>
                                                                                </h1>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-ffa7f8f elementor-widget elementor-widget-text-editor"
                                                                        data-id="ffa7f8f" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>LAZ Zakat Sukses meluncurkan program Qurban Sukses, program ini dilaksanakan di kantor Zakat Sukses yang berlokasi di Sukmajaya kota Depok dan akan didistribusikan untuk masyarakat dhuafa yang lebih membutuhkan di 11 kecamatan di kota Depok dan sekitarnya.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-43ad1c7"
                                                            data-id="43ad1c7" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-b21a503 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="b21a503" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-9e956e9"
                                                            data-id="9e956e9" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            {{-- <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-88b44f8 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="88b44f8" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                                <div class="elementor-container elementor-column-gap-narrow">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-28e52c0"
                                                            data-id="28e52c0" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-4a5d7ef elementor-widget elementor-widget-image"
                                                                        data-id="4a5d7ef" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <img width="640" height="704"
                                                                                    src="/wp-content/uploads/2020/07/Qurban-Dunia-Islam-931x1024.jpeg"
                                                                                    class="attachment-large size-large"
                                                                                    alt="" loading="lazy"
                                                                                    srcset="/wp-content/uploads/2020/07/Qurban-Dunia-Islam-931x1024.jpeg 931w, /wp-content/uploads/2020/07/Qurban-Dunia-Islam-273x300.jpeg 273w, /wp-content/uploads/2020/07/Qurban-Dunia-Islam-768x845.jpeg 768w, /wp-content/uploads/2020/07/Qurban-Dunia-Islam-600x660.jpeg 600w, /wp-content/uploads/2020/07/Qurban-Dunia-Islam.jpeg 1000w"
                                                                                    sizes="(max-width: 640px) 100vw, 640px" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-5e3c67b"
                                                            data-id="5e3c67b" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-6434a92 elementor-widget elementor-widget-text-editor"
                                                                        data-id="6434a92" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <h1><strong>QURBAN DUNIA ISLAM</strong>
                                                                                </h1>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-c8ad99e elementor-widget elementor-widget-text-editor"
                                                                        data-id="c8ad99e" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>QURBAN PELOSOK NUSANTARA &amp; DUNIA
                                                                                    ISLAM</p>
                                                                                <p>LAZ Zakat Sukses meluncurkan program
                                                                                    Qurban Dunia Islam. Qurban Dunia
                                                                                    Islam adalah qurban yang
                                                                                    dilaksanakan di negara Islam yang
                                                                                    sedang mengalami konflik seperti
                                                                                    Palestina, Suriah, Yaman, dan
                                                                                    lain-lain. Daging qurban nanti juga
                                                                                    akan didistribusikan untuk kaum
                                                                                    muslimin di sana. Penuhi panggilan
                                                                                    qurban dan mari menebar kebahagiaan.
                                                                                    Yuk, sisihkan buat qurban dari
                                                                                    sekarang dengan berdonasi ke nomor
                                                                                    rekening Yayasan Zakat Sukses.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-88edc83"
                                                            data-id="88edc83" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section> --}}
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .entry-content -->
                        </article><!-- #post -->
                    </main><!-- #main -->
                </div>
            </div>
        </div><!-- #primary -->

        <footer id="zita-footer">

        <div class="footer-wrap widget-area">

            <div class="widget-footer">
                <div class="widget-footer-bar ft-wgt-three">
                    <div class="container">
                        <div class="widget-footer-container">
                            <div class="widget-footer-col1">
                                <div id="text-3" class="widget widget_text">
                                    <h3 class="widget-title">Zakat Sukses</h3>
                                    <div class="textwidget">
                                        <p style="margin-top:20px;"><img loading="lazy" class="alignnone wp-image-332 mt-2"
                                                src="/wp-content/uploads/2020/06/Logo-ZS-High-Res-300x242.png"
                                                alt="" width="89" height="74" /></p>
                                        <p style="margin-top:30px;">Sebuah institusi LAZ kota yang menjadi model nasional dalam mewujudkan
                                            masyarakat berdaya dan peduli</p>
                                    </div>
                                </div>
                            </div>
                            <div class="widget-footer-col2">
                                <div id="text-4" class="widget widget_text">
                                    <div class="textwidget"></div>
                                </div>
                            </div>
                            <div class="widget-footer-col3">
                                <div id="astra-widget-address-2" class="widget astra-widget-address">
                                    <h3 class="widget-title">Alamat</h3>
                                    <div class="address clearfix">
                                        <address class="widget-address widget-address-stack widget-address-icons-1">

                                            <div class="widget-address-field">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="address-icons"
                                                    width="15px" height="15px" viewBox="0 0 496 512">
                                                    <path
                                                        d="M336.5 160C322 70.7 287.8 8 248 8s-74 62.7-88.5 152h177zM152 256c0 22.2 1.2 43.5 3.3 64h185.3c2.1-20.5 3.3-41.8 3.3-64s-1.2-43.5-3.3-64H155.3c-2.1 20.5-3.3 41.8-3.3 64zm324.7-96c-28.6-67.9-86.5-120.4-158-141.6 24.4 33.8 41.2 84.7 50 141.6h108zM177.2 18.4C105.8 39.6 47.8 92.1 19.3 160h108c8.7-56.9 25.5-107.8 49.9-141.6zM487.4 192H372.7c2.1 21 3.3 42.5 3.3 64s-1.2 43-3.3 64h114.6c5.5-20.5 8.6-41.8 8.6-64s-3.1-43.5-8.5-64zM120 256c0-21.5 1.2-43 3.3-64H8.6C3.2 212.5 0 233.8 0 256s3.2 43.5 8.6 64h114.6c-2-21-3.2-42.5-3.2-64zm39.5 96c14.5 89.3 48.7 152 88.5 152s74-62.7 88.5-152h-177zm159.3 141.6c71.4-21.2 129.4-73.7 158-141.6h-108c-8.8 56.9-25.6 107.8-50 141.6zM19.3 352c28.6 67.9 86.5 120.4 158 141.6-24.4-33.8-41.2-84.7-50-141.6h-108z">
                                                    </path>
                                                </svg>
                                                <span class="address-meta"><a><i> Jl. K.H.M. Yusuf Raya No.95, Mekar Jaya,
                                                    Kec. Sukmajaya, Kota Depok, Jawa Barat 16411</i></a></span>
                                            </div>
                                            <div class="widget-address-field">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="address-icons"
                                                    width="15px" height="15px" viewBox="0 0 512 512">
                                                    <path
                                                        d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z">
                                                    </path>
                                                </svg>
                                                <span class="address-meta">
                                                    <a href="tel:082211627700">0822-1162-7700</a>
                                                </span>
                                            </div>
                                            <div class="widget-address-field">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="address-icons"
                                                    width="15px" height="15px" viewBox="0 0 512 512">
                                                    <path
                                                        d="M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z">
                                                    </path>
                                                </svg>
                                                <span class="address-meta">
                                                    <a
                                                        href="mailto:&#099;o&#110;&#116;&#097;&#099;t&#064;&#122;&#097;ka&#116;suk&#115;&#101;s.o&#114;g">&#099;on&#116;&#097;c&#116;&#064;z&#097;&#107;&#097;t&#115;&#117;k&#115;e&#115;&#046;&#111;&#114;&#103;</a>
                                                </span>
                                            </div>
                                        </address>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
        <div class="joinchat joinchat--right"
            data-settings='{"telephone":"6282211627700","mobile_only":false,"button_delay":3,"whatsapp_web":true,"message_views":2,"message_delay":10,"message_badge":false,"message_send":"Assalamu`alaikum , saya ingin bertanya mengenai Qurban","message_hash":"b61fc3d"}'>
            <div class="joinchat__button">
                <div class="joinchat__button__open"></div>
                <div class="joinchat__button__sendtext">Open chat</div>
                <svg class="joinchat__button__send" viewbox="0 0 400 400" stroke-linecap="round" stroke-width="33">
                    <path class="joinchat_svg__plain"
                        d="M168.83 200.504H79.218L33.04 44.284a1 1 0 0 1 1.386-1.188L365.083 199.04a1 1 0 0 1 .003 1.808L34.432 357.903a1 1 0 0 1-1.388-1.187l29.42-99.427" />
                    <path class="joinchat_svg__chat"
                        d="M318.087 318.087c-52.982 52.982-132.708 62.922-195.725 29.82l-80.449 10.18 10.358-80.112C18.956 214.905 28.836 134.99 81.913 81.913c65.218-65.217 170.956-65.217 236.174 0 42.661 42.661 57.416 102.661 44.265 157.316" />
                </svg>
                <div class="joinchat__tooltip">
                    <div>Assalamu`alaikum,</div>
                </div>
            </div>
            <div class="joinchat__box">
                <div class="joinchat__header">
                    <a class="joinchat__copy"
                        href="https://join.chat/en/powered/?site=Cinta+Qurban&url=http%3A%2F%2Fqurban.zakatsukses.org%2Fprogram-qurban"
                        rel="nofollow noopener" target="_blank">
                        Powered by <svg viewbox="0 0 1424 318">
                            <title>Join.chat</title>
                            <path
                                d="M170.93 7c1.395 0 3.255.583 5.58 1.75 2.325 1.165 3.487 2.331 3.487 3.497l-.013.532-.03.662c-.042.827-.115 2.012-.22 3.554l-.574 8.06c-.418 6.108-.837 14.2-1.255 24.275-.415 9.985-.645 20.527-.69 31.626l.002 31.293.027 5.908c.027 4.503.072 9.813.136 15.928l.265 23.666c.127 12.388.19 22.877.19 31.466 0 21.982-5.813 42.824-17.44 62.525-11.628 19.701-27.876 35.67-48.743 47.905S67.997 318 43.289 318c-13.912 0-24.605-2.748-32.08-8.243-7.475-5.496-11.212-13.22-11.212-23.175 0-7.258 2.336-13.48 7.008-18.664 4.671-5.185 10.952-7.777 18.842-7.777 6.852 0 13.081 1.97 18.688 5.91 5.412 3.805 9.664 7.947 12.754 12.428l.326.482a96.787 96.787 0 0010.278 12.91c3.738 3.94 7.164 5.91 10.278 5.91 3.945 0 7.164-2.023 9.655-6.066 2.449-3.975 4.496-11.704 6.143-23.19l.086-.607c1.634-11.63 2.465-27.476 2.491-47.537v-116.21l.103-.075.001-27.831c0-1.537-.206-2.557-.618-3.06l-.08-.089c-.413-.414-1.377-.829-2.892-1.243l-.595-.156-11.856-2.099c-1.86-.233-2.79-2.449-2.79-6.647 0-3.731.93-5.947 2.79-6.647 26.968-10.495 56.145-26.587 87.531-48.277 1.163-.7 2.093-1.049 2.79-1.049zm1205 43c3.926 0 5.992.835 6.199 2.505 1.24 9.605 2.066 21.819 2.48 36.642h.488c3.02-.005 8.54-.058 16.557-.156 7.836-.097 13.55-.149 17.144-.156h.832c1.653 0 2.79.678 3.41 2.035s.929 4.019.929 7.986-.31 6.524-.93 7.673c-.62 1.148-1.756 1.722-3.409 1.722h-1.912c-15.123-.008-26.056-.113-32.8-.313v62.01c0 13.78 1.705 23.279 5.114 28.499 3.41 5.22 8.73 7.829 15.961 7.829 1.447 0 2.996-.313 4.65-.94 1.652-.626 2.685-.94 3.098-.94 1.86 0 3.72.993 5.58 2.976 1.859 1.984 2.479 3.706 1.859 5.168-4.133 10.648-11.468 19.886-22.005 27.716-10.538 7.83-22.625 11.744-36.262 11.744-16.116 0-28.41-4.854-36.881-14.563-3.314-3.798-5.98-8.164-7.998-13.097l-.422.42-.568.56c-17.407 17.12-32.986 25.68-46.738 25.68-18.674 0-31.745-13.069-39.215-39.206-4.98 12.348-11.982 21.97-21.007 28.864-9.026 6.895-19.244 10.342-30.656 10.342-11.826 0-21.526-4.168-29.1-12.503-7.572-8.335-11.359-18.574-11.359-30.717 0-9.467 1.66-17.133 4.98-22.999 3.32-5.865 9.025-10.959 17.117-15.281 13.14-6.924 35.318-13.848 66.536-20.771l1-.221v-10.617c-.072-10.763-1.731-19.264-4.977-25.503-3.32-6.38-7.884-9.57-13.694-9.57-2.82 0-4.466 1.551-4.94 4.653l-.04.287-2.178 14.818-.088.638c-1.512 10.59-5.217 18.557-11.116 23.904-6.017 5.454-13.486 8.181-22.408 8.181-5.187 0-9.544-1.543-13.072-4.63-3.527-3.088-5.29-7.307-5.29-12.658 0-10.702 8.766-21.712 26.298-33.032S1214.6 88 1237.007 88c41.082 0 61.829 15.23 62.24 45.688l.01.928v57.47c.019 4.635.226 8.426.622 11.372.415 3.087.986 5.454 1.712 7.1.726 1.647 1.66 2.676 2.8 3.088 1.142.411 2.335.411 3.58 0 1.245-.412 2.8-1.235 4.668-2.47.682-.507 1.224-.806 1.625-.896-.622-4.09-.932-8.452-.932-13.086v-85.811c0-1.462-.207-2.401-.62-2.819-.413-.417-1.446-.835-3.1-1.252l-11.157-1.566c-1.653-.209-2.479-2.297-2.479-6.264 0-4.384.826-6.681 2.48-6.89 15.909-3.758 29.03-8.664 39.36-14.72 10.331-6.054 20.662-14.51 30.993-25.367 1.653-1.67 4.029-2.505 7.128-2.505zM290.13 88c27.5 0 49.688 7.203 66.563 21.61 16.875 14.406 25.312 33.958 25.312 58.655 0 25.726-9.01 45.947-27.031 60.662S313.255 251 283.88 251c-27.5 0-49.688-7.203-66.563-21.61-16.874-14.406-25.312-33.958-25.312-58.655 0-25.726 9.01-45.947 27.031-60.662S260.755 88 290.13 88zm588.15 0c18.56 0 33.407 4.116 44.542 12.348 11.136 8.233 16.704 17.803 16.704 28.71 0 6.175-2.166 11.269-6.496 15.282s-9.898 6.02-16.703 6.02c-12.992 0-24.024-8.541-33.098-25.623-5.568-10.496-9.847-17.34-12.837-20.53s-6.238-4.785-9.743-4.785c-7.424 0-11.136 5.454-11.136 16.362 0 13.583 3.093 28.247 9.28 43.992 6.186 15.744 13.92 28.247 23.199 37.508 8.042 8.027 16.497 12.04 25.364 12.04 7.63 0 15.363-3.293 23.2-9.879 1.443-1.029 3.505-.617 6.186 1.235 2.68 1.852 3.712 3.602 3.093 5.248-5.155 12.349-14.744 22.948-28.767 31.797-14.022 8.85-30.21 13.275-48.563 13.275-23.303 0-42.377-7.41-57.225-22.227-14.847-14.818-22.271-34.164-22.271-58.038 0-24.491 8.97-44.403 26.911-59.736C827.86 95.666 850.647 88 878.28 88zm-402.36-2.78c1.228 0 2.864.52 4.91 1.56 2.044 1.039 3.067 2.079 3.067 3.119 0 .832-.205 4.055-.614 9.67-.409 5.616-.818 13.415-1.227 23.398-.385 9.395-.589 19.344-.611 29.845l-.002 1.975v74.247l.004.246c.076 2.265 1.221 3.624 3.436 4.077l.241.045 10.43 2.184.135.022c.142.028.277.074.405.135.125-.045.257-.076.394-.093l10.534-2.174.244-.045c2.316-.467 3.474-1.9 3.474-4.301v-81.921c-.024-1.298-.23-2.14-.617-2.529-.414-.414-1.446-.828-3.099-1.242l-10.534-1.863-.148-.023c-1.554-.305-2.331-2.263-2.331-5.876 0-3.312.826-5.278 2.479-5.899 21.069-8.28 45.856-22.561 74.36-42.846.827-.62 1.653-.931 2.48-.931 1.239 0 2.891.517 4.957 1.552s3.098 2.07 3.098 3.105v.07c-.013.815-.22 4.828-.62 12.039a392.8 392.8 0 00-.619 21.733c4.544-10.142 11.722-18.784 21.534-25.925 9.811-7.14 21.12-10.711 33.927-10.711 16.318 0 29.177 4.657 38.575 13.971 9.399 9.315 14.098 22.355 14.098 39.12v88.42c.08 2.335 1.318 3.702 3.714 4.102l10.534 2.174.136.022c1.562.313 2.343 2.582 2.343 6.808 0 4.347-.826 6.52-2.479 6.52h-.08c-1.25-.017-7.576-.38-18.975-1.087-11.67-.724-21.947-1.086-30.829-1.086s-18.848.362-29.9 1.086c-11.05.725-17.092 1.087-18.125 1.087-1.652 0-2.478-2.173-2.478-6.52 0-3.933.826-6.21 2.478-6.83l8.366-2.174.303-.078c1.476-.394 2.408-.834 2.795-1.319.413-.517.62-1.5.62-2.95v-61.884c-.066-14.105-2.079-24.007-6.04-29.706-4.028-5.796-11.206-8.693-21.534-8.693-3.098 0-5.37.31-6.816.931v99.636c.025 1.294.231 2.183.617 2.666.413.518 1.446.983 3.098 1.397l8.366 2.174.152.063c1.551.701 2.326 2.957 2.326 6.767 0 4.347-.826 6.52-2.478 6.52h-.085c-1.243-.018-7.205-.38-17.886-1.087-10.948-.724-20.862-1.086-29.744-1.086s-19.21.362-30.984 1.086c-11.774.725-18.177 1.087-19.21 1.087-.165 0-.32-.022-.469-.065-.107.032-.22.052-.337.06l-.127.005h-.08c-1.238-.017-7.5-.38-18.788-1.092-11.555-.728-21.73-1.092-30.525-1.092-8.794 0-19.02.364-30.678 1.092S397.483 249 396.461 249c-1.637 0-2.455-2.184-2.455-6.551 0-4.246.773-6.527 2.32-6.841l.134-.022 10.431-2.184.241-.045c2.215-.453 3.36-1.812 3.436-4.077l.004-.246v-82.046l-.002-.267c-.024-1.304-.228-2.15-.611-2.54-.384-.39-1.306-.78-2.768-1.17l-.3-.079-10.43-1.871-.147-.024c-1.539-.306-2.308-2.273-2.308-5.904 0-3.327.818-5.303 2.454-5.927 23.725-9.359 49.393-23.71 77.003-43.05 1.023-.625 1.84-.937 2.455-.937zM1014.74 10c1.24 0 2.892.513 4.957 1.538 2.066 1.025 3.099 2.05 3.099 3.076 0 .82-.207 3.999-.62 9.535-.413 5.537-.826 13.227-1.24 23.07-.412 9.843-.619 20.3-.619 31.374v42.756l.391-.674c5.136-8.727 12.235-16.09 21.298-22.088 9.295-6.152 19.83-9.228 31.603-9.228 16.318 0 29.177 4.614 38.575 13.842 9.399 9.228 14.098 22.146 14.098 38.757v87.599c.08 2.312 1.318 3.667 3.714 4.063l10.534 2.153.136.022c1.562.31 2.343 2.559 2.343 6.746 0 4.306-.826 6.459-2.479 6.459h-.08c-1.25-.017-7.576-.376-18.975-1.077-11.67-.717-21.947-1.076-30.829-1.076s-18.848.359-29.9 1.076c-11.05.718-17.092 1.077-18.125 1.077-1.652 0-2.478-2.153-2.478-6.46 0-3.896.826-6.151 2.478-6.767l8.366-2.153.303-.077c1.476-.39 2.408-.826 2.795-1.307.413-.512.62-1.487.62-2.922v-61.31c-.066-13.974-2.08-23.784-6.04-29.43-4.028-5.742-11.206-8.613-21.534-8.613-3.098 0-5.37.308-6.816.923v98.711c.025 1.282.231 2.163.617 2.641.413.513 1.446.974 3.098 1.384l8.366 2.153.152.063c1.551.695 2.326 2.93 2.326 6.705 0 4.306-.826 6.459-2.478 6.459h-.085c-1.243-.018-7.205-.376-17.886-1.077-10.948-.717-20.862-1.076-29.744-1.076s-19.21.359-30.984 1.076c-11.774.718-18.177 1.077-19.21 1.077-1.653 0-2.479-2.153-2.479-6.46 0-4.306.826-6.561 2.479-6.767l10.534-2.153.244-.044c2.316-.463 3.474-1.883 3.474-4.262V70.624c-.026-1.277-.232-2.106-.617-2.489-.414-.41-1.446-.82-3.099-1.23l-10.534-1.846-.148-.023c-1.554-.302-2.331-2.242-2.331-5.821 0-3.281.826-5.23 2.479-5.844 23.96-9.228 49.884-23.377 77.77-42.448 1.032-.615 1.858-.923 2.478-.923zM271.77 99.927c-7.676 0-11.514 6.807-11.514 20.42 0 16.503 3.734 38.213 11.203 65.131 7.468 26.919 14.52 43.679 21.159 50.28 3.112 3.093 6.327 4.64 9.646 4.64 7.676 0 11.514-6.807 11.514-20.42 0-16.502-3.734-38.213-11.203-65.131-7.468-26.919-14.52-43.678-21.159-50.279-3.112-3.094-6.327-4.641-9.646-4.641zm939.17 64.935c-6.093 0-9.14 4.29-9.14 12.873 0 8.378 2.364 15.837 7.092 22.375 4.727 6.54 9.823 9.809 15.286 9.809 2.196 0 4.012-.646 5.45-1.937l.223-.209v-22.228c-.114-5.728-2.318-10.681-6.615-14.86-3.992-3.882-8.09-5.823-12.292-5.823zM450.63.002c10.302 0 18.802 3.439 25.499 10.317 6.697 6.877 10.045 15.422 10.045 25.635 0 10.212-3.4 18.757-10.2 25.635-6.593 6.878-15.042 10.317-25.344 10.317-10.303 0-18.803-3.44-25.5-10.317-6.696-6.878-10.045-15.423-10.045-25.635 0-10.213 3.349-18.758 10.045-25.635C431.827 3.441 440.327.002 450.63.002zm297.39 249c8.835 0 16.17-2.736 22.008-8.208 5.995-5.472 8.992-12.236 8.992-20.292s-2.958-14.82-8.874-20.292-13.291-8.208-22.126-8.208-16.21 2.736-22.126 8.208-8.874 12.236-8.874 20.292 2.958 14.82 8.874 20.292 13.291 8.208 22.126 8.208z" />
                        </svg>
                    </a>
                    <div class="joinchat__close" title="Close"></div>
                </div>
                <div class="joinchat__box__scroll">
                    <div class="joinchat__box__content">
                        <div class="joinchat__message">Assalamu`alaikum , saya ingin bertanya mengenai Qurban</div>
                    </div>
                </div>
            </div>
            <svg height="0" width="0">
                <defs>
                    <clipPath id="joinchat__message__peak">
                        <path
                            d="M17 25V0C17 12.877 6.082 14.9 1.031 15.91c-1.559.31-1.179 2.272.004 2.272C9.609 18.182 17 18.088 17 25z" />
                    </clipPath>
                </defs>
            </svg>
        </div>
        <link rel='stylesheet' id='astra-widgets-astra-widget-address-css'
            href='/wp-content/plugins/astra-widgets/assets/css/minified/astra-widget-address.min.css?ver=1.2.5'
            type='text/css' media='all' />
        <style id='astra-widgets-astra-widget-address-inline-css' type='text/css'>
            #astra-widget-address-2 .widget-address-field svg {
                fill: #ed8f23;
            }

            #astra-widget-address-2 .widget-address .widget-address-field .address-meta {
                margin-left: 25px;
            }

            #astra-widget-address-2 .widget-address.widget-address-stack .widget-address-field {
                padding-top: 0;
                padding-bottom: 15px;
            }

            #astra-widget-address-2 .widget-address.widget-address-inline .widget-address-field {
                padding-right: 15px;
            }

            #astra-widget-address-2 .address .widget-address.widget-address-stack .widget-address-field:last-child {
                padding-bottom: 0;
            }

            #astra-widget-address-2 .address .widget-address.widget-address-inline .widget-address-field:last-child {
                padding-right: 0;
            }

        </style>
        <script type='text/javascript' id='contact-form-7-js-extra'>
            /* <![CDATA[ */
            var wpcf7 = {
                "apiSettings": {
                    "root": "\/wp-json\/contact-form-7\/v1",
                    "namespace": "contact-form-7\/v1"
                }
            };
            /* ]]> */

        </script>
        <script type='text/javascript'
            src='/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2'
            id='contact-form-7-js'></script>
        <script type='text/javascript' id='pt-cv-content-views-script-js-extra'>
            /* <![CDATA[ */
            var PT_CV_PUBLIC = {
                "_prefix": "pt-cv-",
                "page_to_show": "5",
                "_nonce": "d4af945036",
                "is_admin": "",
                "is_mobile": "",
                "ajaxurl": "\/wp-admin\/admin-ajax.php",
                "lang": "",
                "loading_image_src": "data:image\/gif;base64,R0lGODlhDwAPALMPAMrKygwMDJOTkz09PZWVla+vr3p6euTk5M7OzuXl5TMzMwAAAJmZmWZmZszMzP\/\/\/yH\/C05FVFNDQVBFMi4wAwEAAAAh+QQFCgAPACwAAAAADwAPAAAEQvDJaZaZOIcV8iQK8VRX4iTYoAwZ4iCYoAjZ4RxejhVNoT+mRGP4cyF4Pp0N98sBGIBMEMOotl6YZ3S61Bmbkm4mAgAh+QQFCgAPACwAAAAADQANAAAENPDJSRSZeA418itN8QiK8BiLITVsFiyBBIoYqnoewAD4xPw9iY4XLGYSjkQR4UAUD45DLwIAIfkEBQoADwAsAAAAAA8ACQAABC\/wyVlamTi3nSdgwFNdhEJgTJoNyoB9ISYoQmdjiZPcj7EYCAeCF1gEDo4Dz2eIAAAh+QQFCgAPACwCAAAADQANAAAEM\/DJBxiYeLKdX3IJZT1FU0iIg2RNKx3OkZVnZ98ToRD4MyiDnkAh6BkNC0MvsAj0kMpHBAAh+QQFCgAPACwGAAAACQAPAAAEMDC59KpFDll73HkAA2wVY5KgiK5b0RRoI6MuzG6EQqCDMlSGheEhUAgqgUUAFRySIgAh+QQFCgAPACwCAAIADQANAAAEM\/DJKZNLND\/kkKaHc3xk+QAMYDKsiaqmZCxGVjSFFCxB1vwy2oOgIDxuucxAMTAJFAJNBAAh+QQFCgAPACwAAAYADwAJAAAEMNAs86q1yaWwwv2Ig0jUZx3OYa4XoRAfwADXoAwfo1+CIjyFRuEho60aSNYlOPxEAAAh+QQFCgAPACwAAAIADQANAAAENPA9s4y8+IUVcqaWJ4qEQozSoAzoIyhCK2NFU2SJk0hNnyEOhKR2AzAAj4Pj4GE4W0bkJQIAOw=="
            };
            var PT_CV_PAGINATION = {
                "first": "\u00ab",
                "prev": "\u2039",
                "next": "\u203a",
                "last": "\u00bb",
                "goto_first": "Go to first page",
                "goto_prev": "Go to previous page",
                "goto_next": "Go to next page",
                "goto_last": "Go to last page",
                "current_page": "Current page is",
                "goto_page": "Go to page"
            };
            /* ]]> */

        </script>
        <script type='text/javascript'
            src='/wp-content/plugins/content-views-query-and-display-post-page/public/assets/js/cv.js?ver=2.3.3'
            id='pt-cv-content-views-script-js'></script>
        <script type='text/javascript'
            src='/wp-includes/js/jquery/jquery.form.min.js?ver=4.2.1' id='jquery-form-js'>
        </script>
        <script type='text/javascript'
            src='/wp-includes/js/jquery/ui/effect.min.js?ver=1.11.4'
            id='jquery-effects-core-js'></script>
        <script type='text/javascript'
            src='/wp-content/themes/zita/js/zita-menu.js?ver=5.5.3' id='zita-menu-js-js'>
        </script>
        <script type='text/javascript'
            src='/wp-content/themes/zita/js/zita-custom.js?ver=5.5.3'
            id='zita-custom-js-js'></script>
        <script type='text/javascript' id='load-more-posts-js-js-extra'>
            /* <![CDATA[ */
            var wp_ajax_url = "\/wp-admin\/admin-ajax.php";
            /* ]]> */

        </script>
        <script type='text/javascript'
            src='/wp-content/themes/zita/inc/pagination/js/load-more-posts.js?ver=0.1'
            id='load-more-posts-js-js'></script>
        <script type='text/javascript'
            src='/wp-content/themes/zita/inc/pagination/js/infinite-scroll.js?ver=0.1'
            id='script_ajax-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/creame-whatsapp-me/public/js/joinchat.min.js?ver=4.0.9'
            id='joinchat-js'></script>
        <script type='text/javascript'
            src='/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4' id='jquery-ui-core-js'>
        </script>
        <script type='text/javascript'
            src='/wp-includes/js/jquery/ui/datepicker.min.js?ver=1.11.4'
            id='jquery-ui-datepicker-js'></script>
        <script type='text/javascript' id='jquery-ui-datepicker-js-after'>
            jQuery(document).ready(function(jQuery) {
                jQuery.datepicker.setDefaults({
                    "closeText": "Close",
                    "currentText": "Today",
                    "monthNames": ["January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December"
                    ],
                    "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                        "Oct", "Nov", "Dec"
                    ],
                    "nextText": "Next",
                    "prevText": "Previous",
                    "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                        "Saturday"
                    ],
                    "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                    "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
                    "dateFormat": "MM d, yy",
                    "firstDay": 1,
                    "isRTL": false
                });
            });

        </script>
        <script type='text/javascript' id='lfb_f_js-js-extra'>
            /* <![CDATA[ */
            var frontendajax = {
                "ajaxurl": "\/wp-admin\/admin-ajax.php"
            };
            /* ]]> */

        </script>
        <script type='text/javascript'
            src='/wp-content/plugins/lead-form-builder/js/f-script.js?ver=1.4.11'
            id='lfb_f_js-js'></script>
        <script type='text/javascript' src='/wp-includes/js/wp-embed.min.js?ver=5.5.3'
            id='wp-embed-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.0.2'
            id='elementor-frontend-modules-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=2.4.7'
            id='elementor-sticky-js'></script>
        <script type='text/javascript' id='elementor-pro-frontend-js-extra'>
            /* <![CDATA[ */
            var ElementorProFrontendConfig = {
                "ajaxurl": "\/wp-admin\/admin-ajax.php",
                "nonce": "2ba503a422",
                "shareButtonsNetworks": {
                    "facebook": {
                        "title": "Facebook",
                        "has_counter": true
                    },
                    "twitter": {
                        "title": "Twitter"
                    },
                    "google": {
                        "title": "Google+",
                        "has_counter": true
                    },
                    "linkedin": {
                        "title": "LinkedIn",
                        "has_counter": true
                    },
                    "pinterest": {
                        "title": "Pinterest",
                        "has_counter": true
                    },
                    "reddit": {
                        "title": "Reddit",
                        "has_counter": true
                    },
                    "vk": {
                        "title": "VK",
                        "has_counter": true
                    },
                    "odnoklassniki": {
                        "title": "OK",
                        "has_counter": true
                    },
                    "tumblr": {
                        "title": "Tumblr"
                    },
                    "delicious": {
                        "title": "Delicious"
                    },
                    "digg": {
                        "title": "Digg"
                    },
                    "skype": {
                        "title": "Skype"
                    },
                    "stumbleupon": {
                        "title": "StumbleUpon",
                        "has_counter": true
                    },
                    "telegram": {
                        "title": "Telegram"
                    },
                    "pocket": {
                        "title": "Pocket",
                        "has_counter": true
                    },
                    "xing": {
                        "title": "XING",
                        "has_counter": true
                    },
                    "whatsapp": {
                        "title": "WhatsApp"
                    },
                    "email": {
                        "title": "Email"
                    },
                    "print": {
                        "title": "Print"
                    }
                },
                "facebook_sdk": {
                    "lang": "en_US",
                    "app_id": ""
                }
            };
            /* ]]> */

        </script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=2.4.7'
            id='elementor-pro-frontend-js'></script>
        <script type='text/javascript'
            src='/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'
            id='jquery-ui-position-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1'
            id='elementor-dialog-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'
            id='elementor-waypoints-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6'
            id='swiper-js'></script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.0.2'
            id='share-link-js'></script>
        <script type='text/javascript' id='elementor-frontend-js-before'>
            var elementorFrontendConfig = {
                "environmentMode": {
                    "edit": false,
                    "wpPreview": false
                },
                "i18n": {
                    "shareOnFacebook": "Share on Facebook",
                    "shareOnTwitter": "Share on Twitter",
                    "pinIt": "Pin it",
                    "download": "Download",
                    "downloadImage": "Download image",
                    "fullscreen": "Fullscreen",
                    "zoom": "Zoom",
                    "share": "Share",
                    "playVideo": "Play Video",
                    "previous": "Previous",
                    "next": "Next",
                    "close": "Close"
                },
                "is_rtl": false,
                "breakpoints": {
                    "xs": 0,
                    "sm": 480,
                    "md": 768,
                    "lg": 1025,
                    "xl": 1440,
                    "xxl": 1600
                },
                "version": "3.0.2",
                "is_static": false,
                "urls": {
                    "assets": "\/wp-content\/plugins\/elementor\/assets\/"
                },
                "settings": {
                    "page": [],
                    "editorPreferences": []
                },
                "kit": {
                    "global_image_lightbox": "yes",
                    "lightbox_enable_counter": "yes",
                    "lightbox_enable_fullscreen": "yes",
                    "lightbox_enable_zoom": "yes",
                    "lightbox_enable_share": "yes",
                    "lightbox_title_src": "title",
                    "lightbox_description_src": "description"
                },
                "post": {
                    "id": 568,
                    "title": "Program%20Qurban%20%E2%80%93%20Cinta%20Qurban",
                    "excerpt": "",
                    "featuredImage": false
                }
            };

        </script>
        <script type='text/javascript'
            src='/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.0.2'
            id='elementor-frontend-js'></script>
        <script>
            /(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window
                .addEventListener("hashchange", function() {
                    var t, e = location.hash.substring(1);
                    /^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (
                        /^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus())
                }, !1);

        </script>
        <script type="text/javascript" id="uagb-script-frontend">
            document.addEventListener("DOMContentLoaded", function() {
                (function($) {})(jQuery)
            })

        </script>
</body>

</html>
