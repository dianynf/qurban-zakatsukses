@extends('layouts.menu')
@section('title', 'FAQ - Cinta Qurban')

@section('content')
<div id="page" class="zita-site">
    <div id="content" class="site-content fullwidthcontained">
        <div id="container" class="site-container no-sidebar">
            <div id="primary" class="main content-area">
                <main id="main" class="site-main" role="main">
                    <article id="post-492" class="zita-article post-492 page type-page status-publish hentry">
                        <div class="entry-header entry-page">
                            <h1 class='entry-title'>FAQ</h1>
                        </div>
                        <div class="entry-content">
                            <div data-elementor-type="wp-page" data-elementor-id="492" class="elementor elementor-492"
                                data-elementor-settings="[]">
                                <div class="elementor-inner">
                                    <div class="elementor-section-wrap">
                                        <section
                                            class="elementor-section elementor-top-section elementor-element elementor-element-f6c33ae elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                            data-id="f6c33ae" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5d6a3c3"
                                                        data-id="5d6a3c3" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-37fa433 elementor-widget elementor-widget-accordion"
                                                                    data-id="37fa433" data-element_type="widget"
                                                                    data-widget_type="accordion.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-accordion" role="tablist">
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5861"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="1" role="tab"
                                                                                    aria-controls="elementor-tab-content-5861">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bagaimana sistem
                                                                                        berqurban di Zakat
                                                                                        Sukses?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5861"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="1" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5861">
                                                                                    <p>Sistem qurban di Zakat Sukses
                                                                                        adalah pequrban membeli hewan
                                                                                        qurban yang akan kami sembelih
                                                                                        sesuai dengan syariat dan kami
                                                                                        bagikan langsung di lokasi
                                                                                        penyaluran, adapun penyalurannya
                                                                                        tersebar di wilayah Depok dan
                                                                                        sekitarnya dan pelosok NTT,
                                                                                        Setelah pelaksanaan, pequrban
                                                                                        akan dikirimkan notifikasi serta
                                                                                        laporan dan dokumentasi
                                                                                        implementasi melalui
                                                                                        email/whatsapp sebagai bukti
                                                                                        qurban telah dilaksanakan.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5862"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="2" role="tab"
                                                                                    aria-controls="elementor-tab-content-5862">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bisakah pequrban
                                                                                        membawa pulang hewan
                                                                                        qurbannya untuk disembelih
                                                                                        di rumah ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5862"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="2" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5862">
                                                                                    <p>Tidak bisa. Qurban sepenuhnya
                                                                                        disembelih dan disalurkan
                                                                                        langsung di tempat penyaluran.
                                                                                        Pequrban menerima bukti qurban
                                                                                        dalam bentuk laporan dan
                                                                                        dokumentasi qurban, bukan berupa
                                                                                        hewan qurban yang masih dalam
                                                                                        keadaan hidup.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5863"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="3" role="tab"
                                                                                    aria-controls="elementor-tab-content-5863">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bagaimana cara
                                                                                        pembelian hewan di Zakat
                                                                                        Sukses ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5863"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="3" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5863">
                                                                                    <p>Zakat Sukses memfasilitasi
                                                                                        beberapa metode pembelian hewan
                                                                                        qurban:</p>
                                                                                    <ul>
                                                                                        <li>Melalui website <a
                                                                                                style="color:#7a7a7a;"
                                                                                                href="https://qurban.zakatsukses.org">https://qurban.zakatsukses.org</a>
                                                                                        </li>
                                                                                        <li>Melalui transfer
                                                                                            langsung ke rekening atas
                                                                                            nama Yayasan Zakat Sukses.
                                                                                            Konfirmasi melalui
                                                                                            <strong><a
                                                                                                style="color:#7a7a7a;"
                                                    href=>WA 0822-1162-7700</a>
                                                                                                wajib</strong> dilakukan
                                                                                            setelahnya dengan
                                                                                            melampirkan bukti transfer
                                                                                            dan data diri pequrban untuk
                                                                                            kebutuhan pengiriman
                                                                                            laporan.
                                                                                        </li>
                                                                                        <li>Melalui layanan jemput
                                                                                            qurban oleh petugas Zakat
                                                                                            Sukses. Konsultan Qurban
                                                                                            Zakat Sukses akan melayani
                                                                                            Anda dalam transaksi qurban.
                                                                                            Layanan Jemput qurban bisa melalui nomor berikut <strong><a
                                                                                                style="color:#7a7a7a;"
                                                    href=>WA 0822-1162-7700</a>
                                                                                                wajib</strong>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5864"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="4" role="tab"
                                                                                    aria-controls="elementor-tab-content-5864">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Kemana qurban akan
                                                                                        disalurkan ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5864"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="4" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5864">
                                                                                    <p>Qurban disalurkan kepada
                                                                                        masyarakat yang lebih
                                                                                        membutuhkan (dhuafa) di 11
                                                                                        kecamatan di kota Depok dan
                                                                                        sekitarnya dan wilayah pelosok
                                                                                        NTT yang meliputi Kabupaten
                                                                                        Alor, Desa Baranusa, Desa Pameti
                                                                                        Karata, Kec. Lewa, Kab. Sumba
                                                                                        Timur. Lokasi dapat berubah
                                                                                        sesuai kondisi, namun tetap di
                                                                                        wilayah pelosok Nusa Tenggara
                                                                                        Timur.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5865"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="5" role="tab"
                                                                                    aria-controls="elementor-tab-content-5865">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Berapa harga hewan
                                                                                        qurban di Zakat Sukses ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5865"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="5" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5865">
                                                                                    <ul>
                                                                                        <p>Harga Hewan Qurban Sukses
                                                                                            (pemotongan dan distribusi
                                                                                            di Depok dan sekitarnya)</p>
                                                                                        <li
                                                                                            style="list-style-type: none;">
                                                                                            <ul>
                                                                                                <li> Kambing :
                                                                                                    Rp3.500.000 (≥30kg)
                                                                                                </li>
                                                                                                <li> Sapi : Rp20.000.000
                                                                                                    (≥260kg) </li>
                                                                                                <li> Sapi Patungan (1/7)
                                                                                                    : Rp3.500.000
                                                                                                    (≥260kg) </li>
                                                                                            </ul>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <ul>
                                                                                        <p>Harga Hewan Qurban Pelosok
                                                                                            Nusantara (pemotongan dan
                                                                                            distribusi di Pedalaman NTT)</p>
                                                                                        <li
                                                                                            style="list-style-type: none;">
                                                                                            <ul>
                                                                                                <li> Kambing :
                                                                                                    Rp2.150.000
                                                                                                    (≥27-30kg) </li>
                                                                                                <li> Sapi : Rp15.000.000
                                                                                                    (≥225kg) </li>
                                                                                                <li> Sapi Patungan (1/7)
                                                                                                    : Rp2.150.000
                                                                                                    (≥225kg) </li>
                                                                                            </ul>
                                                                                        </li>
                                                                                    </ul>
                                                                                    <!-- <ul>
                                                                                        <p>Harga Hewan Qurban Dunia
                                                                                            Islam (pemotongan dan
                                                                                            distribusi di wilayah Gaza,
                                                                                            Palestina </p>
                                                                                        <li
                                                                                            style="list-style-type: none;">
                                                                                            <ul>
                                                                                                <li> Domba : Rp5.000.000
                                                                                                    (50kg)</li>
                                                                                                <li> Sapi Palestina :
                                                                                                    Rp30.000.000 (380kg)
                                                                                                </li>
                                                                                                <li> Sapi Utuh Somalia :
                                                                                                    Rp11.000.000 (120kg)
                                                                                                </li>
                                                                                                <li> Sapi Patungan
                                                                                                    Somalia :
                                                                                                    Rp1.900.000 (120kg)
                                                                                                </li>
                                                                                            </ul>
                                                                                        </li>
                                                                                    </ul> -->
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5866"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="6" role="tab"
                                                                                    aria-controls="elementor-tab-content-5866">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bagaimana kondisi
                                                                                        hewan qurban, apakah telah
                                                                                        sesuai syariat ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5866"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="6" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5866">
                                                                                    <p>Hewan qurban yang disembelih
                                                                                        dipastikan layak konsumsi dan
                                                                                        memenuhi syariat Islam, yaitu
                                                                                        telah cukup umur, sehat dan
                                                                                        gemuk, tidak cacat, dan
                                                                                        didapatkan secara sah baik
                                                                                        melalui Lumbung Ternak maupun
                                                                                        bermitra dengan peternakan yang
                                                                                        telah terjamin kualitas dan
                                                                                        kesesuaian dengan syariatnya.
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5867"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="7" role="tab"
                                                                                    aria-controls="elementor-tab-content-5867">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah prosesi
                                                                                        penyembelihan dilakukan
                                                                                        sesuai syariat ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5867"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="7" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5867">
                                                                                    <p>Tim penyembelihan qurban di Zakat
                                                                                        Sukses merupakan orang-orang
                                                                                        yang telah mafhum mengenai tata
                                                                                        cara penyembelihan qurban sesuai
                                                                                        syariat Islam, seperti
                                                                                        menyembelih dengan pisau yang
                                                                                        dipastikan tajam, menempatkan
                                                                                        hewan sedemikian rupa sesuai
                                                                                        posisi yang disyariatkan,
                                                                                        membaca basmalah dan takbir,
                                                                                        serta melakukan penyembelihan
                                                                                        pada waktu yang telah ditetapkan
                                                                                        yaitu 10, 11, 12, dan 13
                                                                                        Dzulhijjah.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5868"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="8" role="tab"
                                                                                    aria-controls="elementor-tab-content-5868">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah pequrban bisa
                                                                                        menerima sepertiga bagian
                                                                                        dari daging qurbannya?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5868"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="8" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5868">
                                                                                    <p>Untuk qurban yang dilakukan di
                                                                                        pelosok nusantara, daging hewan
                                                                                        qurban seluruhnya disalurkan
                                                                                        kepada para penerima manfaat.
                                                                                        Insya Allah qurban Anda akan
                                                                                        bermanfaat maksimal karena
                                                                                        disalurkan kepada mereka yang
                                                                                        benar-benar membutuhkan. Untuk
                                                                                        qurban yang dilakukan di Depok, pequrban akan
                                                                                        mendapatkan bagiannya dan dikirimkan ke alamat pequrban atau pequrban dapat
                                                                                        mengambilnya langsung dikantor
                                                                                        LAZ Zakat Sukses, Jl. KH. M.
                                                                                        Yusuf Raya No. 95, Depok.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-5869"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="9" role="tab"
                                                                                    aria-controls="elementor-tab-content-5869">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bagaimana caranya
                                                                                        pequrban mengetahui bahwa
                                                                                        hewan qurban telah
                                                                                        didistribusikan ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-5869"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="9" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-5869">
                                                                                    <p>Notifikasi insya Allah akan segera diterima oleh pequrban
                                                                                        setelah qurban ditunaikan,
                                                                                        sehingga memudahkan pequrban
                                                                                        yang hendak melakukan pemotongan
                                                                                        kuku atau rambut. Laporan
                                                                                        berbentuk dokumen yang
                                                                                        dilengkapi foto-foto qurban juga
                                                                                        akan diterima pequrban
                                                                                        selambat-lambatnya 3 pekan
                                                                                        setelah hari tasyrik ketiga
                                                                                        dalam bentuk softcopy ke
                                                                                        e-mail/WA pequrban.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58610"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="10" role="tab"
                                                                                    aria-controls="elementor-tab-content-58610">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah pequrban bisa
                                                                                        memilih lokasi distribusi
                                                                                        atau penyaluran qurbannya
                                                                                        ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58610"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="10" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58610">
                                                                                    <p>Pequrban tidak bisa memilih
                                                                                        lokasi penyaluran qurbannya
                                                                                        sendiri secara spesifik</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58610"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="11" role="tab"
                                                                                    aria-controls="elementor-tab-content-58610">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah bisa melakukan
                                                                                        cicilan qurban di Zakat
                                                                                        Sukses?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58610"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="11" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58610">
                                                                                    <p>Insya Allah bisa, dengan besaran
                                                                                        cicilan per bulan yang
                                                                                        ditentukan oleh Zakat Sukses,
                                                                                        dan harus sudah lunas sebelum
                                                                                        Hari Raya Idul Adha. Jika tidak,
                                                                                        qurban akan disembelih pada
                                                                                        tahun berikutnya setelah
                                                                                        pelunasan.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58611"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="12" role="tab"
                                                                                    aria-controls="elementor-tab-content-58611">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah pequrban bisa
                                                                                        mendatangi lokasi
                                                                                        penyembelihan qurbannya
                                                                                        ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58611"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="12" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58611">
                                                                                    <p>Untuk qurban yang dilakukan di
                                                                                        pelosok nusantara, Zakat Sukses
                                                                                        belum bisa memfasilitasi hal
                                                                                        tersebut karena wilayah
                                                                                        distribusi qurban yang sulit
                                                                                        dijangkau. Untuk qurban yang
                                                                                        dilakukan di Depok, Zakat Sukses
                                                                                        akan memfasilitasi baik offline
                                                                                        atau online (via zoom/live
                                                                                        IG/Facebook/YouTube).</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58612"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="13" role="tab"
                                                                                    aria-controls="elementor-tab-content-58612">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bolehkah berqurban
                                                                                        jika belum aqiqah ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58612"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="13" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58612">
                                                                                    <p>Qurban tetap sah meskipun belum
                                                                                        melakukan aqiqah. Qurban adalah
                                                                                        ibadah sunnah yang istimewa
                                                                                        karena hanya bisa dilakukan 4
                                                                                        hari dalam satu tahun (10, 11,
                                                                                        12, 13 Dzulhijjah), sedangkan
                                                                                        aqiqah adalah ibadah sunnah yang
                                                                                        dapat dilakukan kapanpun (tidak
                                                                                        harus di bulan Dzulhijjah).
                                                                                        Aqiqah pun merupakan tanggung
                                                                                        jawab dari orang tua, meskipun
                                                                                        anak juga diperbolehkan
                                                                                        mengaqiqah diri sendiri untuk
                                                                                        meringankan tanggung jawab orang
                                                                                        tua.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58613"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="14" role="tab"
                                                                                    aria-controls="elementor-tab-content-58613">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bolehkah berqurban
                                                                                        atas nama orang tua yang
                                                                                        sudah meninggal ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58613"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="14" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58613">
                                                                                    <p>Boleh. Harap dibedakan antara
                                                                                        amal dan pahala. Qurban adalah
                                                                                        amal individual, jadi kita
                                                                                        niatkan qurban adalah atas nama
                                                                                        orang tua yang sudah meninggal,
                                                                                        dan pahalanya bisa kita niatkan
                                                                                        untuk kita sekeluarga.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58614"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="15" role="tab"
                                                                                    aria-controls="elementor-tab-content-58614">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bolehkah menjual
                                                                                        bagian hewan qurban yang
                                                                                        telah didapatkan?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58614"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="15" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58614">
                                                                                    <p>Bagian hewan qurban yang telah
                                                                                        kita dapatkan tidak boleh
                                                                                        dijual, namun boleh diberikan
                                                                                        kepada orang lain sebagai
                                                                                        hadiah.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58615"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="16" role="tab"
                                                                                    aria-controls="elementor-tab-content-58615">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bagaimana hukum
                                                                                        menyedekahkan hewan qurban
                                                                                        ?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58615"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="16" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58615">
                                                                                    <p>Apabila seseorang membeli hewan
                                                                                        qurban, kemudian hewan tersebut
                                                                                        disedekahkan kepada orang lain
                                                                                        sehingga disembelih menggunakan
                                                                                        atas nama orang tersebut, hal
                                                                                        tersebut diperbolehkan.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58616"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="17" role="tab"
                                                                                    aria-controls="elementor-tab-content-58616">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah bisa berqurban
                                                                                        misalnya 1 kambing atas nama
                                                                                        keluarga besar bukan untuk 1
                                                                                        orang saja?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58616"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="17" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58616">
                                                                                    <p>Amalan qurban satu kambing hanya
                                                                                        untuk satu orang saja. Pahalanya
                                                                                        dapat diniatkan untuk
                                                                                        sekeluarga.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58617"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="18" role="tab"
                                                                                    aria-controls="elementor-tab-content-58617">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Apakah sah qurban yang
                                                                                        wilayah distribusinya kurang
                                                                                        tepat sasaran?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58617"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="18" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58617">
                                                                                    <p>Qurban tidak spesifik dikhususkan
                                                                                        hanya untuk kaum fakir miskin
                                                                                        saja, tetapi untuk muslim
                                                                                        seluruhnya. Namun jika qurban
                                                                                        dirasa berlebih, selayaknya
                                                                                        disebar ke wilayah lain yang
                                                                                        tidak pernah merasakan
                                                                                        keberkahan daging qurban.</p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-accordion-item">
                                                                                <div id="elementor-tab-title-58618"
                                                                                    class="elementor-tab-title"
                                                                                    data-tab="19" role="tab"
                                                                                    aria-controls="elementor-tab-content-58618">
                                                                                    <span
                                                                                        class="elementor-accordion-icon elementor-accordion-icon-left"
                                                                                        aria-hidden="true">
                                                                                        <span
                                                                                            class="elementor-accordion-icon-closed"><i
                                                                                                class="fas fa-plus"></i></span>
                                                                                        <span
                                                                                            class="elementor-accordion-icon-opened"><i
                                                                                                class="fas fa-minus"></i></span>
                                                                                    </span>
                                                                                    <a class="elementor-accordion-title"
                                                                                        href="">Bagaimana jika qurban
                                                                                        disembelih dan disalurkan ke
                                                                                        suatu negara/wilayah yang
                                                                                        memiliki zona waktu berbeda
                                                                                        dengan negara/wilayah asal
                                                                                        pequrban, pada batas akhir
                                                                                        penyembelihan qurban (hari
                                                                                        terakhir tasyrik), padahal zona
                                                                                        waktu di negara tersebut lebih
                                                                                        lambat daripada negara asal
                                                                                        pequrban? Apakah sah berqurban
                                                                                        di negara/wilayah yang belum
                                                                                        usai masa tasyriknya?</a>
                                                                                </div>
                                                                                <div id="elementor-tab-content-58618"
                                                                                    class="elementor-tab-content elementor-clearfix"
                                                                                    data-tab="19" role="tabpanel"
                                                                                    aria-labelledby="elementor-tab-title-58618">
                                                                                    <p>Boleh, rentang waktu pelaksanaan
                                                                                        qurban mengikuti zona waktu di
                                                                                        lokasi penyaluran atau
                                                                                        distribusi qurban.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .entry-content -->
                    </article><!-- #post -->
                </main><!-- #main -->
            </div>
        </div>
    </div><!-- #primary -->
</div>
@endsection
