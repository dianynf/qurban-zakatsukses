@extends('layouts.success')
@section('title', 'Checkout Success')

@section('content')
    <section class="section-details-header"></section>
    <section class="section-details-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 pl-lg-0"></div>
                <div class="col-lg-8 pl-lg-0">
                    <h4 class="pd-h4">Data Pembeli Qurban</h4>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 style="font-weight: bold;">Nama Pequrban</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">{{ $items->name }}</div>
                            </div>
                            <div class="row mt-4">
                                <div class="col">
                                    <h5 style="font-weight: bold;">Email</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">{{ $items->email }}</div>
                            </div>
                            <div class="row mt-4">
                                <div class="col">
                                    <h5 style="font-weight: bold;">Nomor Handphone</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">{{ $items->nohp }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-lg-2 pl-lg-0"></div>
                <div class="col-lg-8 pl-lg-0">
                    <h4 class="pd-h4">Transaksi Pembelian Qurban</h4>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    @if ($items->product_id == '1')
                                        QURBAN PELOSOK NUSANTARA
                                    @elseif ($items->product_id == '2')
                                        QURBAN PELOSOK NUSANTARA
                                    @elseif ($items->product_id == '3')
                                        QURBAN PELOSOK NUSANTARA
                                    @elseif ($items->product_id == '4')
                                        QURBAN SUKSES
                                    @elseif ($items->product_id == '5')
                                        QURBAN SUKSES
                                    @elseif ($items->product_id == '6')
                                        QURBAN SUKSES
                                    @elseif ($items->product_id == '7')
                                        QURBAN DUNIA ISLAM
                                    @elseif ($items->product_id == '8')
                                        QURBAN DUNIA ISLAM
                                    @else @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <h5 style="font-weight: bold;">
                                        @if ($items->product_id == '1')
                                            Kambing
                                        @elseif ($items->product_id == '2')
                                            Sapi
                                        @elseif ($items->product_id == '3')
                                            Sapi Patungan
                                        @elseif ($items->product_id == '4')
                                            Kambing
                                        @elseif ($items->product_id == '5')
                                            Sapi
                                        @elseif ($items->product_id == '6')
                                            Sapi Patungan
                                        @elseif ($items->product_id == '7')
                                            Domba
                                        @elseif ($items->product_id == '8')
                                            Sapi
                                        @else @endif
                                    </h5>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col">
                                    <h1 style="font-weight: bold;">Rp. {{ number_format($items->total_seluruh) }}</h1>
                                </div>
                            </div>
                            @if ($items->bank_id >= '11' and $items->bank_id <= '19')
                                <div class="row mt-4">
                                    <div class="col">
                                        <img src="{{ asset('/metode_pembayaran/' . $items->bank->img) }}"
                                            width="120px" />
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col">
                                        <h4>No VA: {{ session()->get('va')['va_number'] }}</h4>
                                        <h4>Transfer sebelum {!! $items->payment_expired !!} atau transaksimu akan dibatalkan secara otomatis </h4>
                                    </div>
                                </div>

                            @elseif ($items->bank_id >= '1' and $items->bank_id <= '9' )
                            <div class="row mt-4">
                                    <div class="col">
                                        <img src="{{ asset('/metode_pembayaran/' . $items->bank->img) }} "
                                            width="120px" />
                                    </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h4>{{ $items->bank->name }} {{ $items->bank->rek }} <br>
                                    an Yayasan Zakat Sukses</h4>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-8 offset-md-2">
                                <h5 class="text-center">*Transfer sebelum 1x24 jam dan simpan bukti pembayaran,
                                    kirim bukti pembayaran
                                    melalui link konfirmasi di bawah.
                                    <br><br>
                                    Sudah transfer melalui ATM/internet/mobile banking? Klik tombol di bawah
                                    untuk
                                    mengonfirmasi. Sistem
                                    tidak memproses transaksi yang belum dikonfirmasi.
                                </h5>
                            </div>
                        </div>
                    @else
                        @endif
                    </div>
                </div>
                {{-- <div class="card card-details">
                        <div class="card-body">
                            <div class="row mt-2">
                                <div class="col-6 col-md-4 text-warning">
                                    <h5>TOTAL HARGA</h5>
                                </div>
                                <div class="col-md-8">
                                    <h5 style="color: rgb(30, 157, 4)"><b>: Rp.
                                            {{ number_format($items->total_seluruh) }}</b></h5>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 col-md-4">Tanggal Pembelian</div>
                                <div class="col-md-8"><b>: {{ $items->date }}</b></div>
                            </div>
                            <hr>
                            <div class="row mt-2">
                                <div class="col-6 col-md-4">No Invoice</div>
                                <div class="col-md-8"><b>: {{ $items->invoice }}</b></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 col-md-4">Metode Pembayaran</div>
                                <div class="col-md-8"><b>: {{ $items->bank->name }}</b></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-6 col-md-4">Nomor Rekening</div>
                                <div class="col-md-8"><b>: {{ $items->bank->rek }}</b></div>
                            </div>
                            <div class="mt-4 text-center">
                                <b style="color: #f28837;">Nama Rekening Yayasan Zakat Sukses</b>
                            </div>
                            <div class="mt-2 text-center">
                                <b style="color: #f28837;">Konfirmasi Qur'ban 0822 1162 7700</b>
                            </div>
                        </div>
                    </div> --}}
            </div>
        </div>
        <div class="row mb-5 mt-4">
            <div class="col-md-2 ml-md-auto">
                <form action="/send-wa" method="POST">
                    @csrf
                    <input type="text" value="{{ $items->name }}" name="nama_lengkap" hidden>
                    <input type="text" value="{{ $items->email }}" name="email" hidden>
                    <input type="text" value="{{ number_format($items->total_seluruh) }}" name="nominal" hidden>
                    <input type="text" value="{{ $items->bank->name }}" name="bank" hidden>
                    <input type="text" value="SAPI" name="akad" hidden>
                    <button type="submit" class="btn btn-join-bayar"> Konfirmasi
                    </button>
                </form>
            </div>
            <div class="col-md-0 ml-md-auto"></div>
        </div>
        </div>
    </section>
@endsection
