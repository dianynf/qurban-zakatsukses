@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        @if (session()->has('sukses'))
                            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                {{ session()->get('sukses') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h3 class="h3 mb-0 text-gray-800">Dompet</h3>
                                    </div>

                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Invoice</th>
                                                <th>Nama</th>
                                                <th>Telp</th>
                                                <th>Total</th>
                                                <th>Pembayaran</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @forelse($items as $item)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $item->invoice }}</td>
                                                    <td>{{ $item->user->username }}</td>
                                                    <td>{{ $item->nohp }}</td>
                                                    <td><?php echo 'Rp. ' . number_format($item->total_seluruh,
                                                        0, '.', '.'); ?>
                                                    </td>
                                                    <td>{{ $item->transaction_status }}</td>
                                                    <td>{{ $item->bank->name }}</td>
                                                    <td>
                                                        <a href="{{ route('transaction.show', $item->id) }}"
                                                            class="btn btn-primary">
                                                            <i class="fa fa-search-plus"></i>
                                                        </a>
                                                        @if (Auth::user()->role_id == 1)
                                                            <a href="{{ route('transaction.edit', $item->id) }}"
                                                                class="btn btn-info">
                                                                <i class="fa fa-pencil-alt"></i>
                                                            </a>
                                                            <form action="{{ route('transaction.destroy', $item->id) }}"
                                                                method="post" class="d-inline">
                                                                @csrf
                                                                @method('delete')
                                                                <button class="btn btn-danger">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </form>
                                                        @else
                                                        @endif
                                                    </td>
                                                </tr>
                                            @empty
                                                <td colspan="7" class="text-center">
                                                    Data Kosong
                                                </td>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
