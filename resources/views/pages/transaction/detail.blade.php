@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        @if (session()->has('sukses'))
                            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                {{ session()->get('sukses') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h1 class="h3 mb-0 text-gray-800">Detail Transaksi</h1>
                                    </div>
                                    <!-- Content Row -->
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Invoice</th>
                                            <td>{{ $item->invoice }}</td>
                                        </tr>
                                        <tr>
                                            <th width="25%">Pembeli</th>
                                            <td>{{ $item->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Tanggal</th>
                                            <td>{{ $item->created_at }}</td>
                                        </tr>
                                        <tr>
                                            <th>Bank</th>
                                            <td>{{ $item->bank->name }}</td>
                                        </tr>
                                        <tr>
                                            <th>Total Transaksi</th>
                                            <td><b><?php echo 'Rp. ' . number_format($item->total_seluruh, 0,
                                                    '.', '.'); ?></b></td>
                                        </tr>
                                        <tr>
                                            <th>Status Transaksi</th>
                                            <td>
                                                @if ($item->transaction_status == 'SUCCESS')
                                                    <b style="color:#d07c28;">LUNAS</b>
                                                @elseif($item->transaction_status != 'SUCCESS')
                                                    <b style="color:#d07c28;">BELUM LUNAS</b>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td>{{ $item->address }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kota</th>
                                            <td>{{ $item->city }}</td>
                                        </tr>
                                        <tr>
                                            <th>Provinsi</th>
                                            <td>{{ $item->state }}</td>
                                        </tr>
                                        <tr>
                                            <th>Kode Post</th>
                                            <td>{{ $item->post_code }}</td>
                                        </tr>
                                        <tr>
                                            <th colspan="2" style="color:#d07c28;">Qurban</th>
                                        </tr>
                                        <tr>
                                            <th>{{ $item->product->title }}</th>
                                            <td>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                    </tr>
                                                    @php $no = 1; @endphp
                                                    @foreach ($item->details as $detail)
                                                        <tr>
                                                            <td>{{ $no++ }}</td>
                                                            <td>{{ $detail->username }}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
