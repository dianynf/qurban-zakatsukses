@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">
                                            Tambah Transaksi
                                        </h3>
                                        <hr>
                                        <form action="{{ route('addtransaksi.store') }}" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <input hidden type="text" name="invoice" id="invoice"
                                                    value="<?php echo date('Y'); ?>" />
                                            </div>
                                            <div class="form-group">
                                                <select name="users_id" name="users_id" required class="form-control">
                                                    <option disabled selected>Akun User</option>
                                                    @foreach ($user as $item)
                                                        <option value="{{ $item->id }}">
                                                            {{ $item->username }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <p> *Jika user memiliki akun pembeli <b style="color:#d07c28;">Mohon
                                                        diisi</b>, Jika
                                                    tidak memiliki akun
                                                    silahkan
                                                    pilih akun <b>GUEST</b>.</p>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" id="name"
                                                    placeholder="Nama Pembeli" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="email" id="email"
                                                    placeholder="Email" value="{{ old('email') }}" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="nohp" id="nohp"
                                                    placeholder="No Telp" value="{{ old('nohp') }}" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control datepicker" name="date" id="date"
                                                    placeholder=" Tanggal" required value="{{ old('date') }}" required>
                                            </div>
                                            <div class="form-group">
                                                <select name="product_id" name="product_id" required class="form-control">
                                                    <option disabled selected>Tipe Hewan</option>
                                                    @foreach ($product as $item)
                                                        <option value="{{ $item->id }}">
                                                            {{ $item->title }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="quant" id="quant"
                                                    placeholder="Jumlah Hewan" value="{{ old('quant') }}" required>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="total_seluruh" id="rupiah"
                                                    placeholder="Total Transaksi" value="{{ old('total_seluruh') }}"
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <input hidden type="text" class="form-control" name="transaction_total"
                                                    id="rupiah" placeholder="Total Transaksi" value="0" required>
                                            </div>
                                            <div class="form-group">
                                                <select name="transaction_status" id="transaction_status" required
                                                    class="form-control">
                                                    <option disabled selected>Status Transaksi</option>
                                                    <option value="INCART">INCART</option>
                                                    <option value="PENDING">Pending</option>
                                                    <option value="SUCCESS">Success</option>
                                                    <option value="CANCEL">Cancel</option>
                                                    <option value="FAILED">Failed</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select name="bank_id" name="bank_id" required class="form-control">
                                                    <option disabled selected>Bank Pembayaran</option>
                                                    @foreach ($banks as $item)
                                                        <option value="{{ $item->id }}">
                                                            {{ $item->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <a href="#" class="addtransaksi btn btn-sm btn admin-login"
                                                    style="float:left;" required>Tambah
                                                    Pengkurban</a>
                                            </div>
                                            <br><br>
                                            <div class="form-group">
                                                <div class="transaksi">
                                                    <div class="form-group row">
                                                        <div class="col-7"><input type="text" class="form-control"
                                                                name="username[]" id="username"
                                                                placeholder="Nama Pengkurban"
                                                                value="{{ old('username') }}" required></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit"
                                                class="form-control btn admin-login btn-user btn-block admin-btn">
                                                Simpan
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script type="text/javascript">
        $('.addtransaksi').on('click', function() {
            addtransaksi();
        });

        function addtransaksi() {
            var transaksi =
                '<div><div class="form-group row"><div class="col-7"><input type="text" class="form-control" name="username[]" id="username" placeholder="Nama Pengkurban" value="{{ old('username') }}"></div><div class="col-5"><a href="#" class="remove btn btn-sm btn-danger mt-2" style="float:left;">Hapus</a></div></div></div>';
            $('.transaksi').append(transaksi);
        };

        $('.remove').live('click', function() {
            $(this).parent().parent().parent().remove();
        });

    </script>

    <script>
        var rupiah = document.getElementById("rupiah");
        rupiah.addEventListener("keyup", function(e) {
            rupiah.value = formatRupiah(this.value, "Rp. ");
        });

        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, "").toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            if (ribuan) {
                separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
        }

    </script>
@endsection
