@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">
                                            Ubah {{ $item->name }}
                                        </h3>
                                        <hr>
                                        <form action="{{ route('banks.update', $item->id) }}" method="post" enctype="multipart/form-data">
                                            @method('put')
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="name" placeholder="Nama Bank"
                                                    value="{{ $item->name }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="rek" placeholder="Rekening"
                                                    value="{{ $item->rek}}">
                                            </div>
                                            <div class="form-group">
                                                <input type="file" class="form-control" name="img">
                                            </div>
                                            <div class="form-file row justify-content-end">
                                                <button type="submit" class="btn admin-login btn-user btn-block admin-btn">
                                                    Simpan
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
