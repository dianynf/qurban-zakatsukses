@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        @if (session()->has('sukses'))
                            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                {{ session()->get('sukses') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                                        <h3 class="h3 mb-0 text-gray-800">Roles</h3>
                                        <a href="{{ route('role.create') }}"
                                            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                                            <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Role
                                        </a>
                                    </div>

                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Role</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @forelse($roles as $item)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>
                                                        <a href="{{ route('role.edit', $item->id) }}" class="btn btn-info">
                                                            <i class="fa fa-pencil-alt"></i>
                                                        </a>
                                                        <form action="{{ route('role.destroy', $item->id) }}" method="post"
                                                            class="d-inline">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                        </form>

                                                    </td>
                                                </tr>
                                            @empty
                                                <td colspan="7" class="text-center">
                                                    Data Kosong
                                                </td>
                                            @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
