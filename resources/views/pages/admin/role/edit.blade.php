@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">
                                            Edit Role {{ $role->name }}
                                        </h3>
                                        <hr>
                                        <form
                                            action="{{ isset($role) ? route('role.update', $role->id) : route('role.store') }}"
                                            method="POST">
                                            @csrf
                                            @if (isset($role))
                                                @method('PATCH')
                                            @endif
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="name" name="name" required
                                                    autocomplete="name" placeholder="Role" autofocus
                                                    value="{{ isset($role) ? $role->name : old('name') }}">
                                            </div>
                                            <div class="form-file row justify-content-end">
                                                <button type="submit" class="btn admin-login btn-user btn-block admin-btn">
                                                    Simpan
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- End of Main Content -->
