@extends('layouts.admin')
@section('content')
    <div class="container-fluid">
        <!-- Content Row -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        <div class="card border-left-warning shadow h-100 py-2">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">
                                            Edit Product {{ $item->title }}
                                        </h3>
                                        <hr>
                                        <form action="{{ route('product.update', $item->id) }}" method="post">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="title"
                                                    placeholder="Nama Product" value="{{ $item->title }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="slug"
                                                    placeholder="ID Hewan Qurban" value="{{ $item->slug }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="berat"
                                                    placeholder="Berat > 00-00 kg" value="{{ $item->berat }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="harga"
                                                    placeholder="Harga Rp. 000.000" value="{{ $item->harga }}">
                                            </div>
                                            <div class="form-group">
                                                <textarea name="about" placeholder="About" rows="10"
                                                    class="d-block w-100 form-control">{{ $item->about }}</textarea>
                                            </div>
                                            <button type="submit" class="btn admin-login btn-user btn-block admin-btn">
                                                Simpan
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
@endsection
