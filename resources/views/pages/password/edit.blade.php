@extends('layouts.admin')

@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <div class="row mt-12">
            <div class="col md-5">
                <div class="card-deck">
                    <div class="col-xl-12 col-md-8 mb-5">
                        @if (session()->has('sukses'))
                            <div class="alert alert-info col-xl-12 alert-dismissible fade show" role="alert">
                                {{ session()->get('sukses') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="card border-left-warning shadow h-100 py-2">
                            <!-- Page Heading -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="modal-body">
                                        <h3 class="h3 mb-4 text-gray-800">Edit Password</h3>
                                        <hr>

                                        <form method="POST" action="{{ route('user.password.update') }}">
                                            @method('patch')
                                            @csrf
                                            <div class="form-group">
                                                <input id="current_password" type="password"
                                                    class="form-control @error('current_password') is-invalid @enderror"
                                                    name="current_password" placeholder="Password Lama" required
                                                    autocomplete="current_password">
                                                @error('current_password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input id="password" type="password"
                                                    class="form-control @error('password') is-invalid @enderror"
                                                    name="password" placeholder="Password Baru" required
                                                    autocomplete="new-password">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input id="password-confirm" type="password" class="form-control"
                                                    name="password_confirmation" placeholder="Konfirmasi Password" required
                                                    autocomplete="new-password">
                                            </div>
                                            <div class="form-group">
                                                <button type="submit"
                                                    class="btn admin-login btn-user btn-block admin-btn">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
