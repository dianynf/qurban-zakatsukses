@extends('layouts.auth')
@section('title', 'Detail Travel')

@section('content')
    <main>
        <section class="section-details-header"></section>
        <section class="section-details-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 pl-lg-0"></div>
                    <div class="col-lg-8 pl-lg-0">
                        <div class="card card-details">
                            @if ($item->galleries->count() > 0)
                                <div class="gallery">
                                    <div class="xzoom-container">
                                        <img src="{{ asset('wp-content/uploads/2022/06/' . $item->galleries->first()->image) }}"
                                            class="xzoom" id="xzoom-default"
                                            xoriginal="{{ asset('wp-content/uploads/2022/06/' . $item->galleries->first()->image) }}" />
                                    </div>
                                    <div class="xzoom-thumbs">
                                        @foreach ($item->galleries as $gallery)
                                            <a href="{{ asset('wp-content/uploads/2022/06/' . $gallery->image) }}">
                                                <img src="{{ asset('wp-content/uploads/2022/06/' . $gallery->image) }}"
                                                    class="xzoom-gallery" width="128"
                                                    xpreview="{{ asset('wp-content/uploads/2022/06/' . $gallery->image) }}" />
                                            </a>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            {{-- <h2>Informasi</h2>
                            {!! $item->about !!}
                        </div>
                    </div>
                    <div class="col-lg-6 pl-lg-0">
                        <div class="card card-details">
                            <h2>
                                @if ($item->id == '1')
                                    QURBAN PELOSOK NUSANTARA
                                @elseif ($item->id == '2')
                                    QURBAN PELOSOK NUSANTARA
                                @elseif ($item->id == '3')
                                    QURBAN PELOSOK NUSANTARA
                                @elseif ($item->id == '4')
                                    QURBAN SUKSES
                                @elseif ($item->id == '5')
                                    QURBAN SUKSES
                                @elseif ($item->id == '6')
                                    QURBAN SUKSES
                                @elseif ($item->id == '7')
                                    QURBAN DUNIA ISLAM
                                @elseif ($item->id == '8')
                                    QURBAN DUNIA ISLAM
                                @elseif ($item->id == '9')
                                    QURBAN DUNIA ISLAM
                                @elseif ($item->id == '10')
                                    QURBAN DUNIA ISLAM
                                @else @endif
                            </h2>
                            <h1><b>
                                    @if ($item->id == '1')
                                        Kambing
                                    @elseif ($item->id == '2')
                                        Sapi
                                    @elseif ($item->id == '3')
                                        Sapi Patungan
                                    @elseif ($item->id == '4')
                                        Kambing
                                    @elseif ($item->id == '5')
                                        Sapi
                                    @elseif ($item->id == '6')
                                        Sapi Patungan
                                    @elseif ($item->id == '7')
                                        Domba Palestina
                                    @elseif ($item->id == '8')
                                        Sapi Palestina
                                    @elseif ($item->id == '9')
                                        Sapi Somalia
                                    @elseif ($item->id == '10')
                                        Sapi Somalia
                                    @else @endif
                                </b></h1> --}}
                            <hr />
                            <div class="container trip-informations">
                                <div class="row col">
                                    <div class="col warna-th">
                                        Berat
                                    </div>
                                    <div class="col warna-text text-left">
                                        <input type="text" class="warna-text text-bold text-gs" value="{{ $item->berat }}"
                                            readonly style="color: #FFA825; padding: 1px; background-color: #ffffff;" />
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="container trip-informations">
                                <div class="row col">
                                    <div class="col warna-th">
                                        Harga
                                    </div>
                                    <div class="col warna-text text-left">
                                        <input type="text" class="warna-text text-bold text-gs"
                                            value="Rp. {{ number_format($item->harga) }}/Ekor" readonly
                                            style="color: #FFA825; padding: 1px; background-color: #ffffff; font-weight: bold;" />
                                        <input type="text" value="{{ $item->harga }}" hidden id="text1" onkeyup="sum();"
                                            readonly>
                                    </div>
                                </div>
                            </div>
                            <form action="{{ route('checkout_process', $item->id) }}" method="post">
                                @csrf
                                <hr>
                                <div class="container">
                                    <div class="row col">
                                        <div class="col warna-th">
                                            Kuantitas
                                        </div>
                                        <div class="col warna-text text-left">
                                            {{-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js">
                                                </script>
                                                <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> --}}
                                            <div class="input-group">
                                                {{-- <button type="button" class="btn btn-number btn-join-noww" data-type="minus"
                                                        data-field="quant[2]">
                                                        <i class="fas fa-minus"></i>
                                                    </button> --}}

                                                {{-- <input style="text" class="form-control text-center input-number col-md"
                                                        name="quant" value="1" min="1" max="100" id="text2" onkeyup="sum();"> --}}
                                                <input style="text" class="form-control input-number col-md" name="quant"
                                                    value="1" min="1" max="100" id="text2" onkeyup="sum();">
                                                {{-- <button type="button" class="btn btn-number btn-number-cust btn-join-nowww"
                                                        data-type="plus" data-field="quant[2]">
                                                        <i class="fas fa-plus"></i>
                                                    </button> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input hidden type="text" name="invoice" id="invoice"
                                        value="<?php echo date('Y'); ?>" />
                                </div>
                                <hr>
                                <div class="container trip-informations">
                                    <div class="row col">
                                        <div class="col warna-th">
                                            Total
                                        </div>
                                        <div class="col warna-text">
                                            <input type="hidden" name="date"
                                                value="<?php echo date('d-m-Y'); ?>">
                                            <input type="text" name="total_seluruh"
                                                value="Rp. {{ number_format($item->harga) }}" id="total_seluruh"
                                                class="warna-text text-bold text-gs" name="total_seluruh" readonly
                                                style="color: #FFA825; padding: 1px; background-color: #ffffff; font-weight: bold;" />
                                        </div>
                                    </div>
                                </div>
                                @if (Cookie::has('agent'))
                                    <input hidden type="number" name="agent_id" value="{{ Cookie::get('agent_id') }}">
                                @endif
                                <button class="btn btn-block btn-join-now mt-4 py-2" type="submit">
                                    <i class="fas fa-cart-arrow-down"></i> Beli Sekarang
                                </button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <script src="{{ asset('assets/js/accounting.js') }}"></script>
    <script>
        function sum() {
            var txtFirstNumberValue = document.getElementById('text1').value;
            var txtSecondNumberValue = document.getElementById('text2').value;
            var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('total_seluruh').value = 'Rp. ' +
                    (accounting.formatNumber(result, "0", "."));
            }
        }

    </script>
    <script>
        $('.btn-number').click(function(e) {
            e.preventDefault();

            fieldName = $(this).attr('data-field');
            type = $(this).attr('data-type');
            var input = $("input[name='" + fieldName + "']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if (type == 'minus') {

                    if (currentVal > input.attr('min')) {
                        input.val(currentVal - 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('min')) {
                        $(this).attr('disabled', true);
                    }

                } else if (type == 'plus') {

                    if (currentVal < input.attr('max')) {
                        input.val(currentVal + 1).change();
                    }
                    if (parseInt(input.val()) == input.attr('max')) {
                        $(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        $('.input-number').focusin(function() {
            $(this).data('oldValue', $(this).val());
        });
        $('.input-number').change(function() {

            minValue = parseInt($(this).attr('min'));
            maxValue = parseInt($(this).attr('max'));
            valueCurrent = parseInt($(this).val());

            name = $(this).attr('name');
            if (valueCurrent >= minValue) {
                $(".btn-number[data-type='minus'][data-field='" + name + "']")
                    .removeAttr('disabled')
            } else {
                alert('Sorry, the minimum value was reached');
                $(this).val($(this).data('oldValue'));
            }
            if (valueCurrent <= maxValue) {
                $(".btn-number[data-type='plus'][data-field='" + name + "']")
                    .removeAttr('disabled')
            } else {
                alert('Sorry, the maximum value was reached');
                $(this).val($(this).data('oldValue'));
            }


        });
        $(".input-number").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e
                    .keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    </script>
@endsection

@push('prepend-style')
    <link rel="stylesheet" href="{{ url('frontend/libraries/xzoom/xzoom.css') }}" />
@endpush

@push('addon-script')
    <script src="{{ url('frontend/libraries/xzoom/xzoom.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.xzoom, .xzoom-gallery').xzoom({
                zoomWidth: 500,
                title: false,
                tint: '#333',
                Xoffset: 15
            });
        });

    </script>
@endpush
