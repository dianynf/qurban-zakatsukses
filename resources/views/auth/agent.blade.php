@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="card o-hidden border-0 shadow-lg my-5 col-lg-11 mx-auto">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4"><b>Isi Lengkap Data Diri Anda</b></h1>
                            </div>
                            <form method="POST" action="{{ route('register.prosess') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="REFERENSI ZAKAT SUKSES" readonly>
                                </div>
                                <div class="form-group" id="only-number">
                                    <input id="username" type="text"
                                        class="form-control @error('username') is-invalid @enderror" name="username"
                                        value="{{ old('username') }}" required autocomplete="username"
                                        placeholder="Nama Lengkap">
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="nik" type="text" class="form-control @error('nik') is-invalid @enderror"
                                        name="nik" value="{{ old('nik') }}" required autocomplete="nik" autofocus
                                        placeholder="NIK Sesuai KTP">
                                    @error('nik')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email"
                                        placeholder="E-Mail Address">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <p> Mohon isi email dengan benar, untuk mengaktivasi keagenan perlu verifikasi yang akan
                                        dikirimkan melalui email tersebut</p>
                                </div>
                                <div class="form-group" id="only-number">
                                    <input type="number"
                                        class="form-control input-number @error('nohp') is-invalid @enderror" required
                                        maxlength="13" id="nohp" name="nohp" placeholder="Nomor Telepon"
                                        value="{{ old('nohp') }}">
                                    @error('nohp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="show_hide_password">
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            required autocomplete="new-password" placeholder="Password">
                                        {{-- <div class="input-group-append">
                                            <div class="btn btn-default viewpass" data-target="#password">
                                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>
                                        </div> --}}
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group" id="show_hide_passwordd">
                                        <input id="password-confirm" type="password" class="form-control"
                                            name="password_confirmation" required autocomplete="new-password"
                                            placeholder="Konfirmasi Password">
                                        {{-- <div class="input-group-append">
                                            <div class="btn btn-default viewpass" data-target="#password">
                                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control @error('nik') is-invalid @enderror" id="alamat"
                                        name="alamat" placeholder="Masukkan Alamat" value="{{ old('alamat') }}">
                                </div>
                                {{-- <div class="form-group">
                                    <select name="provinsi" id="provinsi" class="form-control" required>
                                        <option>Pilih Provinsi</option>

                                    </select>

                                </div>
                                <div class="form-group">
                                    <select name="kabupaten" id="kabupaten" class="form-control">
                                        <option>Pilih Kabupaten</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <select name="kecamatan" id="kecamatan" class="form-control">
                                        <option>Pilih Kecamatan</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <select name="desa" id="desa" class="form-control">
                                        <option>Pilih Kelurahan</option>
                                    </select>

                                </div> --}}
                                {{-- <div class="form-group">
                                    <input type="text" class="form-control @error('kodepos') is-invalid @enderror"
                                        maxlength="7" id="kodepos" name="kodepos" placeholder="Kode Pos"
                                        value="{{ old('kodepos') }}">
                                </div> --}}
                                <br>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="form-check checkbox-warning-filled">
                                                <input type="checkbox" class="form-check-input filled-in btn-checbox"
                                                    id="orangeExample" checked>
                                                <label class="form-check-label sm-register mt-2" for="orangeExample">Saya
                                                    Menyetujui
                                                    {{-- <a class="link-lupp collapsed" style="text-decoration:none;"
                                                        data-toggle="modal" href="#exampleModal"><b>Syarat dan
                                                            Ketentuan</b></a> --}}
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5" style="text-align: right;">
                                            @if (Route::has('password.request'))
                                                <a class="small link-lupp" style="text-decoration:none;"
                                                    href="{{ route('password.request') }}">
                                                    {{ __('Lupa Password?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn button-login btn-block">
                                    Daftar
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header modal-heder">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Kebijakan privasi yang dimaksud <strong>Global Qurban</strong> (<strong>GQ</strong>) sebagai bagian
                        dari ACT adalah acuan yang mengatur dan melindungi penggunaan data dan informasi penting para
                        Pengguna globalqurban.com, Data dan informasi yang telah dikumpulkan pada saat mendaftar, mengakses,
                        dan menggunakan layanan di globalqurban.com seperti alamat, nomor kontak, alamat e-mail, foto,
                        gambar, dan lain-lain.
                        <br><br>
                        <strong>Kebijakan-kebijakan tersebut di antaranya:</strong>
                        <ol class="list-ketentuan">
                            <li><strong>GQ</strong> tunduk terhadap kebijakan perlindungan data pribadi Pengguna sebagaimana
                                yang diatur dalam Peraturan Menteri Komunikasi dan Informatika Nomor 20 Tahun 2016 Tentang
                                Perlindungan Data Pribadi Dalam Sistem Elektronik yang mengatur dan melindungi penggunaan
                                data dan informasi penting para Pengguna.</li>
                            <li><strong>GQ</strong> melindungi segala informasi yang diberikan Pengguna pada saat
                                pendaftaran, mengakses, dan menggunakan seluruh layanan globalqurban.com.</li>
                            <li><strong>GQ</strong> berhak menggunakan data dan informasi para Pengguna demi meningkatkan
                                mutu dan pelayanan di donation.act.id.</li>
                            <li><strong>GQ</strong> tidak bertanggung jawab atas pertukaran data yang dilakukan sendiri di
                                antara Pengguna</li>
                            <li><strong>GQ</strong> hanya dapat memberitahukan data dan informasi yang dimiliki oleh para
                                Pengguna bila diwajibkan dan/atau diminta oleh institusi yang berwenang berdasarkan
                                ketentuan hukum yang berlaku, perintah resmi dari Pengadilan, dan/atau perintah resmi dari
                                instansi atau aparat yang bersangkutan.</li>
                            <li><strong>GQ</strong> tunduk terhadap UU Hak Cipta Nomor 28 tahun 2014 dan seluruh konten di
                                dalamnya dilindungi oleh UU di atas.</li>
                            <li><strong>GQ</strong> tunduk terhadap UU Nomor 11 tahun 2008 tentang Informasi dan Transaksi
                                Elektronik (ITE).</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
