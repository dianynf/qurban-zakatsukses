<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cinta Qurban - Menjangkau Pelosok Negeri Dunia</title>

    @stack('prepend-style')
    @include('includes.auth.style')
    @stack('addon-style')

</head>

<body
    class="page-template-default page page-id-502 wp-custom-logo fullwidthcontained mhdrleft abv-none fullwidth no-home elementor-default elementor-kit-325 elementor-page elementor-page-502">
    @include('includes.auth.navbar')
    <div id="app">
        <main class="py-4">
            {{-- @include('partials.alerts') --}}
            @yield('content')
        </main>
    </div>
    @include('includes.footer')

    @stack('prepend-script')
    @include('includes.script')
    @stack('addon-script')

    {{-- @include('includes.auth.script') --}}
</body>

</html>
