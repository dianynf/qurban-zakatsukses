<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>@yield('title')</title>

    @stack('prepend-style')
    @include('includes.auth.style')
    @stack('addon-style')

</head>

<body class="page-template-default page page-id-502 wp-custom-logo fullwidthcontained mhdrleft abv-none fullwidth no-home elementor-default elementor-kit-325 elementor-page elementor-page-502">
    @include('includes.auth.navbar')
    @yield('content')
    @include('includes.footer')

    @stack('prepend-script')
    @include('includes.script')
    @stack('addon-script')
</body>
</body>

</html>
