<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $userRole = Role::where('name', 'user')->first();
        $agentRole = Role::where('name', 'agent')->first();

        $admin = User::create([
            'name' => 'Admin',
            'email' => 'dianbugas@gmail.com',
            'username' => 'admin utama',
            'password' => Hash::make('password')
        ]);

        $user = User::create([
            'name' => 'User',
            'email' => 'wajokromawi@gmail.com',
            'username' => 'user',
            'password' => Hash::make('password')
        ]);

        $agent = User::create([
            'name' => 'Agent',
            'email' => 'dianbugasbugas@gmail.com',
            'username' => 'dian',
            'password' => Hash::make('password')
        ]);

        $admin->roles()->attach($adminRole);
        $user->roles()->attach($userRole);
        $agent->roles()->attach($agentRole);
    }
}
