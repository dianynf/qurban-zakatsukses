-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 16 Jul 2021 pada 16.59
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qurbanzs`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `banks`
--

CREATE TABLE `banks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rek` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `banks`
--

INSERT INTO `banks` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`, `rek`, `img`) VALUES
(1, 'Bank Muamalat', NULL, '2021-02-08 23:38:00', '2021-02-08 23:38:00', '3390111111', 'muamalat.png'),
(2, 'Bank BJB', NULL, '2021-02-09 00:52:31', '2021-02-09 00:54:33', '0095367011100', 'bjb.png'),
(3, 'Bank DKI', NULL, '2021-02-09 00:52:36', '2021-02-09 00:52:36', '70509111111', 'dki.png'),
(4, 'Bank Mega Syariah', NULL, '2021-02-09 00:53:40', '2021-02-09 00:54:46', '1000136124', 'mega_syariah.png'),
(5, 'BNI Syariah', NULL, '2021-02-09 00:54:07', '2021-02-09 00:54:07', '8281111111', 'bni_syariah.png'),
(6, 'BRI Syariah', NULL, '2021-02-09 00:55:10', '2021-02-09 00:55:17', '1005856333', 'bri_syariah.png'),
(7, 'Mandiri Syariah', NULL, '2021-02-09 00:55:35', '2021-02-09 00:55:35', '7039797498', 'mandiri_syariah.png'),
(8, 'BCA Syariah', NULL, '2021-02-09 00:56:18', '2021-02-09 00:56:18', '0101004000', 'bca.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `galleries`
--

INSERT INTO `galleries` (`id`, `product_id`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'kambing.png', NULL, '2021-02-08 23:40:59', '2021-02-08 23:40:59'),
(2, 2, 'sapi.png', NULL, '2021-02-09 20:34:50', '2021-02-09 20:34:50'),
(3, 3, 'sapi.png', NULL, '2021-02-09 20:35:24', '2021-02-09 20:35:24'),
(4, 4, 'kambing.png', NULL, '2021-02-09 20:35:37', '2021-02-09 20:35:37'),
(5, 5, 'sapi.png', NULL, '2021-02-09 20:35:48', '2021-02-09 20:35:48'),
(6, 5, 'sapi.png', '2021-02-09 20:36:26', '2021-02-09 20:36:01', '2021-02-09 20:36:26'),
(7, 7, 'domba.png', NULL, '2021-02-09 20:36:20', '2021-02-09 20:36:20'),
(8, 6, 'sapi.png', NULL, '2021-02-09 20:36:34', '2021-02-09 20:36:34'),
(9, 8, 'istimewa.png', NULL, '2021-02-09 20:36:46', '2021-02-09 20:36:46'),
(10, 9, 'istimewa.png', NULL, '2021-07-15 00:32:16', '2021-07-15 00:32:16'),
(11, 10, 'istimewa.png', NULL, '2021-07-15 00:32:32', '2021-07-15 00:32:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_27_150610_create_banks_table', 1),
(5, '2019_10_27_171216_add_roles_field_to_users_table', 1),
(6, '2019_10_27_182002_add_username_field_to_users_table', 1),
(7, '2020_08_19_065409_create_roles_table', 1),
(8, '2020_08_19_065603_create_role_user_table', 1),
(9, '2020_10_26_125416_create_products_table', 1),
(10, '2020_10_26_125510_create_galleries_table', 1),
(11, '2020_10_27_040756_create_transactions_table', 1),
(12, '2020_10_27_040823_create_transaction_details_table', 1),
(13, '2021_02_09_062516_create_agents_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dianbugas@gmail.com', '$2y$10$VXGML9DzgZMmXJm7SGpR7es6Q/RY/yEEANDImIokPwcEqeTSnWrBe', '2021-02-18 08:01:15'),
('dianynf20@gmail.com', '$2y$10$2E0aancBYtK2iHR8M9Jlkexa3lYyH4L5Q10yD2vpuWmOR.TkUW.zW', '2021-02-18 18:02:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `berat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `about`, `berat`, `harga`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Kambing QURBAN PELOSOK NUSANTARA', 'kambing-qpn', 'sapi', '> 27 – 30 kg', 2150000, NULL, '2021-02-08 23:39:41', '2021-06-08 03:24:20'),
(2, 'SAPI QURBAN PELOSOK NUSANTARA', 'sapi-qpn', 'kambing adsa', '> 225 kg', 15000000, NULL, '2021-02-09 20:24:16', '2021-06-08 03:18:49'),
(3, 'Sapi Patungan QURBAN PELOSOK NUSANTARA', 'patungan-sapi-qpn', 'Sapi berkualitas terbaik dengan bobot 225 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia sapi lebih dari dua tahun). Sapi regular tipe B akan disalurkan untuk penerima manfaat di salah satu wilayah yang berada di 34 provinsi Indonesia', '> 225 kg', 2150000, NULL, '2021-02-09 20:25:28', '2021-02-09 20:25:28'),
(4, 'Kambing QURBAN ISTIMEWA', 'kambing-qi', 'Kambing berkualitas terbaik dengan bobot 27-30 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia kambing lebih dari satu tahun). Kambing regular tipe A akan disalurkan untuk penerima manfaat di salah satu wilayah yang berada di 34 provinsi Indonesia', '> 30 kg', 3500000, NULL, '2021-02-09 20:26:25', '2021-02-09 20:27:36'),
(5, 'Sapi  QURBAN ISTIMEWA', 'sapi-qi', 'Sapi berkualitas terbaik dengan bobot 260 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia sapi lebih dari dua tahun). Sapi regular tipe A akan disalurkan untuk penerima manfaat di salah satu wilayah yang berada di 34 provinsi Indonesia', '> 260 kg', 20000000, NULL, '2021-02-09 20:27:29', '2021-02-09 20:27:29'),
(6, 'Sapi Patungan QURBAN ISTIMEWA', 'patungan-sapi-qi', 'Sapi berkualitas terbaik dengan bobot 260 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia sapi lebih dari dua tahun). Sapi regular tipe A akan disalurkan untuk penerima manfaat di salah satu wilayah yang berada di 34 provinsi Indonesia', '> 260 kg', 3000000, NULL, '2021-02-09 20:29:18', '2021-02-09 20:29:18'),
(7, 'Domba QURBAN DUNIA ISLAM', 'domba-qdi', 'Domba berkualitas terbaik dengan bobot 40 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia kambing lebih dari satu tahun). Domba akan disalurkan untuk penerima manfaat di salah satu wilayah yang berada di 34 provinsi Indonesia', '> 40 kg', 5000000, NULL, '2021-02-09 20:31:40', '2021-02-09 20:31:40'),
(8, 'SAPI QURBAN DUNIA ISLAM', 'sapi_qdi', 'Sapi berkualitas terbaik dengan bobot 350 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia sapi lebih dari dua tahun). Sapi regular tipe A akan disalurkan untuk penerima manfaat di salah satu wilayah yang berada di 34 provinsi Indonesia', '> 350 kg', 30000000, NULL, '2021-02-09 20:33:19', '2021-07-14 09:41:08'),
(9, 'SAPI UTUH SOMALIA QURBAN DUNIA ISLAM', 'sapi-utuh-somalia-qurban-dunia-islam', 'Sapi berkualitas terbaik dengan bobot 120 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia sapi lebih dari dua tahun). Sapi regular tipe A akan disalurkan untuk penerima manfaat di Somalia.', '> 120 kg', 11000000, NULL, '2021-07-14 21:47:53', '2021-07-14 21:47:53'),
(10, 'SAPI PATUNGAN SOMALIA QURBAN DUNIA ISLAM', 'patungan-sapi-somalia-qurban-dunia-islam', 'Sapi berkualitas terbaik dengan bobot 120 kilogram yang dipastikan memenuhi seluruh kriteria hewan qurban yang layak diqurbankan dan telah cukup usianya (usia sapi lebih dari dua tahun). Sapi regular tipe A akan disalurkan untuk penerima manfaat di Somalia.', '> 120 kg', 1900000, NULL, '2021-07-14 21:49:11', '2021-07-14 21:49:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2021-02-08 23:33:54', '2021-02-08 23:33:54'),
(2, 'User', '2021-02-08 23:33:54', '2021-02-08 23:33:54'),
(3, 'Agent', '2021-02-08 23:33:54', '2021-02-08 23:33:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_user`
--

CREATE TABLE `role_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 2, 2, NULL, NULL),
(3, 3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `transaction_total` int(11) NOT NULL,
  `transaction_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quant` int(11) NOT NULL,
  `total_seluruh` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `date` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `invoice` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transactions`
--

INSERT INTO `transactions` (`id`, `product_id`, `users_id`, `transaction_total`, `transaction_status`, `deleted_at`, `created_at`, `updated_at`, `email`, `nohp`, `name`, `quant`, `total_seluruh`, `agent_id`, `date`, `bank_id`, `invoice`) VALUES
(206, 2, 3, 15000000, 'PENDING', '2021-06-05 00:55:34', '2021-03-27 23:25:12', '2021-06-05 00:55:34', 'agent@gmail.com', '0895702211234', 'Agent Satu', 1, '15000000', NULL, '28-03-2021', 7, '202161656689554'),
(210, 1, 29, 2150000, 'INCART', '2021-06-05 00:55:35', '2021-04-04 08:01:25', '2021-06-05 00:55:35', 'muhammadardiyansyah889@gmail.comccc', '0895702213410', 'Muhammad Ardiansyah', 1, '2150000', NULL, '04-04-2021', 7, '202160772781493'),
(211, 2, 1, 15000000, 'INCART', '2021-06-05 00:55:36', '2021-06-02 19:13:13', '2021-06-05 00:55:36', 'dianbugas@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 1, '15000000', NULL, '03-06-2021', 2, '202153266631417'),
(212, 1, 1, 2150000, 'INCART', '2021-06-05 00:55:38', '2021-06-02 19:24:15', '2021-06-05 00:55:38', 'ardian200616@gmail.com', '0895702213410', 'Admin Utamas', 1, '2150000', NULL, '03-06-2021', 2, '202181425880191'),
(213, 7, 1, 5000000, 'INCART', '2021-06-05 00:55:38', '2021-06-02 21:06:32', '2021-06-05 00:55:38', 'muhammadardiyansyah889@gmail.com', '0895702213410', 'Admin Utama', 1, '5000000', NULL, '03-06-2021', 4, '202185809639386'),
(214, 1, 2, 2150000, 'INCART', '2021-06-05 00:55:40', '2021-06-02 22:47:49', '2021-06-05 00:55:40', 'muhammadardiyansyah889@gmail.com', '111111111111111', 'Admin Utama', 1, '2150000', NULL, '03-06-2021', 4, '202173935226278'),
(215, 8, 2, 35000000, 'INCART', '2021-06-05 00:55:41', '2021-06-02 23:33:29', '2021-06-05 00:55:41', 'dianbugas@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 2, '7000000034', NULL, '03-06-2021', 5, '202191697440860'),
(216, 2, NULL, 15000000, 'INCART', '2021-06-05 00:55:42', '2021-06-02 23:39:35', '2021-06-05 00:55:42', '', '', '', 1, '15000000', NULL, '03-06-2021', 7, '202139951673656'),
(217, 5, NULL, 20000000, 'INCART', '2021-06-05 00:55:50', '2021-06-02 23:44:12', '2021-06-05 00:55:50', '', '', '', 2, '40000000', NULL, '03-06-2021', 7, '202149328708231'),
(218, 8, 2, 35000000, 'INCART', '2021-06-05 00:55:53', '2021-06-02 23:46:28', '2021-06-05 00:55:53', 'muhammadardiyansyah889@gmail.com', '0895702213410', 'Admin Utama', 3, '105000016', NULL, '03-06-2021', 2, '202199142628474'),
(219, 4, 2, 3500000, 'INCART', '2021-06-05 23:29:17', '2021-06-02 23:48:11', '2021-06-05 23:29:17', 'dianbugas@gmail.com', '0895702213412', 'Muhammad Ardiansyah', 1, '3500077', NULL, '03-06-2021', 2, '202141612573280'),
(220, 1, 2, 2150000, 'INCART', '2021-06-05 23:29:16', '2021-06-04 22:33:33', '2021-06-05 23:29:16', 'muhammadardiyansyah889@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 1, '2150029', NULL, '05-06-2021', 5, '202149563264646'),
(221, 1, 2, 2150000, 'INCART', '2021-06-05 23:29:15', '2021-06-05 00:49:24', '2021-06-05 23:29:15', 'muhammadardiyansyah889@gmail.com', '0895702213410', 'Admin Utama', 1, '2150069', NULL, '05-06-2021', 7, '202149919564875'),
(222, 4, 55, 3500000, 'INCART', '2021-06-05 23:29:13', '2021-06-05 23:25:22', '2021-06-05 23:29:13', 'muhammadardiyansyah889@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 1, '3500093', NULL, '06-06-2021', 7, '202161556751845'),
(223, 1, 2, 2150000, 'INCART', NULL, '2021-06-05 23:51:10', '2021-06-05 23:51:25', 'dianbugas@gmail.com', '0895702213410', 'Admin Utama', 1, '2150061', NULL, '06-06-2021', 7, '202156357293493'),
(224, 1, 1, 2150000, 'INCART', NULL, '2021-06-05 23:56:02', '2021-06-05 23:56:02', 'admin@gmail.com', '22222222222212', 'Admin Utama', 1, '2150000', NULL, '06-06-2021', 7, '202171381348385'),
(225, 7, 1, 5000000, 'INCART', NULL, '2021-06-05 23:58:28', '2021-06-05 23:58:28', 'admin@gmail.com', '22222222222212', 'Admin Utama', 1, '5000000', NULL, '06-06-2021', 7, '202159842920850'),
(226, 1, 2, 2150000, 'INCART', NULL, '2021-06-19 06:31:28', '2021-06-19 06:32:06', 'ardian200616@gmail.com', '0895702213410', 'asdasdas', 1, '2150085', NULL, '19-06-2021', 7, '202126192894282'),
(227, 1, 2, 2150000, 'INCART', NULL, '2021-07-06 07:20:33', '2021-07-06 07:20:46', 'dianbugas@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 1, '2150000', NULL, '06-07-2021', 6, '202167033577032'),
(228, 1, 1, 2150000, 'PENDING', NULL, '2021-07-06 07:21:45', '2021-07-06 07:21:48', 'admin@gmail.com', '22222222222212', 'Admin Utama', 1, '2150000', NULL, '06-07-2021', NULL, NULL),
(229, 1, 3, 2150000, 'PENDING', NULL, '2021-07-07 19:45:18', '2021-07-07 19:45:24', 'agent@gmail.com', '0895702211234', 'Agent Satu ok', 1, '2150000', NULL, '08-07-2021', NULL, NULL),
(230, 1, NULL, 2150000, 'INCART', NULL, '2021-07-09 15:05:46', '2021-07-09 15:05:46', '', '', '', 1, '2150000', NULL, '09-07-2021', 7, '202127558190483'),
(231, 1, NULL, 2150000, 'INCART', NULL, '2021-07-11 20:29:02', '2021-07-11 20:29:02', '', '', '', 1, '2150000', NULL, '12-07-2021', 7, '202190520534049'),
(232, 5, NULL, 20000000, 'INCART', NULL, '2021-07-11 20:32:01', '2021-07-11 20:32:01', '', '', '', 1, '20000000', NULL, '12-07-2021', 7, '202124716124505'),
(233, 1, NULL, 2150000, 'INCART', NULL, '2021-07-14 19:21:12', '2021-07-14 19:21:12', '', '', '', 1, '2150000', NULL, '15-07-2021', 7, '20211054340826'),
(234, 10, 1, 1900000, 'INCART', NULL, '2021-07-15 00:33:42', '2021-07-15 00:33:42', 'admin@gmail.com', '22222222222212', 'Admin Utama', 1, '1900000', NULL, '15-07-2021', 7, '202115865040872'),
(235, 9, 1, 11000000, 'INCART', NULL, '2021-07-15 00:39:24', '2021-07-15 00:46:21', 'dianbugas@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 1, '11000061', NULL, '15-07-2021', 2, '202130405757323'),
(236, 10, 1, 1900000, 'INCART', NULL, '2021-07-15 00:47:07', '2021-07-15 00:47:07', 'admin@gmail.com', '22222222222212', 'Admin Utama', 1, '1900000', NULL, '15-07-2021', 7, '202193110549046'),
(237, 2, 1, 15000000, 'INCART', NULL, '2021-07-15 00:58:35', '2021-07-15 00:58:35', 'admin@gmail.com', '22222222222212', 'Admin Utama', 1, '15000000', NULL, '15-07-2021', 7, '202145440877339'),
(238, 10, 1, 1900000, 'INCART', NULL, '2021-07-15 01:01:53', '2021-07-15 01:02:30', 'muhammadardiyansyah889@gmail.com', '0895702213410', 'Muhammad Ardiansyah', 2, '3800014', NULL, '15-07-2021', 8, '202180322895846'),
(239, 9, 1, 11000000, 'INCART', NULL, '2021-07-15 01:02:01', '2021-07-15 01:02:01', 'admin@gmail.com', '22222222222212', 'Admin Utama', 2, '22000000', NULL, '15-07-2021', 7, '20212421231924'),
(240, 1, NULL, 2150000, 'INCART', NULL, '2021-07-16 00:35:14', '2021-07-16 00:35:14', '', '', '', 2, '4300000', NULL, '16-07-2021', 7, '202110910969316');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaction_details`
--

CREATE TABLE `transaction_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transactions_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaction_details`
--

INSERT INTO `transaction_details` (`id`, `transactions_id`, `username`, `deleted_at`, `created_at`, `updated_at`) VALUES
(317, 155, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(318, 155, NULL, NULL, NULL, NULL),
(319, 155, NULL, NULL, NULL, NULL),
(320, 155, NULL, NULL, NULL, NULL),
(321, 155, NULL, NULL, NULL, NULL),
(322, 155, NULL, NULL, NULL, NULL),
(323, 155, NULL, NULL, NULL, NULL),
(324, 156, 'asdas', NULL, NULL, NULL),
(325, 170, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(326, 170, 'asd', NULL, NULL, NULL),
(327, 170, 'asd', NULL, NULL, NULL),
(328, 170, 'dasdas', NULL, NULL, NULL),
(329, 171, 'asd', NULL, NULL, NULL),
(330, 171, NULL, NULL, NULL, NULL),
(331, 171, NULL, NULL, NULL, NULL),
(332, 171, NULL, NULL, NULL, NULL),
(333, 171, NULL, NULL, NULL, NULL),
(334, 171, NULL, NULL, NULL, NULL),
(335, 171, NULL, NULL, NULL, NULL),
(336, 171, NULL, NULL, NULL, NULL),
(337, 171, NULL, NULL, NULL, NULL),
(338, 171, NULL, NULL, NULL, NULL),
(339, 171, NULL, NULL, NULL, NULL),
(340, 171, NULL, NULL, NULL, NULL),
(341, 171, NULL, NULL, NULL, NULL),
(342, 171, NULL, NULL, NULL, NULL),
(343, 171, 'dasd', NULL, NULL, NULL),
(344, 171, NULL, NULL, NULL, NULL),
(345, 171, NULL, NULL, NULL, NULL),
(346, 171, NULL, NULL, NULL, NULL),
(347, 171, NULL, NULL, NULL, NULL),
(348, 171, NULL, NULL, NULL, NULL),
(349, 171, NULL, NULL, NULL, NULL),
(350, 171, NULL, NULL, NULL, NULL),
(351, 171, NULL, NULL, NULL, NULL),
(352, 171, NULL, NULL, NULL, NULL),
(353, 171, NULL, NULL, NULL, NULL),
(354, 171, NULL, NULL, NULL, NULL),
(355, 171, NULL, NULL, NULL, NULL),
(356, 171, NULL, NULL, NULL, NULL),
(357, 171, 'dasd', NULL, NULL, NULL),
(358, 171, NULL, NULL, NULL, NULL),
(359, 171, NULL, NULL, NULL, NULL),
(360, 171, NULL, NULL, NULL, NULL),
(361, 171, NULL, NULL, NULL, NULL),
(362, 171, NULL, NULL, NULL, NULL),
(363, 171, NULL, NULL, NULL, NULL),
(364, 171, NULL, NULL, NULL, NULL),
(365, 171, NULL, NULL, NULL, NULL),
(366, 171, NULL, NULL, NULL, NULL),
(367, 171, NULL, NULL, NULL, NULL),
(368, 171, NULL, NULL, NULL, NULL),
(369, 171, NULL, NULL, NULL, NULL),
(370, 171, NULL, NULL, NULL, NULL),
(371, 171, 'asdas', NULL, NULL, NULL),
(372, 171, NULL, NULL, NULL, NULL),
(373, 171, NULL, NULL, NULL, NULL),
(374, 171, NULL, NULL, NULL, NULL),
(375, 171, NULL, NULL, NULL, NULL),
(376, 171, NULL, NULL, NULL, NULL),
(377, 171, NULL, NULL, NULL, NULL),
(378, 171, NULL, NULL, NULL, NULL),
(379, 171, NULL, NULL, NULL, NULL),
(380, 171, NULL, NULL, NULL, NULL),
(381, 171, NULL, NULL, NULL, NULL),
(382, 171, NULL, NULL, NULL, NULL),
(383, 171, NULL, NULL, NULL, NULL),
(384, 171, NULL, NULL, NULL, NULL),
(385, 174, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(386, 175, 'dian bugas', NULL, NULL, NULL),
(387, 180, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(388, 180, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(389, 180, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(390, 180, 'bbbbbbbbbb', NULL, NULL, NULL),
(391, 180, 'dasdas', NULL, NULL, NULL),
(392, 180, NULL, NULL, NULL, NULL),
(393, 180, NULL, NULL, NULL, NULL),
(394, 181, 'asd', NULL, NULL, NULL),
(395, 182, 'asdas', NULL, NULL, NULL),
(396, 182, 'asdas', NULL, NULL, NULL),
(397, 185, 'asdas', NULL, NULL, NULL),
(398, 186, 'asdas', NULL, NULL, NULL),
(399, 186, 'asdas', NULL, NULL, NULL),
(400, 186, 'eewwwssss', NULL, NULL, NULL),
(401, 187, 'dasd', NULL, NULL, NULL),
(402, 187, 'dasd', NULL, NULL, NULL),
(403, 187, 'asdasd', NULL, NULL, NULL),
(404, 188, 'asdas', NULL, NULL, NULL),
(405, 189, 'asdasdas', NULL, NULL, NULL),
(406, 189, 'bbbbbbbbbb', NULL, NULL, NULL),
(407, 188, 'asdas', NULL, NULL, NULL),
(408, 190, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(409, 194, 'adsadas', NULL, NULL, NULL),
(410, 195, 'asdasd', NULL, NULL, NULL),
(411, 196, 'bbbbbbbbbb', NULL, NULL, NULL),
(412, 202, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(413, 202, NULL, NULL, NULL, NULL),
(414, 202, NULL, NULL, NULL, NULL),
(415, 202, NULL, NULL, NULL, NULL),
(416, 202, NULL, NULL, NULL, NULL),
(417, 202, NULL, NULL, NULL, NULL),
(418, 202, NULL, NULL, NULL, NULL),
(419, 202, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(420, 202, NULL, NULL, NULL, NULL),
(421, 202, NULL, NULL, NULL, NULL),
(422, 202, NULL, NULL, NULL, NULL),
(423, 202, NULL, NULL, NULL, NULL),
(424, 202, NULL, NULL, NULL, NULL),
(425, 202, NULL, NULL, NULL, NULL),
(426, 202, 'dasd', NULL, NULL, NULL),
(427, 202, 'asdas', NULL, NULL, NULL),
(428, 202, NULL, NULL, NULL, NULL),
(429, 202, NULL, NULL, NULL, NULL),
(430, 202, NULL, NULL, NULL, NULL),
(431, 202, NULL, NULL, NULL, NULL),
(432, 202, NULL, NULL, NULL, NULL),
(433, 203, 'asda', NULL, NULL, NULL),
(434, 203, 'asd', NULL, NULL, NULL),
(435, 203, 'asd', NULL, NULL, NULL),
(436, 203, 'asd', NULL, NULL, NULL),
(437, 203, 'asdasd', NULL, NULL, NULL),
(438, 203, 'das', NULL, NULL, NULL),
(439, 203, 'dasd', NULL, NULL, NULL),
(440, 204, 'eewwwssss', NULL, NULL, NULL),
(441, 204, NULL, NULL, NULL, NULL),
(442, 204, NULL, NULL, NULL, NULL),
(443, 204, NULL, NULL, NULL, NULL),
(444, 204, NULL, NULL, NULL, NULL),
(445, 204, NULL, NULL, NULL, NULL),
(446, 204, NULL, NULL, NULL, NULL),
(447, 204, 'eewwwssss', NULL, NULL, NULL),
(448, 204, NULL, NULL, NULL, NULL),
(449, 204, NULL, NULL, NULL, NULL),
(450, 204, NULL, NULL, NULL, NULL),
(451, 204, NULL, NULL, NULL, NULL),
(452, 204, NULL, NULL, NULL, NULL),
(453, 204, NULL, NULL, NULL, NULL),
(454, 204, 'asdas', NULL, NULL, NULL),
(455, 204, NULL, NULL, NULL, NULL),
(456, 204, 'asd', NULL, NULL, NULL),
(457, 204, NULL, NULL, NULL, NULL),
(458, 204, NULL, NULL, NULL, NULL),
(459, 204, NULL, NULL, NULL, NULL),
(460, 204, NULL, NULL, NULL, NULL),
(461, 208, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(462, 210, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(463, 211, 'Muhammad', NULL, NULL, NULL),
(464, 211, 'dian', NULL, NULL, NULL),
(465, 211, NULL, NULL, NULL, NULL),
(466, 211, NULL, NULL, NULL, NULL),
(467, 211, NULL, NULL, NULL, NULL),
(468, 211, NULL, NULL, NULL, NULL),
(469, 211, NULL, NULL, NULL, NULL),
(470, 212, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(471, 212, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(472, 212, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(473, 213, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(474, 214, 'asdas', NULL, NULL, NULL),
(475, 214, 'asdas', NULL, NULL, NULL),
(476, 214, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(477, 214, 'dasdas', NULL, NULL, NULL),
(478, 215, 'bbbbbbbbbb', NULL, NULL, NULL),
(479, 215, NULL, NULL, NULL, NULL),
(480, 215, NULL, NULL, NULL, NULL),
(481, 215, NULL, NULL, NULL, NULL),
(482, 215, NULL, NULL, NULL, NULL),
(483, 215, NULL, NULL, NULL, NULL),
(484, 215, NULL, NULL, NULL, NULL),
(485, 215, NULL, NULL, NULL, NULL),
(486, 215, NULL, NULL, NULL, NULL),
(487, 215, NULL, NULL, NULL, NULL),
(488, 215, NULL, NULL, NULL, NULL),
(489, 215, NULL, NULL, NULL, NULL),
(490, 215, NULL, NULL, NULL, NULL),
(491, 215, NULL, NULL, NULL, NULL),
(492, 215, 'bbbbbbbbbb', NULL, NULL, NULL),
(493, 215, NULL, NULL, NULL, NULL),
(494, 215, NULL, NULL, NULL, NULL),
(495, 215, NULL, NULL, NULL, NULL),
(496, 215, NULL, NULL, NULL, NULL),
(497, 215, NULL, NULL, NULL, NULL),
(498, 215, NULL, NULL, NULL, NULL),
(499, 215, NULL, NULL, NULL, NULL),
(500, 215, NULL, NULL, NULL, NULL),
(501, 215, NULL, NULL, NULL, NULL),
(502, 215, NULL, NULL, NULL, NULL),
(503, 215, NULL, NULL, NULL, NULL),
(504, 215, NULL, NULL, NULL, NULL),
(505, 215, NULL, NULL, NULL, NULL),
(506, 215, 'bbbbbbbbbb', NULL, NULL, NULL),
(507, 215, NULL, NULL, NULL, NULL),
(508, 215, NULL, NULL, NULL, NULL),
(509, 215, NULL, NULL, NULL, NULL),
(510, 215, NULL, NULL, NULL, NULL),
(511, 215, NULL, NULL, NULL, NULL),
(512, 215, NULL, NULL, NULL, NULL),
(513, 215, NULL, NULL, NULL, NULL),
(514, 215, NULL, NULL, NULL, NULL),
(515, 215, NULL, NULL, NULL, NULL),
(516, 215, NULL, NULL, NULL, NULL),
(517, 215, NULL, NULL, NULL, NULL),
(518, 215, NULL, NULL, NULL, NULL),
(519, 215, NULL, NULL, NULL, NULL),
(520, 216, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(521, 216, NULL, NULL, NULL, NULL),
(522, 216, NULL, NULL, NULL, NULL),
(523, 216, NULL, NULL, NULL, NULL),
(524, 216, NULL, NULL, NULL, NULL),
(525, 216, NULL, NULL, NULL, NULL),
(526, 216, NULL, NULL, NULL, NULL),
(527, 216, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(528, 216, NULL, NULL, NULL, NULL),
(529, 216, NULL, NULL, NULL, NULL),
(530, 216, NULL, NULL, NULL, NULL),
(531, 216, NULL, NULL, NULL, NULL),
(532, 216, NULL, NULL, NULL, NULL),
(533, 216, NULL, NULL, NULL, NULL),
(534, 217, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(535, 217, NULL, NULL, NULL, NULL),
(536, 217, NULL, NULL, NULL, NULL),
(537, 217, NULL, NULL, NULL, NULL),
(538, 217, NULL, NULL, NULL, NULL),
(539, 217, NULL, NULL, NULL, NULL),
(540, 217, NULL, NULL, NULL, NULL),
(541, 217, NULL, NULL, NULL, NULL),
(542, 217, NULL, NULL, NULL, NULL),
(543, 217, NULL, NULL, NULL, NULL),
(544, 217, NULL, NULL, NULL, NULL),
(545, 217, NULL, NULL, NULL, NULL),
(546, 217, NULL, NULL, NULL, NULL),
(547, 217, NULL, NULL, NULL, NULL),
(548, 217, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(549, 217, NULL, NULL, NULL, NULL),
(550, 217, NULL, NULL, NULL, NULL),
(551, 217, NULL, NULL, NULL, NULL),
(552, 217, NULL, NULL, NULL, NULL),
(553, 217, NULL, NULL, NULL, NULL),
(554, 217, NULL, NULL, NULL, NULL),
(555, 217, NULL, NULL, NULL, NULL),
(556, 217, NULL, NULL, NULL, NULL),
(557, 217, NULL, NULL, NULL, NULL),
(558, 217, NULL, NULL, NULL, NULL),
(559, 217, NULL, NULL, NULL, NULL),
(560, 217, NULL, NULL, NULL, NULL),
(561, 217, NULL, NULL, NULL, NULL),
(562, 218, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(563, 218, NULL, NULL, NULL, NULL),
(564, 218, NULL, NULL, NULL, NULL),
(565, 218, NULL, NULL, NULL, NULL),
(566, 218, NULL, NULL, NULL, NULL),
(567, 218, NULL, NULL, NULL, NULL),
(568, 218, NULL, NULL, NULL, NULL),
(569, 218, NULL, NULL, NULL, NULL),
(570, 218, NULL, NULL, NULL, NULL),
(571, 218, NULL, NULL, NULL, NULL),
(572, 218, NULL, NULL, NULL, NULL),
(573, 218, NULL, NULL, NULL, NULL),
(574, 218, NULL, NULL, NULL, NULL),
(575, 218, NULL, NULL, NULL, NULL),
(576, 218, NULL, NULL, NULL, NULL),
(577, 218, NULL, NULL, NULL, NULL),
(578, 218, NULL, NULL, NULL, NULL),
(579, 218, NULL, NULL, NULL, NULL),
(580, 218, NULL, NULL, NULL, NULL),
(581, 218, NULL, NULL, NULL, NULL),
(582, 218, NULL, NULL, NULL, NULL),
(583, 219, 'asdas', NULL, NULL, NULL),
(584, 219, 'asdas', NULL, NULL, NULL),
(585, 219, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(586, 219, 'asdas', NULL, NULL, NULL),
(587, 219, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(588, 220, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(589, 221, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(590, 222, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(591, 223, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(592, 226, 'asdasd', NULL, NULL, NULL),
(593, 227, 'asdas', NULL, NULL, NULL),
(594, 229, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(595, 229, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(596, 235, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(597, 235, 'asdas', NULL, NULL, NULL),
(598, 235, 'asdas', NULL, NULL, NULL),
(599, 235, NULL, NULL, NULL, NULL),
(600, 235, NULL, NULL, NULL, NULL),
(601, 235, NULL, NULL, NULL, NULL),
(602, 235, NULL, NULL, NULL, NULL),
(603, 238, 'aaaaaaaaaaaa', NULL, NULL, NULL),
(604, 238, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodepos` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rekening` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `atasnama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cabang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fotobuku` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fotonik` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fotoselfi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kabupaten` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `image`, `nohp`, `alamat`, `kodepos`, `rekening`, `atasnama`, `cabang`, `fotobuku`, `nik`, `fotonik`, `fotoselfi`, `bank_id`, `provinsi`, `kabupaten`, `kecamatan`, `desa`, `created_at`, `updated_at`, `roles`, `username`, `role_id`) VALUES
(1, 'admin3282078', 'admin@gmail.com', '2021-02-02 03:17:23', '$2y$10$VZlUDe1SKmPyQ0C7c7h50OCotG./GRZUJ8a5ec8vOLM6GWYOrAsoi', NULL, 'Screenshot from 2021-03-30 22-31-31.png', '22222222222212', 'Depok', NULL, '21232131231', 'Zakat Sukses', 'BRI Syariah', 'Screenshot from 2021-02-19 08-21-50.png', '92384234323', 'vector-tugu-34.jpg', 'defaultfikzakatsuksesok.jpg', 6, NULL, NULL, NULL, NULL, '2021-02-09 20:17:12', '2021-06-08 18:22:44', 'ADMIN', 'Admin Utama', 1),
(3, 'agent5817812', 'agent@gmail.com', '2021-02-01 03:01:33', '$2y$10$n4O/fV6ODVvQZdcC23qO2ONpXTuim2K5isw6fvIz2X3hZZcZZKmBi', NULL, NULL, '0895702211234', 'Depok', NULL, '21232131231', 'Admin', 'BRI Syariah', 'defaultfkuzakatsuksesok.png', '9238423432', 'defaultnikzakatsuksesok.jpg', 'defaultfikzakatsuksesok.jpg', 5, NULL, NULL, NULL, NULL, '2021-02-09 19:58:45', '2021-03-29 16:15:11', 'AGENT', 'Agent Satu ok', 3),
(13, 'user9364506', 'usersatu@gmail.coms', '2021-04-01 15:00:28', '$2y$10$onVAwd.rADThr69cOfEVyemCBPgcFfJMo3AULweHizkh42ULRHdC2', 'dHOb9IJGDIIVlfg70AhpyMjwqQkDxN1b3JX7jTuKcwVdRfFbkUPKotDx7zVB', NULL, '0895702213410', NULL, NULL, 'asda', NULL, NULL, 'defaultfkuzakatsuksesok.png', NULL, 'defaultnikzakatsuksesok.jpg', 'defaultfikzakatsuksesok.jpg', NULL, NULL, NULL, NULL, NULL, '2021-02-18 03:35:51', '2021-02-23 21:35:41', 'USER', 'User Satu', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `banks`
--
ALTER TABLE `banks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT untuk tabel `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=605;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
