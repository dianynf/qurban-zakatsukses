<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id');
            $table->bigInteger('users_id')->nullable();
            $table->bigInteger('transaction_total');
            $table->string('transaction_status');
            $table->string('email')->nullable();
            $table->string('nohp')->nullable();
            $table->string('name')->nullable();
            $table->bigInteger('quant');
            $table->string('total_seluruh');
            $table->bigInteger('agent_id')->nullable();
            $table->string('date')->nullable();
            $table->bigInteger('bank_id')->nullable();
            $table->string('invoice')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('post_code')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
