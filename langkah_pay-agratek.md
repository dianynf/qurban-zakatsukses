Untuk dapat integrasi dengan agratek:

.env ditambahkan

AGRATEK_CLIENT_ID="devel"
AGRATEK_CLIENT_KEY="mtrn30E2JxLjWFpTkQXbiLNNDrYujhHVwu4RzXWKOAZiCBvo1dZwUffCEKsymIcx"
AGRATEK_CLIENT_DOM="https://devel.agratek.id/api/merchant"
AGRATEK_IS_SANDBOX=false

Aplikasi:
Tambahkan alias pada config\app.php Karena ada bentrok nama class
'AgratekTransaction' => Agratek\Payment\Transaction::class,

File:
config\agratek.php

<?php

return [
    'clientId' => env('AGRATEK_CLIENT_ID', null),
    'clientKey' => env('AGRATEK_CLIENT_KEY', null),
    'clientDom' => env('AGRATEK_CLIENT_DOM', null),
    'isSandbox' => env('AGRATEK_IS_SANDBOX', true),
];
?>

Tambahkan daftar bank pada tabel Banks:
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(11,'Bank Mandiri','VA­MANDIRI','mandiri.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(12,'Bank Negara Indonesia 46','VA­BNI','bni.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(13,'Bank Rakyat Indonesia','VA­BRI','bri.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(14,'Bank Danamon Indonesia','VA­DANAMON','danamon.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(15,'Bank International Indonesia Maybank','VA­MAYBANK','maybank.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(16,'Bank Permata','VA-PERMATA','permata.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(17,'Bank KEB Hana Indonesia','VA­HANABANK','hana.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(18,'Bank CIMB Niaga','VA-CIMB','cimb.png',STR_TO_DATE('2021-07-22','%Y-%m-%d'));
INSERT INTO banks (id,name,rek,img,created_at)
VALUES(19,'Pay With Flip','PAY-WITH-FLIP','flip.png',STR_TO_DATE('2021-07-22','%Y-%m-%d')); 

Includekan package Agratek

Checkout Controller
    public function process(Request $request, $id)
    {
        ...
        ...
        ...
        $data['address'] = $request['address']; //kebutuhan data tambahan
        $data['city'] = $request['city']; //kebutuhan data tambahan
        $data['state'] = $request['state']; //kebutuhan data tambahan
        $data['post_code'] = $request['post_code']; //kebutuhan data tambahan
        
        $item = Transaction::findOrFail($id);
        $item->update($data);
        
        $bank = Banks::findOrFail($data["bank_id"]);
        if (substr($bank->rek, 0, 2)=='VA' || $bank->rek=='PAY-WITH-FLIP')
        {
            $this->success($request, $id);
        }
        else{
            return redirect()->route('invoice', $id);
        }
    }

   public function success(Request $request, $id)
    {
        $transaction = Transaction::with(['details', 'product.galleries', 'user'])
            ->findOrFail($id);
        $transaction->transaction_status = 'PENDING';
        $transaction->save();
        //Start Here 
        Config::$clientKey = config('agratek.clientKey');
        Config::$clientId = config('agratek.clientId');
        Config::$clientDom = config('agratek.clientDom');
        Config::$isProduction = config('agratek.isProduction');
        Config::$isSanitized = config('agratek.isSanitized');
        Config::$is3ds = config('agratek.is3ds');
        $params = array(
            "customer"=>array(
                "name"=> $transaction->name,
                "phone"=> $transaction->nohp,
                "email"=> $transaction->email,
                "address"=> $request['address'],
                "city"=> $request['city'],
                "state"=> $request['state'],
                "post_code"=> $request['post_code'],
                "country"=> "Indonesia",
                "session_id"=> session_id(),
                "ip"=> $_SERVER['REMOTE_ADDR']
            ),
            'cart' => array(
                "count"=> "1",
                "item"=> array(
                    array(
                        "img_url"=> "",
                        "goods_name"=> $transaction->product->title." ".$transaction->product->berat,
                        "goods_detail"=> $transaction->product->about,
                        "goods_amt"=> $transaction->product->harga
                    )
                )
            ),
            'account'=>array(
                "denom"=>$transaction->bank->rek, 
                "amount"=>$transaction->total_seluruh,
                "description"=>$transaction->product->title,
                "invoice_no"=>strval($transaction->id),
            )
        );
        $params["deliver_to"] = $params["customer"];
        $result = AgratekTransaction::register($params);
        try {
            if (array_key_exists("va",$result)){
                $va=$result["va"];
                $data=array(
                    "va_number"=>$va["vacct_no"],
                    "valid_date"=>$va["valid_date"], 
                    "valid_time"=>$va["valid_time"],
                );
                $transaction->update($data);
                return redirect()->route('invoiceva', $id);  //redirect ke tampilan Invoice VA
                
            }elseif(array_key_exists("pwf",$result)){
                $pwf=$result["pwf"];
                header('Location: ' . $pwf["pwf_url"]); //di redirect ke url flip
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
