<?php

return [
    'clientId' => env('AGRATEK_CLIENT_ID', null),
    'clientKey' => env('AGRATEK_CLIENT_KEY', null),
    'clientDom' => env('AGRATEK_CLIENT_DOM', null),
    'isSandbox' => env('AGRATEK_IS_SANDBOX', true),
    'isSanitized' => env('AGRATEK_IS_SANITIZED', true),
    'is3ds' => env('AGRATEK_IS_3DS', true),
];
