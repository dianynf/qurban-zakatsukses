<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('/program', 'MenuController@program')
    ->name('program');
Route::get('/faq', 'MenuController@faq')
    ->name('faq');
Route::get('/galeri', 'MenuController@galeri')
    ->name('galeri');
Route::get('/download', 'MenuController@download')
    ->name('download');

Route::get('/agent', 'HomeController@agent');

Route::get('/register/agen', 'Auth\RegisterController@showAgenRegisterForm')
    ->name('register.agen');

Route::post('/register/prosess', 'Auth\RegisterController@prosess')
    ->name('register.prosess');

Route::get('/detail/{slug}', 'DetailController@index')
    ->name('detail');

// user login
Route::post('/checkout/{id}', 'CheckoutController@process')
    ->name('checkout_process');
// ->middleware(['auth', 'verified']);

Route::get('/checkout/{id}', 'CheckoutController@index')
    ->name('checkout');
// ->middleware(['auth', 'verified']);

Route::post('/checkout/create/{detail_id}', 'CheckoutController@create')
    ->name('checkout-create');
// ->middleware(['auth', 'verified']);

Route::get('/checkout/remove/{detail_id}', 'CheckoutController@remove')
    ->name('checkout-remove');
// ->middleware(['auth', 'verified']);

Route::post('/send-wa', 'CheckoutController@sendWhatsapp');

Route::get('/checkout/confirm/{id}', 'CheckoutController@success')
    ->name('checkout-success');
// ->middleware(['auth', 'verified']);

Route::group(['middleware' => 'auth', 'verified'], function () {

    Route::get('/profile', 'ProfileController@edit')
        ->name('profile.edit')
        ->middleware(['auth', 'verified']);

    Route::patch('/profile', 'ProfileController@update')
        ->name('profile.update')->middleware(['auth', 'verified']);

    Route::get('/pengaturan-bank', 'PengaturanBankController@edit')
        ->name('pengaturan-bank.edit')->middleware(['auth', 'verified']);

    Route::patch('/pengaturan-bank', 'PengaturanBankController@update')
        ->name('pengaturan-bank.update')->middleware(['auth', 'verified']);

    Route::get('/document', 'DocumentController@edit')
        ->name('document.edit')->middleware(['auth', 'verified']);

    Route::patch('/document', 'DocumentController@update')
        ->name('document.update')->middleware(['auth', 'verified']);

    Route::get('password', 'PasswordController@edit')
        ->name('user.password.edit')->middleware(['auth', 'verified']);

    Route::patch('password', 'PasswordController@update')
        ->name('user.password.update')->middleware(['auth', 'verified']);

    Route::get('dashboard', 'DashboardController@index')
        ->name('dashboard.index')->middleware(['auth', 'verified']);
    Route::resource('transaction', 'TransactionController')->middleware(['auth', 'verified']);
    // Route::get('transaction', 'TransactionController@index')
    //     ->name('transaction.index')->middleware(['auth', 'verified']);
    Route::get('dompet', 'DompetController@index')
        ->name('dompet.index')->middleware(['auth', 'verified']);
});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function () {
    Route::resource('/users', 'UsersController', ['except' => ['show', 'create', 'store']]);
});

//agratek
Route::post('/agratek/callback', 'InvoiceController@agratekCallback');

// admin
Route::prefix('admin')
    ->namespace('Admin')
    ->middleware(['auth', 'admin'])
    ->group(function () {
        Route::resource('users', 'UsersController');
        Route::resource('banks', 'BanksController');
        Route::resource('role', 'RoleController');
        Route::resource('product', 'ProductController');
        Route::resource('gallery', 'GalleryController');

        Route::resource(
            'addtransaksi',
            'AddtransaksiController',
            ['except' => [
                'show', 'update', 'index', 'destroy ',
                'edit'
            ]]
        );
    });

Auth::routes(['verify' => true]);

// midtrans
// Route::post('/midtrans/callback', 'MidtransController@notificationHandler');
Route::get('/invoice/{id}', 'InvoiceController@finishRedirect')
    ->name('invoice');
// Route::get('/midtrans/unfinish', 'MidtransController@unfinishRedirect');
// Route::get('/midtrans/error', 'MidtransController@errorRedirect');
